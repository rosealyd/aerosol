#!/usr/bin/env python
import pickle
import json
import numpy as np
from operator import itemgetter
import sys
from scipy.stats import linregress
import copy
import time


#ROOT_DIR = "/home/adouglas2/Summer/aerosol/js_files/"
BINNED_VARIABLES = ["geoidx","aodidx","AMSR-e_LWP"]
NUMBER_BINS = 20
ALLOWED_STD_DEV = 2.0


#Global variables because I'm being lazy
#For AI limits
ai_bin_dict = ""

#For lwp limits
lwp_bin_dict = ""

#Geo limits
geo_bin_dict = ""

#RRidx limits
rr_bin_dict = ""

#AI Dump bin 
aerosol_index_dict = ""
#######################
	
def begin_sort(directions):
	#Flagged if there needs to be a linear regression calculated
	flag = True
	directions = load_dictionaries(directions)
	for lat_lon_key, lat_lon in directions.iteritems():
		for aerosol_key, aerosol in lat_lon.iteritems():
			if directions[lat_lon_key][aerosol_key]["barplot"]:
				flag = False
				#If plotting a bar plot
				if directions[lat_lon_key][aerosol_key]["separate"]:
					directions[lat_lon_key][aerosol_key] = separating_barplot(directions[lat_lon_key][aerosol_key])
				else:
					if directions[lat_lon_key][aerosol_key]["percent_net"]:
						non_separating_percent_net_barplot(directions[lat_lon_key][aerosol_key])
					else:
						directions[lat_lon_key][aerosol_key] = non_separating_barplot(directions[lat_lon_key][aerosol_key])
			#IF plotting a scatter plot
			else:
				if directions[lat_lon_key][aerosol_key]["separate"]:
					directions[lat_lon_key][aerosol_key] = separating(directions[lat_lon_key][aerosol_key])
				else:
					directions[lat_lon_key][aerosol_key] = non_separating(directions[lat_lon_key][aerosol_key])
					
	if flag:
		linear_regress(directions)
	return directions
def load_dictionaries(directions):
	global ai_bin_dict, lwp_bin_dict, geo_bin_dict, rr_bin_dict, aerosol_index_dict
	ROOT_DIR = directions['ROOT_DIR']
	#For AI limits
	dumped_file = open(ROOT_DIR + "ai_bin_dict.p", "r")
	ai_bin_dict = pickle.load(dumped_file)
	
	#For lwp limits
	dumped_file = open(ROOT_DIR + "lwp_bin_dict.js","r")
	lwp_bin_dict = json.load(dumped_file)


	#Geo limits
	dumped_file = open(ROOT_DIR + "geo_bin_dict.js","r")
	geo_bin_dict = json.load(dumped_file)

	#RRidx limits
	dumped_file = open(ROOT_DIR + "rridx_bin_dict.js","r")
	rr_bin_dict = json.load(dumped_file)

	#AI Dump bin 
	dumped_file = open(ROOT_DIR + "AI_comparison.js","r")
	aerosol_index_dict = json.load(dumped_file)
	directions.pop('ROOT_DIR')
	print "Loaded all dictionaries"
	return directions

#For the special case of making a net percent barplot
def non_separating_percent_net_barplot(region):
	copy_xs = copy.deepcopy(region["x"])
	copy_cs = copy.deepcopy(region["c"])
	nets = region["12_net"]
	sum_net = sum(nets)
	
	points_total = len(nets)
	region["y"] = region["12_CLWFTOA"]
	region = non_separating_barplot(region, sum_net, points_total)
	region["up_barplot_list"] = copy.deepcopy(region["barplot_list"])
	region["barplot_list"] = []
	#region["y"] = percent_SW
	region["y"] = region["12_CSWFTOA"]
	region["c"] = copy_cs
	region["x"] = copy_xs
	region = non_separating_barplot(region, sum_net, points_total)
	
	temp = copy.deepcopy(region["barplot_list"])
	down_list = []
	for x,y,w in temp:
		down_list.append((x,-y,w))
	
	region["down_barplot_list"] = down_list
	

	return region
	
	
def non_separating_barplot(region, all_bin_sum = 0, points_total = 0):
	#This does two things. It creates the dict ["barplot_points"] and also gets us the dict ["percentiles"]
	region = non_separating(region)
	#Finding the percentage contribution of each bin
	number_points = 0
	ys = []
	for bin in region["barplot_points"]:
		number_points += len(bin)
		for x,y,c in bin:
			if not region["percent_net"]:
				all_bin_sum += y

	for i,bin in enumerate(region["barplot_points"]):
		bin_sum = 0
		number_in_bin = len(bin)
		for x,y,c in bin:
			bin_sum += y
		

		weight = (.1*number_points) / (number_in_bin)
		ys.append(abs(((bin_sum / all_bin_sum) * weight)*100.0))
		
	
	width = [1 for p in ys]
	xs = [np.sum(width[:i]) for i in range(len(width)+1)]
	
	
	region["barplot_list"] = [(x,y,w) for x,y,w in zip(xs,ys,width)]
	
	return region

def separating_barplot(region):
	region["barplot_list"] = []
	all_bin_sum = 0
	number_points = 0
	points = separating(region)
	ys = [ [ [] for j in range(len(points[i])) ] for i in range(len(points))]
	xs = [ [ [] for j in range(len(points[i])) ] for i in range(len(points))]
	width =  [ [ [] for j in range(len(points[i])) ] for i in range(len(points))]
	region["percentiles"] = list(np.linspace(10,100,len(points[0])))
	
	for i in range(len(points)):
		for j in range(len(points[i])):
			for x,y,c in points[i][j]:
				ys[i][j].append(y)
				all_bin_sum += y
				number_points += 1
			
			width[i] = [.25 for k in range(len(points[i]))]

	for i in range(len(ys)):
		for j in range(len(ys[i])):
			xs[i][j] = (j + (.25 * i))
			print xs[i][j]
			number_in_bin = len(ys[i][j])
			bin_sum = sum(ys[i][j])
			if number_in_bin > 0:
				if region["separate_variable"] == "amsrrridx" or region["separate_variable"] == "CloudSat_rain":
					weight = ((.1/2)*number_points)/ (number_in_bin)
					#print weight
				else:
					weight = ((.1/3)*number_points)/ (number_in_bin)
				ys[i][j] = ((bin_sum/all_bin_sum) * weight) *100
			else:
				ys[i][j] = 0
				print "144", j
	#To double check any weights being used and know all percentiles add to 100
	check_sum = 0
	for xbin, ybin, wbin in zip(xs, ys, width):
		temp = []
		for x,y,w in zip(xbin, ybin, wbin):
			temp.append((x,y,w))
			check_sum += y
		region["barplot_list"].append(temp)
	#print check_sum
	return region

def non_separating(region):
	global ALLOWED_STD_DEV
	points = []
	#Put x,y,c,l into a tuple list to keep all values collected together, together
	for x,y,c in zip(region["x"],region["y"],region["c"]):
		point = (x,y,c)
		points.append(point)
	if region["xvariable"] in BINNED_VARIABLES:
		lowest_index = min(region["x"])
		print lowest_index
		highest_index = max(region["x"])
		print highest_index
		num_indices = highest_index - lowest_index + 1
		print num_indices
		percentiles = list(np.linspace(lowest_index+1, highest_index+1, num_indices))
		percentiles = [tile * 10 for tile in percentiles]
		temp, points, temp2 = find_bin_return_points(points, region, "xvariable")
		region["barplot_points"] = copy.deepcopy(points)
	elif region["xvariable"] == "CloudSat_rain":
		percentiles, points = sort_points(points, region, bin_limits =  [-1,.75,1.25,1.9,2.1,3.1,4.1,5.1,6.1,7.1], NUMBER_BINS = 11)
	else:
		percentiles, points = sort_points(points, region)
		region["barplot_points"] = copy.deepcopy(points)
		
	standard_deviation = []

	for i in range(len(points)):
		if len(points[i]) != 0:
			std_dev = np.std([t[1] for t in points[i]])
			mean_y = np.mean([t[1] for t in points[i]])
			
			upper_bound = mean_y + (std_dev * ALLOWED_STD_DEV)
			lower_bound = mean_y - (std_dev * ALLOWED_STD_DEV)
		
			within_bounds = [t for t in points[i] if t[1] >= lower_bound and t[1] <= upper_bound]
			std_dev = np.std([t[1] for t in within_bounds])
			x_avg = np.mean([t[0] for t in within_bounds])
			y_avg = np.mean([t[1] for t in within_bounds])
			c_avg = np.mean([t[2] for t in within_bounds])
		
			points[i] = (x_avg, y_avg, c_avg)
			standard_deviation.append(std_dev)
			points[i] = (x_avg, y_avg, c_avg)

	#Remove all empty lists
	percentiles = [percentiles[i] for i,p in enumerate(points) if p!=[]]
	points = [p for p in points if p!=[]]
	print "ys", [p[1] for p in points]
	
	
	region["plotting_list"] = points
	
	region["standard_deviation"] = standard_deviation
	region["percentiles"] = percentiles
	return region

#Replace binned variables with the midpoint of their bin limits, Binned variable expected to be the first tuple		
def replace_xs_with_numbers(ps, bin_dict, reg):
	ps = sorted(ps, key= lambda p: p[0])
	new_ps = []
	
	if reg["separate"]:
		sorted_points = [[[] for p in range(len(bin_dict))] for i in range(3)]
		for i, p in enumerate(ps):
			xp = p[0]
			if reg["include_end_ais"]:
				xp = bin_dict[int(xp)]
				new_ps.append((xp,p[1],p[2],p[3]))
			else:
				if xp != 9 and xp != 0:
					xp = bin_dict[int(xp)]
					new_ps.append((xp,p[1],p[2],p[3]))
				
			
		return bin_dict, new_ps, sorted_points
	else:
		number_none = 0
		
		sorted_points = [[] for p in range(len(bin_dict))]
		for x,y,c in ps:
			if x is not None and y is not None and c is not None:
				if reg["include_end_ais"]:
					sorted_points[int(x)].append((bin_dict[int(x)],y,c))
				else:
					if reg['xvariable'] == 'aodidx':
						
						if x!= 0 and x!= 9 and x!= 8:
							if x == 0:
								print 'here'
							sorted_points[int(x)].append((bin_dict[int(x)],y,c))
					if reg['xvariable'] == 'AMSR-e_LWP':
						if x < 13:
							sorted_points[int(x)].append((bin_dict[int(x)],y,c))
			else:
				number_none += 1
		print "number none:", number_none
		return None, sorted_points, None

		
#Returns the bin limits or a list of sorted points depending on the boolean of region["separate"]
def sort_points(all_points, reg, bin_limits = None, NUMBER_BINS = 20):
	
	all_points = sorted(all_points, key=lambda p:p[0])
	bin_percentiles = list(np.linspace(0,100,NUMBER_BINS))
	print bin_limits
	temp_xs = [t[0] for t in all_points]
	if bin_limits is None:
		bin_limits = list(np.percentile(temp_xs, bin_percentiles, interpolation='midpoint'))
	
	if reg["separate"]:
		return bin_limits
	sorted_points = [[] for b in range(NUMBER_BINS)]
	
	number_none = 0
	bin_limits[-1] +=1
	for x,y,c in all_points:
		if x is not None:
			for i,(low_bin_limit, high_bin_limit) in enumerate(zip(bin_limits[:-1],bin_limits[1:])):
				print low_bin_limit, high_bin_limit, x
				time.sleep(3)
				if x>= low_bin_limit and x < high_bin_limit:
					if y is not None and c is not None:
						sorted_points[i].append((x,y,c))
					else:
						number_none += 1
	sorted_points = [ points for points in sorted_points if points != []]
	sys.exit()
	
	return bin_percentiles,sorted_points
	
	
#Takes the first tuple of points and assigns it a value based on the bin dict assigned to the variable
def find_bin_return_points(need_points, region, variable):
	global lwp_bin_dict
	global ai_bin_dict
	print ai_bin_dict.keys()
	print ai_bin_dict['0-0'].keys()
	global geo_bin_dict
	'''if "CloudSat_rain" in region[variable]:
		print "here at cloudsat_rain"
		temp_dict = [((a+b)/2) for a,b in rr_bin_dict[region["layer"]][region["lat_lon"]]]
		temp_dict.insert(0,0.0)
		temp_dict.pop()
		temp_dict.append(5.0)
		corrected_points = []
		if region["separate"]:
			for p, y, c,l in need_points:
				if p not in [1.0,2.0] and p != 0.0:
					p = p-2.0
					corrected_points.append((p,y,c,l))
				if p == 0.0:
					corrected_points.append((p,y,c,l))
		else:
			for p, y, c in need_points:
				if p not in [1.0,2.0] and p != 0.0:
					p = p-2.0
					corrected_points.append((p,y,c))
				if p == 0.0:
					corrected_points.append((p,y,c))
		right_bin_limits, right_points, sorted_points_array = replace_xs_with_numbers(corrected_points, temp_dict, region)'''
			
	if "aodidx" in region[variable]:
		print region['aerosol']
		print region['lat_lon']
		temp_dict = ai_bin_dict[region["lat_lon"]][region["aerosol"]]
		print temp_dict
		temp_dict = [((a+b)/2) for a,b in zip(temp_dict[:-1], temp_dict[1:])]
		print temp_dict
		right_bin_limits, right_points,sorted_points_array = replace_xs_with_numbers(need_points,temp_dict,region)
		if region["separate"]:
			right_bin_limits.insert(0,0.0)
	if region[variable] == "Geophysical_parameter":
		temp_dict =  [((a+b)/2) for a,b in geo_bin_dict[region["layer"]][region["lat_lon"]]]
		right_bin_limits, right_points, sorted_points_array = replace_xs_with_numbers(need_points,temp_dict,region)
	if region[variable] == "AMSR-e_LWP":
		layer = str(region["layer"])
		lat_lon = str(region["lat_lon"])
		temp_dict = [((a+b)/2) for a,b in lwp_bin_dict[region["layer"]][region["lat_lon"]]]
		#temp_dict =[((a+b)/2) for a,b in lwp_bin_dict[layer][lat_lon]]
		right_bin_limits, right_points, sorted_points_array = replace_xs_with_numbers(need_points,temp_dict,region)
	if region[variable] == 'CloudSat_rain':
		rain_rates = [0.0, 0.5, 1.0, 1.75, 2.0, 3.0, 3.75, 4.0, 5.0, 6.0, 7]
		right_bin_limits, right_points, sorted_points_array = replace_xs_with_numbers(need_points, rain_rates, region)
		
	return right_bin_limits, right_points, sorted_points_array
	
#For when separating region in scatter plot by a low,medium,high variable
def separating(region):
	global ALLOWED_STD_DEV
	points = []
	#Put x,y,c,l into a tuple list to keep all values collected together, together
	for x,y,c,ls in zip(region["x"],region["y"],region["c"],region["separating_list"]):
		point = (x,y,c,ls)
		points.append(point)
	
	bin_limits = []
	#If the x variable is a binned variable, replace xs with midway point of bin limits and have bin limits be the predetermined bin limits 
	if region["xvariable"] in BINNED_VARIABLES:
		bin_limits, points, sorted_points = find_bin_return_points(points, region, "xvariable")
		
		
	else:

		sorted_points = [[[] for p in range(NUMBER_BINS)] for i in range(3)]
		bin_limits = sort_points(points, region)

	#If the sorting variable is also a binned variable, replace with the midway point of the bin limits
	if region["separate_variable"] in BINNED_VARIABLES:
		
		l_first_points = [(l,x,y,c) for (x,y,c,l) in points]
		temp, l_replaced_points, temp2 = find_bin_return_points(l_first_points, region, "separate_variable")
		points = []
		points = [(x,y,c,l) for (l,x,y,c) in l_replaced_points]
	
	#Replace with midpoint value of bin if color variable is binned
	if region["color_variable"] in BINNED_VARIABLES:
		c_first_points = [(c,x,y,l) for (x,y,c,l) in points]
		temp, c_replaced_points, temp2 = find_bin_return_points(c_first_points, region, "color_variable")
		points = []
		points = [(x,y,c,l) for (c,x,y,l) in c_replaced_points]
	
	#Get the sorting variable l from points to create limits of percentiles
	#If rain index, use different limits than percentiles	
	sorting_points = [t[3] for t in points]
	if region["separate_variable"] == "amsrrridx" or region["separate_variable"] == "CloudSat_rain":
		sorting_limits = [-1,.24,10]
	else:
		sorting_limits = list(np.percentile(sorting_points, [0,33,66,100], interpolation = 'lower'))
	
	#If need to double check the bounds of the x or l variable
	range_xs_test = []
	range_ls_test = []
	number_none = 0
	print len(points)
	for i,(x,y,c,l) in enumerate(points):
		#Adding to test only
		if l not in range_ls_test:
			range_ls_test.append(l)
		if x not in range_xs_test:
			range_xs_test.append(x)
		#Sorting of variables begins
		#If X is None or l is None that means the variable wasn't retrieved properly from satellite/model and cannot add measurement to the plot
		if x is not None and l is not None:
			for j,(low_separate_limit, high_separate_limit) in enumerate(zip(sorting_limits[:-1],sorting_limits[1:])):
				for k,(low_bin_limit, high_bin_limit) in enumerate(zip(bin_limits[:-1],bin_limits[1:])):
					if l >= low_separate_limit and l < high_separate_limit:			
						if x > low_bin_limit and x <= high_bin_limit:
							try:
								if y is not None and c is not None:
									sorted_points[j][k].append((x,y,c))
								else:
									number_none += 1
							except IndexError:
								print "Error in separating region, when indexing to sorted_points"
								print i, j, k
								print x,y,c,l
								print low_separate_limit, high_separate_limit
								print low_bin_limit, high_bin_limit
								print len(sorted_points), len(sorted_points[-1]), len(sorted_points[-1][-1])
								sys.exit()
		
	print number_none
	#If last region
	if region["separate_variable"] == "amsrrridx" or region["separate_variable"] == "CloudSat_rain":
		print sorted_points.pop()
	
	if region["barplot"]:
		return sorted_points

	standard_deviation = []
	
	for i in range(len(sorted_points)):
		bin_std = []
		for j in range(len(sorted_points[i])):
			std = np.std([t[1] for t in sorted_points[i][j]])
			temp_mean = np.mean([t[1] for t in sorted_points[i][j]])
			upper_bound = temp_mean + (std*ALLOWED_STD_DEV)
			lower_bound = temp_mean - (std*ALLOWED_STD_DEV)
			
			within_bounds = [t for t in sorted_points[i][j] if t[1] >= lower_bound and t[1] <= upper_bound]
			#print "within bounds", within_bounds
			std = np.std([t[1] for t in within_bounds])
			number_points = len(within_bounds)
			x_avg = np.mean([t[0] for t in within_bounds])
			y_avg = np.mean([t[1] for t in within_bounds])
			c_avg = np.mean([t[2] for t in within_bounds])
			if not np.isnan(x_avg):
				sorted_points[i][j] = (x_avg, y_avg, c_avg)
			else:
				sorted_points[i][j] = (0,0,0)
			if not np.isnan(std):
				bin_std.append(std)

		standard_deviation.append(bin_std)

	#Remove all empty lists
	for i in range(len(sorted_points)):
		sorted_points[i] = [p for p in sorted_points[i] if p!=(0,0,0)]

	region["standard_deviation"] = standard_deviation
	
	region["plotting_list"] = sorted_points

	return region

#If separating into bins, pass a bin to do calculations on, otherwise the bin is the plotting list of region
def calc_regression(region, bin = None):
	if not region["separate"]:
		bin = region["plotting_list"]
		
	xs = [b[0] for b in bin]
	ys = [b[1] for b in bin]
	regress = linregress(xs,ys)
	#linregress returns [0] slope, [1] intercept, [2] r value
	line = [ (x,x*regress[0] + regress[1]) for x in xs]
	r2 = round(regress[2]**2,2)
	slope = round(regress[0],2)
	if region["separate"]:
		region["linear_regression"].append(line)
		region["r_squared"].append(r2)
		region["slope"].append(regress[0])
		region["error"].append(regress[-1])
	else:
		region["linear_regression"] = line
		region["r_squared"] = r2
		region["slope"] = slope
		region["error"] = regress[-1]
		
	return region
		
def linear_regress(directions):
	
	for lat_lon_key, lat_lon in directions.iteritems():
		for aerosol_key, aerosol in lat_lon.iteritems():
			region = directions[lat_lon_key][aerosol_key]

			if region["separate"]:
				#Create lists to append values for each separating bin
				directions[lat_lon_key][aerosol_key]["linear_regression"] = []
				directions[lat_lon_key][aerosol_key]["r_squared"] = []
				directions[lat_lon_key][aerosol_key]["slope"] = []
				directions[lat_lon_key][aerosol_key]["error"] = []
				for separate_bin in region["plotting_list"]:
					region = calc_regression(region, separate_bin)
					
			else:
				region = calc_regression(region)
	
	return directions
	

	
	
	
	

