#!/usr/bin/env python

import Get_user_input
import Sort_lists
import Plot
import sys

def get_multi_directions(args):
	
	sys.argv = args.split()
	args = True
	directions = []
	directions = Get_user_input.start_directions(args)

	directions = Sort_lists.begin_sort(directions)
	
	#Plot.create_figure(directions)
	
	return directions
