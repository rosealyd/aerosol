#!/usr/bin/env python

import Get_user_input
import Sort_lists
import Plot
import sys

if len(sys.argv) > 1:
	args = True
else:
	args = False
print sys.argv

directions = Get_user_input.start_directions(args)

directions = Sort_lists.begin_sort(directions)

Plot.create_figure(directions)

