#!/usr/bin/env python

import Get_directions
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import time
import matplotlib.colors as clrs
import subprocess
import sys
import copy
from sys import argv
from pylab import rcParams
from scipy.stats import linregress

DUMP_DIR = '/home/adouglas2/Summer/aerosol/Website_figures/10by10/'

aerosol = 'MODISAI'
cases = ['0_0','0_-15','0_-30','-15_0','-15_-15','-15_-30','-30_0','-30_-15','-30_-30']
#cases = ['0_0', '-15_0', '-30_0']
#cases = ['-15_-15','-15_-30']
x_axis = []
y_axis = []
split_var_list = []
mean_var = []
rain = []

compare = False
temp_comp = False

length = False
stand_dev = False
slope = True
ind_var = 'aodidx'

see_xlabels = False
split = False
split_var = 'AMSR-e_LWP'

all_rainstates = True
raining = False
if all_rainstates:
	raining = False
if raining:
	nonraining = False
else:
	nonraining = True
	
width = 0
height = 0


if length:
	if stand_dev:
		print 'Must choose length or stand_dev, not both'
		sys.exit()
	if slope:
		print "Must choose length, sd, or slope, not all three"
		sys.exit()


xvar = 'EIS3'
#yvar = 'AMSR-e_LWP'
yvar = 'RH'

if len(argv) > 1:
	var = argv[1]
	against_var = argv[1]
	#scale = argv[1]
	#var = "{}_CF_CSWFTOA".format(scale)
	#against_var = "{}_CF_CLWFTOA".format(scale)
else:
	var = '24_cloud_albedo'
	against_var = '96_CF_CLWFTOA'

def get_var(needed_var, case = '0_0'):
	global aerosol
	ARG = "Overall.py --lat_lons={} --aerosols {} --xvariable {} --yvariable {} --color_variable EC_LTSS --layer all --15degree".format(case, aerosol, needed_var, needed_var)

	directions = Get_directions.get_multi_directions(ARG, Sort = False)

	temp = directions[case][aerosol]['x']
	
	return temp


def set_midpoint(cmin, cmax, midpoint):
	size = max(abs(midpoint - cmin), abs(midpoint - cmax))
	return midpoint - size, midpoint + size

def set_1_as_midpoint(cmin, cmax):
	print cmin, cmax
	new_cmin = 2 - cmax
	if new_cmin > cmin:
		cmax = 2*cmax
		new_cmin, cmax = set_1_as_midpoint(cmin, cmax)
	if new_cmin < cmin:
		print 'found cmin'
		print new_cmin, cmax
		return new_cmin, cmax

#For each bin/list, finds the sd/length/mean
#Finds the absolute minimum color and maximum color
def final_step(bins):
	global length, stand_dev, slope
	max_c = 0
	min_c = 1000000
	for i, bin in enumerate(bins):
		for j, b in enumerate(bin):
		#If doing by length
			if length:
				bins[i][j] = len(b)
				if len(b) > max_c:
					max_c = len(b)
				if len(b) < min_c:
					min_c = len(b)
			elif slope:
				print 'slope'
				if len(b) > 15:
					
					independent = [v[0] for v in bins[i][j]]
					dependent = [v[1] for v in bins[i][j]]
					#print independent, dependent
					slope = linregress(independent, dependent)[0]
					
					bins[i][j] = float(slope)
					if max_c < float(slope):
						max_c = float(slope)
					if min_c > float(slope):
						min_c = float(slope)
				else:
					bins[i][j] = 0.0
			elif stand_dev:
				if len(b) > 9:
					bins[i][j] = np.std(b)
					if np.std(b) > max_c:
						max_c = np.std(b)
					if np.std(b) < min_c:
						min_c = np.std(b)
				else:
					bins[i][j] = 0.0
			#If doing by mean
			else:
				b = [n for n in b if n != None]
				if len(b) < 10:
					if compare:
						bins[i][j] = 1
					else:
						bins[i][j] = None
				else:
					bins[i][j] = abs(np.mean(b))
					if abs(bins[i][j]) > max_c:
						max_c = abs(np.mean(b))
					if abs(bins[i][j]) < min_c:
						min_c = abs(np.mean(b))
	print min_c, max_c
	print bins

	return min_c, max_c, bins
#Begins the process of first sorting the var into bins based on the xs and ys
#Then does the final step of finding min/max color and taking the mean/length/sd of each bin
def sort_into_bins(bins, x_percentiles, y_percentiles, xs, ys, var):
	global split
	for x,y,m in zip(xs, ys, var):
		for i, (low_x, high_x) in enumerate(zip(x_percentiles[:-1],x_percentiles[1:])):
			for j, (low_y, high_y) in enumerate(zip(y_percentiles[:-1], y_percentiles[1:])):
				if x >= low_x and x <= high_x and y >= low_y and y <= high_y:
					bins[i][j].append(m)
	min_c, max_c, bins = final_step(bins)
	return min_c, max_c, bins
	
####FOR the scatter plot
def find_mean(percentiles, x_axis, y_axis, axes, xlabel, mean = True, show_y_label = True, title = None):
	global var
	y_percentiles = np.percentile(y_axis, percentiles, interpolation = 'midpoint')
	total_length = float(len([x for x in x_axis if x != None]))
	y_percentiles[-1] += 1
	bins = [[] for y in percentiles]
	for x, y in zip(x_axis, y_axis):
		for i, (low_limit, high_limit) in enumerate(zip(y_percentiles[:-1], y_percentiles[1:])):
			if y >= low_limit and y < high_limit:
				bins[i].append(x)
	for j, bin in enumerate(bins):
		if mean:
			bins[j] = np.mean(bin)
		else:
			bins[j] = np.std(bin)
	axes.plot(percentiles, bins)
	axes.set_xlabel(xlabel, fontsize = 10)
	if show_y_label:
		axes.set_ylabel(var)
	if title != None:
		axes.set_title(title, fontsize = 10, loc = 'right')
	return
#####


def make_plot(bins, x_axis, y_axis, percentiles, mean_var, plot_number = 111, title = None, leave = False, overall_min = None, overall_max = None, axes = None, get_color = False): 
	global width, height, stand_dev
	
	x_percentiles = np.percentile(x_axis, percentiles, interpolation = 'midpoint')

	y_percentiles = np.percentile(y_axis, percentiles, interpolation = 'midpoint')

	min_c, max_c, bins = sort_into_bins(bins, x_percentiles, y_percentiles, x_axis, y_axis, mean_var)

	if leave:
		return min_c, max_c
	if overall_min != None and overall_max != None:
		min_c = overall_min
		max_c = overall_max
		
	#Figuring out color layout
	print "color range limits", min_c, max_c
	if compare:
		if min_c < 1:
			cmap = cm.get_cmap('coolwarm')
		else:
			cmap = cm.get_cmap('Reds')
		
		min_c, max_c = set_midpoint(cmin, cmax, 1.)
		norm = clrs.Normalize(vmin = min_c, vmax = max_c)
		color = cm.ScalarMappable(norm = norm, cmap = cmap)
	elif stand_dev:
		cmap = cm.get_cmap('OrRd')
		norm = clrs.Normalize(vmin = min_c, vmax = max_c)
		color = cm.ScalarMappable(norm = norm, cmap = cmap)
	elif slope:
		min_c, max_c = set_midpoint(min_c, max_c, 0)
		cmap = cm.get_cmap('seismic')
		norm = clrs.Normalize(vmin = min_c, vmax = max_c)
		color = cm.ScalarMappable(norm = norm, cmap = cmap)
	else:
		
		cmap = cm.get_cmap('Blues')
		norm = clrs.Normalize(vmin = min_c, vmax = max_c)
		color = cm.ScalarMappable(norm = norm, cmap = cmap)
	
	if get_color:
		return cmap
	
	if axes == None:
		axes = plt.subplot(plot_number)
	width = 100/number_bins
	for i in range(len(bins)):
		for j in range(len(bins[i])):
			if bins[i][j] == None:
				c = color.to_rgba(min_c)
				print i, j
			else:
				c = color.to_rgba(bins[i][j])
			
			#If not plotting the first row, put the bottom at the end of the last row
			if j > 0:
				axes.bar(percentiles[i], width, color = c, width = width, bottom = percentiles[j-1])
			else:
				axes.bar(percentiles[i], width, color = c, width = width)
		if title == None:
			title = '{} vs {}\n{}'.format(xvar, yvar, var)
		axes.set_title(title)
		if see_xlabels:
			axes.set_xticklabels([round(x,2) for x in x_percentiles], fontsize = 8)
	if not split:
		levels = np.linspace(min_c, max_c, 100)
		clrbr = plt.contourf([[0,0],[0,0]], levels, cmap = cmap)
		clrticks = list(np.linspace(min_c, 0, 3))
		clrticks.extend(list(np.linspace(0, max_c, 3)))
		print clrticks
		bar = plt.colorbar(clrbr)
		bar.set_ticks(clrticks)
		bar.set_ticklabels([round(t,2) for t in clrticks])
		plt.xlabel(xvar)
		plt.ylabel(yvar)
		plt.title('d{}/d{}'.format(var, ind_var))
	return plt

def set_axes_clear(axes):
	axes.spines['top'].set_color('none')
	axes.spines['bottom'].set_color('none')
	axes.spines['left'].set_color('none')
	axes.spines['right'].set_color('none')
	axes.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
	return
################################################################################################
###############################START OF CODE#############################################################
################################################################################################
for case in cases:
	print "Getting case:", case
	x_axis.extend(get_var(xvar, case = case))

	y_axis.extend(get_var(yvar, case = case))

	rain.extend(get_var('amsrrridx', case = case))
	
	#If calculating slope, must be done first for sorted plots
	if slope:
		dependent = get_var(var, case = case)
		independent = get_var(ind_var, case = case)
		temp_list = []
		for i, d in zip(independent, dependent):
			if i!= None and d != None:
				temp_list.append((i,d))
			else:
				temp_list.append((0.0,0.0))
		mean_var.extend(temp_list)
	#If comparing ratio
	elif compare:
		temp_mean = get_var(var, case = case)
		temp_comp = get_var(against_var, case = case)
		temp = []
		
		for m,c in zip(temp_mean, temp_comp):
			try:
				temp.append(abs(m/c))
			except ZeroDivisionError:
				temp.append(0.0)
			except TypeError:
				temp.append(0.0)
			except:
				temp.append(0.0)
		mean_var.extend(temp)
	else:
		mean_var.extend(get_var(var, case = case))
	if split:
		print 'Getting', split_var
		split = get_var(split_var, case = case)
		split_var_list.extend(split)

if split:
	copy_x = []
	copy_y = []
	copy_v = []
	copy_split = []
	for i, (x,y,v, s,r) in enumerate(zip(x_axis, y_axis, mean_var, split_var_list,rain)):
		if x!= None and y != None and v != None and s != None:
			if all_rainstates:
				copy_x.append(x)
				copy_y.append(y)
				copy_v.append(v)
				copy_split.append(s)
			else:
				if raining:
					if r == 1:
						copy_x.append(x)
						copy_y.append(y)
						copy_v.append(v)
						copy_split.append(s)
				if nonraining:
					print 'nonraining'
					if r == 0:
						copy_x.append(x)
						copy_y.append(y)
						copy_v.append(v)
						copy_split.append(s)
	x_axis = copy.deepcopy(copy_x)
	y_axis = copy.deepcopy(copy_y)
	mean_var = copy.deepcopy(copy_v)
	split_var_list = copy.deepcopy(copy_split)
number_bins = 5
bins = [[[] for i in range(number_bins)] for j in range(number_bins)]

percentiles = np.linspace(0,100,number_bins+1)

if split:
	width = 17.5
	height = 10.
	rcParams['figure.figsize'] = width, height#width, height
else:
	width = 12.
	height = 11.
	rcParams['figure.figsize'] = width, height #width, height
#First figure out the color bars for stand dev and mean
#Then make the all plots
#Then make the low, med, high plots
if split:
	#First find the limits to the color bars, make a fake instance of a color bar, clear all plots
	#Finding the list for each low, med, high
	split_limits = [0,33.3,66.6,100]
	split_limits = np.percentile(split_var_list, split_limits, interpolation = 'midpoint')
	xs = [[],[],[]]
	ys = [[],[],[]]
	vs = [[],[],[]]
	for x,y,v,s in zip(x_axis, y_axis, mean_var, split_var_list):
		for i,(low_limit, high_limit) in enumerate(zip(split_limits[:-1], split_limits[1:])):
			if s >= low_limit and s < high_limit:
			
				xs[i].append(x)
				ys[i].append(y)
				vs[i].append(v)
	mins = []
	maxs = []
	#makes the first row of plots of mean
	for x_list, y_list, v_list in zip(xs, ys, vs):
		temp_min, temp_max = make_plot(copy.deepcopy(bins), x_list, y_list, percentiles, v_list, leave = True)
		mins.append(temp_min)
		maxs.append(temp_max)
	mean_true_min = min(mins)
	mean_true_max = max(maxs)
	mean_true_min = mean_true_min - (.2 * mean_true_min)
	color_map = make_plot(copy.deepcopy(bins), x_list, y_list, percentiles, v_list, get_color = True, overall_min = mean_true_min, overall_max = mean_true_max)
	levels = np.linspace(mean_true_min, mean_true_max, 100)
	mean_color = plt.contourf([[0,0],[0,0]], levels, cmap = color_map)
	plt.close()
	if compare:
		temp_comp = True
		compare = False
	#Repeat all above for stand dev
	stand_dev = True
	mins = []
	maxs = []
	for x_list, y_list, v_list in zip(xs, ys, vs):
		temp_min, temp_max = make_plot(copy.deepcopy(bins), x_list, y_list, percentiles, v_list, leave = True)
		mins.append(temp_min)
		maxs.append(temp_max)
	sd_true_min = min(mins)
	sd_true_max = max(maxs)
	sd_true_min = sd_true_min - (.2*sd_true_min)
	sd_color_map = make_plot(copy.deepcopy(bins), x_list, y_list, percentiles, v_list, get_color = True, overall_min = sd_true_min, overall_max = sd_true_max)
	levels = np.linspace(sd_true_min, sd_true_max, 100)
	sd_color = plt.contourf([[0,0],[0,0]], levels, cmap = sd_color_map)
	plt.close()
	stand_dev = False
	###
	length = True
	mins = []
	maxs = []
	for x_list, y_list, v_list in zip(xs, ys, vs):
		temp_min, temp_max = make_plot(copy.deepcopy(bins), x_list, y_list, percentiles, v_list, leave = True)
		mins.append(temp_min)
		maxs.append(temp_max)
	len_true_min = min(mins)
	len_true_max = max(maxs)
	len_true_min = len_true_min - (.2 * len_true_min)
	len_color_map = make_plot(copy.deepcopy(bins), x_list, y_list, percentiles, v_list, get_color = True, overall_min = len_true_min, overall_max = len_true_max)
	levels = np.linspace(len_true_min, len_true_max, 100)
	len_color = plt.contourf([[0,0],[0,0]], levels, cmap = len_color_map)
	plt.close()
	length = False
	#Set rcparams############
	width = 19
	height = 10.
	rcParams['figure.figsize'] = width, height#width, height
	########################
	#Now make the first plots

	f, axarr = plt.subplots(4, 6, squeeze = True)
	plt.subplots_adjust(hspace = .255, wspace = .22)
	clear_axes = [axarr[3][1],axarr[3][2],axarr[3][3],axarr[3][4], axarr[0][-2],axarr[2][-2],axarr[1][-2]]
	for axes in clear_axes:
		set_axes_clear(axes)
	#Add the scatter plot to the last axes
	find_mean(percentiles, copy.deepcopy(mean_var), copy.deepcopy(y_axis), axarr[3][-1], 'EIS')
	#find_mean(percentiles, copy.deepcopy(mean_var), copy.deepcopy(y_axis), axarr[1][-1],'EIS',  mean = False)
	find_mean(percentiles, copy.deepcopy(mean_var), copy.deepcopy(x_axis), axarr[3][0], 'RH')
	if temp_comp:
		compare = True
	#Make bar plot for all
	plt = make_plot(copy.deepcopy(bins), copy.deepcopy(x_axis), copy.deepcopy(y_axis), percentiles, copy.deepcopy(mean_var), title = 'All', axes = axarr[0][0])
	if compare:
		compare = False
	stand_dev = True
	plt = make_plot(copy.deepcopy(bins), copy.deepcopy(x_axis), copy.deepcopy(y_axis), percentiles, copy.deepcopy(mean_var), title = "", axes = axarr[1][0])
	axarr[0] = list(axarr[0])
	axarr[1] = list(axarr[1])
	stand_dev = False
	length = True
	plt = make_plot(copy.deepcopy(bins), copy.deepcopy(x_axis), copy.deepcopy(y_axis), percentiles, copy.deepcopy(mean_var), title = "", axes = axarr[2][0])
	length = False
	if temp_comp:
		compare = True
	#Make bar plot for low, med, high
	for x_list, y_list, v_list, number, tit in zip(xs, ys, vs, axarr[0][1:-2], ['low', 'med', 'high']):
		plt = make_plot(copy.deepcopy(bins), x_list, y_list, percentiles, v_list, title = "{} {}".format(split_var, tit), overall_min = mean_true_min, overall_max = mean_true_max, axes = number)
		row = ['low','med','high'].index(tit)
		find_mean(percentiles, copy.deepcopy(v_list), copy.deepcopy(y_list), axarr[row][-1], "EIS", show_y_label= False, title = tit)
	mean_colorbar = plt.colorbar(mean_color, ax = axarr[0][-2], use_gridspec = True, fraction = 1)
	ticks = np.linspace(mean_true_min, mean_true_max, 5)
	mean_colorbar.set_ticks(ticks)
	mean_colorbar.set_ticklabels([round(t,2) for t in ticks])
	if compare:
		compare = False
	#Now for the second row
	stand_dev = True
	for x_list, y_list, v_list, number in zip(xs, ys, vs, axarr[1][1:-2]):
		plt = make_plot(copy.deepcopy(bins), x_list, y_list, percentiles, v_list, title = "", overall_min = sd_true_min, overall_max = sd_true_max, axes = number)
	ticks = np.linspace(sd_true_min, sd_true_max, 5)
	sd_color_bar = plt.colorbar(sd_color, ax = axarr[1][-2], use_gridspec = True, fraction = 1)
	sd_color_bar.set_ticks(ticks)
	sd_color_bar.set_ticklabels([round(t,2) for t in ticks])
	stand_dev = False
	#third row of length
	length = True
	
	for x_list, y_list, v_list, number in zip(xs, ys, vs, axarr[2][1:-2]):
		plt = make_plot(copy.deepcopy(bins), x_list, y_list, percentiles, v_list, title = "", overall_min = len_true_min, overall_max = len_true_max, axes = number)
		
	len_ticks = np.linspace(len_true_min, len_true_max, 5)
	len_colorbar = plt.colorbar(len_color, ax = axarr[2][-2], fraction = 1)
	len_colorbar.set_ticks(len_ticks)
	len_colorbar.set_ticklabels([round(t,2) for t in len_ticks])
	length = False
	#Add the title and axes labels
	plt.suptitle("{}".format(var), fontsize = 19)
	
	f.text(.03, .5, xvar, fontsize = 17)
	f.text(.5, .03, yvar, fontsize = 17)
else:
	plt = make_plot(bins, x_axis, y_axis, percentiles, mean_var)
	
				

if compare or temp_comp:
	var = var + "_against_" + against_var
if length:
	length = 'density'
else:
	length = 'mean'
if stand_dev:
	length = 'std_dev'
rainstate = ""
if raining:
	rainstate = "raining"
else:
	rainstate = "nonraining"
if all_rainstates:
	rainstate = ""
fig_name = DUMP_DIR +'{}_by_{}_{}_{}_vs_{}_{}_{}.png'.format(number_bins, number_bins, rainstate, xvar, yvar, length, var)
if split:
	fig_name = fig_name.replace(".", "_split_" + split_var + ".")



#plt.show()
plt.savefig(fig_name)
temp_name = "eog " + fig_name + " &"
print fig_name
print len(argv)
if len(argv) <2:
	subprocess.call(temp_name.split(" "))
