#!/usr/bin/env python
from collections import defaultdict
import glob
import sys
import json
import argparse
import numpy as np

LETTERS = list(map(chr, range(97,123)))
for a in list(map(chr, range(97,123))):
	for b in list(map(chr, range(97,123))):
		LETTERS.append("{}{}".format(a,b))
		
SEPARATE_MARKERS = [["o","v","s"],["*","x","8"],["h","+","H"],[".","<","d"]]
MARKERS = ["o","v","s","*","x","8","h","+","H",".","<","d"]
INCLUDE_END_AIS = False

#To check the user input and make sure it is within the limits of the alphabet printed out
def check(options,user_input):
	
	if user_input in LETTERS[:len(options)]:
		return
	else:
		print "That was bad input"
		sys.exit()

#Returns a list of keys for the level of the dictionary wanted
def get_keys(dictionary, level_num):
	for latlonkey, latlon in dictionary.iteritems():
		if level_num ==1:
			return latlon.keys()
		
		for errorkey, error in latlon.iteritems():
			if level_num == 2:
				return error.keys()
				
			for touchingkey, touching in error.iteritems():
				if level_num ==3:
					return touching.keys()
				
				for aerosolkey, aerosol in touching.iteritems():
					if level_num == 4:
						return aerosol.keys()
#The start to creating directions, if need user input goes to that otherwise parses arguments and creates directions
def start_directions(args):
	if args:
		directions = parse_arguments()
	else:
		directions = get_all_user_info()
	return directions
	
#Create the dictionary directions from the provided variables
def create_directions(lat_lons, aerosols, plotting_dict, xvariable, yvariable, color_variable, separate, separate_variable, separate_show, layer, error_bar, barplot, percent_net, multiple_barplots, names, root_dir, sav_dir, season_var):
	global MARKERS
	global SEPARATE_MARKERS
	global INCLUDE_END_AIS
	print sav_dir
	directions = defaultdict(dict)
	directions['ROOT_DIR'] = root_dir
	for i,lat_lon in enumerate(lat_lons):
		print lat_lon
		for j,aerosol in enumerate(aerosols):
			directions[lat_lon][aerosol] = defaultdict(dict)
			
			if xvariable != "aodidx" or xvariable != 'AMSR-e_LWP':
				INCLUDE_END_AIS = False
			directions[lat_lon][aerosol] = {"xvariable":xvariable,"yvariable":yvariable,"color_variable":color_variable,"separate":separate,
			"separate_variable":separate_variable, "separate_show":separate_show, "layer":layer,"error_bar":error_bar,"barplot":barplot,
			"aerosol":aerosol,"lat_lon":lat_lon,"percent_net":percent_net,"multiple_barplots" : multiple_barplots, "include_end_ais": INCLUDE_END_AIS, 'season_var': season_var}
			if directions[lat_lon][aerosol]["separate"]:
				i = i + 1
				j = j+ 1
				try:
					directions[lat_lon][aerosol]["markers"] = SEPARATE_MARKERS[i*j - 1]
				except:
					directions[lat_lon][aerosol]["markers"] = MARKERS[i*j]
			else:
				i = i + 1
				j = j+1
				
				directions[lat_lon][aerosol]["markers"] = MARKERS[(i*j) - 1]

			directions[lat_lon][aerosol]["x"] = plotting_dict[lat_lon]["noerror"][layer][aerosol][xvariable]
			directions[lat_lon][aerosol]["y"] = plotting_dict[lat_lon]["noerror"][layer][aerosol][yvariable]
			directions[lat_lon][aerosol]["c"] = plotting_dict[lat_lon]["noerror"][layer][aerosol][color_variable]

			if directions[lat_lon][aerosol]["separate"]:
				directions[lat_lon][aerosol]["separating_list"] = plotting_dict[lat_lon]["noerror"][layer][aerosol][separate_variable]
				directions[lat_lon][aerosol]["separate_show_names"] = names
			#If trying to find the percent net CSWFTOA and CLWFTOA contribute to the net forcing, cannot separate by LMH yet
			if directions[lat_lon][aerosol]["percent_net"]:
				directions[lat_lon][aerosol]["separate"] = False
				directions[lat_lon][aerosol]["12_CLWFTOA"] = plotting_dict[lat_lon]["noerror"][layer][aerosol]["12_CF_CLWFTOA"]
				directions[lat_lon][aerosol]["12_CSWFTOA"] = plotting_dict[lat_lon]["noerror"][layer][aerosol]["12_CF_CSWFTOA"]
				directions[lat_lon][aerosol]["12_net"] = plotting_dict[lat_lon]["noerror"][layer][aerosol]["12_net"]
				
			directions[lat_lon][aerosol]['save_dir'] = sav_dir
	return directions
	
#Takes in user input and parses as variables held in the dict args_dict which are then transferred to the overall containing dictionary	
#If the --15degree argument given, use the 15 degree boxes, otherwise use 5 degree boxes	
def parse_arguments():
	#--lat_lons 0-0 --aerosols MODISAI --xvariable aodidx --yvariable 12_net --color_variable EC_LTSS --layer touching

	global MARKERS
	global SEPARATE_MARKERS
	parser = argparse.ArgumentParser()

	parser.add_argument('--lat_lons', nargs='+')
	parser.add_argument('--aerosols', nargs='+')
	parser.add_argument("--xvariable")
	parser.add_argument("--yvariable")
	parser.add_argument("--color_variable")

	parser.add_argument('--separate', action="store_true")
	parser.add_argument('--15degree', action='store_true')
	parser.add_argument("--separate_variable", type = str)
	parser.add_argument("--separate_show")

	parser.add_argument("--error_bar")

	parser.add_argument("--barplot", action="store_true")
	parser.add_argument("--percent_net",action="store_true")
	parser.add_argument("--multiple_barplots",action="store_true")

	parser.add_argument("--layer")
	
	parser.add_argument("--seasons", action = "store_true")
	parser.add_argument("--season_var", type = str)

	args = parser.parse_args()

	args_dict = vars(args)


	plotting_dict = defaultdict(dict)

	if args_dict['15degree']:
		ROOT_DIR = "/home/adouglas2/Summer/aerosol/new_Angola_json_files/"
		SAV_DIR = '/home/adouglas2/Summer/aerosol/Website_figures/15by15/'
	else:
		ROOT_DIR = "/home/adouglas2/Summer/aerosol/test_5by5_files/"
		SAV_DIR = '/home/adouglas2/Summer/aerosol/Website_figures/5by5/'

	
	for i, lat_lon in enumerate(args_dict["lat_lons"]):
		if args_dict['seasons']:
			temp_lat_lon = args_dict['season_var'] + "_" + lat_lon
			with open(ROOT_DIR+"season/" + temp_lat_lon + ".txt","r") as source:
				result = json.load(source)
			print temp_lat_lon
		else:
			with open(ROOT_DIR + lat_lon + ".txt","r") as source:
				result = json.load(source)
				
		plotting_dict[lat_lon] = result
	if args_dict['seasons']:
		season_var = args_dict['season_var']
	else:
		season_var = None
	#ALl of  the variables needed for directions
	lat_lons = args_dict["lat_lons"]
	aerosols = args_dict["aerosols"]
	xvariable = args_dict["xvariable"]
	yvariable = args_dict["yvariable"]
	color_variable = args_dict["color_variable"]

	separate = args_dict["separate"]
	separate_variable = args_dict["separate_variable"]
	separate_show = args_dict["separate_show"]
	
	if separate:
		separate_show = separate_show.split(",")
		separate_show = [int(s) for s in separate_show]
		print separate_show
		names = ["low","medium","high"]
		names = [names[int(n)] for n in separate_show]
	else:
		names = None

	layer = args_dict["layer"]
	barplot = args_dict["barplot"]
	multiple_barplots = args_dict["multiple_barplots"]
	error_bar = args_dict["error_bar"]
	percent_net = args_dict["percent_net"]
	directions =  create_directions(lat_lons, aerosols, plotting_dict, xvariable, yvariable, color_variable, separate, separate_variable, separate_show, layer, error_bar, barplot, percent_net, multiple_barplots, names, ROOT_DIR, SAV_DIR, season_var)
	return directions


#Returns a list of the wanted user input variables/options
#If level is a list and dictionary is none, level is the list of options
def get_user_input(question, level, dictionary,user_input = False):
	variables = []
	try:
		temp = get_keys(dictionary, level)
		print "keys:", temp
	except:
		temp = level
	if not user_input:
		#If many choices, separate into 3 columns
		if len(temp) > 25:
			temp = sorted(temp)
			total = len(temp)
			temp1 = temp[:total/3]
			temp2 = temp[total/3:2*total/3]
			temp3 = temp[2*total/3:]
			for (var1, var2, var3) in zip(temp1, temp2, temp3):
				i = str(LETTERS[temp.index(var1)] + ") " + var1)
				j = LETTERS[temp.index(var2)] + ") " + var2
				k = LETTERS[temp.index(var3)] + ") " + var3
				print ("{: <25} {: <25} {: <25}").format(i,j,k)
		else:
			#Otherwise print as is
			for i, item in enumerate(temp):
				print LETTERS[i],")", item
		user_input = raw_input(question)
		user_input = user_input.split(",")
	
	for item in user_input:
		check(temp, item)
		item = temp[LETTERS.index(item)]
		variables.append(item)

	return variables

#Gets all input information necessary to start sorting then plotting
def get_all_user_info():
	
	directions = defaultdict(dict)
	plotting_dict = defaultdict(dict)
	temp = []
	user_input = raw_input("Which resolution would you like to use? \na) 5 degree\nb) 15 degree\n")
	if user_input == "a":
		ROOT_DIR = "/home/adouglas2/Summer/aerosol/test_5by5_files/"
		SAV_DIR = '/home/adouglas2/Summer/aerosol/Website_figures/5by5/'
	else:
		ROOT_DIR = "/home/adouglas2/Summer/aerosol/new_Angola_json_files/"
		SAV_DIR = '/home/adouglas2/Summer/aerosol/Website_figures/15by15/'
	print glob.glob(ROOT_DIR + "*.txt")
	season = raw_input("Would you like to look at a season? If so, which?\na) No \nb) Wet \nc) Dry\n")
	if season == "b":
		filetag = ROOT_DIR + "season/wet" + "*.txt"
		season = True
		season_var = 'wet'
	elif season == 'c':
		filetag = ROOT_DIR + "season/dry" + "*.txt"
		season = True
		season_var = 'dry'
	else:
		season_var = None
		filetag = ROOT_DIR + "*.txt"
		season = False

	for i,filename in enumerate(glob.glob(filetag)):
		item = filename.replace(ROOT_DIR,"").strip(".txt")
		temp.append(item)
	
	lat_lons = get_user_input("Which region would you like to plot? If more than one, separate with , \n", temp, None)
	for i,lat_lon in enumerate(lat_lons):
		print "Loading",lat_lon,"data"
		with open(ROOT_DIR + lat_lon + ".txt","r") as source:
		#with open('/home/adouglas2/Summer/aerosol/new_Angola_json_files/test_-15_-15.txt', "r") as source:
			result = json.load(source)
			#print result
			print lat_lon
			if season:
				lat_lon = lat_lon[11:]
			print lat_lon
		lat_lons[i] = lat_lon
		plotting_dict[lat_lon] = result
	print ROOT_DIR
	
	print "\n"

	aerosols = get_user_input("Which aerosol(s) would you like to plot? If more than one, separate with , \n", 3, plotting_dict)
	
	xvariable = get_user_input("What is your x variable?\n",4,plotting_dict)[0]
	
	yvariable = get_user_input("What is your y variable?\n",4,plotting_dict)[0]
	
	color_variable = get_user_input("What is your color variable?\n",4,plotting_dict)[0]
	
	separate = get_user_input("Would you like to separate by low, medium, and high?\n",["Yes","No"],None)

	if "Yes" in separate:
		separate = True
		separate_variable = get_user_input("What is your separating variable? \n",4,plotting_dict)[0]
		names = ["low","medium","high"]
		separate_show = get_user_input("What levels would you like to show? If more than one, separate with ,\n",names,None)
		separate_show = [names.index(name) for name in names]
		names = [names[i] for i in separate_show]
		
		
	else:
		separate = False
		separate_variable = None
		separate_show = None
		names = None
	
	layer = get_user_input("What layer would you like to use?\n",2,plotting_dict)[0]
	
	error_bar = get_user_input("Would you like errorbars?\n",["Yes","No"],None)
	if "Yes" in error_bar:
		error_bar = True
	else:
		error_bar = False
		
	barplot = get_user_input("Would you like a bar plot?\n",["Yes","No"],None)
	if "Yes" in barplot:
		barplot = True
		percent_net = get_user_input("Would you like to compare CLWFTOA and CSWFTOA percent net? \n",["Yes","No"],None)
		if "Yes" in percent_net:
			percent_net = True
		else:
			percent_net = False
		if separate:
			multiple_barplots = get_user_input("Would you like to have the components split?\n",["Yes","No"],None)
			if "Yes" in multiple_barplots:
				multiple_barplots = True
			else:
				multiple_barplots = False
		else:
			multiple_barplots = False
				
		
	else:
		barplot = False
		percent_net = False
		multiple_barplots = False
	print SAV_DIR
	directions = create_directions(lat_lons, aerosols, plotting_dict, xvariable, yvariable, color_variable, separate, separate_variable, separate_show, layer, error_bar, barplot, percent_net, multiple_barplots, names, ROOT_DIR, SAV_DIR, season_var)
	return directions
	

