#!/usr/bin/env python
import Get_directions
import numpy as np
import sys 

output_file = "newtonian.txt"

def sign(number):
	if number * -1 < 0:
		return 1.0
	elif number * -1 > 0:
		return -1.0

def get_change(case, aerosol, xvariable, yvariable):
	ARG = "Overall.py --lat_lons={} --aerosols {} --xvariable {} --yvariable {} --color_variable EC_LTSS --layer touching --15degree".format(case, aerosol, xvariable, yvariable)
	
	directions = Get_directions.get_multi_directions(ARG)
	
	xs = [x for x,y,c in directions[case][aerosol]['plotting_list']]

	
	change = xs[-1] - xs[0]
	return change

def get_mean_separate(case, aerosol, xvariable, yvariable, separate_var):
	ARG = "Overall.py --lat_lons={} --aerosols {} --xvariable {} --yvariable {} --color_variable EC_LTSS --layer touching --15degree --separate --separate_variable={} --separate_show 0,1,2".format(case, aerosol, xvariable, yvariable, separate_var)

	directions = Get_directions.get_multi_directions(ARG)
	
	low_mean = np.mean([y for x,y,c in directions[case][aerosol]['plotting_list'][0]])
	med_mean = np.mean([y for x,y,c in directions[case][aerosol]['plotting_list'][1]])
	high_mean = np.mean([y for x,y,c in directions[case][aerosol]['plotting_list'][2]])
	
	return low_mean, med_mean, high_mean

def get_mean(case, aerosol, xvariable, yvariable):
	ARG = "Overall.py --lat_lons={} --aerosols {} --xvariable {} --yvariable {} --color_variable EC_LTSS --layer touching --15degree".format(case, aerosol, xvariable, yvariable)
	
	directions = Get_directions.get_multi_directions(ARG)
	
	mean = np.mean([y for x,y,c in directions[case][aerosol]['plotting_list']])
	
	return mean
	
def get_slope_separate(case, aerosol, xvariable, yvariable, separate_var):
	ARG = "Overall.py --lat_lons={} --aerosols {} --xvariable {} --yvariable {} --color_variable EC_LTSS --layer touching --15degree --separate --separate_variable={} --separate_show 0,1,2".format(case, aerosol, xvariable, yvariable, separate_var)
	
	directions = Get_directions.get_multi_directions(ARG)
	
	low_slope,med_slope,high_slope = directions[case][aerosol]['slope'][0],directions[case][aerosol]['slope'][1],directions[case][aerosol]['slope'][2]
	
	return low_slope, med_slope, high_slope

def get_slope(case, aerosol, xvariable, yvariable):
	ARG = "Overall.py --lat_lons={} --aerosols {} --xvariable {} --yvariable {} --color_variable EC_LTSS --layer touching --15degree".format(case, aerosol, xvariable, yvariable)
	
	directions = Get_directions.get_multi_directions(ARG)
	
	slope = directions[case][aerosol]['slope']
	
	return slope
	
def calc_cre(case, aerosol, separate = False, separate_var = None):

	if separate:
		mean_CSW = get_mean_separate(case, aerosol, 'aodidx', '12_CSWFTOA', separate_var)
		mean_CLW = get_mean_separate(case, aerosol, 'aodidx', '12_CLWFTOA', separate_var)
		mean_CF = get_mean_separate(case, aerosol, 'aodidx', '12_cloud_fraction', separate_var)
		
		dCF_dAI = get_slope_separate(case, aerosol, 'aodidx', '12_cloud_fraction', separate_var)
		
		dAlbedo_dAI = get_slope_separate(case, aerosol, 'aodidx', '12_albedo', separate_var)
		dCSW_dAlbedo = get_slope_separate(case, aerosol, '12_albedo', '12_CSWFTOA', separate_var)
		
		dCTH_dAI = get_slope_separate(case, aerosol, 'aodidx', '12_cloud_top', separate_var)
		dCLW_dCTH = get_slope_separate(case, aerosol, '12_cloud_top', '12_CLWFTOA', separate_var)
		
		CRE = []
		for SW, LW, CF, CFAI, ALAI, SWAL, CTHAI, LWCTH in zip(mean_CSW, mean_CLW, mean_CF, dCF_dAI, dAlbedo_dAI, dCSW_dAlbedo, dCTH_dAI, dCLW_dCTH):
				temp_CSW_CRE = (CFAI * SW) + (CF * ALAI * SWAL)
				temp_CLW_CRE = (CFAI * LW) + (CF * CTHAI * LWCTH)
				CRE.append(temp_CSW_CRE + temp_CLW_CRE)
		actual_CRE = get_slope_separate(case, aerosol, 'aodidx', '12_net', separate_var)
	
		return CRE, actual_CRE
		
	else:
		mean_CSW = get_mean(case, aerosol, 'aodidx', '12_CSWFTOA')
		mean_CLW = get_mean(case, aerosol, 'aodidx', '12_CLWFTOA')
		mean_CF = get_mean(case, aerosol, 'aodidx', '12_cloud_fraction')
		
		mean_RH = get_mean(case, aerosol, 'aodidx', 'RH')
		
		dCF_dRH = get_slope(case, aerosol, 'RH', '12_cloud_fraction')
		dRH_dAI = get_slope(case, aerosol, 'aodidx', 'RH')
		dCF_dAI = get_slope(case, aerosol, 'aodidx', '12_cloud_fraction')
		
		
		dAlbedo_dAI = get_slope(case, aerosol, 'aodidx', '12_albedo')
		dCSW_dAlbedo = get_slope(case, aerosol, '12_albedo', '12_CSWFTOA')
		
		dCTH_dAI = get_slope(case, aerosol, 'aodidx','12_cloud_top')
		dCLW_dCTH = get_slope(case, aerosol, '12_cloud_top','12_CLWFTOA')
		
		CSW_CRE = (dCF_dAI * mean_CSW) + (mean_CF * dAlbedo_dAI * dCSW_dAlbedo)
		CLW_CRE = (dCF_dAI * mean_CLW) + (mean_CF * dCTH_dAI * dCLW_dCTH)
		
		#testing out new fit with RH
		CSW_CRE = (dRH_dAI * mean_CSW) + (mean_RH *dCF_dAI* dAlbedo_dAI * dCSW_dAlbedo)
		CLW_CRE = (dRH_dAI * mean_CLW) + (mean_RH *dCF_dAI* dCTH_dAI * dCLW_dCTH)
		CRE = CSW_CRE + CLW_CRE
		
		true_CRE = get_slope(case, aerosol, 'aodidx', '12_net')
		return CRE, true_CRE

def newtonian_breakdown(case, aerosol, separate = False, separate_var = None):
	if separate:
		pass
	else:
		dCSW_dAI = get_slope(case, aerosol, 'aodidx', '12_CSWFTOA')
		dCSW_dEIS = get_slope(case, aerosol, 'EIS3', '12_CSWFTOA')
		dCSW_dRH = get_slope(case, aerosol, 'RH', '12_CSWFTOA')
		dCSW_dCF = get_slope(case, aerosol, '12_cloud_fraction', '12_CSWFTOA')
		
		dCSW_dAl = get_slope(case, aerosol, '12_albedo', '12_CSWFTOA')
		change_albedo = get_change(case, aerosol, '12_albedo', '12_net')
		
		change_RH = get_change(case, aerosol, 'RH', '12_CSWFTOA')
		change_AI = get_change(case, aerosol, 'aodidx', '12_CSWFTOA')
		change_EIS = get_change(case, aerosol, 'EIS3', '12_CSWFTOA')
		change_CF = get_change(case, aerosol, '12_cloud_fraction', '12_CSWFTOA')
		
		dCLW_dAI = get_slope(case, aerosol, 'aodidx', '12_CLWFTOA')
		dCLW_dEIS = get_slope(case, aerosol, 'EIS3', '12_CLWFTOA')
		dCLW_dRH = get_slope(case, aerosol, 'RH', '12_CLWFTOA')
		dCLW_dCF = get_slope(case, aerosol, '12_cloud_fraction', '12_CLWFTOA')
		
		CSW = (dCSW_dAI * change_AI) + (dCSW_dEIS * change_EIS) + (dCSW_dRH * change_RH) + (dCSW_dCF * change_CF) + (dCSW_dAl * change_albedo)
		
		CLW = (dCLW_dAI * change_AI) + (dCLW_dEIS * change_EIS) + (dCLW_dRH * change_RH) + (dCSW_dCF * change_CF)
		
		#actual = get_slope(case, aerosol, 'aodidx', '12_CSWFTOA')
		change = get_slope(case, aerosol, 'aodidx', '12_CSWFTOA')
		real_actual = get_change(case, aerosol, '12_CSWFTOA', '12_net') * sign(change)
		actual = "{}  {}   {}  {}  {}".format(str(dCSW_dAI * change_AI), str(dCSW_dEIS * change_EIS), str(dCSW_dRH * change_RH), str(dCSW_dCF * change_CF), str(dCSW_dAl * change_albedo))
		return (CSW, real_actual), (actual,change)
		
out = open(output_file, 'w')

#test_cases = ['-15_-15', '-30_-15', '-45_-15', '0_-15', '15_-15', '-15_-30', '-30_-30', '-45_-30', '0_-30', '15_-30', '-15_0', '-30_0', '-45_0', '0_0', '-30_15', '-45_15']
test_cases = ['0_0','0_-15','0_-30','-15_0','-15_-15','-15_-30','-30_0','-30_-15','-30_-30']

for case in test_cases:
	out.write("{:30}\n".format(case))

	#calculated, actual = calc_cre(case, "MODISAI")
	calculated, actual = newtonian_breakdown(case, 'MODISAI')
	out.write("{:15}{:15}\n".format("calculate", calculated))
	out.write("{:15}{:15}\n".format("retrieved", actual))
	#Code for separate cases writing out
	'''
	calculated, actual = calc_cre(case, 'MODISAI', True, 'RH')
	error = [((c-a)/c) for c,a in zip(calculated, actual)]
	if max(error) < 2:
		out.write("****")
	out.write("{:30}\n".format('calculated'))
	out.write("{:10} {:10} {:10}\n".format('low','medium','high'))
	out.write("{:10} {:10} {:10}\n".format(calculated[0],calculated[1],calculated[2]))
	out.write('{:30}\n'.format('retrieved'))
	out.write("{:10} {:10} {:10}\n".format(actual[0],actual[1],actual[2]))'''
out.close()
	

