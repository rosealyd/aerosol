#!/usr/bin/env python
import sys
import json
import glob
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress
from scipy import stats
import plot_scatter
import automated_10_by_10
import cases_class
import calc_region_regression

cases = cases_class.Cases()
globe = cases.Angola
calc_region_regression.regress(globe, 'aodidx', '12_CSWFTOA', 'touching_day')
sys.exit()







ROOT_DIR = '/home/adouglas2/Summer/aerosol/test_Globe/'
cases =['-15_-105', '-15_-120', '-15_-135', '-15_-15', '-15_-150', '-15_-165', '-15_-180', '-15_-30', '-15_-45', '-15_-60', '-15_-75', '-15_-90', '-15_0', '-15_15', '-15_30', '-15_45', '-15_60', '-15_75', '-15_90', '-30_-105', '-30_-120', '-30_-135', '-30_-15', '-30_-150', '-30_-165', '-30_-180', '-30_-30', '-30_-45', '-30_-60', '-30_-75', '-30_-90', '-30_0', '-30_15', '-30_30', '-30_45', '-30_60', '-30_75', '-30_90', '-45_-105', '-45_-120', '-45_-135', '-45_-15', '-45_-150', '-45_-165', '-45_-180', '-45_-30', '-45_-45', '-45_-60', '-45_-75', '-45_-90', '-45_0', '-45_15', '-45_30', '-45_45', '-45_60', '-45_75', '-45_90', '-60_-105', '-60_-120', '-60_-135', '-60_-15', '-60_-150', '-60_-165', '-60_-180', '-60_-30', '-60_-45', '-60_-60', '-60_-75', '-60_-90', '-60_0', '-60_15', '-60_30', '-60_45', '-60_60', '-60_75', '-60_90', '0_-105', '0_-120', '0_-135', '0_-15', '0_-150', '0_-165', '0_-180', '0_-30', '0_-45', '0_-60', '0_-75', '0_-90', '0_0', '0_15', '0_30', '0_45', '0_60', '0_75', '0_90', '15_-105', '15_-120', '15_-135', '15_-15', '15_-150', '15_-165', '15_-180', '15_-30', '15_-45', '15_-60', '15_-75', '15_-90', '15_0', '15_15', '15_30', '15_45', '15_60', '15_75', '15_90', '30_-105', '30_-120', '30_-135', '30_-15', '30_-150', '30_-165', '30_-180', '30_-30', '30_-45', '30_-60', '30_-75', '30_-90', '30_0', '30_15', '30_30', '30_45', '30_60', '30_75', '30_90', '45_-105', '45_-120', '45_-135', '45_-15', '45_-150', '45_-165', '45_-180', '45_-30', '45_-45', '45_-60', '45_-75', '45_-90', '45_0', '45_15', '45_30', '45_45', '45_60', '45_75', '45_90']
#cases = ['0_0', '0_-15', '0_-30', '-15_0', '-15_-15', '-15_-30']


xs = []
ys = []
colors = []
percentile = []
above = 0
total = 0
plot_x = []
plot_y = []
#make 2 d plot shaded by LWP
for case in cases:
	print case
	filename = ROOT_DIR + case + '.txt'
	with open(filename, 'r') as source:
		result = json.load(source)
		plot_x.extend(result['noerror']['touching_day']['MODISAI']['48_cspop'])
		plot_y.extend(result['noerror']['touching_day']['MODISAI']['48_cld_top'])
		xs.extend(result['noerror']['touching_day']['MODISAI']['eis4'])
		ys.extend(result['noerror']['touching_day']['MODISAI']['rh'])
		colors.extend(result['noerror']['touching_day']['MODISAI']['lwp'])
		if len([m for m in result['noerror']['touching_day']['MODISAI']['lwp'] if type(m) == float]) > 0:
			p = float(len([m for m in result['noerror']['touching_day']['MODISAI']['lwp'] if type(m) == float and m > .7]))/len([m for m in result['noerror']['touching_day']['MODISAI']['lwp'] if type(m) == float])
			total += len([m for m in result['noerror']['touching_day']['MODISAI']['lwp'] if type(m) == float])
			above += float(len([m for m in result['noerror']['touching_day']['MODISAI']['lwp'] if type(m) == float and m > .7]))
			percentile.append(p)
raining = [i for i, (rr, lwp) in enumerate(zip(plot_x, colors)) if rr > 0 and lwp < .7]
unstable = [j for j, eis in enumerate(xs) if eis < 0 and j in raining]
stable = [j for j, eis in enumerate(xs) if eis > 0 and eis < 10 and j in raining]


good = [i for i,(x,y,c) in enumerate(zip(xs, ys, colors)) if type(x) == float and type(y) == float and type(c) == float and c <= 1.0]

#xs = list(np.array(xs)[good])
#ys = list(np.array(ys)[good])
#colors = list(np.array(colors)[good])
xs = list(np.array(plot_x)[unstable])
ys = list(np.array(plot_y)[unstable])
xmin, xmax = min(xs), max(xs)
ymin, ymax = min(ys), max(ys)

x,y = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
positions = np.vstack([x.ravel(), y.ravel()])
values = np.vstack([xs, ys])
kernel = stats.gaussian_kde(values)
f = np.reshape(kernel(positions).T, x.shape)

plt.imshow(np.rot90(f), cmap = plt.cm.seismic, extent = [xmin, xmax, ymin, ymax])
cset = plt.contour(x, y, f)
plt.clabel(cset, inline=1, fontsize = 16)
plt.title('US')
plt.show()
