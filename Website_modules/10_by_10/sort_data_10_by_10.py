#!/usr/bin/env python
import numpy as np
from collections import defaultdict
import sys
import copy
import time
from scipy.stats import linregress
from scipy.cluster.vq import vq, kmeans, whiten
import matplotlib.pyplot as plt
import math
import run_MVLR as MVLR_mod


temp_r = []


poss_calc = []
poss_ret = []
#Toggling tools
num_bins = 5
percentiles = np.linspace(0,100,num_bins + 1)
high_ai_threshold =0
len_limit = 25
show_retrieved = True
min_r = .45
min_p = .05
within_1std = []
count = 0
def find_length(plotting_list):
	total_length = 0.0				
	for p in plotting_list:
		for o in p:
			if type(o) != list:
				if len(o['cf']) > len_limit:
					total_length += len(o['cf'])
			if type(o) == list:
				if len(o) > len_limit:
					total_length += len(o)
			
	return total_length

#Finding the slope by binning by the x axis
def bin_regress(xs, ys, get_err = None, constrain = False):
	global count
	'''	bin_limits = list(np.percentile(xs, np.linspace(0,100,11), interpolation = 'midpoint'))
	bin_limits[-1] = bin_limits[-1] * 1.1
	
	bins = [[] for i in range(10)]
	for x, y in zip(xs, ys):
		for p, (low, high) in enumerate(zip(bin_limits[:-1], bin_limits[1:])):
			if x >= low and x < high:
				bins[p].append(y)
	
	bin_limits = [(a+b)/2 for a, b in zip(bin_limits[:-1], bin_limits[1:])]
	for i, bin in enumerate(bins):
		bin = [b for b in bin if b < (np.mean(bin) + np.std(bin)) and b > (np.mean(bin) - np.std(bin))]
		bins[i] = np.mean(bin)
	xs = []
	ys = []
	for x, y in zip(bin_limits, bins):
		if not np.isnan(y) and type(y) != None:
			xs.append(x)
			ys.append(y)
	'''
	#print xs, ys
	#plt.scatter(xs, ys)
	#plt.show()
#	print max(xs)
	regress = linregress(xs, ys)
	
	if constrain:
	#TOggle here for p value and r value limits
		if get_err and abs(regress[2]) > min_r:
			if 'low' in get_err:
				return float(regress[0]) - float(regress[-1])
			if 'high' in get_err:
				return float(regress[0]) + float(regress[-1])
		
		if abs(regress[2]) >  min_r:
			temp_r.append(regress[2])
			return float(regress[0])
		else:
			count+=1
			#print count
			return 0.0
	else:
		if get_err:
			if 'low' in get_err:
				return float(regress[0]) - float(regress[-1])
			if 'high' in get_err:
				return float(regress[0]) + float(regress[-1])
		else:
			return float(regress[0])
		
#Finding teh slope using representative points
def k_means_slope(cfs, ais):
	#Each row is an observation or single measurement
	#Column is a variable
	cf_ai = []
	for cf, ai in zip(cfs, ais):
		if ai < high_ai_threshold:
			cf_ai.append([cf, ai])
	new_pts = kmeans(np.array(cf_ai),int(.1 * len(cf_ai)))[0]
	cfs = []
	ais = []
	for pair in new_pts:
		cfs.append(tuple(pair)[0])
		ais.append(tuple(pair)[1])
	regress = linregress(ais, cfs)
	return regress[0]
	
#Sorting for calculated values
def sort_calc(region, plotting_list, x_bin_limits, y_bin_limits, run_MVLR = False):
	global temp_r
	rad_var = region['radiation variable']
	dep_var = region['dependent variable']
	region['min r'] = min_r**2
	#Initialize lists in plotting dictionary
	for i,p in enumerate(plotting_list):
		for j,o in enumerate(p):
			plotting_list[i][j]['cf'] = []
			plotting_list[i][j][dep_var] = []
			plotting_list[i][j]['AI'] = []
			plotting_list[i][j][rad_var] = []
			plotting_list[i][j]['CF_'+rad_var] = []
	
	high_ai_threshold = list(np.percentile(region['AI'], percentiles))[-1]
	
	scale = region['scale']
	
	for x, y, cf, dv, f, ai, cf_f  in zip(region['x'], region['y'], region[scale + "_cloud_fraction"], region[scale + '_'+dep_var], region[scale + '_'+rad_var], region['AI'], region[scale + '_CF_' + rad_var]):
			for i,(low_x_bin_limit, high_x_bin_limit) in enumerate(zip(x_bin_limits[:-1], x_bin_limits[1:])):
				for j,(low_y_bin_limit, high_y_bin_limit) in enumerate(zip(y_bin_limits[:-1], y_bin_limits[1:])):
					if x >= low_x_bin_limit and x < high_x_bin_limit:
						if y >= low_y_bin_limit and y < high_y_bin_limit:
							plotting_list[i][j]['cf'].append(cf)
							plotting_list[i][j][dep_var].append(dv)
							plotting_list[i][j]['AI'].append(ai)
							plotting_list[i][j][rad_var].append(f)
							plotting_list[i][j]['CF_' + rad_var].append(cf_f)
							'''print x,y
							print low_x_bin_limit, high_x_bin_limit
							print low_y_bin_limit, high_y_bin_limit
							print i,j
							sys.exit()'''
	#FInd the total length for finding the weight of each element
	total_length = find_length(plotting_list)
	weight_array = copy.deepcopy(plotting_list)
	retrieved = copy.deepcopy(plotting_list)
	retrieved_low = copy.deepcopy(plotting_list)
	retrieved_high = copy.deepcopy(plotting_list)
	standard_error = copy.deepcopy(plotting_list)
	calc_low = copy.deepcopy(plotting_list)
	calc_high = copy.deepcopy(plotting_list)
	MLVRs = copy.deepcopy(plotting_list)
	CREs = copy.deepcopy(plotting_list)
	min_c = 1000
	max_c = 0
		
	for i, p in enumerate(plotting_list):
		for j, o in enumerate(p):
		#Must be above the length threshold
			if len(o['cf']) > len_limit:
				print 'len cf', len(o['cf'])
				#RUn multivariate linear regression to test significance of each bin
				if run_MVLR:
					print i,j
					#print 'x1 is AI, x2 is cf, x3 is cloud albedo, x4 is cond mean forcing'
					
					simulated, actual = MVLR_mod.find_MVLR(o[rad_var], o['AI'], o['cf'], o[dep_var], o['CF_' + rad_var])
					CREs.append(actual)
				#Find the CRE using tristans method
				if region['to_find'] == 'Tristan':
					print 'here using tristan'
					cf_ai = bin_regress(o['AI'], o['cf'],constrain = True)
					
					mean_cf = np.mean(o['cf'])
					
					mean_swf = np.mean(o['CF_' + rad_var])
					
					lifetime = cf_ai * mean_swf
					if region['two term']:
						print 'Two term'
						Twomey = bin_regress(o['AI'], o['CF_' + rad_var]) * mean_cf
					else:
						albedo_ai = bin_regress(o['AI'], o[dep_var], constrain = True)
						swf_albedo = bin_regress(o[dep_var], o['CF_'+rad_var], constrain = True)
						print 'Three term'
						Twomey = albedo_ai * swf_albedo * mean_cf
					swf_ai = bin_regress(o['AI'],o['CF_'+rad_var])
					CRE = lifetime + Twomey
					
					#For low calc
					cf_ai =  bin_regress(o['AI'], o['cf'], get_err = 'low', constrain = True)
					low_lifetime = cf_ai * mean_swf
					
					if region['two term']:
						low_Twomey = bin_regress(o['AI'], o['CF_' + rad_var], get_err = 'low', constrain = True) * mean_cf
					else:
						albedo_ai = bin_regress(o['AI'], o[dep_var], get_err = 'low', constrain = True)
						swf_albedo = bin_regress(o[dep_var], o['CF_' + rad_var], get_err = 'low', constrain = True)
						low_Twomey = albedo_ai * swf_albedo * mean_cf
					low_CRE = low_lifetime + low_Twomey
					
					#For high calc
					cf_ai =  bin_regress(o['AI'], o['cf'], get_err = 'high', constrain = True)
					high_lifetime = cf_ai * mean_swf
					if region['two term']:
						high_Twomey = bin_regress(o['AI'], o['CF_' + rad_var], get_err = 'high', constrain = True) * mean_cf
					else:
						albedo_ai = bin_regress(o['AI'], o[dep_var], get_err = 'high', constrain  = True)
						swf_albedo = bin_regress(o[dep_var], o['CF_' + rad_var], get_err = 'high', constrain = True)
						high_Twomey = albedo_ai * swf_albedo * mean_cf
					high_CRE = high_lifetime + high_Twomey
					
					
					if region['Twomey ratio'] and CRE != 0.0:
						print 'taking Twomey ratio'
						CRE = (Twomey) / CRE
						low_CRE = (low_Twomey) / low_CRE
						high_CRE = (high_Twomey) / high_CRE
					if region['lifetime ratio'] and CRE != 0.0:
						print 'taking lifetime ratio'
						CRE = (lifetime) / CRE
						low_CRE = (low_lifetime) / low_CRE
						high_CRE = (high_lifetime) / high_CRE
					if region['only Twomey']:
						CRE = Twomey
						low_CRE = low_Twomey
						high_CRE = high_Twomey
					if region['only lifetime']:
						CRE = lifetime
						low_CRE = low_lifetime
						high_CRE = high_lifetime	
						
				#IF doing newtonian the derivatives are different and indirect effect calculated diff
				if region['to_find'] == 'Newtonian':
					cf_ai = linregress(o['cf'], o['AI'])[0]
					albedo_ai = linregress(o[dep_var], o['AI'])[0]
					swf_albedo = linregress(o[rad_var], o[dep_var])[0]
					swf_cf = linregress(o[rad_var], o['cf'])[0]
					CRE = ((cf_ai * swf_cf) + (albedo_ai * swf_albedo))
					
				if region['to_find'] == 'longwave':
					cf_ai = bin_regress(o['AI'], o['cf'], constrain = True)
					low_cf_ai = bin_regress(o['AI'], o['cf'], get_err = 'low', constrain = True)
					high_cf_ai = bin_regress(o['AI'], o['cf'], get_err = 'high', constrain = True)
					
					mean_lw = np.mean(o['CF_' + rad_var])
					
					if region['two term']:
						lw_ai = bin_regress(o['AI'], o['CF_' + rad_var], constrain = True)
						mean_cf = np.mean(o['cf'])
						CRE = (cf_ai * mean_lw) + (lw_ai * mean_cf)
						
						low_lw_ai = bin_regress(o['AI'], o['CF_' + rad_var], get_err = 'low', constrain = True)
						high_lw_ai = bin_regress(o['AI'], o['CF_' + rad_var], get_err = 'low', constrain = True)
						
						low_CRE = (low_cf_ai * mean_lw) + (low_lw_ai * mean_cf)
						high_CRE = (high_cf_ai * mean_lw) + (high_lw_ai * mean_cf)
						
					else:
						cld_top_ai = bin_regress(o['AI'], o[dep_var], constrain = True)
						lw_cld_top = bin_regress(o[dep_var], o['CF_' + rad_var], constrain = True)
						mean_cf = np.mean(o['cf'])
						CRE = (cf_ai * mean_lw) + (cld_top_ai * lw_cld_top * mean_cf)
						
						low_cld_top_ai = bin_regress(o['AI'], o[dep_var], get_err = 'low', constrain = True)
						high_cld_top_ai = bin_regress(o['AI'], o[dep_var], get_err = 'high', constrain = True)
						
						low_lw_cld_top = bin_regress(o[dep_var], o['CF_' + rad_var], get_err = 'low', constrain = True)
						high_lw_cld_top = bin_regress(o[dep_var], o['CF_' + rad_var], get_err = 'high', constrain = True)
						
						low_CRE = (low_cf_ai * mean_lw) + (low_cld_top_ai * low_lw_cld_top * mean_cf)
						high_CRE = (high_cf_ai * mean_lw) + (high_cld_top_ai * high_lw_cld_top * mean_cf)
						
						
				#no matter how its calculated, the CRE replaces the dict used to calc it
				plotting_list[i][j] = CRE
				calc_low[i][j] = low_CRE
				calc_high[i][j] = high_CRE
				#THe retrieved CRE
				
				
				if CRE != 0.0:
					weight_array[i][j] = o['cf']
					retrieved[i][j] = bin_regress(o['AI'], o[rad_var])
					retrieved_low[i][j] = bin_regress(o['AI'], o[rad_var], get_err = 'low')
					retrieved_high[i][j] = bin_regress(o['AI'], o[rad_var], get_err = 'high')
				else:
					#print i, j, 'will not contribute to weight'
					weight_array[i][j] = []
					retrieved[i][j] = 0.0
					retrieved_low[i][j] = 0.0
					retrieved_high[i][j] = 0.0
				
			else:
				#print i, j, 'less than length limit'
				plotting_list[i][j] = 0.0
				calc_low[i][j] = 0.0
				calc_high[i][j] = 0.0
				retrieved[i][j] = 0.0
				retrieved_low[i][j] = 0.0
				retrieved_high[i][j] = 0.0
			
	total_length = find_length(weight_array)
	print 'total length', total_length
	
	for i in range(len(plotting_list)):
		for j in range(len(plotting_list[i])):
			plotting_list[i][j] = plotting_list[i][j] * len(weight_array[i][j])/total_length
			calc_low[i][j] = calc_low[i][j] *len(weight_array[i][j])/total_length
			calc_high[i][j] = calc_high[i][j] * len(weight_array[i][j])/total_length
			
			retrieved[i][j] = retrieved[i][j] * len(weight_array[i][j])/total_length
			retrieved_low[i][j] = retrieved_low[i][j] * len(weight_array[i][j])/total_length
			retrieved_high[i][j] = retrieved_high[i][j] * len(weight_array[i][j])/total_length
			if plotting_list[i][j] != 0.0:
				if plotting_list[i][j] < 0 and retrieved[i][j] > 0:
					standard_error[i][j] = ( retrieved[i][j] - plotting_list[i][j] ) / np.mean([plotting_list[i][j], retrieved[i][j]])
				else:
					standard_error[i][j] = ( plotting_list[i][j] - retrieved[i][j] ) / np.mean([plotting_list[i][j], retrieved[i][j]])
			else:
				standard_error[i][j] = 0.0
	region['retrieved low list'] = retrieved_low
	region['retrieved list'] = retrieved
	region['retrieved high list'] = retrieved_high
	if show_retrieved:
		region['retrieved'] = sum([p for o in retrieved for p in o])
		region['retrieved low'] = sum([p for o in retrieved_low for p in o])
		region['retrieved high'] = sum([p for o in retrieved_high for p in o])
	######
	region['calc low list'] = calc_low
	region['calc high list'] = calc_high
	region['calc low'] = sum([p for o in calc_low for p in o])
	region['calc high'] = sum([p for o in calc_high for p in o])
	
	####
	
	region['standard error'] = sum([p for o in standard_error for p in o])
	
	return plotting_list

def sort(region, plotting_list, x_bin_limits, y_bin_limits):
	var_name = region['var_name']
#If not calculating cre then we have the variable name from our region dict and can use it as a key to access the list
	for i in range(len(plotting_list)):
		for j in range(len(plotting_list[i])):
			plotting_list[i][j][var_name] = []
			plotting_list[i][j]['AI'] = []
	for x, y, v, ai in zip(region['x'], region['y'], region[var_name], region['AI']):
			for i,(low_x_bin_limit, high_x_bin_limit) in enumerate(zip(x_bin_limits[:-1], x_bin_limits[1:])):
				for j,(low_y_bin_limit, high_y_bin_limit) in enumerate(zip(y_bin_limits[:-1], y_bin_limits[1:])):
					if x >= low_x_bin_limit and x < high_x_bin_limit:
						if y >= low_y_bin_limit and y < high_y_bin_limit:
							plotting_list[i][j][var_name].append(v)
							plotting_list[i][j]['AI'].append(ai)
							
	total_length = find_length(plotting_list)

	for i in range(len(plotting_list)):
		for j in range(len(plotting_list[i])):
			if len(plotting_list[i][j][var_name]) > len_limit:
				if 'mean' in region['to_find']:
					plotting_list[i][j] = np.mean(plotting_list[i][j][var_name])
				if 'length' in region['to_find']:
					plotting_list[i][j] = len(plotting_list[i][j][var_name])/total_length
				if 'sd' in region['to_find']:
					plotting_list[i][j] = np.std(plotting_list[i][j][var_name])
				if 'slope' in region['to_find']:
					plotting_list[i][j] = bin_regress(plotting_list[i][j]['AI'], plotting_list[i][j][var_name])
			else:
				plotting_list[i][j] = 0.0
	return plotting_list
				
def start_sort(region):
	global high_ai_threshold 
	print 'len x', len(region['x'])
	percentiles = np.linspace(0,100,region['num_bins'] + 1)
	
	#First find x and y bin limits
	if region['use_hard']:
		#x_bin_limits = np.linspace(-2, 8, 11)
		x_bin_limits = region['x_bin_limits']
		y_bin_limits = region['y_bin_limits']
		#y_bin_limits = np.linspace(0, 1.1, 11)
		print 'Updating number of bins'
		#region['num_bins'] = 10
	else:
		x_bin_limits = np.percentile(region['x'], percentiles, interpolation = 'midpoint')
		y_bin_limits = np.percentile(region['y'], percentiles, interpolation = 'midpoint')
		region['x_bin_limits'] = x_bin_limits
		region['y_bin_limits'] = y_bin_limits
		#region['num_bins'] = num_bins
	#List of dictionaries which contain the lists of variables for each regime
	temp = defaultdict(dict)
	plotting_list = [ [copy.deepcopy(temp) for j in range(len(y_bin_limits[:-1]))] for i in range(len(x_bin_limits[:-1]))]
	#If calculating a cloud shortwave
	if region['to_find'] == 'Newtonian' or region['to_find'] == 'Tristan' or region['to_find'] == 'longwave':
		plotting_list = sort_calc(region, plotting_list, x_bin_limits, y_bin_limits, run_MVLR = region['MVLR'])
		region['calculated'] = sum([p for o in plotting_list for p in o])
	else:
		plotting_list = sort(region, plotting_list, x_bin_limits, y_bin_limits)
	region['plotting list'] = plotting_list
	
	return region
