#!/usr/bin/env python
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import automated_10_by_10
from collections import defaultdict
import numpy as np
import get_cmap
import matplotlib.colors as clrs
import sys
import cases_class
import calc_region_regression
import plot_10_by_10
from pylab import rcParams
import subprocess
import time

#For each regime, get the value of each lat_lon/case
#after getting 
mean = False
retrieved = False
error = False
regress = False
lifetime = False
Twomey = False

def find_corners(lats, lons, LENGTH=15):
	x_coords_corners = []
	y_coords_corners = []
	for x,y in zip(lons,lats):
		x1 = x
		x2 = x
		x3 = x+LENGTH
		x4 = x+LENGTH
		y1 = y
		y2 = y+LENGTH
		y3 = y
		y4=y+LENGTH
		x_coords_corners.append(np.array([[x1,x2],[x3,x4]]))
		y_coords_corners.append(np.array([[y1,y2],[y3,y4]]))

	return x_coords_corners, y_coords_corners
	
def make_map(values, lats, lons, lifetime):
	#rcParams['figure.figsize'] = 12,11 #width, height
	min_value = min([v  for v in values if not np.isnan(v)])
	max_value = max([v  for v in values if not np.isnan(v)])
	#min_value, max_value = plot_10_by_10.set_midpoint(min_value, max_value, 0.)
	min_value, max_value = plot_10_by_10.set_midpoint(min_value, max_value, 0., offset = 1.5)
	print min_value, max_value
	plt.figure(figsize=(24,24))
	m = Basemap(projection = 'gall', llcrnrlat=-90,urcrnrlat=90,  llcrnrlon=-180, urcrnrlon=180, resolution = 'c')
	
	cmap = get_cmap.get_cmap()
	norm = clrs.Normalize(vmin =min_value, vmax = max_value)
	#norm = clrs.SymLogNorm(linthresh = .9 * max_value, vmin =min_value, vmax = max_value)
	m.fillcontinents(color = '#005C1F', lake_color = '#005C1F')
	
	corner_lons, corner_lats = find_corners(lats, lons)
	
	for i, value in enumerate(values):
		print i
		
		x = np.arange(3)
		y = np.arange(3)
		X, Y = np.meshgrid(x, y)
		
		value = np.array([[value, value], [value, value]])
		if not np.isnan(value[0][0]):
			print value[0][0]
			aerosol = m.pcolormesh(corner_lons[i], corner_lats[i],value, shading = 'flat', norm = norm, cmap = cmap, latlon = True, vmin = max_value, vmax = min_value)
		else:
			print 'is nan', corner_lons[i], corner_lats[i], value[0][0]
			aerosol = m.pcolormesh(corner_lons[i], corner_lats[i],value, shading = 'flat', norm = norm, cmap = cmap, latlon = True, vmin = max_value, vmax = min_value, alpha = 0.)
			
	clabels =[(round(a,5),"{}".format(round(a,2))) for a in list(np.linspace(min_value, max_value, 7))]
	if lifetime:
		clabels[0] = (clabels[0][0], "Decreased Cloudiness")
		clabels[-1] = (clabels[-1][0], "Increased Cloudiness")
	elif error:
		clabels[0] = (clabels[0][0], "{}".format(clabels[0][0]))
		clabels[-1] = (clabels[-1][0], "{}".format(clabels[-1][0]))
	else:
		clabels[0] = (clabels[0][0], "Decreased Effect")
		clabels[-1] = (clabels[-1][0], "Increased Effect")
	bar = plt.colorbar(ticks = [t[0] for t in clabels], fraction = .03)
	bar.set_ticklabels([c[1] for c in clabels])
	bar.ax.tick_params(labelsize = 16)
	
	return plt	
	
def get_value(cases, lifetime, Twomey):
	map_values = defaultdict(dict)
	remove = []
	for case in cases:
		#print case
		#Not splitting
		try:
			if lifetime:
				print 'getting lifetime'
				temp = automated_10_by_10.auto_decompose_sw(False, False, False, True, False, False, False, 'Tristan', False, False, str(case), 'lwp', True, True, 'touching_day', 'all', True, False, case)
			elif Twomey:
				print 'getting twomey'
				temp = automated_10_by_10.auto_decompose_sw(False, False, False, False, False, True, False, 'Tristan', False, False, str(case), 'lwp', True, True, 'touching_day', 'all', True, False, case)
			else:	
				print 'getting calculated'
				temp = automated_10_by_10.auto_decompose_sw(False, False, False, False, False, False, False, 'Tristan', False, False, str(case), 'lwp', True, True, 'touching_day', 'all', True, False, case)
	
			temp['plotting list']= [ [point for point in regime if not np.isnan(point)] for regime in temp['plotting list']]
			print temp['plotting list']
			map_values[case] = temp
		except:
			remove.append(case)
	cases = [case for case in cases if case not in remove]
	print 'nan', remove
	return map_values, cases
	
def plot_map(cases, mean, retrieved, error, regress, lifetime, Twomey):
	if lifetime:
		save_name = '/home/adouglas2/Summer/aerosol/Website_modules/world_map_figures/' + sys.argv[1] + '_' +'lifetime_'+ sys.argv[2] + '.png'
	elif Twomey:
		save_name = '/home/adouglas2/Summer/aerosol/Website_modules/world_map_figures/' + sys.argv[1] + '_' +'Twomey_'+ sys.argv[2] + '.png'
	else:
		save_name = '/home/adouglas2/Summer/aerosol/Website_modules/world_map_figures/' + sys.argv[1] + '_' + sys.argv[2] + '.png'
	print save_name
	lats = []
	lons = []
	for case in cases:
		lat = float(case.split('_')[0])
		lon = float(case.split('_')[1])
		lats.append(lat)
		lons.append(lon)
	
	if regress:
		remove = []
		values = []
		total_points = []
		for case in cases:
			print case
			if lifetime:
				print 'finding lifetime'
				temp_val, num_points = calc_region_regression.regress([case], 'aodidx', '12_cloud_fraction', 'touching_day')
			elif Twomey:
				print 'finding Twomey'
				temp_val, num_points = calc_region_regression.regress([case], 'aodidx', '12_CF_CSWFTOA', 'touching_day')
			else:
				'Finding indirect'
				temp_val, num_points = calc_region_regression.regress([case], 'aodidx', '12_CSWFTOA', 'touching_day')
			if not np.isnan(temp_val):
				values.append(temp_val)
				total_points.append(num_points)
			else:
				remove.append(case)
		print 'removed', remove
		cases = [case for case in cases if case not in remove]
		total = sum(total_points)
		#total number of points globally is...
		#total = 241842.
		values = [v*n/total for v, n in zip(values, total_points)]
		plt = make_map(values, lats, lons, lifetime)
		
	else:
		print 'Getting values'
		values_for_map, cases = get_value(cases, lifetime, Twomey)
		values = []
		
		for case in cases:
			if np.isnan(sum([a for b in values_for_map[case]['plotting list'] for a in b])):
				print case, 'is nan'
				print values_for_map[case]['plotting list']
				time.sleep(5)
			if error:
				print 'here at error'
				calc = sum([a for b in values_for_map[case]['plotting list'] for a in b])
				ret = sum([a for b in values_for_map[case]['retrieved list'] for a in b])
				difference = calc - ret
				print difference, case
				values.append(difference)
			elif retrieved:
				print 'here at retrieved'
				values.append(sum([a for b in values_for_map[case]['retrieved list'] for a in b]))
			else:
				print 'getting mean, twomey, or lifetime'
				values.append(sum([a for b in values_for_map[case]['plotting list'] for a in b]))
				
				
		print len(values), len(cases)
		plt = make_map(values, lats, lons, lifetime)
		#plt.show()
	print save_name
	plt.savefig(save_name, dpi = 300, transparent = True, orientation = 'landscape', bbox_inches = 'tight', pad_inches = 0, frameon = False)
	call_name = 'eog ' + save_name + ' &'
	subprocess.call(call_name.split(' '))

	return		
				
				
test_cases = cases_class.Cases()	
				
if len(sys.argv) > 1:			
	test_cases = test_cases.get(sys.argv[1])
	if len(sys.argv) > 2:
		if 'regress' in sys.argv[2]:
			regress = True
		if 'mean' in sys.argv[2]:
			mean = True
		if 'retrieved' in sys.argv[2]:
			retrieved = True
		if 'error' in sys.argv[2]:
			error = True
	if len(sys.argv) > 3:
		if 'lifetime' in sys.argv[3]:
			lifetime = True
		if 'Twomey' in sys.argv[3]:
			Twomey = True
sys.argv[1] = sys.argv[1].replace(' ', '')

print mean, retrieved, error, regress, lifetime, Twomey
if not mean and not retrieved and not error and not regress:
	print 'Must choose one'
	sys.exit()
plot_map(test_cases, mean, retrieved, error, regress, lifetime, Twomey)
