#!/usr/bin/env python
import calc_region_regression
import cases_class

cases_c = cases_class.Cases()
world = cases_c.Globe
slope, num_pts = calc_region_regression.get_x_ys('AI', '12_CSWFTOA', world, 'touching_day')
