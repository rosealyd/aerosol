#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import get_cmap

#Given the output from 10 by 10, plot the map as filled, smoothed contours
def plot_contour(xs, ys, color, min_c, max_c, cmap, midpoints = False):
	if not midpoints:
		xs = [(a+b)/2 for a,b in zip(xs[:-1], xs[1:])]
		ys = [(a+b)/2 for a,b in zip(ys[:-1], ys[1:])]
	X,Y = np.meshgrid(xs,ys)
	plt.imshow(np.rot90(color), interpolation='Gaussian', extent = (min(xs),max(xs),min(ys),max(ys)), aspect = 'auto', cmap = cmap, vmin = min_c, vmax = max_c)
	plt.colorbar()
	plt.show()
	return

