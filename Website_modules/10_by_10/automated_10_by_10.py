#!/usr/bin/env python
from collections import defaultdict
import get_user_input_10_by_10
import sort_data_10_by_10
import split_10_by_10
 

def auto_decompose_sw(use_hard, MVLR, ITCZ, only_lifetime, Twomey_ratio, only_Twomey, show_part, to_find, split, ratio, region, split_var, two_term, show_retrieved, touching, rain_flag, take_log, lifetime_ratio, lat_lons, ROOT_DIR =  '/home/adouglas2/Summer/aerosol/test_Globe/', scale = '12', dependent_variable = 'cloud_albedo', num_bins = 5, x_var = 'eis4', y_var = 'rh', radiation_variable = 'CSWFTOA', x_title = 'EIS', y_title = 'RH', been_through = False):
	if use_hard:
		num_bins = 10
	region_fake = defaultdict(dict)
	region_fake = {'use_hard': use_hard, 'MVLR': MVLR, 'ITCZ': ITCZ, 'only lifetime': only_lifetime, 'scale' : scale, 'Twomey ratio' : Twomey_ratio, 'only Twomey': only_Twomey, 'show part' : show_part, 'been through' : been_through, 'to_find': to_find, 'split': split, 'dependent variable': dependent_variable, 'num_bins': num_bins, 'x_var': x_var, 'radiation variable': radiation_variable, 'x var title':x_title, 'y var title': y_title, 'ratio' : ratio, 'region' : region, 'split_var' : split_var, 'two term': two_term, 'show_retrieved' : show_retrieved, 'touching': touching, 'rain flag': rain_flag, 'take log': take_log, 'y_var': y_var, 'lifetime ratio': lifetime_ratio}
	needs = [scale + '_cloud_fraction', 'aodidx', scale + '_cloud_albedo', scale + '_CSWFTOA', scale + '_CF_CSWFTOA']
	region_fake = get_user_input_10_by_10.get_var(needs,[lat_lons], ROOT_DIR, region_fake, x_var, y_var, split_var, touching)
	temp_lists = []
	#Now if sorting by rain, lwp, or not sorting progression
	if region_fake['split']:
		if 'all' not in region_fake['rain flag']:
			reg_list = split_10_by_10.both(region_fake)
		elif 'all' in regions_dict['rain flag']:
			reg_list = split_10_by_10.split_by_lwpidx(region_fake)
		for lwp, state in zip(reg_list, ['low', 'med', 'high']):
			temp = sort_data_10_by_10.start_sort(lwp)
			temp_lists.append((state, temp))
		return temp_lists
	elif 'all' not in region_fake['rain flag'] and not region_fake['split']:
		region_fake = split_10_by_10.split_by_raining(region_fake)
		region_fake = sort_data_10_by_10.start_sort(region_fake)
		return region_fake
	else:
		region_fake = sort_data_10_by_10.start_sort(region_fake)
		return region_fake

#case = '-15_-15'
#print auto_decompose_sw(False, False, False, False, False, True, False, 'Tristan', False, False, str(case), 'lwp', True, True, 'touching_day', 'all', True, False, case)
