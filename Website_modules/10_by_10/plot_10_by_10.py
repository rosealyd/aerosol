#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as clrs
import get_cmap
import subprocess
import sys
from pylab import rcParams
import plot_contourf

def set_midpoint(cmin, cmax, midpoint, offset = None):
	size = max(abs(midpoint - cmin), abs(midpoint - cmax))
	if not offset:
		print 'not offset'
		return midpoint - size, midpoint + size
	else:
		return (midpoint - size) * offset, (midpoint+size) * offset
	
def create_colorbar(cmap, min_c, max_c):
	levels = np.linspace(min_c, max_c, 100)
	useless_plot = plt.contourf([[0,0],[0,0]], levels, cmap = cmap)
	bar = plt.colorbar(useless_plot, fraction = .09, ticks = [round(t, 3) for t in list(np.linspace(min_c, max_c, 7))] )
	return bar

		
def plot(region, lwp_perc = None):
	touching_status = region['touching']
	rcParams['figure.figsize'] = 11.5, 8 #width, height
	rcParams['xtick.major.pad']='10'
	rcParams['ytick.major.pad']='10'
	
	min_c = min([p for o in region['plotting list'] for p in o])
	max_c = max([p for o in region['plotting list'] for p in o])
	
	num_bins = region['num_bins']
	percentiles = np.linspace(0,100,num_bins+1)
	width = 100/num_bins
	print min_c, max_c
	if min_c < 0 and max_c > 0:
		min_c, max_c, = set_midpoint(min_c, max_c, 0)
		cmap = get_cmap.get_cmap()
	if min_c >= 0 and max_c > 0:
		cmap = cm.get_cmap('Reds')
	if min_c < 0 and max_c <= 0:
		cmap = cm.get_cmap('Blues_r')
	print region['var_name']
	if '12_cloud_fraction' in region['var_name']:
		print min_c
		min_c = min([p for o in region['plotting list'] for p in o if p != 0.0])
		print min_c
	print cmap
	norm = clrs.Normalize(vmin = min_c, vmax = max_c)
	color = cm.ScalarMappable(norm = norm, cmap = cmap)
	levels = np.linspace(min_c, max_c, 100)
	useless_plot = plt.contourf([[0,0],[0,0]], levels, cmap = cmap)
	bar = plt.colorbar(useless_plot, fraction = .09, ticks = [round(t, 3) for t in list(np.linspace(min_c, max_c, 7))] )
	bar.ax.tick_params(labelsize=16) 
	
	for i in range(len(region['plotting list'])):
		for j in range(len(region['plotting list'][i])):
			if region['plotting list'][i][j] == 0.0:
				c = (.5, .5, .5, .5)
			else:
				c = color.to_rgba(region['plotting list'][i][j])
			if j > 0:
				plt.bar(percentiles[i], width, color = c, width = width, bottom = percentiles[j])
			else:
				plt.bar(percentiles[i], width, color = c, width = width)
	plt.xlabel(region['x var title'], size = 26)
	plt.ylabel(region['y var title'], size = 26)
	
	
	plt.xticks(percentiles, [round(t,1) for t in region['x_bin_limits']], size = 20)
	plt.yticks(percentiles, [round(t,1) for t in region['y_bin_limits']], size = 20)
	folder = region['region'] + "/"
	if 'Tristan' in region['to_find']:
		print 'Tristan add ons for plot'
		if not region['Twomey ratio'] and not region['ratio'] and not region['show part']:
			ret_err_range = region['retrieved'] - region['retrieved low']
			calc_err_range = region['calculated'] - region['calc low']
			standard_error = ( region['retrieved'] - region['calculated'] )/ region['retrieved']
			region['standard error'] = standard_error
			plt.title('Calc: {}  Err range: {}  Standard Err:{}\nRet: {} Err range: {}'.format(round(region['calculated'], 2), round(calc_err_range,2), round(region['standard error'], 2), round(region['retrieved'], 2), round(ret_err_range, 2)), size = 25)
		
		if region['use_hard']:
			limits = 'hard_{}_'.format(region['num_bins'])
		else:
			limits = ''
		if 'all' in region['rain flag']:
			rain = ''
		else:
			rain = region['rain flag'] + '_'
	
		if region['split']:
			split = 'split_by' + region['split_var'] + "_" + lwp_perc + '_'
		else:
			split = ''
		if region['region'] != 'ITCZ' and not region['ITCZ']:
			itcz = 'without_itcz_'
		else:
			itcz = ''
		if region['take log']:
			ai = 'ln_AI_'
		else:
			ai = ""
		if region['two term']:
			Twomey = 'two_term_'
		else:
			Twomey = 'three_term_'
		
		plotted = ''
		if region['only Twomey']:
			plotted = 'only_Twomey_'
		if region['only lifetime']:
			plotted = 'only_lifetime_'
		if region['Twomey ratio']:
			plotted = 'Twomey_ratio_'
		if region['lifetime ratio']:
			plotted = 'lifetime_ratio_'
	
		if region['show_retrieved'] and region['been through']:
			showing = 'retrieved_'
			if region['difference']:
				showing = 'differenced_'
		else:
			showing = 'calculated_'
		showing = showing + '_' + touching_status + '_'
		if '80' in region['ROOT DIR']:
			save_name = '/home/adouglas2/Summer/aerosol/Website_modules/10_by_10_figures/80_percentile/'+folder + "{}_{}_{}_vs_{}_{}{}{}{}{}{}{}{}{}.png".format(region['aerosol'], Twomey, region['x_var'],region['y_var'], plotted, showing, ai, region['scale'] + "_",limits, rain, split, itcz, region['min r'])
		elif '90' in region['ROOT DIR']:
			save_name = '/home/adouglas2/Summer/aerosol/Website_modules/10_by_10_figures/90_percentile/'+folder + "{}_{}_{}_vs_{}_{}{}{}{}{}{}{}{}{}.png".format(region['aerosol'], Twomey, region['x_var'],region['y_var'], plotted, showing, ai, region['scale'] + "_",limits, rain, split, itcz, region['min r'])
		else:
			save_name = '/home/adouglas2/Summer/aerosol/Website_modules/10_by_10_figures/100_percentile/'+folder + "{}_{}_{}_vs_{}_{}{}{}{}{}{}{}{}{}.png".format(region['aerosol'], Twomey, region['x_var'],region['y_var'], plotted, showing, ai, region['scale'] + "_",limits, rain, split, itcz, region['min r'])
	else:
		save_name =  '/home/adouglas2/Summer/aerosol/Website_modules/10_by_10_figures/' + folder + '{}_{}_vs_{}_{}_{}.png'.format(region['aerosol'], region['x_var'], region['y_var'], region['to_find'], region['var_name'])

	plt.savefig(save_name)
	if region['show_retrieved'] and not region['been through']:
		print 'c:',region['calculated'], 'r:', region['retrieved']
	plt.clf()
	call_name = 'eog ' + save_name + ' &'
	subprocess.call(call_name.split(' '))
	print call_name
	#print region['plotting list']
	#print list(region['x_bin_limits'])
	#print list(region['y_bin_limits'])
	#plot_contourf.plot_contour(region['x_bin_limits'], region['y_bin_limits'], region['plotting list'], min_c, max_c, cmap)
	return
