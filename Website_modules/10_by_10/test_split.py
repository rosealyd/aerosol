#!/usr/bin/env python
import numpy as np
import random

variable1 = random.sample(range(100),100)
variable2 = random.sample(range(100),100)
variable3 = random.sample(range(100),100)

variable2_percentiles = np.percentile(variable2, [0,50,100], interpolation = 'midpoint')
variable3_percentiles = np.percentile(variable3, [0,50,100], interpolation = 'midpoint')

binned_variable1_list = [ [ [] for j in range(2)] for i in range(2)]

for v1, v2, v3 in zip(variable1, variable2, variable3):
	for i,(low_v2, high_v2) in enumerate(zip(variable2_percentiles[:-1], variable2_percentiles[1:])):
		for j,(low_v3, high_v3) in enumerate(zip(variable3_percentiles[:-1], variable3_percentiles[1:])):
			if v2 >= low_v2 and v2 < high_v2:
				if v3 >= low_v3 and v3 < high_v3:
					binned_variable1_list[i][j].append(v1)
for row in binned_variable1_list:
	print row
	print len(row[0]), len(row[1]) #These aren't equal.  I want the len of these lists to be equal
