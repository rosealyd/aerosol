#!/usr/bin/env python
import matplotlib.pyplot as plt

def get_cmap():    
	cdict3 = {'red':  ((0.0, 0.0, 0.0),
                   (0.5, 1.0, 1.0),
                   (1.0, 1.0, 1.0)),

         'green': ((0.0, 0.0, 0.0),
                   (0.5, 1.0, 1.0),
                   (1.0, 0.0, 0.0)),

         'blue':  ((0.0, 1.0, 1.0),
                   (0.5, 1.0, 1.0),
                   (1.0, 0.0, 0.0))
        }
	plt.register_cmap(name = 'BlueRed', data = cdict3)
	cmap = plt.get_cmap('BlueRed')
	return cmap
