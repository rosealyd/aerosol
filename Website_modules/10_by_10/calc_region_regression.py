#!/usr/bin/env python
import json
import numpy as np
from scipy.stats import linregress
import math
import matplotlib.pyplot as plt
import plot_scatter
import time
import sys
sys.path.append('/home/adouglas2/Summer/aerosol/')
import get_bin_limits



ROOT_DIR = '/home/adouglas2/Summer/aerosol/Globe/80_percent/'

def bin_aodidx(xs, ys):
	print 'Binning aodidx'
	bins = [[] for i in list(set(xs))]
	xs_points = list(set(xs))
	for x, y in zip(xs, ys):
		bins[xs_points.index(x)].append(y)
	for i, bin in enumerate(bins):
		bins[i] = np.mean(bin)
	return xs_points, bins
	
def bin(xs, ys):
	print 'Binning, not aodidx'
	bins = [[] for i in range(9)]
	xs_points = np.percentile(xs, np.linspace(0,100,10), interpolation = 'midpoint')
	for i,( x_low, x_high) in enumerate(zip(xs_points[:-1], xs_points[1:])):
		within = [j for j,x in enumerate(xs) if x <= x_high and x >= x_low]
		bins[i] = list(np.array(ys)[within])
	for i, bin in enumerate(bins):
		bins[i] = np.mean(bin)
	xs_points = [(a+b)/2 for a, b in zip(xs_points[:-1], xs_points[1:])]
	
	return xs_points, bins
	
def validate(xs, ys):
	print 'Validating'
	good = [i for i,(x,y) in enumerate(zip(xs, ys)) if type(x) == float and type(y) == float]
	xs = list(np.array(xs)[good])
	ys = list(np.array(ys)[good])
	return xs, ys

def get_x_ys(x_var, y_var, cases, touching_status):
	xs = []
	ys = []
	for case in cases:
		print case
		filename = ROOT_DIR + case + '.txt'
		with open(filename, 'r') as case_file:
			box = json.load(case_file)
			
			xs.extend(box['noerror'][touching_status]['MODISAI'][x_var])
			ys.extend(box['noerror'][touching_status]['MODISAI'][y_var])
	if 'aodidx' in x_var or 'AI' in x_var:
		xs = [x if x == 0.0 else math.log(x) for i, x in enumerate(xs)]
	print min(xs)
	print xs.count(min(xs))

	return xs, ys

def regress(cases, x_var, y_var, touching_status):
	xs, ys = get_x_ys(x_var, y_var, cases, touching_status)
	xs, ys = validate(xs, ys)
	if x_var in 'aodidx':
		xs, ys = bin_aodidx(xs, ys)
	else:
		xs, ys = bin(xs, ys)
	#plot_scatter.plot_density(xs, ys)
	num_points = len(xs)
	slope = linregress(xs, ys)[0]
	if np.isnan(slope):
		print cases, 'is nan'
		print len(xs), len(ys)
	intercept = linregress(xs, ys)[1]
	rsquare = linregress(xs, ys)[2]
	plt.scatter(xs, ys)
	plt.plot(xs, [intercept + (slope * x) for x in xs])
	plt.title(x_var + ' vs ' + y_var + '\nslope: {}, r2: {}     {}'.format(slope, rsquare, cases))
	#if rsquare > .5:
	#	plt.show()
	#plot_scatter.plot_density_scatter(xs, ys, fit_line = False)
	plt.show()
	return slope, num_points, rsquare
	
'''
test = ['0_0', '0_-15', '0_-30', '-15_0', '-15_-15', '-15_-30', '-30_0', '-30_-15', '-30_-30']
for case in test:
	regress([case], '48_cloud_albedo', '48_CSWFTOA', 'touching_day')
	regress([case], 'aodidx', '48_cloud_albedo', 'touching_day')
'''
