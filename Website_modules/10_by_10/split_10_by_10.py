#!/usr/bin/env python
from collections import defaultdict
import numpy as np
import copy
import sys
import time

def split_by_lwpidx(region):
	print len(region['l'])
	temp_region = copy.deepcopy(region)
	print "Splitting by liquid water path"
	
	scale = region['scale']
	rad_var = region['radiation variable']
	dep_var = region['dependent variable']
	#Create new lists for this new region which will contain one third of the lwps and be passed as region for plotting purposes
	for key in ['x', 'y', 'l','{}_cloud_fraction'.format(scale), 'AI', '{}_{}'.format(scale, rad_var), '{}_{}'.format(scale,dep_var), '{}_CF_{}'.format(scale, rad_var), 'r']:
		temp_region[key] = []
		
	
	lwp_bin_limits = np.percentile(region['l'],[0.0, 33.0, 66.0, 100.0], interpolation = 'midpoint')
	if 'AI' in region['split_var']:
		lwp_bin_limits = np.percentile(region['l'],[0.,33.,66.,100.], interpolation = 'midpoint')
	if 'eis4' in region['split_var']:
		lwp_bin_limits =  np.percentile(region['l'],[0.,40.,60.,100.], interpolation = 'midpoint')
		print '0, 40, 60, 100'

		
	lwps = [copy.deepcopy(temp_region), copy.deepcopy(temp_region), copy.deepcopy(temp_region)]

	for x, y, l, r, cf, ai, f, dv, cf_f in zip(region['x'],region['y'], region['l'], region['r'], region[scale + '_cloud_fraction'], region['AI'], region[scale + '_' + rad_var], region[scale + '_'+dep_var], region[scale + '_CF_' + rad_var]):
		
		for i, (low_lim, high_lim) in enumerate(zip(lwp_bin_limits[:-1], lwp_bin_limits[1:])):
			if l >= low_lim and l < high_lim:
				lwps[i]['x'].append(x)
				lwps[i]['y'].append(y)
				lwps[i]['r'].append(r)
				lwps[i][scale + '_cloud_fraction'].append(cf)
				lwps[i]['AI'].append(ai)
				lwps[i][scale + '_'+rad_var].append(f)
				lwps[i][scale + '_CF_' + rad_var].append(cf_f)
				lwps[i][scale + '_'+dep_var].append(dv)
				lwps[i]['l'].append(l)


	print len(lwps[0]['x']), len(lwps[1]['x']), len(lwps[-1]['x'])
	return lwps
	
def split_by_raining(region):
	print "Splitting by " + region['rain flag']
	temp_region = copy.deepcopy(region)
	scale = region['scale']
	rad_var = region['radiation variable']
	dep_var = region['dependent variable']
	#Make a new key in the temp dict
	for key in ['x', 'y', '{}_cloud_fraction'.format(scale), 'AI', '{}_{}'.format(scale, rad_var), '{}_{}'.format(scale, dep_var),'{}_CF_{}'.format(scale, rad_var), 'r', 'l']:
			temp_region[key] = []
			
	for x, y, r, l, cf, ai, f, dv, cf_f in zip(region['x'], region['y'], region['r'], region['l'], region[scale + '_cloud_fraction'], region['AI'], region[scale + '_'+rad_var], region[scale + '_'+ dep_var], region[scale + '_CF_' + rad_var]):
		if region['rain flag'] == 'raining':
			if r > 0:
				temp_region['x'].append(x)
				temp_region['y'].append(y)
				temp_region[scale+'_cloud_fraction'].append(cf)
				temp_region['AI'].append(ai)
				temp_region[scale+'_'+rad_var].append(swf)
				temp_region[scale+'_'+dep_var].append(dv)
				temp_region[scale + '_CF_' + rad_var].append(cf_f)
				temp_region['r'].append(r)
				temp_region['l'].append(l)
		if region['rain flag'] == 'nonraining':
			if r < 1:
				temp_region['x'].append(x)
				temp_region['y'].append(y)
				temp_region[scale+'_cloud_fraction'].append(cf)
				temp_region['AI'].append(ai)
				temp_region[scale+'_'+rad_var].append(swf)
				temp_region[scale+'_'+dep_var].append(dv)
				temp_region[scale + '_CF_' + rad_var].append(cf_f)
				temp_region['r'].append(r)
				temp_region['l'].append(l)

	return temp_region
	
def both(region):
	temp_reg = split_by_raining(region)
	print len(temp_reg['l'])
	print np.percentile(temp_reg['l'], [0,33,66,100], interpolation = 'midpoint')
	new = split_by_lwpidx(temp_reg)
	print 'should be done', len(new[0]['x']), len(new[1]['x']), len(new[2]['x'])
	return new
