#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
from pylab import rcParams
from scipy.stats import gaussian_kde
from matplotlib.colors import LogNorm
from scipy.stats import linregress



def plot_density_scatter(xs, ys, fit_line = True, x_var_name = '', y_var_name = '', region = ''):
	ROOT_DIR = '/home/adouglas2/Summer/aerosol/Website_modules/10_by_10_figures/'
	print 'creating density scatter plot'
	#To fit all other plots
	rcParams['figure.figsize'] = 11.5, 8 #width, height
	rcParams['xtick.major.pad']='10'
	rcParams['ytick.major.pad']='10'
	plt.ylim(ymin = 0)
	#Density scatter plot
	xy = np.vstack([xs, ys])
	z = gaussian_kde(xy)(xy)
	plt.scatter(xs, ys, c = z, s = 10, edgecolor = '')
	#line of best fit
	regress = linregress(xs, ys)
	if fit_line:
		print 'adding line of best fit'
		plt.plot(xs, [(x*regress[0]) + regress[1] for x in xs], 'r', linewidth = 7)
	plt.xlabel(x_var_name, size = 20)
	plt.ylabel(y_var_name, size = 20)
	plt.title(round(regress[2]**2, 2), size = 20)
	plt.xlim(min(xs), max(xs))
	plt.ylim(min(ys), max(ys))
	print ROOT_DIR + region + '/scatter_plot_{}_vs_{}'.format(x_var_name, y_var_name)
	plt.savefig(ROOT_DIR + region + '/scatter_plot_{}_vs_{}.png'.format(x_var_name, y_var_name))
	plt.show()
	return
	
def plot_density(xs, ys):
	heatmap, xedges, yedges = np.histogram2d(xs, ys, bins = (64, 64))
	extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
	plt.imshow(heatmap, interpolation = 'gaussian')
	plt.show()
	return
	
