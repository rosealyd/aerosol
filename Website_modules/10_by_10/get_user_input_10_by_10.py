#!/usr/bin/env python

from scipy.stats import linregress
import matplotlib.pyplot as plt
from collections import defaultdict
import json
import copy
import sys
import split_10_by_10
sys.path.insert(0, '/home/adouglas2/Summer/aerosol/Website_modules/')
import Get_user_input
import Get_directions
import numpy as np
from pylab import rcParams
import math
import plot_scatter
import cases_class as cc

#Toggle tools
all_variables = [u'36_CLR_OLR', u'48_CLWFTOA', u'96_CLR_OLR', u'12_CF_CLWFTOA', u'12_albedo', u'24_CLWFTOA', u'24_CSWFSFC', u'MODIS_CTT', u'geoidx', u'MODIS_CTP', u'cwv', u'modiscounter', u'36_clear_sky_albedo', u'96_net', u'96_cloud_fraction', u'ec_sst', u'96_counter', u'48_CLWFSFC', u'36_daycounter', u'24_cloud_albedo', u'36_CLWFSFC', u'24_CF_CSWFTOA', u'48_daycounter', u'cerescounter', u'96_daycounter', u'96_CERES_albedo', u'eis4', u'eis3', u'24_CF_CLWFTOA', u'24_CSWFTOA', u'96_cspop', u'12_CLWFTOA', u'36_OLR', u'12_OLR', u'36_cloud_albedo', u'rh', u'merra_omega', u'48_cloud_fraction', u'12_net', u'48_cspop', u'fl', u'12_CSWFTOA', u'96_CSWFTOA', u'12_CERES_albedo', u'48_counter', u'48_pop', u'amsrrridx', u'24_cspop', u'CLR_OLR', u'96_OLR', u'12_cld_top', u'12_CF_CSWFTOA', u'cld_top', u'96_CF_CLWFTOA', u'MODIS_5km_CF', u'12_CLWFSFC', u'48_clear_sky_albedo', u'96_CSWFSFC', u'12_counter', u'96_pop', u'12_daycounter', u'net', u'rridx', u'48_CSWFSFC', u'MODIS_LWP', u'CSWFSFC', u'48_CERES_albedo', u'36_cerescounter', u'24_CLWFSFC', u'96_cloud_albedo', u'CLWFTOA', u'12_cloud_albedo', u'12_clear_sky_albedo', u'24_cloud_fraction', u'36_albedo', u'96_SFC_emiss', u'12_cspop', u'12_cerescounter', u'24_pop', u'48_CF_CLWFTOA', u'CLWFSFC', u'CSWFTOA', u'24_CLR_OLR', u'36_CSWFSFC', u'36_CLWFTOA', u'24_CERES_albedo', u'36_CERES_albedo', u'48_cloud_albedo', u'48_CLR_OLR', u'12_CSWFSFC', u'aodidx', u'48_cld_top', u'96_cld_top', u'OLR', u'24_OLR', u'36_net', u'daycounter', u'96_CLWFSFC', u'lwpidx', u'MODIS_Re', u'merra_ltss', u'24_daycounter', u'36_SFC_emiss', u'amsrr_pop', u'48_CSWFTOA', u'12_pop', u'12_SFC_emiss', u'CERES_albedo', u'24_counter', u'12_CLR_OLR', u'ec_ltss', u'36_CF_CLWFTOA', u'SFC_emiss', u'36_cspop', u'merra_sst', u'amsr_sst', u'cld_thick', u'pia', u'96_albedo', u'24_net', u'36_pop', u'species', u'MODIS_COT', u'36_CF_CSWFTOA', u'sfc_wind', u'24_clear_sky_albedo', u'48_cerescounter', u'48_albedo', u'96_CF_CSWFTOA', u'48_SFC_emiss', u'36_cloud_fraction', u'24_cerescounter', u'96_clear_sky_albedo', u'96_cerescounter', u'12_cloud_fraction', u'AI', u'24_albedo', u'CloudSat_pop', u'36_counter', u'96_CLWFTOA', u'48_CF_CSWFTOA', u'24_SFC_emiss', u'counter', u'24_cld_top', u'48_net', u'36_CSWFTOA', u'36_cld_top', u'48_OLR', 'lwp', 'merra_evap', '12_cld_ceres', '24_cld_ceres', '36_cld_ceres', '48_cld_ceres', '96_cld_ceres']

title_variables = [u'36 KM CLEAR OLR', u'48 KM CLWFTOA', u'96 KM CLEAR OLR', u'12 KM CLOUD FRACTION CLWFTOA', u'12 KM ALBEDO', u'24 KM CLWFTOA', u'24 KM CSWFSFC', u'MODIS CTT', u'GEOIDX', u'MODIS CTP', u'CWV', u'MODISCOUNTER', u'36 KM CLEAR SKY ALBEDO', u'96 KM NET', u'96 KM CLOUD FRACTION', u'EC SST', u'96 KM COUNTER', u'48 KM CLWFSFC', u'36 KM DAYCOUNTER', u'24 KM CLOUD ALBEDO', u'36 KM CLWFSFC', u'24 KM CLOUD FRACTION CSWFTOA', u'48 KM DAYCOUNTER', u'CERESCOUNTER', u'96 KM DAYCOUNTER', u'96 CERES ALBEDO', u'EIS', u'EIS', u'24 CLOUD FRACTION KM CLWFTOA', u'24 KM CSWFTOA', u'96 KM CLOUDSAT POP', u'12 KM CLWFTOA', u'36 KM OLR', u'12 KM OLR', u'36 KM CLOUD ALBEDO', u'RH', u'MERRA OMEGA', u'48 KM CLOUD FRACTION', u'12 KM NET', u'48 KM CLOUDSAT POP', u'FL', u'12 KM CSWFTOA', u'96 KM CSWFTOA', u'12 KM CERES ALBEDO', u'48 KM COUNTER', u'48 KM POP', u'AMSRRRIDX', u'24 KM CLOUDSAT POP', u'CLEAR OLR', u'96 KM OLR', u'12 KM CLOUD TOP', u'12 KM CLOUD FRACTION CSWFTOA', u'CLD TOP', u'96 CLOUD FRACTION CLWFTOA', u'MODIS 5KM CLOUD FRACTION', u'12 KM CLWFSFC', u'48 CLEAR SKY ALBEDO', u'96 CSWFSFC', u'12 KM COUNTER', u'96 POP', u'12 KM DAYCOUNTER', u'NET', u'RRIDX', u'48 CSWFSFC', u'MODIS LWP', u'CSWFSFC', u'48 CERES ALBEDO', u'36 KM CERESCOUNTER', u'24 KM CLWFSFC', u'96 CLOUD ALBEDO', u'CLWFTOA', u'12 KM CLOUD ALBEDO', u'12 KM CLEAR SKY ALBEDO', u'24 KM CLOUD FRACTION', u'36 KM ALBEDO', u'96 SFC EMISS', u'12 KM CLOUDSAT POP', u'12 KM CERESCOUNTER', u'24 KM POP', u'48 CLOUD FRACTION CLWFTOA', u'CLWFSFC', u'CSWFTOA', u'24 KM CLEAR OLR', u'36 KM CSWFSFC', u'36 KM CLWFTOA', u'24 KM CERES ALBEDO', u'36 KM CERES ALBEDO', u'48 CLOUD ALBEDO', u'48 CLEAR OLR', u'12 KM CSWFSFC', u'AODIDX', u'48 CLD TOP', u'96 CLD TOP', u'OLR', u'24 KM OLR', u'36 KM NET', u'DAYCOUNTER', u'96 CLWFSFC', u'LWPIDX', u'MODIS RE', u'MERRA LTSS', u'24 KM DAYCOUNTER', u'36 KM SFC EMISS', u'AMSRR POP', u'48 CSWFTOA', u'12 KM POP', u'12 KM SFC EMISS', u'CERES ALBEDO', u'24 KM COUNTER', u'12 KM CLEAR OLR', u'EC LTSS', u'36 KM CLOUD FRACTION CLWFTOA', u'SFC EMISS', u'36 KM CLOUDSAT POP', u'MERRA SST', u'AMSR SST', u'CLD THICK', u'PIA', u'96 ALBEDO', u'24 KM NET', u'36 KM POP', u'SPECIES', u'MODIS COT', u'36 KM CLOUD FRACTION CSWFTOA', u'SFC WIND', u'24 KM CLEAR SKY ALBEDO', u'48 CERESCOUNTER', u'48 ALBEDO', u'96 CLOUD FRACTION CSWFTOA', u'48 SFC EMISS', u'36 KM CLOUD FRACTION', u'24 KM CERESCOUNTER', u'96 CLEAR SKY ALBEDO', u'96 CERESCOUNTER', u'12 KM CLOUD FRACTION', u'AI', u'24 KM ALBEDO', u'CLOUDSAT POP', u'36 KM COUNTER', u'96 CLWFTOA', u'48 CLOUD FRACTION CSWFTOA', u'24 KM SFC EMISS', u'COUNTER', u'24 KM CLD TOP', u'48 NET', u'36 KM CSWFTOA', u'36 KM CLD TOP', u'48 OLR', 'LWP', 'MERRA EVAP', '12 CERES SW CRE', '24 CERES SW CRE', '36 CERES SW CRE', '48 CERES SW CRE', '96 CERES SW CRE']

aerosols = [u'CALIPSOAOD', u'CALIPSOCC', u'CALIPSOCM', u'CALIPSODU', u'CALIPSOPC', u'CALIPSOPD', u'CALIPSOSM', u'GEMSAOD', u'GEMSBC', u'GEMSDU', u'GEMSOC', u'GEMSSA', u'GEMSSU', u'MODISAI', u'MODISAOD', u'SPRINTARSAOD', u'SPRINTARSBC', u'SPRINTARSDU', u'SPRINTARSOC', u'SPRINTARSSA', u'SPRINTARSSU']

temp = None

def ask_question(question, options):
	for opt in options:
		print opt
	user_input = raw_input(question+"\n")
	Get_user_input.check(options, user_input)
	print "\n"
	return user_input


def validate_data(region):
	print "Validating data"
	scale = region['scale']
	temp_region = copy.deepcopy(region)
	rad_var = region['radiation variable']
	dep_var = region['dependent variable']
	print len(region['x'])
	if region['to_find'] == 'Tristan' or region['to_find'] == 'Newtonian' or region['to_find'] == 'longwave':
		print 'here at line 42'
		for key in ['x', 'y', '{}_cloud_fraction'.format(scale), 'AI', '{}_{}'.format(scale, rad_var),'{}_CF_{}'.format(scale, rad_var), '{}_{}'.format(scale, dep_var), 'r', 'l']:
			temp_region[key] = []
		
		for x, y, r , l, cf, ai, dv, f, cf_f in zip(region['x'], region['y'], region['r'], region['l'], region[scale + '_cloud_fraction'], region['AI'], region[scale + '_' + dep_var], region[scale + '_' + rad_var], region[scale + '_CF_' + rad_var]):
		####l < .7 only for LWPIDX
			if type(x) == float and type(y) == float and type(r) == float and type(cf) == float and type(l) == float and type(ai) == float and type(dv) == float and type(f) == float and type(cf_f) == float and l <= .7:
				if region['take log']:
					ai = math.log(ai)
				temp_region['x'].append(x)
				temp_region['y'].append(y)
				temp_region['r'].append(r)
				temp_region['l'].append(l)
				temp_region['{}_cloud_fraction'.format(scale)].append(cf)
				temp_region['AI'].append(ai)
				temp_region[scale + '_' + dep_var].append(dv)
				temp_region[scale + '_' + rad_var].append(f)
				temp_region[scale + '_CF_' + rad_var].append(cf_f)
		

				
	else:
		var_name = region['var_name']
		for key in ['x', 'y', var_name, 'AI']:
			temp_region[key] = []
		for x, y, v, ai in zip(region['x'], region['y'], region[var_name], region['AI']):
			if type(x) == float and type(y) == float and type(v) == float and type(ai) == float:
				temp_region['x'].append(x)
				temp_region['y'].append(y)
				temp_region[var_name].append(v)
				temp_region['AI'].append(ai)
	print len(temp_region['x'])
	return temp_region
		
	
	
def get_var(variables, cases, ROOT_DIR, region_dict, x_var, y_var, split_var, touching_st):
	aero = region_dict['aerosol']
	
	for var in variables:
		if 'aodidx' in var:
			region_dict['AI'] = []
		else:
			region_dict[var] = []
			
	region_dict['x'] = []
	region_dict['y'] = []
	region_dict['r'] = []
	region_dict['l'] = []

	for case in cases:
		print 'Getting', case
		filename = ROOT_DIR + case + '.txt'
		with open(filename, 'r') as source:
			result = json.load(source)
		for var in variables:
			if 'aod' in var:
				print 'using aodidx'
				region_dict['AI'].extend(result['noerror'][touching_st][aero][var])
			else:
				region_dict[var].extend(result['noerror'][touching_st][aero][var])
		region_dict['x'].extend(result['noerror'][touching_st][aero][x_var])
		region_dict['y'].extend(result['noerror'][touching_st][aero][y_var])
		region_dict['r'].extend(result['noerror'][touching_st][aero]['rridx'])
		region_dict['l'].extend(result['noerror'][touching_st][aero][split_var])
		

	region_dict = validate_data(region_dict)
	
	'''
	plot_scatter.plot_density_scatter(region_dict['x'], region_dict['y'], x_var_name = region_dict['x var title'], y_var_name = region_dict['y var title'], region = region_dict['region'])
	swfs = []
	albedos = []
	for cf, cswf in zip(region_dict['12_cloud_fraction'], region_dict['12_CSWFTOA']):
		try:
			swf = cswf/cf
		except ZeroDivisionError:
			swf = 0.0
		if swf > 1000:
			swf = 0.0
		swfs.append(swf)
		albedos.append(albedos)
	#var = '{}_cloud_fraction'.format(region_dict['scale'])
	rcParams['figure.figsize'] = 13, 8 #width, height
	plt.scatter(region_dict['12_cloud_albedo'], swfs, s = 30)
	plt.xlabel('12_cloud_abedo', size = 18)
	plt.ylabel('SWF', size = 18)
	plt.title('{}'.format(region_dict['region']), size = 22)
	x_locs, x_labels = plt.xticks()
	plt.xticks([l2 for l1, l2 in zip(x_locs[:-1], x_locs[1:])], size = 14)
	y_locs, y_labels = plt.yticks()
	plt.yticks([l2 for l1, l2 in zip(y_locs[:-1], y_locs[1:])], size = 14)
	plt.xlim(xmin = 0, xmax = 1.0)
	plt.ylim(ymin = 0)
	plt.show()
	sys.exit()'''
	return region_dict
	
def get_user_input(dictionary):
	#Finding the region to plot and making a dict to later append the variables to
	region_dict = defaultdict(dict)
	#As default will not plot the following
	region_dict['ratio'] = False
	region_dict['show_retrieved'] = False
	region_dict['been through'] = False
	region_dict['Twomey ratio'] = False
	region_dict['lifetime ratio'] = False
	region_dict['take log'] = False
	region_dict['only Twomey'] = False
	region_dict['only lifetime'] = False
	region_dict['ratio'] = False
	region_dict['show part'] = False
	
	ai_bin = ask_question('What AI range would you like to plot?', ['a) All', 'b) 90 Percent', 'c) 80 Percent'])
	if ai_bin == 'a':
		ROOT_DIR = '/home/adouglas2/Summer/aerosol/Globe/all_ai/'
	elif ai_bin == 'b':
		ROOT_DIR = '/home/adouglas2/Summer/aerosol/Globe/90_percent/'
	elif ai_bin == 'c':
		ROOT_DIR = '/home/adouglas2/Summer/aerosol/Globe/80_percent/'
	print ROOT_DIR
	region_dict['ROOT DIR'] = ROOT_DIR
	#Find the aerosol type to plot
	aerosol = Get_user_input.get_user_input('What variable would you like to look as your y variable?\n', aerosols, temp)[0]
	region_dict['aerosol'] = aerosol
	#Find the region to plot
	region = ask_question("Which region would you like to plot?", ["a) Angola Basin", "b) South Pacific", "c) ITCZ", 'd) Globe', 'e) AB and SP', 'f) Barbados'])
	cc_class = cc.Cases()
	region_dict['ITCZ'] = True
	if region == 'a':
		region_dict['region'] = 'Angola'
		cases = cc_class.Angola
	if region == 'b':
		region_dict['region'] = 'SouthPacific'
		cases = cc_class.SoutheastPacific
	if region == 'c':
		cases = cc_class.ITCZ
		region_dict['region'] = 'ITCZ'
		
	if region == 'd':
		cases = cc_class.Globe
		region_dict['region'] = 'Global'
	if region == 'e':
		region_dict['region'] = 'AB_SP'
		cases = cc_class.Angola.extend(cc_class.SoutheastPacific)
		
	if region == 'f':
		region_dict['region'] = 'Barbados'
		cases = cc_class.Barbados
	
	#Get touching status
	touching_stat = ask_question('Day or night?\n', ['a) Day', 'b) Night', 'c) All'])
	if touching_stat == 'a':
		touching_status = 'touching_day'
	if touching_stat == 'b':
		touching_status = 'touching_night'
	if touching_stat == 'c':
		touching_status = 'touching'
	region_dict['touching'] = touching_status
	#Run multivariate linear regression for each region?
	mvlr = ask_question('Run MVLR for each region?\n', ['a) Yes', 'b) No'])
	if mvlr == 'a':
		region_dict['MVLR'] = True
	if mvlr == 'b':
		region_dict['MVLR'] = False
		
	#Finding x axis variable
	x_var = Get_user_input.get_user_input('What variable would you like as your x variable?\n', all_variables, temp)[0]
	region_dict['x_var'] = x_var
	region_dict['x var title'] = title_variables[all_variables.index(x_var)]
	
	#Finding y axis variable
	y_var = Get_user_input.get_user_input('What variable would you like to look as your y variable?\n', all_variables, temp)[0]
	region_dict['y_var'] = y_var
	region_dict['y var title'] = title_variables[all_variables.index(y_var)]
	
	#Default number of bins, changed if using hard limits
	region_dict['num_bins'] = 5
	#Find if use hard limits
	if 'eis3' in x_var or 'eis4' in x_var and 'rh' in y_var:
		use_hard = ask_question("Do you want to use hard limits?", ['a) Yes', 'b) No'])
		if use_hard == 'a':
			#If using hard limits, set the use_hard to True for sorting purposes and plot name purposes and set the x, y limits here
			region_dict['use_hard'] = True
			region_dict['num_bins'] = 10
			region_dict['x_bin_limits'] = np.linspace(-2.5, 6.5, region_dict['num_bins']+1)
			region_dict['y_bin_limits'] = np.linspace(0,.85,region_dict['num_bins']+1)
		if use_hard == 'b':
			region_dict['use_hard'] = False
	else:
		region_dict['use_hard'] = False
	if 'merra_ltss' in x_var and 'rh' in y_var:
		use_hard = ask_question("Do you want to use hard limits?", ['a) Yes', 'b) No'])
		if use_hard == 'a':
			region_dict['use_hard'] = True
			region_dict['hard_limits'] = np.linspace(9,19,11)
		if use_hard == 'b':
			region_dict['use_hard'] = False
	
	#Find if splitting by a variable
	split = ask_question("Would you like to split by a variable?", ['a) Yes', 'b) No'])
	if split == 'a':
		region_dict['split'] = True
		split_var = Get_user_input.get_user_input("What variable would you like to split with?\n", all_variables, temp)
		region_dict['split_var'] = split_var[0]
	else:
		region_dict['split'] = False
		region_dict['split_var'] = 'lwp'
	
	#Finding what to plot
	to_plot = ask_question("What would you like to plot?", ["a) Calculated dCSW/dAI", "b) Retrieved dCSW/dAI", 'c) length', 'd) mean', 'e) slope', 'f) variation', 'g) Calculated dCLW/dAI'])
	#If a or b need to have a scale
	if to_plot == 'a' or to_plot == 'b' or to_plot == 'g':
		scale = ask_question("What scale would you like to look at?", ['a) 12', 'b) 24', 'c) 36', 'd) 48', 'e) 96'])
		scale = ['12', '24', '36', '48', '96'][ord(scale) - 97]
		region_dict['scale'] = scale
		if to_plot == 'a':
			ratio = ask_question("Would you like to look at the ratio?", ['a) Yes', 'b) No'])
			if ratio == 'a':
				region_dict['ratio'] = True
				what_ratio = ask_question("Which ratio would you like to look at?", ['a) Twomey', 'b) Lifetime'])
				if what_ratio == 'a':
					region_dict['Twomey ratio'] = True
				if what_ratio == 'b':
					region_dict['lifetime ratio'] = True
			#Can only plot only twomey/lifetime if not plotting ratio
			if ratio == 'b':
				what_plot = ask_question("Would you like to only look at the Twomey or lifetime?", ['a) No', 'b) Yes Twomey', 'c) Yes lifetime'])
				if what_plot == 'b':
					region_dict['only Twomey'] = True
					region_dict['show part'] = True
				if what_plot == 'c':
					region_dict['only lifetime'] = True	
					region_dict['show part'] = True
			two_term = ask_question("Would you like to use two or three terms in the Twomey effect?", ['a) Two', 'b) Three'])
			if two_term == 'a':
				region_dict['two term'] = True
			if two_term == 'b':
				region_dict['two term'] = False
			
		natural_log = ask_question("Would you like to take the natural log of AI?", ['a) Yes', 'b) No'])
		if natural_log == 'a':
			region_dict['take log'] = True
		
	
	
	#If a need to know how to calc it
	#Tuples in needs means you need to find the slope of the pair (x,y)
	if to_plot == 'a':
		calc_type = ask_question("How would you like to calculate dCSW/dAI?", ["a) Newtonian", "b) Tristan's method"])
		needs = [scale + "_cloud_fraction", "aodidx", scale + "_cloud_albedo", scale + "_CSWFTOA", scale + '_CF_CSWFTOA']
		region_dict['radiation variable'] = 'CSWFTOA'
		region_dict['dependent variable'] = 'cloud_albedo'
		if calc_type == 'a':
			region_dict['to_find'] = 'Newtonian'
		if calc_type == 'b':
			region_dict['to_find'] = 'Tristan'
			if not region_dict['ratio'] and not region_dict['show part']:
				showing = ask_question("Would you like to show retrieved after showing calculated?", ['a) Yes', 'b) No'])
				if showing == 'a':
					region_dict['show_retrieved'] = True
				
	if to_plot == "b":
		region_dict['to_find'] = 'retrieved'
		needs = [scale + '_CSWFTOA', 'aodidx']
	if to_plot == 'g':
		region_dict['to_find'] = 'longwave'
		needs = [scale + '_CF_CLWFTOA', scale + '_cloud_fraction', 'aodidx', scale + '_CLWFTOA', scale + '_cld_top']
		region_dict['radiation variable'] = 'CLWFTOA'
		region_dict['dependent variable'] = 'cld_top'
	#Use the get_user_input module for the long variable lists
	if to_plot != 'a' and to_plot != 'b' and to_plot != 'g':
		var = Get_user_input.get_user_input('What variable would you like to look at?\n', all_variables, temp)
		print var
		needs = var
		#TODO
		#Add in ability to take slope against a new variable not just AI
		needs.append('aodidx')
		if to_plot == 'c':
			region_dict['to_find'] = 'length'
		if to_plot == 'd':
			region_dict['to_find'] = 'mean'
		if to_plot == 'e':
			region_dict['to_find'] = 'slope'
		if to_plot == 'f':
			region_dict['to_find'] = 'sd'
		region_dict['var_name'] = str(var[0])
		print region_dict['var_name']
	#Find if sorting by raining or nonraining
	raining = ask_question("Would you like to look at only raining or nonraining?", ['a) Yes raining', 'b) Yes nonraining', 'c) No all'])
	if raining == 'a':
		region_dict['rain flag'] = 'raining'
	if raining == 'b':
		region_dict['rain flag'] = 'nonraining'
	if raining == 'c':
		region_dict['rain flag'] = 'all'
	
	#Get all data needed here
	region_dict = get_var(needs, cases, ROOT_DIR, region_dict, x_var, y_var, region_dict['split_var'], touching_status)
	
	
	return region_dict
