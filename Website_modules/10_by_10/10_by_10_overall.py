#!/usr/bin/env python
import get_user_input_10_by_10
import sort_data_10_by_10
from collections import defaultdict
import split_10_by_10
import plot_10_by_10
import numpy as np
import matplotlib.pyplot as plt
import copy

def take_difference(value1, value2):
	if value1 < 0 and value2 > 0:
		diff = value2 - value1
	else: 
		diff = value1 - value2
	return diff

def start():
	regions_dict = defaultdict(dict)
	regions_dict = get_user_input_10_by_10.get_user_input(regions_dict)

	if regions_dict['split']:
		if 'all' not in regions_dict['rain flag']:
			print 'Splitting by rainstate and lwp'
			reg_list = split_10_by_10.both(regions_dict)
		elif 'all' in regions_dict['rain flag']:
			print 'Splitting by lwp only'
			reg_list = split_10_by_10.split_by_lwpidx(regions_dict)
		for lwp, state in zip(reg_list, ['low', 'med', 'high']):
			print 'sorting and plotting'
			temp = sort_data_10_by_10.start_sort(lwp)
			print temp['been through'], 'should be false'
			plot_10_by_10.plot(temp, lwp_perc = state)
			if regions_dict['show_retrieved']:
				difference = copy.deepcopy(temp['plotting list'])
				for i,(c_list, r_list) in enumerate(zip(temp['plotting list'], temp['retrieved list'])):
					for j,(c, r) in enumerate(zip(c_list, r_list)):
						difference[i][j] = take_difference(c, r)
				temp['been through'] = True
				temp['plotting list'] = temp['retrieved list']
				plot_10_by_10.plot(temp, lwp_perc = state)
				print temp['been through'], 'should be true'
				temp['plotting list'] = difference
				temp['difference'] = True
				plot_10_by_10.plot(temp, lwp_perc = state)
				print 'Plotted difference'	
		
	elif 'all' not in regions_dict['rain flag'] and not regions_dict['split']:
		print 'Splitting by only rain state'
		regions_dict = split_10_by_10.split_by_raining(regions_dict)
		regions_dict = sort_data_10_by_10.start_sort(regions_dict)
		plot_10_by_10.plot(regions_dict)
		if regions_dict['show_retrieved']:
			difference = copy.deepcopy(regions_dict['plotting list'])
			for i,(c_list, r_list) in enumerate(zip(regions_dict['plotting list'], regions_dict['retrieved list'])):
				for j,(c, r) in enumerate(zip(c_list, r_list)):
					difference[i][j] = take_difference(c, r)
			regions_dict['been through'] = True
			regions_dict['plotting list'] = regions_dict['retrieved list']
			plot_10_by_10.plot(regions_dict)
			regions_dict['plotting list'] = difference
			regions_dict['difference'] = True
			plot_10_by_10.plot(regions_dict)
			print 'Plotted difference'	
		
	else:
		print 'Not splitting at all'
		regions_dict = sort_data_10_by_10.start_sort(regions_dict)
		plot_10_by_10.plot(regions_dict)
		if regions_dict['show_retrieved']:
			difference = copy.deepcopy(regions_dict['plotting list'])
			for i,(c_list, r_list) in enumerate(zip(regions_dict['plotting list'], regions_dict['retrieved list'])):
				for j,(c, r) in enumerate(zip(c_list, r_list)):
					difference[i][j] = take_difference(c, r)
			print 'Plotting retrieved'
			regions_dict['been through'] = True
			regions_dict['plotting list'] = regions_dict['retrieved list']
			plot_10_by_10.plot(regions_dict)
			regions_dict['difference'] = True
			regions_dict['plotting list'] = difference
			plot_10_by_10.plot(regions_dict)
			print 'Plotted difference'	
start()
