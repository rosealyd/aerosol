#!/usr/bin/env python
import copy
import pandas
import sys
# For statistics. Requires statsmodels 5.0 or more
from statsmodels.formula.api import ols
# Analysis of Variance (ANOVA) on linear models
from statsmodels.stats.anova import anova_lm
import numpy as np

def find_MVLR(dep, ind1, ind2, ind3, ind4, simulate = False):

	data = pandas.DataFrame({'x1': ind1, 'x2': ind2, 'x3': ind3, 'x4': ind4, 'y': dep})

	model = ols('y ~ x1 + x2 + x3', data).fit()
	print model.summary()
	if simulate:
		xs = {'x1' : ind1, 'x2': ind2, 'x3': ind3, 'x4': ind4}
		return simulate_y(model.params, xs, dep)
	return 0,0

def simulate_y(slopes, values, actual = None):
	ys = []
	for i, (value_1, value_2, value_3, value_4) in enumerate(zip(values['x1'], values['x2'], values['x3'], values['x4'])):
		y = value_1 * slopes['x1'] + value_2 * slopes['x2'] + value_3 * slopes['x3'] + value_4 * slopes['x4'] + slopes['Intercept']
		if actual:
			#print actual[i], y
			pass
		ys.append(y)
	return np.mean(ys), np.mean(actual)
