#!/usr/bin/env python
import get_user_input
import sort
import plot
import matplotlib.pyplot as plt
import subprocess

data_dict = get_user_input.get_input()
if data_dict['split']:
	list_of_dicts = sort.split_sort(data_dict)
	call_name = plot.split_plot(list_of_dicts)
	
else:
	data_dict = sort.sort(data_dict)
	call_name = plot.line_plot(data_dict)

call_name = 'eog ' + call_name + ' &'
print call_name
subprocess.call(call_name.split(' '))
