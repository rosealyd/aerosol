#!/usr/bin/env python
from collections import defaultdict
import numpy as np
import copy
import sys
import plot
from scipy.stats import linregress
import time
import math

def check_for_nan(var_dict):
	remove = []
	for varkey, varlist in var_dict.iteritems():
		for i, item in enumerate(varlist):
			if np.isnan(item):
				remove.append(i)
	remove = set(remove)
	print 'removing bads at ', remove
	for varkey, varlist in var_dict.iteritems():
		var_dict[varkey] = [v for i, v in enumerate(varlist) if i not in remove]
		
	return var_dict

def aodidx_sort(variables_dict, take_slope = False):
	print 'binning aodidx'
	xs = variables_dict['xs']
	xs = sorted(set(xs))
	bins = [[] for x in xs]
	sorted_points = defaultdict(dict)
	for key in variables_dict.keys():
		sorted_points[key] = copy.deepcopy(bins)
	for i, x in enumerate(xs):
		indices = [j for j, x_val in enumerate(variables_dict['xs']) if x_val == x]
		for key, item in variables_dict.iteritems():
			sorted_points[key][i] = list(np.array(item)[indices])
	if take_slope:
		return sorted_points
	for key, item in sorted_points.iteritems():
		for i, bin in enumerate(item):
			sorted_points[key][i] = np.mean(bin)
	return sorted_points

def sort_nonbinned(variables_dict, take_slope = False):
	print 'binning nonaodidx'
	xs = np.percentile(variables_dict['xs'], np.linspace(0,100,13), interpolation = 'midpoint')
	#One less bin than number in xs since it is the end points of the bins so it will have +1 length compared to bins
	bins = [[] for x in xs[:-1]]
	sorted_points = defaultdict(dict)
	for key in variables_dict.keys():
		sorted_points[key] = copy.deepcopy(bins)
	for i, (low_x, high_x) in enumerate(zip(xs[:-1], xs[1:])):
		indices = [j for j, x_val in enumerate(variables_dict['xs']) if x_val < high_x and x_val > low_x]
		for key, item in variables_dict.iteritems():
			sorted_points[key][i] = list(np.array(item)[indices])
	if take_slope:
		print 'returning here'
		return sorted_points
	for key, item in sorted_points.iteritems():
		for i, bin in enumerate(item):
			sorted_points[key][i] = np.mean(bin)
	
	return sorted_points
	
def sort_slope(sorted_points, y_log, color_error, color_slope):
	print 'Taking slope of ys against slope var'
	for i in range(len(sorted_points['cs'])):
		if y_log:
			sorted_points['slope'][i] = [math.log(x) if x != 0.0 else x for x in sorted_points['slope'][i]]
		regress = linregress(sorted_points['slope'][i], sorted_points['ys'][i])
		sorted_points['ys'][i] = float(regress[0])
		sorted_points['xs'][i] = np.mean(sorted_points['xs'][i])
		if color_error:
			print 'color is error'
			sorted_points['cs'][i] = float(regress[2])
		elif color_slope:
			print 'color is slope'
			sorted_points['cs'][i] = copy.deepcopy(sorted_points['ys'][i])
		else:	
			sorted_points['cs'][i] = np.mean(sorted_points['cs'][i])
		sorted_points['ls'][i] = np.mean(sorted_points['ls'][i])
	sorted_points.pop('slope')
	return sorted_points

def sort(data_dict):
	slope = data_dict['take slope']
	y_log = data_dict['take y log']
	color_slope = data_dict['color slope']
	color_error = data_dict['color error']
	#If aodidx as x var
	if 'LWPIDX' in data_dict['variables key']['xs']:
		data_dict['validate variables'] = aodidx_sort(data_dict['validate variables'], take_slope = slope)
	else:
	#If not aodidx as x var
		data_dict['validate variables'] = sort_nonbinned(data_dict['validate variables'], take_slope = slope)
		
	#If taking slope, validate variables is a list of lists still and will be passed to sort_slope to finish
	if slope:
		data_dict['validate variables'] = sort_slope(data_dict['validate variables'], y_log, color_error, color_slope)
	
	data_dict['validate variables'] = check_for_nan(data_dict['validate variables'])
	return data_dict

def split_sort(data_dict):
	list_of_dicts = []
	split_limits = np.percentile(data_dict['validate variables']['ls'], np.linspace(0,100, data_dict['number split']+1), interpolation = 'midpoint')
	if 'omega' in data_dict['split var']:
		split_limits = [min(data_dict['validate variables']['ls']), 0.0, max(data_dict['validate variables']['ls'])]
	for i, (low_limit, high_limit) in enumerate(zip(split_limits[:-1], split_limits[1:])):
		split_dict = copy.deepcopy(data_dict)
		split_indices = [j for j, l in enumerate(split_dict['validate variables']['ls']) if l > low_limit and l < high_limit]
		for key, item in split_dict['validate variables'].iteritems():
			split_dict['validate variables'][key] = list(np.array(item)[split_indices])
		split_dict = sort(split_dict)
	#	split_dict['validate variables'] = check_for_nan(split_dict['validate variables'])
		list_of_dicts.append(copy.deepcopy(split_dict))
		
	return list_of_dicts
