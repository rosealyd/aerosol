#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
from pylab import rcParams
from scipy.stats import linregress
import sys
sys.path.append('/home/adouglas2/Summer/aerosol/Website_modules/10_by_10/')
import get_cmap

rcParams['figure.figsize'] = 12,11 #width, height
rcParams['ps.fonttype'] = 3
rcParams['pdf.fonttype'] = 3
#Put save dir here so it can be easily changed, in future could also add it to option in get user input if need be
SAVE_DIR = '/home/adouglas2/Summer/aerosol/Website_figures/line_plot/'


def make_name(data_dict):
	global SAVE_DIR
	if data_dict['color error']:
		data_dict['color_var'] = 'r2'
	if data_dict['color slope']:
		data_dict['color_var'] = 'slope'
		data_dict['color var title'] =  r'SW Indirect Effect $\frac{W}{m^2}$'
	if data_dict['take log']:
		data_dict['x_var'] = 'ln_' + data_dict['x_var']
		data_dict['x var title'] = 'ln(' + data_dict['x var title'] + ')'
	if data_dict['take slope']:
		data_dict['y_var'] = 'd' + data_dict['y_var'] + '_d' + data_dict['slope_var']
		data_dict['y var title'] =  r'SW Indirect Effect $\frac{W}{m^2}$'
	if data_dict['split']:
		split = '_split_{}_{}'.format(data_dict['number split'], data_dict['split var'])
	else:
		split = ''
	save_name = SAVE_DIR + '{}/{}_{}_{}_vs_{}{}_color_var_{}.png'.format(data_dict['region'] ,data_dict['aerosol'], data_dict['ai bin'], data_dict['x_var'], data_dict['y_var'], split, data_dict['color_var'])
	return save_name, data_dict

def get_level_list(data_dict):
	if data_dict['number split'] == 2:
		return ['Low', 'High']
	if data_dict['number split'] == 3:
		return ['Low', 'Medium', 'High']
	if data_dict['number split'] == 4:
		return ['Low', 'Medium low', 'Medium high', 'High']
	if data_dict['number split'] == 5:
		return ['Low', 'Medium low', 'Medium high', 'High']
	raise Exception 

def set_midpoint(cmin, cmax, midpoint, offset = None):
	size = max(abs(midpoint - cmin), abs(midpoint - cmax))
	if not offset:
		print 'not offset'
		return midpoint - size, midpoint + size
	else:
		return (midpoint - size) * offset, (midpoint+size) * offset
		
def retrieve_cmap(low_c, high_c):
	if low_c < 0 and high_c > 0:
		cmap = get_cmap.get_cmap()
		low_c, high_c = set_midpoint(low_c, high_c, 0.0)
	elif low_c >= 0 and high_c > 0:
		cmap = get_cmap.get_redmap()
	elif low_c < 0 and high_c <= 0:
		cmap = get_cmap.get_bluemap()
	return cmap, low_c, high_c
	
def line_plot(data_dict):
	
	save_name,data_dict = make_name(data_dict)
	min_color = min(data_dict['validate variables']['cs'])
	max_color = max(data_dict['validate variables']['cs'])
	cmap, min_color, max_color = retrieve_cmap(min_color, max_color)
	for x in data_dict['validate variables']['xs']:
		print x
	print 
	for y in data_dict['validate variables']['ys']:
		print y
	regress = linregress(data_dict['validate variables']['xs'], data_dict['validate variables']['ys'])
	temp = np.mean(data_dict['validate variables']['cs']) * regress[0]
	print np.mean(data_dict['validate variables']['cs'])
	plt.plot(data_dict['validate variables']['xs'], [((x*regress[0]) + regress[1]) for x in data_dict['validate variables']['xs']], 'r-', label = r'slope: {} r2: {}'.format(round(regress[0],2), round(regress[2]**2, 2)), linewidth = 5)
	#plt.plot(data_dict['validate variables']['xs'], [((x*regress[0]) + regress[1]) for x in data_dict['validate variables']['xs']], 'r-', label = r'slope: {} r2: {}'.format(round(temp,2), round(regress[2]**2, 2)), linewidth = 5)
	
	#plt.scatter(data_dict['validate variables']['xs'], [cf * y for cf, y in zip(data_dict['validate variables']['ys'], data_dict['validate variables']['cs'])], c = data_dict['validate variables']['cs'], cmap = cmap, vmin = min_color, vmax = max_color, s = 300)
	
	plt.scatter(data_dict['validate variables']['xs'], data_dict['validate variables']['ys'], c = data_dict['validate variables']['cs'], cmap = cmap, vmin = min_color, vmax = max_color, s = 300)
	plt.legend(fontsize = 20)
	plt.xticks(size = 20)
	plt.yticks(size = 20)
	plt.xlabel(data_dict['x var title'], size = 24)
	plt.ylabel(data_dict['y var title'], size = 24)
	cbar = plt.colorbar(fraction  = .1)
	cbar.set_label(data_dict['color var title'], size = 24)
	cbar.ax.tick_params(labelsize=20) 
	print save_name
	plt.savefig(save_name)
	return save_name

def split_plot(list_of_dicts):
	save_name, list_of_dicts[0] = make_name(list_of_dicts[0])
	min_color = min([d for a in list_of_dicts for d in a['validate variables']['cs']])
	max_color = max([d for a in list_of_dicts for d in a['validate variables']['cs']])
	cmap, min_color, max_color = retrieve_cmap(min_color, max_color)
	level_list = get_level_list(list_of_dicts[0])
	for i, (split_dict, level) in enumerate(zip(list_of_dicts, level_list)):
		
		regress = linregress(split_dict['validate variables']['xs'], split_dict['validate variables']['ys'])
		plt.plot(split_dict['validate variables']['xs'], [((x * regress[0]) + regress[1]) for x in split_dict['validate variables']['xs']], label = '{} slope: {} r2: {}'.format(level, round(regress[0], 2), round(regress[2]**2, 2)), linewidth = 5)
		plt.scatter(split_dict['validate variables']['xs'], split_dict['validate variables']['ys'], c = split_dict['validate variables']['cs'], cmap = cmap, vmin = min_color, vmax = max_color, s = 300)
	plt.legend(fontsize=20)
	plt.xticks(size = 20)
	plt.yticks(size = 20)
	plt.xlabel(list_of_dicts[0]['x var title'], size = 24)
	plt.ylabel(list_of_dicts[0]['y var title'], size = 24)
	cbar = plt.colorbar(fraction  = .1)
	cbar.set_label(list_of_dicts[0]['color var title'], size = 24)
	cbar.ax.tick_params(labelsize=20) 
	print save_name
	plt.savefig(save_name)
	return save_name
			
