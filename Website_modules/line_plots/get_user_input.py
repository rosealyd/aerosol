#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import json
from collections import defaultdict
import copy
import time
import math
import sys
sys.path.append('/home/adouglas2/Summer/aerosol/Website_modules/10_by_10/')
import get_user_input_10_by_10
import cases_class

cc_class = cases_class.Cases()

def remove_high_lwp(data_dict):
	good_lwps = [i for i, l in enumerate(data_dict['remove']) if l < .65 and l > .1]
	data_dict.pop('remove')
	for key, key_list in data_dict.iteritems():
		data_dict[key] = [v for i,v in enumerate(key_list) if i in good_lwps]
	
	return data_dict
	
def validate(data_dict):
	print data_dict.keys()
	good = [i for i, d in enumerate(data_dict['xs'])]
	for key, key_list in data_dict.iteritems():
		#Find acceptable integers for key
		bad_indices = [i for i, v in enumerate(key_list) if type(v) != float]
		#Update to remove bad
		good = [i for i in good if i not in bad_indices]
	for key, item in data_dict.iteritems():
		data_dict[key] = list(np.array(item)[good])
	return data_dict

#First must get data using root dir, region list, and validate list
def get_data(data_dict):
	#This dict will hold the lists
	validated_variables = defaultdict(dict)
	#Create the dict for the title labels
	data_dict['variables key'] = defaultdict(dict)
	for key, item in data_dict['validate variables'].iteritems():
		data_dict['variables key'][key] = get_user_input_10_by_10.title_variables[get_user_input_10_by_10.all_variables.index(item)]
	for key, item in data_dict['variables key'].iteritems():
		print key, item
	#Create the list of needed variables
	for key in data_dict['validate variables'].keys():
		validated_variables[key] = []
	for lat_lon in data_dict['cases']:
		print lat_lon
		with open(data_dict['ROOT DIR'] + lat_lon + '.txt') as dumped:
			region_dict = json.load(dumped)
			for key, item in data_dict['validate variables'].iteritems():
				print 'Getting', item
				validated_variables[key].extend(region_dict['noerror']['touching_day'][data_dict['aerosol']][item])
	if not 'LWPIDX' in data_dict['variables key']['xs']:
		print 'here', data_dict['variables key']['xs']
		validated_variables = remove_high_lwp(validated_variables)
	data_dict['validate variables'] = validate(validated_variables)
	for key, item in data_dict['validate variables'].iteritems():
		try:
			bad = data_dict[key].index(None)
			print bad, 'bad index in', key
			print 'you need to step up your game - Past Alyson'
			sys.exit()
		except:
			pass
	
	#finally if taking the log of AODIDX if its the xaxis variable
	if data_dict['take log']:
		print "Taking log of AI"
		data_dict['validate variables']['xs'] = [math.log(x) for x in data_dict['validate variables']['xs'] if x != 0.0]
	return data_dict
	
#Get the input to create sort dict, sort dict then used in get data then validate and finally sorting whereafter called plot_dict
def get_input():
	aerosols = get_user_input_10_by_10.aerosols
	all_variables = get_user_input_10_by_10.all_variables
	title_variables = get_user_input_10_by_10.title_variables
	print title_variables[all_variables.index('aodidx')]
	#When plotting using this, will have an x, y, color variable, and splitting variable

	sort_dict = defaultdict(dict)
	#Firt will find out what AI range to plot
	ai_bin = get_user_input_10_by_10.ask_question('What AI range would you like to plot?', ['a) All', 'b) 90 Percent', 'c) 80 Percent'])
	if ai_bin == 'a':
		ROOT_DIR = '/thermal/data/Aerosols/Globe_json_files/all_ai/'
		sort_dict['ai bin'] = 'all'
	elif ai_bin == 'b':
		ROOT_DIR = '/thermal/data/Aerosols/Globe_json_files/90_percent/'
		sort_dict['ai bin'] = '90'
	elif ai_bin == 'c':
		ROOT_DIR = '/thermal/data/Aerosols/Globe_json_files/80_percent/'
		sort_dict['ai bin'] = '80'
	sort_dict['ROOT DIR'] = ROOT_DIR

	#Next what aerosol(s) do we want to plot
	#Find the aerosol type to plot
	aerosol = get_user_input_10_by_10.many_options_ask('What variable would you like to look as your y variable?\n', aerosols)[0]
	sort_dict['aerosol'] = aerosol

	#Next what region to plot
	region = get_user_input_10_by_10.ask_question("Which region would you like to plot?", ["a) Angola Basin", "b) South Pacific", "c) ITCZ", 'd) Globe', 'e) AB and SP', 'f) Barbados', 'g) California'])
	
	sort_dict['ITCZ'] = True
	if region == 'a':
		sort_dict['region'] = 'Angola'
		cases = cc_class.Angola
	if region == 'b':
		sort_dict['region'] = 'SouthPacific'
		cases = cc_class.SoutheastPacific
	if region == 'c':
		cases = cc_class.ITCZ
		sort_dict['region'] = 'ITCZ'
	
	if region == 'd':
		cases = cc_class.Globe
		sort_dict['region'] = 'Global'
	if region == 'e':
		sort_dict['region'] = 'AB_SP'
		cases = cc_class.Angola.extend(cc_class.SoutheastPacific)
	
	if region == 'f':
		sort_dict['region'] = 'Barbados'
		cases = cc_class.Barbados
	if region == 'g':
		sort_dict['region'] = 'California'
		cases = cc_class.California
	sort_dict['cases'] = cases
	#Next the x axis variable
	x_var = get_user_input_10_by_10.many_options_ask('What variable would you like as your x variable?\n', all_variables)[0]
	sort_dict['x_var'] = x_var
	print aerosol, x_var
	if 'AOD' in aerosol and x_var == 'aodidx':
		sort_dict['x var title'] = 'AOD'
	else:
		sort_dict['x var title'] = title_variables[all_variables.index(x_var)]
	print sort_dict['x var title']
	sort_dict['take log'] = False
	if 'aodidx' in x_var:
		take_log = get_user_input_10_by_10.ask_question("Would you like to take the log of AI?", ['a) Yes', 'b) No'])
		if take_log == 'a':
			sort_dict['take log'] = True
	
	
	#Then the y axis variable
	y_var = get_user_input_10_by_10.many_options_ask('What variable would you like to look as your y variable?\n', all_variables)[0]
	sort_dict['y_var'] = y_var
	sort_dict['y var title'] = title_variables[all_variables.index(y_var)]
	
	sort_dict['take slope'] = False
	slope_var = 'aodidx'
	sort_dict['take y log'] = False
	sort_dict['color error'] = False
	sort_dict['color slope'] = False
	#Slope at a point
	slope = get_user_input_10_by_10.ask_question('Would you like to take the slope of your y variable against another variable?\n', ['a) Yes', 'b) No'])
	if slope == 'a':
		sort_dict['take slope'] = True
		slope_var = get_user_input_10_by_10.many_options_ask('What variable would you like to look as your y variable?\n', all_variables)[0]
		if 'aodidx' in slope_var:
			take_y_log = get_user_input_10_by_10.ask_question("Would you like to take the log of AI?", ['a) Yes', 'b) No'])
			if take_y_log == 'a':
				sort_dict['take y log'] = True
		c_as_error = get_user_input_10_by_10.ask_question('Would you like the color to be the r2?\n', ['a) Yes', 'b) No'])
		if c_as_error == 'a':
			sort_dict['color error'] = True
		c_as_slope = get_user_input_10_by_10.ask_question("Would you like the color to be the slope?\n", ['a) Yes', 'b) No'])
		if c_as_slope == 'a':
			sort_dict['color slope'] = True
		
	sort_dict['slope_var'] = slope_var
	
	#now the color variable
	color_var = get_user_input_10_by_10.many_options_ask('What variable would you like as your color variable?\n', all_variables)[0]
	sort_dict['color_var'] = color_var
	sort_dict['color var title'] = title_variables[all_variables.index(color_var)]

	#Splitting
	split = get_user_input_10_by_10.ask_question('Would you like to split by a variable?\n', ['a) Yes', 'b) No'])
	if split == 'a':
		split = True
		split_var = get_user_input_10_by_10.many_options_ask("What variable would you like as your split variable?\n", all_variables)[0]
		sort_dict['split var'] = split_var
		try:
			num_split = int(raw_input('How many splits would you like to make?\n'))
		except ValueError:
			print "Must enter integer"
			sys.exit()
		sort_dict['number split'] = num_split
	else:
		split = False
		split_var = 'lwp'

	sort_dict['split var'] = split_var
	sort_dict['split'] = split
	
	#have list of variables in sort dict that need validation
	sort_dict['validate variables'] = {'xs': x_var, 'ys': y_var, 'cs': color_var, 'ls': split_var, 'remove': 'lwp', 'slope': slope_var}

	get_data(sort_dict)
	return sort_dict










