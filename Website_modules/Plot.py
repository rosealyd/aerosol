#!/usr/bin/env python
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from pylab import rcParams
import numpy as np
import subprocess
import sys
from matplotlib import gridspec

#SAVE_DIRECTORY = "/home/adouglas2/Summer/aerosol/Website_figures/5by5/"

def create_figure(directions):
	for lat_lon_key, lat_lon in directions.iteritems():
		for aerosol_key, aerosol in lat_lon.iteritems():
			if directions[lat_lon_key][aerosol_key]["barplot"]:
				if directions[lat_lon_key][aerosol_key]["percent_net"]:
					percent_net_barplot(directions)
				else:
					barplot(directions)
			else:
				plot(directions)
	return

#Percent net bar plot function
def percent_net_barplot(directions):
	for lat_lon_key in directions.keys():
		for aerosol_key in directions[lat_lon_key].keys():
			#directions[lat_lon_key][aerosol_key]["barplot_list"] =directions[lat_lon_key][aerosol_key]["up_barplot_list"]
			#print directions[lat_lon_key][aerosol_key]["barplot_list"] 
			barplot(directions,"#CC3300","up_barplot_list")
			#directions[lat_lon_key][aerosol_key]["barplot_list"] =directions[lat_lon_key][aerosol_key]["down_barplot_list"]
			#print directions[lat_lon_key][aerosol_key]["barplot_list"] 
			barplot(directions,"#1919A3","down_barplot_list")
	directions[lat_lon_key][aerosol_key]["yvariable"] = "percent_net_contribution"
	fig_name = add_labels_barplot(directions)
	print fig_name
	plt.savefig(SAVE_DIRECTORY + fig_name,bbox_inches = "tight")
	open_name = "eog " +SAVE_DIRECTORY +  fig_name + " &"
	print open_name
	subprocess.call(open_name.split(" "))
	#plt.show()
	sys.exit()


#To plot a bar plot not separated by anything
def barplot(directions, color="#FF9933", list_name="barplot_list"):
	global plt
	check_sum = 0
	art = []
	colors = ["b","g","r"]
	names = ["low","medium","high"]
	for lat_lon_key, lat_lon in directions.iteritems():
		for aerosol_key, aerosol in lat_lon.iteritems():
			region = directions[lat_lon_key][aerosol_key]
			if region["separate"]:
				if region["multiple_barplots"]:
					fig_name, art = plot_separate_barplots(directions, list_name)
					fig_name += ".png"

				else:
					for i,bin in enumerate(region[list_name]):
						for x,y,w in bin[:-1]:
							check_sum += y
							plt.bar(x,y,width = w, color = colors[i])
						x= bin[-1][0]
						y = bin[-1][1]
						w = bin[-1][2]
					
						plt.bar(x,y,width = w, color = colors[i], label = "{}".format(names[i]))
					lgd = plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1), title = region["separate_variable"])
					art.append(lgd)
					fig_name = add_labels_barplot(directions)
			else:
				
				for x,y,w in region[list_name]:
					check_sum += y
					plt.bar(x,y,width=w,color=color)
				fig_name = add_labels_barplot(directions)
	#print check_sum
	if region["percent_net"]:
		return
	else:
		
		plt.savefig(SAVE_DIRECTORY + fig_name, bbox_inches = "tight", bbox_extra_artists = art)
		open_name = "eog " + SAVE_DIRECTORY + fig_name + " &"
		print open_name
		subprocess.call(open_name.split(" "))
		#plt.show()
		
		return

def add_labels_barplot(directions):
	
	title = ""
	max_y = 0
	for lat_lon_key, lat_lon in directions.iteritems():
		title += lat_lon_key + " "
		for aerosol_key, aerosol in lat_lon.iteritems():
			title += aerosol_key
			region = directions[lat_lon_key][aerosol_key]
			f_name = make_fig_name(title, region)
			if region["separate"]:
				xs = [p[0] for p in region["barplot_list"][0]]
			else:
				xs = [p[0] for p in region["barplot_list"]]
			
	x_places = [tx + .5 for tx in xs]
	xlabels = ["{}%".format(int(p)) for p in region["percentiles"]]
	plt.xticks(x_places, xlabels)
	#Region should only correspond to one latlon with one aerosol for barplots meaning region is the last set/only region
	plt.ylabel("% {}".format(region["yvariable"].replace("_"," ")).upper())
	plt.xlabel(region["xvariable"].replace("_"," ").upper())
	plt.title(title)
	return f_name
	
def plot_separate_barplots(directions, list_name):
	colors = ["b","g","r"]
	names = ["low","medium","high"]
	art = []
	for lat_lon_key, lat_lon in directions.iteritems():
		for aerosol_key, aerosol in lat_lon.iteritems():
			region = directions[lat_lon_key][aerosol_key]
			gs = gridspec.GridSpec(3,3)
			fig1 = plt.figure(figsize=(11,8))
			ax = fig1.add_subplot(gs[:,:2])
			for i,bin in enumerate(region[list_name]):
				for x,y,w in bin[:-1]:
					ax.bar(x,y,width = w, color = colors[i])
				x= bin[-1][0]
				y = bin[-1][1]
				w = bin[-1][2]
			
				ax.bar(x,y,width = w, color = colors[i], label = "{}".format(names[i]))
			lgd = plt.legend(loc=9, bbox_to_anchor=(0.5, -0.1), title = region["separate_variable"])
			art.append(lgd)
			fig_name = add_labels_barplot(directions)[:-3]
			fig_name += "separate_barplots"
			#Used as the x coordinates since no longer need to be 1/3 of the size
			plotting_xs = list(np.linspace(0,len(region[list_name][0])+1,len(region[list_name][0])))
			label_xs = [x + .5 for x in plotting_xs]
			xlabels = ["{}%".format(int(p)) for p in region["percentiles"]]
			ax1 = fig1.add_subplot(gs[0,2])
			for (temp, y, temp), x in zip(region[list_name][0],plotting_xs):
				ax1.bar(x,y,width = 1, color = colors[0])
			ax1.set_xticks(label_xs)
			ax1.set_xticklabels(xlabels, fontsize="xx-small")
			ax2 = fig1.add_subplot(gs[1,2])
			for (temp, y, temp), x in zip(region[list_name][1],plotting_xs):
				ax2.bar(x,y, width = 1, color = colors[1])
			ax2.set_xticks(label_xs)
			ax2.set_xticklabels(xlabels, fontsize="xx-small")
			ax3 = fig1.add_subplot(gs[2,2])
			for (temp, y, temp), x in zip(region[list_name][2],plotting_xs):
				ax3.bar(x,y,width = 1, color = colors[2])
			ax3.set_xticks(label_xs)
			ax3.set_xticklabels(xlabels, fontsize="xx-small")
	return fig_name, art
	
#All functions below have to do with scatter plots
def get_color_range(directions):
	lowest_color = 1000
	highest_color = 0
	for lat_lon_key, lat_lon in directions.iteritems():
		for aerosol_key, aerosol in lat_lon.iteritems():
			if directions[lat_lon_key][aerosol_key]["separate"]:
				for separate_bin in directions[lat_lon_key][aerosol_key]["plotting_list"]:
					local_min = min(separate_bin, key = lambda t: t[2])[-1]
					local_max = max(separate_bin, key = lambda t: t[2])[-1]
					if local_min < lowest_color:
						lowest_color = local_min
					if local_max > highest_color:
						highest_color = local_max
			else:
				temp_list = directions[lat_lon_key][aerosol_key]["plotting_list"]
				local_min = min(temp_list, key = lambda t: t[2])[-1]
				local_max = max(temp_list, key = lambda t: t[2])[-1]
				if local_min < lowest_color:
					lowest_color = local_min
				if local_max > highest_color:
					highest_color = local_max
	ts = np.linspace(lowest_color, highest_color, 7)
	ts = [round(t, 2) for t in ts]
	return lowest_color, highest_color,ts

#Create save fig name
def make_fig_name(ttl, region):
	
	if region["separate"]:
		separate = "_" + region["separate_variable"]+"_" 
		for bin in region["separate_show_names"]:
			separate += bin + "_"
	else:
		separate = ""
	if region["error_bar"]:
		error_bar = "_errbr_"
	else:
		error_bar = ""
	if region["barplot"]:
		barplot = "_barplot_"
	else:
		barplot = ""
	ttl = ttl.replace(" ","_")
	fig_name = region["layer"] + "_" + separate + ttl + "_" + region["color_variable"] + "_" + region["xvariable"] + "_vs_" + region["yvariable"] + error_bar + barplot + ".png"
	return fig_name
	

#Add all labels, legends, colorbars, and do all calculations for slope and such here
def add_labels_legends(plt,directions):

	#Getting color limits and ticks for color bar
	lowest_color, highest_color,color_ticks = get_color_range(directions)
	#The average slope starts at 0
	avg_slope = 0
	#Creating the title with all lat_lons and aerosol species associated with the lat lon
	title = ""
	for lat_lon_key, lat_lon in directions.iteritems():
		title += lat_lon_key + " "
		for aerosol_key, aerosol in lat_lon.iteritems():
			region = directions[lat_lon_key][aerosol_key]
			title += aerosol_key + " "
			#Color variable for colorbar
			color_var =region["color_variable"].replace("_", " ")
			#X variable for plot label
			x_var = region["xvariable"].replace("_"," ")
			#Y var for plot label
			y_var = region["yvariable"].replace("_"," ")
			#Creating legend title, is the same for all lat lons and aerosols since they share a y variable
			legend_title = "Avg d{}/d{} ".format(region["yvariable"][:3],region["xvariable"][:3])
			if region["separate"]:
				avg_slope += np.mean(region["slope"])
			else:
				avg_slope += region["slope"]
		avg_slope = avg_slope/len(lat_lon.keys())
	avg_slope = avg_slope/len(directions.keys())
	legend_title += str(round(avg_slope,2))
	clbr = plt.colorbar()
	clbr.set_label("Average {}".format(color_var))
	clbr.set_ticks(color_ticks)
	number_columns = len(directions.keys()) 
	if len(directions[0].keys()) > 1 and len(directions.keys()) == 1:
		number_coumns = len(directions[0].keys())
	
	#Legend must be in a list for it to show up in saved figures as an extra artist
	art = []
	lgd = plt.legend(title = legend_title, scatterpoints = 1,prop={'size':12},loc=9, bbox_to_anchor=(0.5, -0.1),ncol = number_columns,borderpad =.09,labelspacing= .2, columnspacing = .10)
	art.append(lgd)
	
	plt.xlabel(x_var.upper())
	plt.ylabel(y_var.upper())
	plt.title(title)
	
	name = make_fig_name(title, region)
	
	return art, name
	
def plot(directions):
	global plt
	#Changing figure size, must be done before plot is plotted on
	rcParams['figure.figsize'] = 13, 8 #width, height
	#Get lowest color, highest color for the color parameters vmin vmax in scatter
	lowest_color, highest_color,color_ticks = get_color_range(directions)
	for lat_lon_key, lat_lon in directions.iteritems():
		for aerosol_key, aerosol in lat_lon.iteritems():
			region = directions[lat_lon_key][aerosol_key]

			SAVE_DIRECTORY = region['save_dir']
			if region['season_var'] != None:
				SAVE_DIRECTORY = SAVE_DIRECTORY.replace('15by15', region['season_var'])
			#For separated plotting
			if region["separate"]:
				names = ["low","med","high"]
				for s,(separate_points,marker,slope,r2) in enumerate(zip(region["plotting_list"],region["markers"],region["slope"],region["r_squared"])):
					#print s, region["separate_show"]
					if s in region["separate_show"]:
						mean_cs = [p[2] for p in separate_points]
						mean_cs = np.mean(mean_cs)
						bin_slope = np.mean(slope)
						bin_r2 = np.mean(r2)
						label = "{} {}\nS:{} R:{}  MeanC:{}".format(region["aerosol"], names[s], round(bin_slope,2), round(bin_r2,2), mean_cs)
						
						for x,y,c in separate_points[:-1]:
							plt.scatter(x,y, c = c, cmap = cm.jet, vmin = lowest_color, vmax = highest_color, marker = marker, s = 300)
							
						#Add in the label for the separate bin
						plt.scatter(separate_points[-1][0],separate_points[-1][1],c=separate_points[-1][2],label=label,cmap = cm.jet, vmin = lowest_color, vmax = highest_color, marker = marker, s = 300)
				
				#plotting linear regression lines
				for separate_lines in region["linear_regression"]:
					xs = [lr[0] for lr in separate_lines]
					ys = [lr[1] for lr in separate_lines]
					plt.plot(xs,ys,"r-")
				#If plotting an error bar in separated plot
				if region["error_bar"]:
					for separate_points, std_dev in zip(region["plotting_list"],region["standard_deviation"]):
						ys = [p[1] for p in separate_points]
						xs = [p[0] for p in separate_points]
						plt.errorbar(xs,ys,std_dev)
						
						
			#For non separated plotting
			else:
				mean_cs = [p[2] for p in region["plotting_list"]]
				mean_cs = round(np.mean(mean_cs),2)
				for i,(x,y,c) in enumerate(region["plotting_list"]):
					if i == 0:
						label = "{} {} \nS:{} R:{} {}".format(region["aerosol"],region["lat_lon"], region["slope"],region["r_squared"],mean_cs)
						plt.scatter(x,y,c = c, cmap = cm.jet, vmin = lowest_color, vmax = highest_color, marker = region["markers"], s = 300, label = label)
					else:
						#label = "{}".format(round(c,2))
						plt.scatter(x,y,c = c, cmap = cm.jet, vmin = lowest_color, vmax = highest_color, marker = region["markers"], s = 300)
						
				xs = [lr[0] for lr in region["linear_regression"]]
				ys = [lr[1] for lr in region["linear_regression"]]
				#plt.plot(xs,ys,"r-")
				
				if region["error_bar"]:
					ys = [p[1] for p in region["plotting_list"]]
					plt.errorbar(xs, ys, region["standard_deviation"])
	art,fig_name = add_labels_legends(plt, directions)

	#plt.show()
	
	plt.savefig(SAVE_DIRECTORY + fig_name,bbox_inches = "tight",bbox_extra_artists = art)
	plt.cla()
	open_name = "eog " +SAVE_DIRECTORY +  fig_name + " &"
	print open_name
	subprocess.call(open_name.split(" "))
	return


	

