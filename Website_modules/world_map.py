#!/usr/bin/env python

import Get_directions
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import sys
import time
import math
import json
import Get_user_input
import glob
import colormap
from matplotlib.colors import Normalize

LENGTH = 15.0
RE = 6378100.0
#ROOT_DIR = "/home/adouglas2/Summer/aerosol/test_5by5_files/"
ROOT_DIR = "/home/adouglas2/Summer/aerosol/new_Angola_json_files/"

test = '0_0.txt'
f = open(ROOT_DIR + test)
test = json.load(f)
temp = {'0-0': test}

lower_left_lon = -35.0
lower_left_lat = -35.0
upper_right_lon = 20.0
upper_right_lat = 20.0

center_lat = -7.5
center_lon = -7.5

class MidpointNormalize(Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))


def find_corners(lats, lons):
	global LENGTH
	x_coords_corners = []
	y_coords_corners = []
	for x in lons:
		for y in lats:
			corner_1 = [x, y]
			corner_2 = [x, y+LENGTH]
			corner_3 = [x+LENGTH, y]
			corner_4 = [x+LENGTH, y+LENGTH]
			
			x_coords_corners.append(np.array([[corner_1[0],corner_2[0]],[corner_3[0],corner_4[0]]]))
			y_coords_corners.append(np.array([[corner_1[1],corner_2[1]],[corner_3[1],corner_4[1]]]))

	return x_coords_corners, y_coords_corners
lats = []
lons = []
for filename in glob.glob(ROOT_DIR+"*.txt"):
	filename = filename.split("/")[-1]
	print filename
	lat = filename.split("_")[0]
	print lat
	lon = filename.split("_")[1].strip(".txt")
	print lon
	if lat not in lats:
		lats.append(lat)
	if lon not in lons:
		lons.append(lon)

lats = sorted(lats)
lons = sorted(lons)


ys = []


test = "Overall.py --lat_lons={} --aerosols MODISAI --xvariable aodidx --yvariable 12_net --color_variable EC_LTSS --layer touching --15degree"

cases = []
exceptions = []
for lon in lons:
	for lat in lats:
		print lat, lon
		
		lat_lon = '{}_{}'.format(int(lat),int(lon))
		time.sleep(1)
		try:
			test_directions = Get_directions.get_multi_directions(test.format(lat_lon))
			#When plotting dy/dx
			temp = test_directions[lat_lon]['MODISAI']['slope']
			#When plotting raw values of y
			#temp = [p[1] for p in test_directions[lat_lon]['MODISAI']['plotting_list']]
			ys.append(temp)
			cases.append('{}_{}'.format(int(lat),int(lon)))
		except:
			print 'Exception', lat, lon
			exceptions.append((lat,lon))
			ys.append(0)

	
print cases
norm = MidpointNormalize(midpoint=0)

lats = [int(lat) for lat in lats]
lons = [int(lon) for lon in lons]
corner_lons, corner_lats = find_corners(lats, lons)

print "values:", ys
plot_max = max(ys)
plot_min = min(ys)
ai_dependent_ys = []

'''
for i in range(len(ys[0])):
	temp = [y[i] for y in ys]
	ai_dependent_ys.append(temp)
for temp_y in ai_dependent_ys:
	for i in range(len(temp_y)):
		plot_min = min(temp_y)
		'''

temp_y = ys
for i in range(len(ys)):
		temp_lons = corner_lons[i]
		temp_lats = corner_lats[i]
		blah = np.array([[temp_y[i],temp_y[i]],[temp_y[i],temp_y[i]]])
		m = Basemap(llcrnrlon= lower_left_lon , llcrnrlat = lower_left_lat, urcrnrlon = upper_right_lon, urcrnrlat = upper_right_lat,lat_1 = center_lat, lon_0 = center_lon, projection = 'lcc',resolution = 'l')
		m.drawcoastlines()
		m.fillcontinents(color = '#005C1F', lake_color = '#005C1F')
		m.drawmapboundary(fill_color = 'white')
		m.drawparallels(np.array([-15.0,0,15]))
		m.drawmeridians(np.array([-15,0,15]))
		aerosol = m.pcolor(temp_lons, temp_lats, blah, shading = 'flat', norm = norm, cmap = plt.cm.coolwarm, vmin = plot_min, vmax = plot_max, latlon = True)
cb = m.colorbar(aerosol, label = 'Change in W/m^2')
plt.show()




