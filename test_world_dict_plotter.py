#!/usr/bin/env python

from cloud.serialization import cloudpickle
import sys
import os
import pickle
import json
import glob
import matplotlib.pyplot as plt
from scipy.stats import linregress
from scipy.stats import scoreatpercentile
import numpy as np
import matplotlib.cm as cm
from pylab import rcParams
import subprocess
import copy
from matplotlib.font_manager import FontProperties


INCLUDE_END_AIS = False

ROOT_DIR = "./test_Angola_json_files/"
BIN_FILE = "ai_bin_dict.p"
LETTERS = list(map(chr, range(97,123)))
for a in list(map(chr, range(97,123))):
	for b in list(map(chr, range(97,123))):
		LETTERS.append("{}{}".format(a,b))

SEPARATE_MARKERS = [["o","v","s"],["*","x","8"],["h","+","H"],[".","<","d"]]
MARKERS = ["o","v","s","*","x","8","h","+","H",".","<","d"]
BINNED_VARIABLES = ["geoidx","amsrrridx","aodidx","AMSR-e_LWP","CloudSat_rain"]
ALLOW_ZEROES = ["12_cloud_fraction","24_cloud_fraction","Merra_Omega","amsrrridx", "rridx","CloudSat_rain", "CloudSat_pop","amsrr_pop"]
STAN_DEV_AMOUNT = 2.0
errorbar = ""
to_show = []

flag = False
#Testing variable here
num_percentiles = 20

def check(options,user_input):
	
	if user_input in LETTERS[:len(options)]:
		return
	else:
		print "That was bad input"
		sys.exit()


def get_keys(dictionary, level_num):
	for latlonkey, latlon in plotting_dicts.iteritems():
		if level_num ==1:
			return latlon.keys()
		
		for errorkey, error in latlon.iteritems():
			if level_num == 2:
				return error.keys()
				
			for touchingkey, touching in error.iteritems():
				if level_num ==3:
					return touching.keys()
				
				for aerosolkey, aerosol in touching.iteritems():
					if level_num == 4:
						return aerosol.keys()

#For sorting data when only plotting a binned variable and separating by  every 33% l,m,h lwp						
def separate_aodidx(xs, ys, cs, ls, bin_dict,aerosol,plotting_dict, plot_latlon_aerosol, percenvar, yvar, xvar,touching,other_layer, barplot,PoP):
	global INCLUDE_END_AIS
	print percenvar
	raining = False
	#INCLUDE_END_AIS = False
	num_bins = 0
	min_bin = 100
	max_bin = 0
	for x in xs:
		if max(x) - min(x) > num_bins:
			num_bins = int(max(x) - min(x))
		if max(x) > max_bin:
			max_bin = int(max(x))
		if min(x) < min_bin:
			min_bin = int(min(x))
	
	
	#Creating temporary bins to place the ys, and colors when sorted
	temp = [[[[] for i in range(num_bins +1)] for i in range(3)] for i in range(len(xs))]
	temp2 = [[[[] for i in range(num_bins + 1)] for i in range(3)] for i in range(len(xs))]
	temp_st_dev = [[[[] for i in range(num_bins + 1)] for i in range(3)] for i in range(len(xs))]
	lwp_percentiles = []
	for i in range(len(ls)):
		temp_ls =  filter(lambda a: a != 0, ls[i])
		lwp_percentiles.append(np.percentile(temp_ls,[0,33,66,100],interpolation="midpoint"))

	#If separating raining from non-raining
	if "rridx" in percenvar or "rain" in percenvar or PoP:
		
		lwp_percentiles = []
		temp = [[[[] for i in range(num_bins +1)] for i in range(2)] for i in range(len(xs))]
		temp2 = [[[[] for i in range(num_bins + 1)] for i in range(2)] for i in range(len(xs))]
		temp_st_dev = [[[[] for i in range(num_bins + 1)] for i in range(2)] for i in range(len(xs))]
		raining = True
		print "Separating raining from non-raining"
		
	print "Removing all outliers"
	for i,(x, y, l, c) in enumerate(zip(xs, ys, ls, cs)):
		mean = np.mean(y)
		stan_dev = np.std(y)
		high_limit = mean + (STAN_DEV_AMOUNT* stan_dev)
		low_limit = mean - (STAN_DEV_AMOUNT*stan_dev)
		if not raining:
			for j,(px, py, pl, pc) in enumerate(zip(x, y, l, c)):
				if py < high_limit and py > low_limit:	
				
					for k, p in enumerate(lwp_percentiles[i][:-1]):
						#print lwp_percentiles[i][k+1], p, pl, k
						if pl<lwp_percentiles[i][k+1] and pl >= p:
							if not INCLUDE_END_AIS:
								if px > min_bin and px < max_bin:
								
									temp[i][k][int(px)].append(py)
									temp2[i][k][int(px)].append(pc)
							else:
							
								temp[i][k][int(px)].append(py)
								temp2[i][k][int(px)].append(pc)
		else:
			for j,(px, py, pl, pc) in enumerate(zip(x, y, l, c)):
				if py < high_limit and py > low_limit:	
				
					if pl > 0:
						k = 1
						if not INCLUDE_END_AIS:
							if px > min_bin and px < max_bin:
							
								temp[i][k][int(px)].append(py)
								temp2[i][k][int(px)].append(pc)
						else:
						
							temp[i][k][int(px)].append(py)
							temp2[i][k][int(px)].append(pc)
					else:
						k = 0
						if not INCLUDE_END_AIS:
							if px > min_bin and px < max_bin:
							
								temp[i][k][int(px)].append(py)
								temp2[i][k][int(px)].append(pc)
						else:
						
							temp[i][k][int(px)].append(py)
							temp2[i][k][int(px)].append(pc)
		
	for item in temp:
		for items in item:
			
			for i, bin in enumerate(items):
				print i, len(bin)
	
	#Gathering the bin data from the bin dictionary.  Only grabbing the necessary points	
	xs = [[]for i in range(len(xs))]
	if xvar == "aodidx":
		
		for index, item in enumerate(plot_latlon_aerosol):
			gar = bin_dict[item[0]][item[1]][min_bin:(max_bin+2)]
			garb1 = [(x+y)/2 for x,y in zip(gar[:-1], gar[1:])]
			garb2 = [(x+y)/2 for x,y in zip(gar[:-1], gar[1:])]
			garb3 = [(x+y)/2 for x,y in zip(gar[:-1], gar[1:])]
		
			xs[index].append(garb1)
			xs[index].append(garb2)
			if not raining:
				xs[index].append(garb3)
	elif xvar == "AMSR-e_LWP":
		for index, item in enumerate(plot_latlon_aerosol):
			gar = lwp_bin_dict[item[2]][item[0]][min_bin:(max_bin+1)]
			
			garb1 = [(x+y)/2 for x,y in gar]
			garb2 = [(x+y)/2 for x,y in gar]
			garb3 = [(x+y)/2 for x,y in gar]
			xs[index].append(garb1)
			xs[index].append(garb2)
			if not raining:
				xs[index].append(garb3)
	elif xvar =="CloudSat_rain":
		for index, item in enumerate(plot_latlon_aerosol):
			gar = rr_bin_dict[item[2]][item[0]][min_bin:(max_bin+1)]
			garb1 = [(x+y)/2 for x,y in gar]
			garb2 = [(x+y)/2 for x,y in gar]
			garb3 = [(x+y)/2 for x,y in gar]
			xs[index].append(garb1)
			xs[index].append(garb2)
			if not raining:
				xs[index].append(garb3)
	else:
		for index, item in enumerate(plot_latlon_aerosol):
			xs[index].append(list(np.linspace(min_bin, max_bin+1, num_bins+1)))
			xs[index].append(list(np.linspace(min_bin, max_bin+1, num_bins+1)))
			if not raining:
				xs[index].append(list(np.linspace(min_bin, max_bin+1, num_bins+1)))
	
	if barplot:
		print len(xs[0])
		return xs, temp, temp2
	
	#Removing zeroes and averaging in the nested loop.  Replacing one list with the average of the list at level 3 (k)
	for i in range(len(temp)):
		for j in range(len(temp[i])):
			for k in range(len(temp[i][j])):
				if yvar not in ALLOW_ZEROES:
					#Remove all zeroes
					for zero in range(temp[i][j][k].count(0)):
						index = temp[i][j][k].index(0)
						temp[i][j][k].pop(index)
						temp2[i][j][k].pop(index)
				
				#temp[i][j][k] =  filter(lambda a: a != 0, temp[i][j][k])
				#Take average if there is data there
				if len(temp[i][j][k]) > 0:
					temp_st_dev[i][j][k] = np.std(temp[i][j][k])
					temp[i][j][k] = sum(temp[i][j][k])/ len(temp[i][j][k])
					temp2[i][j][k] =sum(temp2[i][j][k])/ len(temp2[i][j][k])
				#Otherwise list is zero
				else:
					
					temp[i][j][k] = 0.0
					temp2[i][j][k] = 0.0
			

			
	
	for i in range(len(temp)):

		for j in range(len(temp[i])):

			for zero in range(temp[i][j].count(0.0)):	
				
				index = temp[i][j].index(0.0)
				
				try:
					
					temp_st_dev[i][j].pop(index)
					xs[i][j].pop(index)
					temp[i][j].pop(index)
					temp2[i][j].pop(index)
					print len(xs[0][0]), len(temp[0][0])
				except IndexError, e:
					print "Could not pop 163"
					print i, j, index
					print len(xs), len(xs[i]), len(xs[i][j])
					sys.exit()
			
	print len(xs[0][0]), len(temp[0][0]),"\n", xs[0][0],"\n",temp[0][0]
			
	#print len(xs), len(temp), len(temp2), len(temp_st_dev)
	return xs, temp, temp2, temp_st_dev
						
#For sorting data when only plotting a binned variable 
def aodidx(xs, ys, cs, bin_dict,aerosol,plotting_dict, plot_latlon_aerosol, xvar, comparing, aero2, yvar, touching,other_layer, barplot):
	global INCLUDE_END_AIS
	global rr_bin_dict
	global geo_bin_dict
	global lwp_bin_dict

	print "aodidx"
	min_bin = 100
	max_bin = 0
	num_bins = 0 
	temp_max = 0
	
	
	for x in xs:
		if max(x) - min(x) > num_bins:
			num_bins = int(max(x) - min(x))
		if max(x) > max_bin:
			max_bin = int(max(x))
		if min(x) < min_bin:
			min_bin = int(min(x))
	if xvar == "CloudSat_rain":
		min_bin = -1
		max_bin = 4
		num_bins = 3
		INCLUDE_END_AIS = False
	
	temp = [ [ [] for i in range(num_bins + 1)] for j in range(len(xs))]
	temp2 =  [ [ [] for i in range(num_bins + 1)] for j in range(len(xs))]
	temp_st_dev =  [ [ [] for i in range(num_bins + 1)] for j in range(len(xs))]
	if xvar != "aodidx" and xvar != "CloudSat_rain":
		INCLUDE_END_AIS = True
	#print INCLUDE_END_AIS
	print "Removing all outliers"	
	
	for i,(x, y) in enumerate(zip(xs, ys)):
		mean = np.mean(y)
		stan_dev = np.std(y)
		high_limit = mean + (STAN_DEV_AMOUNT* stan_dev)
		low_limit = mean - (STAN_DEV_AMOUNT*stan_dev)
		if "pop" in yvar:
			high_limit = 10.0
			low_limit = -1.0
			print high_limit, low_limit
		for j,(px, py) in enumerate(zip(x, y)):
			if py > low_limit and py < high_limit:
				try:
					if not INCLUDE_END_AIS:
						if px > min_bin and px < max_bin:
							
							temp[i][int(px)].append(py)
							temp2[i][int(px)].append(cs[i][j])
					else:
						temp[i][int(px)].append(py)
						temp2[i][int(px)].append(cs[i][j])
						
				except IndexError:
					print "Could not pop 204"
					print "i:", i
					print "j:", j
					print px
					sys.exit()

	xs = [ [] for i in range(len(plot_latlon_aerosol))]
	if xvar == "aodidx":
		for index, item in enumerate(plot_latlon_aerosol):
			garb = bin_dict[item[0]][item[1]][min_bin:(max_bin+2)]
			garb = [(x+y)/2 for x,y in zip(garb[:-1], garb[1:])]
			xs[index]= garb
	elif xvar == "CloudSat_rain":
		for i,item in enumerate(plot_latlon_aerosol):
			garb = rr_bin_dict[item[2]][item[0]]
			garb = [(x+y)/2 for x,y in garb]
			print garb, "\n", len(garb)
			xs[i] = garb
	elif xvar == "AMSR-e_LWP":
		for i, item in enumerate(plot_latlon_aerosol):
			print item[2], item[0]
			print min_bin, max_bin
			garb = lwp_bin_dict[item[2]][item[0]][min_bin:(max_bin+1)]
			garb = [(x+y)/2 for x,y in garb]
			xs[i] = garb
	else:
		for index, item in enumerate(plot_latlon_aerosol):
			xs[index] = list(np.linspace(min_bin, max_bin + 1, num_bins + 1))
	if barplot:
		#print "here at 305"
		return xs, temp, temp2
		
	averages = []
	for i in range(len(temp)):
		for j in range(len(temp[i])):
			if yvar not in ALLOW_ZEROES:
				print yvar
				temp[i][j]=  filter(lambda a: a != 0, temp[i][j])
			if len(temp[i][j]) > 0:
				temp_st_dev[i][j] = np.std(temp[i][j])
				temp[i][j] = sum(temp[i][j]) / float(len(temp[i][j]))
				print temp[i][j]
				temp2[i][j] = sum(temp2[i][j])/float(len(temp2[i][j]))
				averages.append(temp[i][j])
			else:
				temp[i][j] = 0.0
				temp2[i][j] = 0.0

	
	print np.mean(averages)
	for i in range(len(temp)):	
		for zero in range(temp[i].count(0.0)):
			try:
				temp_st_dev[i].pop(temp[i].index(0.0))
				xs[i].pop(temp[i].index(0.0))
				temp[i].remove(0.0)
				temp2[i].remove(0.0)
				print len(xs[i]), len(temp[i])
				
			except:
				print "Could not pop 248"
				print i
				print xs[i]
				print temp[i]
				sys.exit()
	if comparing:
		for index, item in enumerate(plot_latlon_aerosol):
			min_aero_bin = int(min(ys[i]))
			max_aero_bin = int(max(ys[i]))
		
			garb = bin_dict[item[0]][aero2][min_aero_bin:max_aero_bin]
			garb = [(x+y)/2 for x,y in zip(garb[:-1], garb[1:])]
			temp[index] = garb
	print temp
	print len(xs[0]), len(temp[0])
	return xs, temp, temp2, temp_st_dev

#To bin the y variable by every 10% of the x variable then average for every 10% bin
def bin_variable(xs, ys, cs, xvar, yvar, barplot):
	global num_percentiles
	if "cloud_fraction" in xvar or "albedo" in xvar:
		num_percentiles = 11
	temp_x = [[[] for i in range(num_percentiles)] for k in range(len(xs))]
	temp_y = [[[] for i in range(num_percentiles)] for k in range(len(ys))]
	temp_c = [[[] for i in range(num_percentiles)] for k in range(len(cs))]
	temp_st_dev = [[[] for i in range(num_percentiles)] for k in range(len(xs))]
	
	for i in range(len(xs)):
		xs[i], ys[i],cs[i] = (list(t) for t in zip(*sorted(zip(xs[i], ys[i],cs[i]))))

	
	
	for i in range(len(xs)):
		ps = list(np.linspace(0.0,100.00,num_percentiles))
		print ps
		if xvar not in ALLOW_ZEROES:
			temp_per =  filter(lambda a: a != 0, xs[i])
		else:
			
			temp_per = xs[i]
		#percentiles = np.percentile(temp_per,ps,'midpoint')
		percentiles = list(np.percentile(temp_per,ps, axis=None, out=None, overwrite_input=False, interpolation='midpoint', keepdims=False))
		print "percentiles", percentiles
		print np.mean(temp_per)
	#percentiles = np.percentile(xs[i],[0,10,20,30,40,50,60,70,80,90,100])
		for j in range(len(xs[i])):
			x = xs[i][j]
			y = ys[i][j]
			c = cs[i][j]
			for m, p in enumerate(percentiles[:-1]):
				if x <= percentiles[m+1] and x >= p:
					temp_x[i][m].append(x)
					temp_y[i][m].append(y)
					temp_c[i][m].append(c)
	print "Removing all outliers"
	#Remove all outliers
	for i,y in enumerate(temp_y):
		for j, lwp in enumerate(y):
			mean = np.mean(lwp)
			stan_dev = np.std(lwp)
			high_limit = mean + (STAN_DEV_AMOUNT* stan_dev)
			low_limit = mean - (STAN_DEV_AMOUNT*stan_dev)
			print "371", len(temp_y[i][j])
			for k, py in enumerate(lwp):
				if py > high_limit or py< low_limit:
					temp_y[i][j].pop(k)
					temp_x[i][j].pop(k)
					temp_c[i][j].pop(k)
			
	for i in range(len(temp_x)):
		for j in range(len(temp_x[i])):
			if yvar not in ALLOW_ZEROES:
				#Remove all zeroes
				temp_x[i][j] = filter(lambda a: a != 0, temp_x[i][j])
				temp_y[i][j] = filter(lambda a: a != 0, temp_y[i][j])
				temp_c[i][j] = filter(lambda a: a != 0, temp_c[i][j])
	
	if barplot:
		return [percentiles], temp_y, temp_c	
	averages = []
	for i in range(len(temp_x)):
		for j in range(len(temp_x[i])):	
			if len(temp_x[i][j]) > 0:
				temp_st_dev[i][j] = np.std(temp_y[i][j])
				#Find average without zeroes
				temp_x[i][j] = sum(temp_x[i][j])/len(temp_x[i][j])
				temp_y[i][j] = sum(temp_y[i][j])/len(temp_y[i][j])
				temp_c[i][j] = sum(temp_c[i][j])/len(temp_c[i][j])
				averages.append(temp_y[i][j])
			else:
				temp_x[i][j] = 0.0
				temp_y[i][j] = 0.0
				temp_c[i][j] = 0.0
				temp_st_dev[i][j] = 0.0
		for zero in range(temp_x[i].count(0.0)):
			temp_x[i].remove(0.0)
			temp_y[i].remove(0.0)
			temp_c[i].remove(0.0)
			temp_st_dev[i].remove(0.0)

	print np.mean(averages)
	
	return temp_x, temp_y, temp_c, temp_st_dev

#To bin the y variable by every 10% of the x variable then average for every 10% bin while binning by every 33% of the LWP	
def separate_bin_variable(xs, ys, cs, ls, percenvar, xvar, yvar, barplot,PoP):
	global ALLOW_ZEROES
	raining = False
	#print percenvar
	
	temp_x = [[[[] for i in range(num_percentiles)] for j in range(3)] for k in range(len(xs))]
	temp_y = [[[[] for i in range(num_percentiles)] for j in range(3)] for k in range(len(ys))]
	temp_c = [[[[] for i in range(num_percentiles)] for j in range(3)] for k in range(len(cs))]
	temp_st_dev = [[[[] for i in range(num_percentiles)] for j in range(3)] for k in range(len(xs))]
	
	
	for i in range(len(xs)):
		xs[i], ys[i],cs[i], ls[i] = (list(t) for t in zip(*sorted(zip(xs[i], ys[i],cs[i], ls[i]))))
		
	for i in range(len(xs)):
		if xvar not in ALLOW_ZEROES:
			temp_xs =  filter(lambda a: a != 0, xs[i])
		else:
			temp_xs = xs[i]
		ps = list(np.linspace(0,100,num_percentiles))
		percentiles = np.percentile(temp_xs,ps,interpolation="midpoint")
	
		#percentiles = np.percentile(temp_xs,[0,10,20,30,40,50,60,70,80,90,100])
		temp_per =  filter(lambda a: a != 0, ls[i])
		lwp_percentiles = np.percentile(temp_per, [0,33,66,100])
		if "rridx" in percenvar or "rain" in percenvar or PoP:
			temp_x = [[[[] for i in range(num_percentiles)] for j in range(2)] for k in range(len(xs))]
			temp_y = [[[[] for i in range(num_percentiles)] for j in range(2)] for k in range(len(ys))]
			temp_c = [[[[] for i in range(num_percentiles)] for j in range(2)] for k in range(len(cs))]
			temp_st_dev = [[[[] for i in range(num_percentiles)] for j in range(2)] for k in range(len(xs))]
			raining = True
		#print lwp_percentiles
		for j in range(len(xs[i])):
			x = xs[i][j]
			y = ys[i][j]
			c = cs[i][j]
			l = ls[i][j]
			if raining:
				print "Separating by raining and nonraining"
				if l > 0:
					n = 1
				else:
					n = 1
				for m, p in enumerate(percentiles[:-1]):
					if x < percentiles[m+1] and x > p:
						temp_x[i][n][m].append(x)
						temp_y[i][n][m].append(y)
						temp_c[i][n][m].append(c)
			else:
				for n, p2 in enumerate(lwp_percentiles[:-1]):
					#print p2, lwp_percentiles[n+1]
					if l < lwp_percentiles[n+1] and l >= p2:
						#print l
						for m, p in enumerate(percentiles[:-1]):
							if x < percentiles[m+1] and x > p:
								temp_x[i][n][m].append(x)
								temp_y[i][n][m].append(y)
								temp_c[i][n][m].append(c)
				
	 
	
	#Remove all outliers
	print "Removing all outliers"
	for i,y in enumerate(temp_y):
		for j, lwp in enumerate(y):
			for m, percentile in enumerate(lwp):
				mean = np.mean(percentile)
				stan_dev = np.std(percentile)
				high_limit = mean + (STAN_DEV_AMOUNT* stan_dev)
				low_limit = mean - (STAN_DEV_AMOUNT*stan_dev)
				for k, py in enumerate(percentile):
					if py > high_limit or py< low_limit:
						
						temp_y[i][j][m].pop(k)
						temp_x[i][j][m].pop(k)
						temp_c[i][j][m].pop(k)
	
	if barplot:
		return temp_y, temp_x, temp_c
	for i in range(len(temp_x)):
		for j in range(len(temp_x[i])):
			for k in range(len(temp_x[i][j])):
				#Remove all zeroes if impossible for variable to be a zero
				if yvar not in ALLOW_ZEROES:
					for zero in range(temp_x[i][j][k].count(0)):
						index = temp_x[i][j][k].index(0)
						temp_x[i][j][k].pop(index)
						temp_y[i][j][k].pop(index)
						temp_c[i][j][k].pop(index)
				
				if len(temp_x[i][j][k]) > 0 and not PoP:
					
					#print len(temp_x[i][j][k]), len(temp_y[i][j][k]), len(temp_c[i][j][k])
					temp_st_dev[i][j][k] = np.std(temp_y[i][j][k])
					#Find average without zeroes
					temp_x[i][j][k] = sum(temp_x[i][j][k])/len(temp_x[i][j][k])
					temp_y[i][j][k] = sum(temp_y[i][j][k])/len(temp_y[i][j][k])
					temp_c[i][j][k] = sum(temp_c[i][j][k])/len(temp_c[i][j][k])
				else:
					"not long enough"
					temp_x[i][j][k] = -9999.00
					temp_y[i][j][k] = 0.0
					temp_c[i][j][k] = 0.0
					temp_st_dev[i][j][k] = 0.0
			for zero in range(temp_x[i][j].count(-9999.00)):
				#print "removing zero"
				t_index = temp_x[i][j].index(-9999.00)
				#print t_index
				temp_x[i][j].pop(t_index)
				temp_y[i][j].pop(t_index)
				temp_c[i][j].pop(t_index)
				temp_st_dev[i][j].pop(t_index)
	
	return temp_x, temp_y, temp_c, temp_st_dev


#find the percent of total y in each bin, and pop all empy lists, makes a width array
def sort_lists(xs,ys, separating, percenvar):
	print "Sorting lists"
	totaly = 0
	width = [ [] for l in range(len(xs)) ]
	absolute_total = 0
	number_total = 0
	sum_all = 0.0
	print len(xs[0])
	for i, y in enumerate(ys):
		
		for j,bin in enumerate(y):
			sum_all += sum(bin)
			number_total += len(bin)
			for py in ys[i][j]:
				absolute_total += py
		
	for i, y in enumerate(ys):
		print y[7]
		for j, bin in enumerate(y):
			absolute_sum = 0
			number_bin = len(bin)
			for py in bin:
				absolute_sum += py
			

			try:
				if separating:
					if percenvar == "CloudSat_rain" or percenvar == "amsrrridx":
						print "Assigning a raining weight"
						weight =((.1/2) * number_total) / float(number_bin)
					else:
						weight = ((.1/3) * number_total) / float(number_bin)
				else:
					weight = (.1 * number_total) / float(number_bin)
				ys[i][j] = float(absolute_sum) / float(absolute_total) * weight
				
			except:
				ys[i][j] = 0 
		xs[i] = [0+(.1*wn) for wn,w in enumerate(y)]
		width[i] = [.1 for w in xs[i]]
		
	
		for zero in range(y.count(0)):
		
			index = ys[i].index(0)
			
			ys[i].pop(index)
			xs[i].pop(index)
			width[i].pop(index)
	ongoing_sum = 0
	for y in ys:
		ongoing_sum += sum(y)
	print "total percentage:", ongoing_sum
	
	return xs,ys,width



def nested_colors(cs):
	overall_min = 1000
	overall_max = 0
	for l in cs:
		
		if min(l) < overall_min:
			overall_min = min(l)
		if max(l) > overall_max:
			overall_max = max(l)
	for i in range(len(cs)):
		for j in range(len(cs[i])):
			cs[i][j] = rgb(overall_min, overall_max, cs[i][j])		
			
	return cs
	

def bar_plot(xs, ys, cs, ls, xvar, yvar, colorvar, percenvar, bin_dict,aerosol,plotting_dict, plot_latlon_aerosol,comparing,aero2, touching, other_layer, separating,PoP):
	global errorbar
	global INCLUDE_END_AIS 
	global STAN_DEV_AMOUNT
	barplot = True
	#if xvar in BINNED_VARIABLES:
	if separating:
			width = [0,0,0]
			overall_min = 1000
			overall_max = 0
			INCLUDE_END_AIS = True
			xs, ys, cs = separate_aodidx(xs, ys, cs, ls, bin_dict,aerosol,plotting_dict, plot_latlon_aerosol, percenvar, yvar, xvar,touching,other_layer, barplot,PoP)
			print "length", len(xs[0])
			for i in range(len(xs)):
				print i
				xs,ys,width = sort_lists(xs[i],ys[i], separating, percenvar)
			
			print "Plotting"
			print len(xs), len(xs[0])
			#top = [0 for i in range(len(xs[0]))]
			plt.figure(1)
			if percenvar == "CloudSat_rain" or percenvar == "amsrrridx":
				labels = ["non-raining","raining"]
			else:
				labels = ["low","medium","high"]
			print len(xs)
			colors = ["r","b","g"][0:len(xs)]
			print colors
			for i,level in zip(range(len(xs)),colors):
				
				#plt.bar(xs[i],ys[i],width =width[i], color=level)
				if i == 0:
					#top = [t+z for t,z in zip(top,ys[i-1])]
					#plt.bar(xs[i],ys[i],bottom =top,width=width[i],color = "w", hatch = level)
					
					plt.bar(xs[i],ys[i],width = .025,color=level,label=labels[i])
				else:
					print i
					#plt.bar(xs[i],ys[i],width=width[i],color = "w", hatch = level)
					#plt.bar(xs[i]+(.1*i),ys[i],color=level)
					for (x,y,w) in zip(xs[i],ys[i],width[i]):
						
						plt.bar(x+(.025*i), y, width = .025, color=level)
					plt.bar(0,0,0,color=level,label= labels[i])
		
			xticks = [x + .05 for x in xs[0]]
			plt.xticks(xticks, ["10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"])
			fontP = FontProperties()
			fontP.set_size("small")
			plt.legend(prop=fontP, bbox_to_anchor=(1.1,1.05))
			#plt.show()
			
			
	else:
		if xvar in BINNED_VARIABLES:
			INCLUDE_END_AIS = True
			xs, ys, cs = aodidx(xs, ys, cs, bin_dict,aerosol,plotting_dict, plot_latlon_aerosol, xvar, comparing, aero2, yvar, touching,other_layer, barplot)
		else:
			print "Not a binned variable"
			xy, ys, cs = bin_variable(xs, ys, cs, xvar, yvar, barplot)
			
		xs,ys,width = sort_lists(xs,ys, separating, "")
		#print xs, ys, width
		for (x,y,w) in zip(xs,ys,width):
			plt.bar(x,y,width = w,color = "#FF9933")
		#print ys

			#plt.show()
	'''else:
		if separating:
			xs, ys, cs = separate_bin_variable(xs, ys, cs, ls, percenvar, xvar, yvar, barplot,PoP)
			for i in range(len(xs)):
				xs,ys,width = sort_lists(xs[i],ys[i])
				
				#xs,ys,width = sort_lists(xs[i],ys[i])
			sys.exit()
				
			top = [0 for i in range(len(xs[0]))]
			for i,level in zip(range(len(xs)),["/","o","-"]):
				if i > 0:
					top = [t+z for t,z in zip(top,ys[i-1])]
					print top
					plt.bar(xs[i],ys[i],bottom =top,width=width[i],color = "w", hatch = level)
				else:
					
					plt.bar(xs[i],ys[i],width=width[i],color = "w", hatch = level)
					
			#plt.show()
		else:
			xs, ys, cs = bin_variable(xs, ys, cs, xvar, yvar, barplot,PoP)
			xs,ys,width = sort_lists(xs,ys)
			
			for (x,y,c,w) in zip(xs, ys, width):
				plt.bar(x, y, width = w)

			#plt.show()
	'''
	#Formatting title of plot
	xticks = [x + .05 for x in xs[0]]
	plt.xticks(xticks, ["10%","20%","30%","40%","50%","60%","70%","80%","90%","100%"])
	fontP = FontProperties()
	fontP.set_size("small")
	plt.legend(prop=fontP, bbox_to_anchor=(1.1,1.05))
	if INCLUDE_END_AIS:
		print "Including all AI bins"
		INCLUDE_END_AIS = "_all_points_"
	else:
		INCLUDE_END_AIS = ""

	
	if errorbar:
		errorbar = "_errbr_"
	else:
		errorbar = ""
	if separating:
		separating = "_lmh_"+percenvar+"_"
	else:
		separating = ""
	if PoP:
		yvariable = "PoP"
	plt.xlabel(xvar)
	plt.ylabel(yvar)
	plt.title(plot_latlon_aerosol[0][0])
	print "Saving figure"
	output_name =layer + "_"+plot_latlon_aerosol[0][0]+"_" + "_" + separating + aerosol[0] + "_color_"  + colorvar +"_"+ errorbar +xvar+ "_vs_" + yvar + "_"+ str(STAN_DEV_AMOUNT) + "_std_" + INCLUDE_END_AIS 	
	
	plt.savefig("./barplots/"+output_name+".png")	
	print "eog ./barplots/"+output_name+".png"
		
	sys.exit()
	


def separate_regress_colors(xs, ys, cs):
#Stuff for color bar
	lowest_color = 1000
	highest_color = 0
	#Creating lines for plot
	print "Calculating linear regression"
	regress = [[[] for j in range(3)] for i in range(len(xs))]
	line = [[[] for j in range(3)] for i in range(len(xs))]
	for j, (xlwp, ylwp) in enumerate(zip(xs, ys)):
		for i, (x, y) in enumerate(zip(xlwp, ylwp)):
			regress[j][i] = linregress(x,y)	
			line[j][i] = [ p * regress[j][i][0] + regress[j][i][1] for p in x]
	
	#Creating the colorbar array and ticks
	for i in cs:
		for j in i:
			if max(j) > highest_color:
				highest_color = max(j)
			if min(j) < lowest_color:
				lowest_color = min(j)
	ts = np.linspace(lowest_color, highest_color, 7)
	ts = [round(i,2) for i in ts]
	print "Choosing prettiest colors"
	return regress, line, ts, lowest_color, highest_color

def regress_colors(xs, ys, cs):
	#print "here"
	lowest_color = 10000
	highest_color = 0
	#Creating lines for plot
	print "Calculating linear regression"
	regress = [[]for i in range(len(xs))]
	line = [[]for i in range(len(xs))]
	for i, (x, y) in enumerate(zip(xs, ys)):
		regress[i] = linregress(x,y)	
		line[i] = [ p * regress[i][0] + regress[i][1] for p in x]
	
	#Creating the colorbar array and ticks
	for i in cs:
		if max(i) > highest_color:
			highest_color = max(i)
		if min(i) < lowest_color:
			lowest_color = min(i)
	ts = np.linspace(lowest_color, highest_color, 7)
	ts = [round(i,2) for i in ts]
	print "Choosing prettiest colors"
	return regress, line, ts, lowest_color, highest_color

########################################################################################################################
########################################################################################################################

#For AI limits
dumped_file = open(BIN_FILE, "r")
bin_dict = pickle.load(dumped_file)

#For lwp limits
dumped_file = open("lwp_bin_dict.js","r")
lwp_bin_dict = json.load(dumped_file)

#Geo limits
dumped_file = open("geo_bin_dict.js","r")
geo_bin_dict = json.load(dumped_file)

#RRidx limits
dumped_file = open("rridx_bin_dict.js","r")
rr_bin_dict = json.load(dumped_file)

#AI Dump bin 
dumped_file = open("AI_comparison.js","r")
aerosol_index_dict = json.load(dumped_file)
#print aerosol_index_dict["0-0"]["touching"]["SPRINTARSSUvsMODISAOD"]['MODISAOD']

#If plotting albedo, can also put cloud fraction on same axis
cfs_on = False

output_name = ""

title = ""

##Plotting variables
xvariable = ""
yvariable = ""
colorvar = ""
aerosol = []

temp = []
plotting_dicts = {}
#Only used when testing
percenvar = "lwpidx"

aerosol_1 = ""
aerosol_2 = ""
if len(sys.argv) > 1:
	args = sys.argv[1:]
else:
	args = False

##Finding the region(s) to plot
for i,filename in enumerate(glob.glob(ROOT_DIR+"*.txt")):
	item = filename.replace(ROOT_DIR,"").strip(".txt")
	temp.append(item)
if args:
	user_input = args[0]
	print args[0]
	
else:
	for i,item in enumerate(temp):
		print LETTERS[i],")", item
	user_input = raw_input("Which region would you like to plot? If more than one, separate with ,\n")
	user_input = user_input.split(",")
	for item in user_input:
		check(temp,item)
for item in user_input:
	item = temp[LETTERS.index(item)]

	with open(ROOT_DIR + item + ".txt","r") as source:
		result = json.load(source)
	plotting_dicts[item] = result

	

#Seeing if comparing aerosols
if args:
	user_input = args[1]
else:
	user_input = raw_input("Would you like to compare two aerosol's variables?\na) yes\nb) no\n")
	check(["a","b"],user_input)
if user_input == "a":
	comparing = True
else:
	comparing = False

if comparing:	
	separate = False
	#Seeing what aerosols to compare
	temp = get_keys(plotting_dicts, 3)

	for i, aero in enumerate(temp):
		print LETTERS[i], ")", aero
	user_input = raw_input("Which aerosol would you like to have as the x axis? \n")
	check(temp, user_input)
	aerosol_1 = temp[LETTERS.index(user_input)]
	#Second aerosol
	for i, aero in enumerate(temp):
		print LETTERS[i], ")", aero
	user_input = raw_input("Which aerosol would you like to have as the y axis? \n")
	check(temp, user_input)
	aerosol_2 = temp[LETTERS.index(user_input)]
	
	#Finding the xvariable to plot
	temp = get_keys(plotting_dicts, 4)

	for i, var in enumerate(temp):
		print LETTERS[i], ")", var
	user_input = raw_input("What is your xvariable? \n")
	check(temp, user_input)
	xvariable = temp[LETTERS.index(user_input)]
	yvariable = temp[LETTERS.index(user_input)]
	if xvariable == "aodidx":
		flag = True
	else: 
		flag = False
	
else:
	temp = get_keys(plotting_dicts, 3)
	if args:
		user_input = args[2]
	else:
		#Finding the aerosol(s) to plot
		for i, aero in enumerate(temp):
			print LETTERS[i], ")", aero
		user_input = raw_input("Which aerosol would you like to plot? If more than one, separate with , \n")
		user_input = user_input.split(",")
	for item in user_input:
		check(temp, item)
		item = temp[LETTERS.index(item)]
		aerosol.append(item)
		print item
	#Finding the xvariable to plot
	temp = get_keys(plotting_dicts, 4)
	temp = sorted(temp)
	if args:
		user_input = args[3]
		
	else:
		total = len(temp)
		temp1 = temp[:total/3]
		temp2 = temp[total/3:2*total/3]
		temp3 = temp[2*total/3:]
		for (var1, var2, var3) in zip(temp1, temp2, temp3):
			i = str(LETTERS[temp.index(var1)] + ") " + var1)
			j = LETTERS[temp.index(var2)] + ") " + var2
			k = LETTERS[temp.index(var3)] + ") " + var3
			#print LETTERS[i], ")", var1, "\t",LETTERS[j],")",var2,"\t",LETTERS[k],")",var3
			print ("{: <25} {: <25} {: <25}").format(i,j,k)
		user_input = raw_input("What is your xvariable? \n")
		check(temp, user_input)
	xvariable = temp[LETTERS.index(user_input)]
	print xvariable

	#Finding the xvariable to plot
	temp = get_keys(plotting_dicts, 4)
	temp = sorted(temp)
	if args:
		user_input = args[4]
		
	else:
		total = len(temp)
		temp1 = temp[:total/3]
		temp2 = temp[total/3:2*total/3]
		temp3 = temp[2*total/3:]
		for (var1, var2, var3) in zip(temp1, temp2, temp3):
			i = str(LETTERS[temp.index(var1)] + ") " + var1)
			j = LETTERS[temp.index(var2)] + ") " + var2
			k = LETTERS[temp.index(var3)] + ") " + var3
			#print LETTERS[i], ")", var1, "\t",LETTERS[j],")",var2,"\t",LETTERS[k],")",var3
			print ("{: <25} {: <25} {: <25}").format(i,j,k)
		user_input = raw_input("What is your yvariable? \n")
		check(temp, user_input)
	yvariable = temp[LETTERS.index(user_input)]
	print yvariable
	'''
	if "albedo" in yvariable and "aodidx" == xvariable:
		user_input = raw_input("Would you like to compare with cloud fraction?\na) yes\nb) no\n")
		check(["a","b"],user_input)
		if user_input == "a":
			print 
			cfs_on = True
		'''
	#To see if they want PoP
	if args:
		user_input = args[5]
	else:
		user_input = raw_input("Would you like your yvariable instead to be PoP?\na) yes\nb) no\n")
		check(["y","n"],user_input)
	if user_input == "a":
		separate = True
		percenvar = "CloudSat_rain"
		PoP = True
	#Cannot separate by a percentage if calculating PoP
	else:

		#Finding if separating by low med high LWP
		if args:
			user_input = args[6]
		else:
			user_input = raw_input("Would you like to separate by low, medium, high lwp?\na) yes\nb) no\n")
			check(["y","n"], user_input)
		if user_input == "a":
			separate = True
			print "Separating"
		else:
			separate = False
		if separate == True:
			#Which high, med, low ones to show
			if args:
				user_input = args[7]
			else:
				for i,one in enumerate(["low","medium","high"]):
					print LETTERS[i],")",one
				user_input = raw_input("Which one(s) would you like to show? Separate by a , if more than one\n")
			try:
				user_input = user_input.split(",")
			except:
				print "Wrong input"
			to_show = []
			for item in user_input:
				print item
				check(["low","med","high"], item)
				to_show.append(LETTERS.index(item))
			print to_show
			#Finding the separating variable
			#Finding the colorbar variable to plot
			temp = get_keys(plotting_dicts, 4)
			temp = sorted(temp)
			if args:
				user_input = args[8]
			else:
				total = len(temp)
				temp1 = temp[:total/3]
				temp2 = temp[total/3:2*total/3]
				temp3 = temp[2*total/3:]
				for (var1, var2, var3) in zip(temp1, temp2, temp3):
					i = str(LETTERS[temp.index(var1)] + ") " + var1)
					j = LETTERS[temp.index(var2)] + ") " + var2
					k = LETTERS[temp.index(var3)] + ") " + var3
					#print LETTERS[i], ")", var1, "\t",LETTERS[j],")",var2,"\t",LETTERS[k],")",var3
					print ("{: <25} {: <25} {: <25}").format(i,j,k)
				user_input = raw_input("What is your sorting variable? \n")
				check(temp, user_input)
			percenvar = temp[LETTERS.index(user_input)]
		PoP = False
	
		
		
	

#Finding whether to print the errorbar or not
if args:
	user_input = args[9]
else:
	user_input = raw_input("Would you like to have errorbars?\na) yes\nb) no\n")
	check(["y","n"],user_input)
if user_input == "a":
	errorbar = True
else:
	errorbar = False

#Finding the colorbar variable to plot
temp = get_keys(plotting_dicts, 4)
temp = sorted(temp)
if args:
	user_input = args[10]
else:
	total = len(temp)
	temp1 = temp[:total/3]
	temp2 = temp[total/3:2*total/3]
	temp3 = temp[2*total/3:]
	for (var1, var2, var3) in zip(temp1, temp2, temp3):
		i = str(LETTERS[temp.index(var1)] + ") " + var1)
		j = LETTERS[temp.index(var2)] + ") " + var2
		k = LETTERS[temp.index(var3)] + ") " + var3
		#print LETTERS[i], ")", var1, "\t",LETTERS[j],")",var2,"\t",LETTERS[k],")",var3
		print ("{: <25} {: <25} {: <25}").format(i,j,k)
	user_input = raw_input("What is your color variable? \n")
	check(temp, user_input)
colorvar = temp[LETTERS.index(user_input)]



#Finding which touching variable to plot
temp = get_keys(plotting_dicts, 2)
temp = sorted(temp)
if args:
	user_input = args[11]
else:
	for i,layer in enumerate(temp):
		print LETTERS[i],")",layer
	user_input = raw_input("Which aerosol layer do you want to plot?\n")
	check(temp, user_input)
layer = temp[LETTERS.index(user_input)]
if "day" in layer or "night" in layer:
	user_input = raw_input("Would you like to compare to the corresponding time?\na) yes\nb) no\n")
	check(["a","b"],user_input)
	if user_input == "a":
		if "day" in layer:
			layer2 = layer.replace("day","night")
		elif "night" in layer:
			layer2 = layer.replace("night","day")
		other_layer = True
	else:
		other_layer = False
		
else:
	other_layer = False
if not PoP:
	if args:
		user_input = args[12]
	else:
		user_input = raw_input("Would you like to make it a bar plot?\na)yes\nb)no\n")
		check(["a","b"],user_input)
	if user_input == "a":
		barplot = True
		INCLUDE_END_AIS = True
	else:
		barplot = False
else:
	barplot = True
	INCLUDE_END_AIS = True



#Create a list for each aerosol being plotted
xs = []
ys = []
cs = []
ls = []
#If cfs
cfs = []
#List of standard deviations for each point
st = []
#Titles for plotting
plot_latlon_aerosol = []


print "Fetching data"
for key in plotting_dicts.keys():
	title = key + " " + title
	output_name = key + "_"
	if comparing and not flag:
		plot_latlon_aerosol.append([key,aerosol_1,layer])
		xs.append(plotting_dicts[key]["noerror"][layer][aerosol_1][xvariable])
		ys.append(plotting_dicts[key]["noerror"][layer][aerosol_2][xvariable])
		cs.append(plotting_dicts[key]["noerror"][layer][aerosol_1][colorvar])
		if other_layer:
			plot_latlon_aerosol.append([key,aerosol_1,layer2])
			xs.append(plotting_dicts[key]["noerror"][layer2][aerosol_1][xvariable])
			ys.append(plotting_dicts[key]["noerror"][layer2][aerosol_2][xvariable])
			cs.append(plotting_dicts[key]["noerror"][layer2][aerosol_1][colorvar])
	if comparing and flag:
		plot_latlon_aerosol.append([key,aerosol_1])
		
		aerosol_2 = "MODISAI"
		try:
			print aerosol_index_dict[key][layer][aerosol_1+"vs"+aerosol_2][aerosol_1]
			xs.append(aerosol_index_dict[key][layer][aerosol_1+"vs"+aerosol_2][aerosol_1])
			ys.append(aerosol_index_dict[key][layer][aerosol_1+"vs"+aerosol_2][aerosol_2])
		except:
			try: 
				xs.append(aerosol_index_dict[key][layer][aerosol_2+"vs"+aerosol_1][aerosol_1])
				ys.append(aerosol_index_dict[key][layer][aerosol_2+"vs"+aerosol_1][aerosol_2])
				print aerosol_index_dict[key][layer][aerosol_2+"vs"+aerosol_1][aerosol_2]
			except:
				sys.exit()
	else:
		for aero in aerosol:
			
			plot_latlon_aerosol.append([key,aero,layer])
			xs.append(plotting_dicts[key]["noerror"][layer][aero][xvariable])
			ys.append(plotting_dicts[key]["noerror"][layer][aero][yvariable])
			cs.append(plotting_dicts[key]["noerror"][layer][aero][colorvar])
			#Temp code to get the conditional means for a region
			'''temp = filter(lambda a: a != 0, xs[-1])
			print np.mean(temp)
			temp = filter(lambda a: a != 0, ys[-1])
			print np.mean(temp)
			sys.exit()'''
			print key, np.mean(plotting_dicts[key]["noerror"][layer][aero]["CWV"])
			
			if other_layer:
				
				plot_latlon_aerosol.append([key,aerosol_1,layer2])
				xs.append(plotting_dicts[key]["noerror"][layer2][aero][xvariable])
				ys.append(plotting_dicts[key]["noerror"][layer2][aero][yvariable])
				cs.append(plotting_dicts[key]["noerror"][layer2][aero][colorvar])
			if separate:
				ls.append(plotting_dicts[key]["noerror"][layer][aero][percenvar])
			if cfs_on:
				print "Gathering cloud fractions"
				cfvariable = yvariable.split("_")[0] + "_cloud_fraction"
				cfs.append(plotting_dicts[key]["noerror"][layer][aero][cfvariable])
				
				

title = "\n" + title
if not comparing:
	for aero in aerosol:
		title = aero + " " + title
elif comparing and not flag:
	title = "comparing {} vs. {}".format(aerosol_1, aerosol_2) + " " + title
elif flag:
	title = "{} vs {} AOD".format(aerosol_1, aerosol_2)
title = "\n" + title
highest_color = 0
lowest_color = 1000
cf_highest_color = 0
cf_lowest_color = 1000
#Sorting the data
if flag == False:
	if barplot:
		bar_plot(xs, ys, cs, ls, xvariable, yvariable, colorvar, percenvar, bin_dict,aerosol,plotting_dicts, plot_latlon_aerosol,comparing,aerosol_2, layer, other_layer, separate,PoP)
		
	if xvariable in BINNED_VARIABLES:
		if separate:
		##### If comparing cf directly to albedo
			print "Sorting cloud fractions"
			if cfs_on:
				cf_xs = xs[:]
				cf_cs = cs[:]
				cf_ls = ls[:]
				cf_st = []
				print cfvariable
				cf_xs, cfs, cf_cs, cf_st = separate_aodidx(cf_xs, cfs, cf_cs, cf_ls, bin_dict, aerosol, plotting_dicts, plot_latlon_aerosol, percenvar, yvariable, xvariable,layer, other_layer,barplot,PoP)
				
				cf_regress, cf_line, cf_ts, cf_lowest_color, cf_highest_color = separate_regress_colors(cf_xs, cfs, cf_cs)
			
			# If plotting normally
			xs, ys, cs, st = separate_aodidx(xs, ys, cs, ls, bin_dict, aerosol, plotting_dicts, plot_latlon_aerosol, percenvar, yvariable, xvariable,layer, other_layer,barplot,PoP)
			#Creating lines for plot
			regress, line, ts, lowest_color, highest_color = separate_regress_colors(xs, ys, cs)
		
					
						
		else:
			#If comparing cf directly to albedo
			if cfs_on:
				cf_xs = xs[:]
				cf_cs = cs[:]
				cf_st = []
				cf_xs, cfs, cf_cs, cf_st = aodidx(cf_xs, cfs, cf_cs, bin_dict, aerosol, plotting_dicts, plot_latlon_aerosol, xvariable, comparing, aerosol_2, yvariable, layer, other_layer,barplot)
				
				cf_regress, cf_line ,cf_ts, cf_lowest_color, cf_highest_color = regress_colors(cf_xs, cfs, cf_cs)
			
			#If plotting normally
			xs, ys, cs, st= aodidx(xs, ys,cs, bin_dict, aerosol, plotting_dicts, plot_latlon_aerosol, xvariable, comparing, aerosol_2, yvariable, layer,other_layer,barplot)
			
			regress, line, ts, lowest_color, highest_color = regress_colors(xs, ys, cs)
	elif xvariable not in BINNED_VARIABLES:
		if separate:
			print "Separating into low, med, high"
			xs, ys, cs, st= separate_bin_variable(xs, ys, cs, ls, percenvar,xvariable, yvariable,barplot,PoP)
			#Creating lines for plot
			regress, line, ts, lowest_color, highest_color = separate_regress_colors(xs, ys, cs)
		else:
			xs, ys, cs, st = bin_variable(xs, ys, cs, xvariable, yvariable,barplot)
		
			regress, line, ts, lowest_color, highest_color = regress_colors(xs, ys, cs)
if cfs_on:
	
	if cf_highest_color > highest_color:
		highest_color = cf_highest_color
	if cf_lowest_color < lowest_color:
		lowest_color = cf_lowest_color
#Plotting separate LWPS
print "Plotting"
#Changing figure size
rcParams['figure.figsize'] = 13, 8 #width, height
lwp = ["low","med","high"]
if "rridx" in percenvar or "rain" in percenvar:
	to_show = [0,1]
	lwp = ["nonraining","raining"]
avg_slope = []
h = ["/","o","-"]

if separate and not flag:
	
	for i in range(len(xs)):
		for j in range(len(xs[i])):
			try:
				if j in to_show:
					temp_label =  "{},{}\n{}:{},{}".format(plot_latlon_aerosol[i][1],lwp[j],plot_latlon_aerosol[i][0],round(regress[i][j][0],3),round(regress[i][j][2],2))
					
					if other_layer:
						temp_label = "{},{},{}\n{}:{},{}".format(plot_latlon_aerosol[i][2],plot_latlon_aerosol[i][1],lwp[j],plot_latlon_aerosol[i][0],round(regress[i][j][0],3),round(regress[i][j][2],2))
					
					print "mean x:",np.mean(xs[i][j]), "mean y:",np.mean(ys[i][j])
					plt.scatter(xs[i][j], ys[i][j], marker = SEPARATE_MARKERS[i][j],c = cs[i][j], cmap = cm.jet,vmin = lowest_color , vmax = highest_color ,s = 300,label = temp_label)
					

					plt.plot(xs[i][j], line[i][j], "r-")
					avg_slope.append(regress[i][j][0])
					if cfs_on:
						temp_label = "CF {},{}\n{}:{},{}".format(plot_latlon_aerosol[i][1],lwp[j],plot_latlon_aerosol[i][0],round(cf_regress[i][j][0],3),round(cf_regress[i][j][2],2)) 
						plt.scatter(cf_xs[i][j], cfs[i][j], marker = SEPARATE_MARKERS[i+3][j], c = cf_cs[i][j], cmap = cm.jet, vmin= lowest_color, vmax = highest_color, s = 300, label = temp_label)
						plt.plot(cf_cs[i][j], cf_line[i][j], "b-")
						
					if errorbar:
						plt.errorbar(xs[i][j], ys[i][j], st[i][j])
						if cfs_on:
							plt.errorbar(cf_xs[i][j],cf_st[i][j])
			except IndexError:
				plt.scatter(xs[i][j], ys[i][j], marker = "o",c = cs[i][j], cmap = cm.jet,vmin = lowest_color , vmax = highest_color ,s = 300,label = "{},{}\n{}:{},{}".format(plot_latlon_aerosol[i][1],lwp[j],plot_latlon_aerosol[i][0],round(regress[i][j][0],3),round(regress[i][j][2],2)))
				plt.plot(xs[i][j], line[i][j], "r-")
				avg_slope.append(regress[i][j][0])
#Plotting not separated data
elif not separate and not flag:
	for i in range(len(xs)):
		#Create label
		temp_label = "{}\n{}:{},{}".format(plot_latlon_aerosol[i][1],plot_latlon_aerosol[i][0],round(regress[i][0],3),round(regress[i][2],2))
		
		
		#Make label if there is another layer being compared ie day vs night
		if other_layer:
			temp_label = "{},{}\n{}:{},{}".format(plot_latlon_aerosol[i][2],plot_latlon_aerosol[i][1],plot_latlon_aerosol[i][0],round(regress[i][0],3),round(regress[i][2],2))
		print "ys:", ys[i]
		#Plotting the first layer or only layer if not being compared
		plt.scatter(xs[i], ys[i], marker = MARKERS[i],c = cs[i], cmap = cm.jet,vmin = lowest_color , vmax = highest_color ,s = 300,label = temp_label)
		plt.plot(xs[i], line[i], "r-")
		#plt.show()
		print line[i]
		#If comparing albedo to cf
		if cfs_on:
			plt.scatter(cf_xs[i], cfs[i], marker = MARKERS[i+3],c = cf_cs[i], cmap = cm.jet,vmin = lowest_color , vmax = highest_color ,s = 300,label = "CF {}\n{}:{},{}".format(plot_latlon_aerosol[i][1],plot_latlon_aerosol[i][0],round(cf_regress[i][0],3),round(cf_regress[i][2],2)))
			plt.plot(cf_xs[i], cf_line[i],"b-")
		avg_slope.append(regress[i][0])
		if errorbar:
			plt.errorbar(xs[i],ys[i], yerr = st[i], linestyle = "")
			if cfs_on:
				plt.errorbar(cf_cs[i], cfs[i], yerr = cf_st[i], linestyle = "")
#If comparing two aerosols same variable, and variable is aodidx
if flag:
	rcParams['figure.figsize'] = 11,10 #width, height
	
	for i,(x,y) in enumerate(zip(xs, ys)):
		x = np.array(x)
		y = np.array(y)
		#plt.plot(x, y,label = str(plot_latlon_aerosol[i][0]))
		plt.quiver(x[:-1], y[:-1], x[1:]-x[:-1], y[1:]-y[:-1], scale_units='xy', angles='xy', scale=1.2)
		plt.xlabel(aerosol_1)
		plt.ylabel(aerosol_2)
		plt.title(aerosol_1+"vs"+aerosol_2)
		output_name = "touching_all_AODIDX_{}_vs_{}_{}_{}_std".format(aerosol_1, aerosol_2, plot_latlon_aerosol[i][0],STAN_DEV_AMOUNT)
		plt.savefig("./individual_plot/" + output_name + ".png")

if cfs_on:
	yvariable = yvariable + "_cloud_fraction"
	
art = []
if not flag:
	#Formatting strings
	if "MODISAI" in aerosol and xvariable == "aodidx":
		xvariable = "AOD"
	if xvariable == "aodidx" and 'MODISAI' not in aerosol:
		xvariable = "AI"
	
	if separate:
		temp = ""
		for j in to_show:
			temp = temp + lwp[j][0:1]
		separate = "{}_{}_".format(temp, percenvar)
	else:
		separate = "_all_"
	if errorbar:
		errorbar = "_errorbar_"
	else:
		errorbar = ""
	temp_title = title.replace("\n","")
	temp_title = temp_title.replace(" ","_")
	if xvariable == "AOD":
		if INCLUDE_END_AIS:
			INCLUDE_END_AIS = "_all_points_"
		else:
			INCLUDE_END_AIS = "_80%_"
	else:
		INCLUDE_END_AIS = ""
	
	output_name = layer + "_" + separate + temp_title + "_color_"  + colorvar +errorbar +xvariable + "_vs_" + yvariable + "_"+ str(STAN_DEV_AMOUNT) + "_std" + INCLUDE_END_AIS 
	if other_layer:
		output_name = output_name + "_day_v_nig"
	if comparing:
		if aerosol_1 == "MODISAI" and xvariable == "AI":
			xvariable = "aodidx"
		if aerosol_2 != "MODISAI" and yvariable == "aodidx":
			yvariable = "ai"
		xvariable = aerosol_1 + "\n" + xvariable
		yvariable = aerosol_2 + "\n" + yvariable
	

	#Setting labels	
	plt.xlabel(xvariable)
	if cfs_on:
		yvariable = yvariable.replace("_"," ")
	plt.ylabel(yvariable)
	if other_layer:
		title = title + "\nDay vs Night"
	plt.title(title)
	title = title.replace("\n","")
	if other_layer:
		title = title.replace("Day vs Night","day_v_night")
	#Messing with the legend
	avg_slope = round(sum(avg_slope)/len(avg_slope),3)
	if comparing:
		ncolumns = 1
	else:
		ncolumns = len(aerosol)
	#print avg_slope
	
	
	lgd = plt.legend(title = "Avg d{}d{}: {}".format(yvariable[:3],xvariable[:3],avg_slope),loc=9, bbox_to_anchor=(0.5, -0.1),ncol = ncolumns,scatterpoints = 1, prop={'size':12},borderpad =.09,labelspacing= .2, columnspacing = .10)
	
	art.append(lgd)

	clbr = plt.colorbar()
	clbr.set_label("Average {}".format(colorvar.replace("_"," ")))
	clbr.set_ticks(ts)

	plt.savefig("./individual_plot/"+output_name + ".png", bbox_inches = "tight", bbox_extra_artists = art)
#plt.show()

open_name = "eog individual_plot/"+output_name+".png &"
print open_name

subprocess.call(open_name.split(" "))

sys.exit()




