#!/usr/bin/env python

class aerosol(object):
 	
	def __init__(self,variables):
		self.species = str(variables[0])
		self.amsrrridx = variables[1]
		self.rridx = int(variables[2])
		self.aodidx = int(variables[3])
		self.geoidx = variables[4]
		self.lwpidx = int(variables[5])
		self.counter = variables[6]
		self.sfcwind = variables[7]
		self.cwv = variables[8]
		self.amsrsst = variables[9]
		self.ecsst = variables[10]
		self.merrasst = variables[11]
		self.ecltss = float(variables[12])
		self.merraltss = variables[13]
		self.merraomega = float(variables[14])
		self.pia = variables[15]
		self.fl = variables[16]
		
	def name(self, nm):
		self.name = nm
	def update(self, vars):
		self.cldtop = float(vars[0])
		self.cldthick = vars[1]
		self.cf11 = float(vars[2])
		self.cf21 = float(vars[3])
		self.cf51= float(vars[4])
		self.cf101 =float(vars[5])
		self.cswftoa = float(vars[6])
		self.clwftoa = float(vars[7])
		self.cswfsfc = vars[8]
		self.clwfsfc = vars[9]
	
	def __str__(self):
		return self.name
	
	def get(self, *args):
		values = []
		if args:
			for arg in args:
				values.append(self.__getattribute__(arg))
		return values


import linecache
import matplotlib.pyplot as plt
import matplotlib.spines as spn
import sys
import re
import os
from matplotlib import gridspec

#To sort POP
def sort_pop(conditions, compare_high_rridx, compare_pltx):
	overallRR = 0.0
	raining = 0.0
	tempbins = make_bins(eval(conditions[2][1]))
	for item in zip(compare_high_rridx, compare_pltx):		
		tempbins[item[1]].append(int(item[0]))
	compare_high_rridx = []
	for bin in tempbins:
		if len(bin) > 15:
			overallRR = 0.0
			raining = 0.0
			for item in bin:
				overallRR += 1.0
				if item > 0:
					raining +=1.0
			compare_high_rridx.append(raining/overallRR)
		else:
			compare_high_rridx.append(0)
		
	#print compare_high_rridx
	#sys.exit()
	return compare_high_rridx
		

#To check user input
def check(options,user_input, conditions, appending):
	letters = list(map(chr,range(97, 97+len(options))))
	if user_input in letters:
		conditions.append(appending[letters.index(user_input)])
		return
	else:
		print "That was bad input"
		sys.exit()


#Find the slope of points given X and Y
def slope(x,y):
	slopez = []
	for index in range((len(y)-1)):
		slopez.append( ( float(y[index+1]) - float(y[index]) ) / ( float(x[index+1]) - float(x[index]) ) )
	return slopez

#Create a y array for the slopes
def y_ticks_create(slope):
	y_ticks = []
	for i in range(len(slope)):
		y_ticks.append(i)
	return y_ticks


#Creates bins (lists inside of lists) to help sort
def make_bins(bin):
	size = len(bin)
	
	temp = []
	for i in range(size):
		temp.append([])
	return temp

#Give list x with bin and list y and returns bin with x sorted
def sort(pltx, plty):
    tempbins = make_bins(eval(conditions[2][1]))
    values = zip(pltx,plty)
    #print len(plty), len(pltx)
    for value in values:
        if value[1] != 0:
            tempbins[int(value[0])].append(float(value[1]))
    temp = [sum(tempbin)/len(tempbin) if len(tempbin) > 15 else 0 for tempbin in tempbins]
    return temp

#Find the average of a list of lists
def average(plty1, plty2):
	averages1 = []
	averages2 = []
	for (list1, list2) in zip(plty1, plty2):
		count1 = 0.0
		count2 = 0.0
		avg1 = 0.0
		avg2 = 0.0
		for (item1, item2) in zip(list1, list2):		
			
			if item1 != 0 and item2 != 0:
			
				count1 += 1.0
				count2 += 1.0
				avg1 += item1
				avg2 += item2
		try:
			avg1 = round((avg1/count1),3)
			avg2 = round((avg2/count2),3)
			averages1.append(avg1)
			averages2.append(avg2)
		except ZeroDivisionError:
			averages1.append(0.0)
			averages2.append(0.0)
		

	return averages1, averages2
				

#Find the average PoP
def average_pop(pop_list):
	average = 0.0
	count = 0
	for item in pop_list:
		if item != 0:
			average += item
			count +=1
	try:
		return average/count
	except:
		return 0


#Gets the points from the aerosol species
def get_points(overall, conditions):
	
	temp_x = []
	temp_y = [[],[],[],[],[],[],[],[],[]]
	####High and low variables
	low_rridx = []
	high_rridx = []
	
	if conditions[5] and not conditions[6]:
		temp_x2 = []
		temp_y2 = [[],[],[],[],[],[],[],[],[]]
	if conditions[5] and conditions[6]:
		temp_x1 = []
		temp_x2 = []
		temp_x3 = []
		temp_x4 = []
		temp_y1 =  [[],[],[],[],[],[],[],[],[]]
		temp_y2 =  [[],[],[],[],[],[],[],[],[]]
		temp_y3 =  [[],[],[],[],[],[],[],[],[]]
		temp_y4 =  [[],[],[],[],[],[],[],[],[]]
		temp_RR1 = []
		temp_RR2 = []
		temp_RR3 = []
		temp_RR4 = []
	average_ecltss = 0.0
	count = 0
	for n in range(len(overall)):
		for k in range(len(overall[n])):
			specimen = overall[n][k]
			if specimen.species == str(conditions[4]):
				if eval(conditions[7][0]):
					average_ecltss += specimen.ecltss
					count += 1
	average_ecltss = average_ecltss/count
	print "Average ecltss for region is:", average_ecltss
	

	for n in range(len(overall)):
		for k in range(len(overall[n])):
			specimen = overall[n][k]
			if specimen.species == str(conditions[4]):
				if eval(conditions[7][0]):
					#If comparing high and low AI index
					if conditions[5] and not conditions[6]:
						if specimen.aodidx < 4 :
							values = specimen.get("cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss")
							for (index,item) in enumerate(values):
								temp_y[index].append(item)
							low_rridx.append(specimen.rridx)
							temp_x.append(eval(conditions[2][0]))
						if specimen.aodidx > 6:
							values = specimen.get("cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss")
							for (index,item) in enumerate(values):
								temp_y2[index].append(item)
							high_rridx.append(specimen.rridx)
							temp_x2.append(eval(conditions[2][0]))
					#If sorting by AI and ECLTSS high and low
					elif conditions[5] and conditions[6]:
						if specimen.aodidx < 4 and specimen.ecltss <= average_ecltss:
							values = specimen.get("cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss")
							for (index, item) in enumerate(values):
								temp_y1[index].append(item)
							
							temp_x1.append(eval(conditions[2][0]))
							temp_RR1.append(specimen.rridx)
						if specimen.aodidx < 4 and specimen.ecltss >= average_ecltss:
							values = specimen.get("cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss")
							for (index, item) in enumerate(values):
								temp_y2[index].append(item)
							
							temp_x2.append(eval(conditions[2][0]))
							temp_RR2.append(specimen.rridx)

						if specimen.aodidx > 6 and specimen.ecltss >= average_ecltss:
							values = specimen.get("cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss")
							for (index, item) in enumerate(values):
								temp_y3[index].append(item)
							
							temp_x3.append(eval(conditions[2][0]))
							temp_RR3.append(specimen.rridx)
						if specimen.aodidx > 6 and specimen.ecltss <= average_ecltss:
							values = specimen.get("cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss")
							for (index, item) in enumerate(values):
								temp_y4[index].append(item)
							
							temp_x4.append(eval(conditions[2][0]))
							temp_RR4.append(specimen.rridx)	
					#If not comparing any high and low
					else:
						#print "Not sorting by high and low"
						values = specimen.get("cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss")
						for (index,item) in enumerate(values):
							temp_y[index].append(item)
						temp_x.append(eval(conditions[2][0]))
					
	
	if conditions[5] and not conditions[6]:
		return temp_x,temp_y,temp_x2,temp_y2, low_rridx, high_rridx
	elif conditions[5] and conditions[6]:
		return temp_y1, temp_y2, temp_y3, temp_y4, temp_x1, temp_x2, temp_x3, temp_x4, temp_RR1, temp_RR2, temp_RR3, temp_RR4
	else:
		return temp_x,temp_y




#Make the averages string
def make_avg_string(var_list, averages, high_list):
	return_string = ""
	for i in range(len(var_list)):
		
		return_string += "Average "
		return_string += var_list[i]
		return_string += ":"
		if len(high_list) > 0:
			
			return_string += "\nLow:"
			return_string += str(averages[i])
			return_string +="\nHigh:"
			return_string += str(high_list[i])
		else:
			return_string += str(averages[i])
		return_string +="\n"
	if len(high_list) > 0:
		return_string += "Average PoP:"
		return_string += "\nLow:"
		return_string += str(averages[-1])
		return_string +="\nHigh:"
		return_string += str(high_list[-1])
	return return_string


#Creates the ticks for plots			
def create_ticks(bin, length):
	xticks = []
	num = 0
	for l in range(length):
		tick1 = str(bin[l][0])
		tick1 = tick1[0:4]
		tick1 = float(tick1)
		tick2 = str(bin[l][1])
		tick2 = tick2[0:4]
		tick2 = float(tick2)
		tick15 = round(((tick1 + tick2)/2),3)
		tick = str(tick15)
		if num == 0:
			xticks.append(tick)
			num += 1
		elif num == 1:
			xticks.append("")
			num = 0
		
		
	return xticks


#Read in the file and create the species array
def read_in_file(filename):
	f = open(filename,'r')
	linecount = 0
	overall = []
	aerosolcount = 0
	rainbins = []
	geobins = []
	LWPbin = []
	aeronames = []

	for line in f:
		linecount += 1
		if "rain" in line:
			limits1 = (f.next()).strip()
			linecount +=1
			limits1 = limits1.split()
			limits2 = (f.next()).strip()
			linecount+=1
			limits2 = limits2.split()
			for n in range(len(limits1)):
				bin = [limits1[n],limits2[n]]
				rainbins.append(bin)
		if "parameter" in line:
			limits1 = (f.next()).strip()
			linecount +=1
			limits2 = (f.next()).strip()
			linecount +=1
			limits3 = (f.next()).strip()
			linecount +=1
			limits4 = (f.next()).strip()
			linecount +=1
			limits1 = limits1.split()
			limits2 = limits2.split()
			limits3 = limits3.split()
			limits4 = limits4.split()
			limitslow = limits1 + limits2
			limitshigh = limits3 + limits4
			for n in range(len(limitslow)):
				bin = [limitslow[n],limitshigh[n]]
				geobins.append(bin)
		if "LWP" in line:
			limits1 = (f.next()).strip()
			linecount +=1
			limits2 = (f.next()).strip()
			linecount +=1
			limits3 = (f.next()).strip()
			linecount +=1
			limits4 = (f.next()).strip()
			linecount +=1
			limits5 = (f.next()).strip()
			linecount +=1
			limits6 = (f.next()).strip()
			linecount +=1
			limits1 = limits1.split()
			limits2 = limits2.split()
			limits3 = limits3.split()
			limits4 = limits4.split()
			limits5 = limits5.split()
			limits6 = limits6.split()
			limitslow = limits1 + limits2 + limits3
			limitshigh = limits4 + limits5 + limits6
			for n in range(len(limitslow)):
				bin = [limitslow[n],limitshigh[n]]
				LWPbin.append(bin)
		if "Number" in line:
			speciesarray = []
			aero = ()
			aerosol1 = (linecache.getline(filename, linecount-2)).strip()
			aerosolcount += 1
			print aerosol1
			aeronames.append(str(aerosol1))
			words = line.split()
			max = int(words[0])
			f.next()
			linecount += 1
			while linecount < (linecount + max):
				try:
					line1 = f.next()
					variables = line1.split()
					aero = aerosol(variables)
					linecount+= 1
				except Exception:
					linecount +=1
					#print "breaking1"
					break	
				try:
					line2 = f.next()
					vars = line2.split()
					aero.update(vars)
					linecount += 1
				except Exception:
					#print "breaking2"
					break
				aero.name(aerosol1)
				speciesarray.append(aero)		
		else:
			continue
		overall.append(speciesarray)
	f.close()
	return overall,LWPbin,rainbins,geobins,aeronames
#Finds the standard deviation of sorted data and returns a list of the values sorted into the correct bin
def find_sd(tempbin, LWP):
	sum_of_var = make_bins(LWP)

	for l in range(len(tempbin)):
		try:
			average = sum(tempbin[l])/len(tempbin[l])
			var_array = []
		except ZeroDivisionError:
			average = 0
			
			
		for j in range(len(tempbin[l])):
			var = tempbin[l][j] - average
			var = var**2
			try:
				var = var/average
			except ZeroDivisionError:
				var = 0
			sum_of_var[l].append(var)
	for l in range(len(sum_of_var)):
		try:
			sum_of_var[l] = (sum(sum_of_var[l]))/len(sum_of_var[l])
		except ZeroDivisionError:
			sum_of_var[l] = 0
			
	return sum_of_var

		

##Getting user input for the conditions
conditions = []

options = [["0","-15","-30"],"LWP","CloudSat rain bins","Geophysical parameter","CSWFTOA","ECLTSS","CF101","CF51","clwfsfc"]

###0 Getting the lat
user_input = raw_input("Which latitude would you like to look at? Enter the corresponding letter \n a){} \n b){} \n c){}\n".format(options[0][0], options[0][1], options[0][2]))
check(options[0], user_input, conditions, options[0])
###1 Getting the lon
user_input = raw_input("Which longitude would you like to look at? Enter the corresponding letter \n a){} \n b){} \n c){}\n".format(options[0][0], options[0][1], options[0][2]))
check(options[0], user_input, conditions, options[0])
lat_lon = str(conditions[0])+"-"+str(conditions[1])

###2 Getting the bin
user_input = raw_input("Which bin would you like to sort by? \n a)LWP \n b)CloudSat rain bins \n c)Geophysical parameter \n")
check([options[1],options[2],options[3]], user_input, conditions, [["specimen.lwpidx","LWPbin"],["specimen.geoidx","geobins"],["specimen.rridx","rainbins"]])
_bin = conditions[2][1]
#Here is where the code for a y-variable control would be placed
conditions.append("")


###4 Finding the aerosol
aerosol_list = ["MODIS AOD","MODIS AI","CALIPSO AOD","CALIPSO CM","CALIPSO DU","CALIPSO PC","CALIPSO CC","CALIPSO PD","CALIPSO SM","SPRINTARS AOD","SPRINTARS SU","SPRINTARS SA","SPRINTARS DU","SPRINTARS OC","SPRINTARS BC","GEMS AOD","GEMS SU","GEMS SA","GEMS DU","GEMS OC","GEMS BC"]

letters = list(map(chr,range(97, 97+len(aerosol_list))))
numbers = list(map(int, range(0, 0+len(aerosol_list))))

for index,item in enumerate(aerosol_list):
	print letters[index],")",item
	pass
user_input = raw_input("Which aerosol would you like to look at?\n")
check(aerosol_list, user_input, conditions, numbers)
aerosol_name = aerosol_list[conditions[4]].replace(" ","")

###5 Sorting
#user_input = raw_input("Would you like to sort into high and low cases? \n a) Yes \n b) No\n")
#check(["yes","no"],user_input, conditions, [True, False])
conditions.append(True)

##6 Sorting variable
if conditions[5]:
	user_input = raw_input("Would you like to sort by high vs. low ECLTSS? \n a) Yes\n b) No\n")
	check(["stability","none"],user_input,conditions,[True,False])
	if conditions[6]:
		_ecltss = "AI_ECLTSS"
	else:
		_ecltss = "AI"
		
else:
	conditions.append(False)
	_ecltss = ""


#7 Raining or non-raining?
user_input = raw_input("Would you like to sort by raining or non-raining? \n a) raining \n b) non-raining \n c) all \n")
check(["y","n","a"],user_input,conditions,[["specimen.rridx>0","raining"],["specimen.rridx == 0","nonraining"],["specimen.rridx > -1","all"]])
rain_index = conditions[7][1]
#8 Touching?
user_input = raw_input("Would you like to sort by touching or non-touching?\n a) touching cases \n b) non-touching cases \n c) all cases\n d) comparing touching or nottouching\n")

check(["a","b","c","d"],user_input,conditions,[".touching",".nottouching","",True])


#9 Compare touching to not-touching or all?
if conditions[8] == True:
	user_input = raw_input("Would you like to compare touching cases? \n a) Touching to non-touching \n b) Touching to all \n c) Non-touching to all\n")
	check(["t to nt","t to all","nt to all","Pass"], user_input, conditions, [[True,True,".touching",".nottouching"],[True,True,".touching",""],[True,True,".nottouching",""]])
	layer1_name = conditions[9][2][1:]
	layer2_name = conditions[9][3][1:]
	touching_index = layer1_name + "vs" + layer2_name
else:
	conditions.append([True,False])
	layer1_name = ""
	layer2_name = conditions[8][1:]
#10 Check for error
user_input = raw_input("Would you like to check for error? \n a) Yes \n b) No \n")
check(["yes","no"],user_input,conditions,[True,False])
if conditions[10]:
	_error = "error"
else:
	_error = "noerror"
###Start reading in the file
overall = []
LWPbin = []
rainbins = []
geobins = []
aeronames = []
been_compared = False
ready_to_plot = False
ready_to_save = False
list_of_vars = ["cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss"]








while conditions[9][0] == True:
	
	if not conditions[10]:
		if type(conditions[8]) is not bool:
			filename = "/thermal/data/Aerosols/AngolaBasin/binneddata.03607_24860.shallow.ocean.regionalbins_0.1.noerror.ltss{}.{}_{}.dat".format(conditions[8],conditions[0],conditions[1])
		else:
			filename = "/thermal/data/Aerosols/AngolaBasin/binneddata.03607_24860.shallow.ocean.regionalbins_0.1.noerror.ltss{}.{}_{}.dat".format(conditions[9][2],conditions[0],conditions[1])
	else:
		if not conditions[9][1] and not been_compared:
			filename = "/thermal/data/Aerosols/AngolaBasin/binneddata.03607_24860.shallow.ocean.regionalbins_0.1.random.ltss{}.{}_{}.dat".format(conditions[8],conditions[0],conditions[1])
		else:
			filename = "/thermal/data/Aerosols/AngolaBasin/binneddata.03607_24860.shallow.ocean.regionalbins_0.1.random.ltss{}.{}_{}.dat".format(conditions[9][2],conditions[0],conditions[1])

	overall,LWPbin,rainbins,geobins,aeronames = read_in_file(filename)
	print filename


	###Creating the plots begins
	if type(conditions[8]) is not bool or been_compared:
		#If not comparing high and low values
		if not conditions[5]:
			pltx, plty = get_points(overall, conditions)
			print "Gathered all data"
			for (index,y_value) in enumerate(plty):
				plty[index] = sort(pltx, y_value)
			average_values, average_values = average(plty,plty)
			pltx = range(0, len(eval(conditions[2][1])))
			print "Sorted all data"
			xticks = create_ticks(eval(conditions[2][1]), len(pltx))
			plot_string = make_avg_string(list_of_vars,average_values,[])
			print "Ready to plot"
			ready_to_plot = True
			#Plotting begins
			fig1 = plt.figure(figsize = (11,8))
			fig2 = plt.figure(figsize = (8,8))
			gs = gridspec.GridSpec(2,3)
			bs = gridspec.GridSpec(2,2)
			
			ax1 = fig1.add_subplot(gs[0,0])
			ax1.plot(pltx, plty[0], "k^")
			ax1.set_xticklabels(xticks)
			ax1.set_ylabel("CSWFTOA")
			
			ax2 = fig1.add_subplot(gs[0,1])
			ax2.plot(pltx, plty[1], "k^")
			ax2.set_xticklabels(xticks)
			ax2.set_ylabel("CLWFTOA")

			ax3 = fig1.add_subplot(gs[1,0])
			ax3.plot(pltx, plty[2], "k^")
			ax3.set_xticklabels(xticks)
			ax3.set_ylabel("Cloud Top")

			ax4 = fig1.add_subplot(gs[1,1])
			ax4.plot(pltx, plty[7], "k^")
			ax4.set_xticklabels(xticks)
			ax4.set_ylabel("Merra Omega")
			
			ax5 = fig1.add_subplot(gs[:,2])
			ax5.set_xticklabels([""],[""])
			ax5.set_yticks([0,1],[])
			ax5.text(0,0,plot_string,fontsize=11)

			fig1.subplots_adjust(wspace=.25)

			bx1 = fig2.add_subplot(bs[0,0])
			bx1.plot(pltx, plty[3], "k^")
			bx1.set_xticklabels(xticks)
			bx1.set_ylabel("CF11")

			bx2 = fig2.add_subplot(bs[0,1])
			bx2.plot(pltx, plty[4], "k^")
			bx2.set_xticklabels(xticks)
			bx2.set_ylabel("CF21")
		
			bx3 = fig2.add_subplot(bs[1,0])
			bx3.plot(pltx, plty[5], "k^")
			bx3.set_xticklabels(xticks)
			bx3.set_ylabel("CF51")
			
			bx4 = fig2.add_subplot(bs[1,1])
			bx4.plot(pltx, plty[6], "k^")
			bx4.set_xticklabels(xticks)
			bx4.set_ylabel("CF101")

			fig2.subplots_adjust(wspace=.25)
			print "Ready to save"
			ready_to_save = True
			if ready_to_plot and been_compared:
				ax1.plot(compare_pltx, compare_plty[0], "go")
				ax2.plot(compare_pltx, compare_plty[1], "go")
				ax3.plot(compare_pltx, compare_plty[2], "go")
				ax4.plot(compare_pltx, compare_plty[7], "go")
				bx1.plot(compare_pltx, compare_plty[3], "go")
				bx2.plot(compare_pltx, compare_plty[4], "go")
				bx3.plot(compare_pltx, compare_plty[5], "go")
				bx4.plot(compare_pltx, compare_plty[6], "go")
			
			#plt.show()
			#sys.exit()
			ready_to_save = True
			
	
		#If comparing by high and low AI only
		if conditions[5] and not conditions[6]:
			low_pltx, low_plty, high_pltx, high_plty, low_rridx, high_rridx = get_points(overall, conditions)
			print "Gathered all data"
			low_rridx = sort_pop(conditions, low_rridx, low_pltx)
			high_rridx = sort_pop(conditions, high_rridx, high_pltx)
			for (index, low_y) in enumerate(low_plty):
				low_plty[index] = sort(low_pltx, low_y)
			for (index,high_y) in enumerate(high_plty):
				high_plty[index] = sort(high_pltx, high_y)
			low_averages, high_averages = average(low_plty,high_plty)
			low_avg_rridx = average_pop(low_rridx)
			low_averages.append(low_avg_rridx)
			#high_averages = average(high_plty)
			high_avg_rridx = average_pop(high_rridx)
			high_averages.append(high_avg_rridx)
			low_pltx = range(0, len(eval(conditions[2][1])))
			high_pltx = range(0, len(eval(conditions[2][1])))
			
			print "All data sorted"
			xticks = create_ticks(eval(conditions[2][1]), len(low_pltx))
			
			plot_string1 =make_avg_string(list_of_vars, low_averages, high_averages)
			print "Ready to plot"
			ready_to_plot = True
			#Plotting begins
			if not been_compared:
				fig1 = plt.figure(figsize = (11,8))
				gs = gridspec.GridSpec(2,3)
			else:
				fig1 = plt.figure(figsize =(15,8))
				gs = gridspec.GridSpec(2,4)
			fig2 = plt.figure(figsize = (8,8))
			
			bs = gridspec.GridSpec(2,2)
			
			ax1 = fig1.add_subplot(gs[0,0])
			ax1.plot(low_pltx, low_plty[0], "k^")
			ax1.plot(high_pltx, high_plty[0], "r^")
			ax1.set_xticklabels(xticks)
			ax1.set_ylabel("CSWFTOA")
			
			ax2 = fig1.add_subplot(gs[0,1])
			ax2.plot(low_pltx, low_plty[1], "k^")
			ax2.plot(high_pltx, high_plty[1], "r^")
			ax2.set_xticklabels(xticks)
			ax2.set_ylabel("CLWFTOA")

			ax3 = fig1.add_subplot(gs[1,0])
			ax3.plot(low_pltx, low_plty[2], "k^")
			ax3.plot(high_pltx, high_plty[2], "r^")
			ax3.set_xticklabels(xticks)
			ax3.set_ylabel("Cloud Top")


			ax4 = fig1.add_subplot(gs[1,1])
			ax4.plot(low_pltx, low_rridx, "k^")
			ax4.plot(high_pltx, high_rridx, "r^")
			ax4.set_xticklabels(xticks)
			ax4.set_ylabel("PoP")

			ax5 = fig1.add_subplot(gs[:,2])
			ax5.text(0,0,"Black is low, red is high\n"+plot_string1,fontsize=11)
			for child in ax5.get_children():
				if isinstance(child, spn.Spine):
					child.set_color('#ffffff')
			ax5.get_yaxis().set_visible(False)
			ax5.get_xaxis().set_visible(False)

			fig1.subplots_adjust(wspace=.25)
			bx1 = fig2.add_subplot(bs[0,0])
			bx1.plot(low_pltx, low_plty[3], "k^")
			bx1.plot(high_pltx, high_plty[3], "r^")
			bx1.set_xticklabels(xticks)
			bx1.set_ylabel("CF11")

			bx2 = fig2.add_subplot(bs[0,1])
			bx2.plot(low_pltx, low_plty[4], "k^")
			bx2.plot(high_pltx, high_plty[4], "r^")
			bx2.set_xticklabels(xticks)
			bx2.set_ylabel("CF21")
		
			bx3 = fig2.add_subplot(bs[1,0])
			bx3.plot(low_pltx, low_plty[5], "k^")
			bx3.plot(high_pltx, high_plty[5], "r^")
			bx3.set_xticklabels(xticks)
			bx3.set_ylabel("CF51")
			
			bx4 = fig2.add_subplot(bs[1,1])
			bx4.plot(low_pltx, low_plty[6], "k^")
			bx4.plot(high_pltx, high_plty[6], "r^")
			bx4.set_xticklabels(xticks)
			bx4.set_ylabel("CF101")
			
			fig2.subplots_adjust(wspace=.25)
			if ready_to_plot and been_compared:
				plot_string2 = make_avg_string(list_of_vars, compare_low_averages, compare_high_averages)
				ax1.plot(compare_low_pltx, compare_low_plty[0], "go")
				ax2.plot(compare_low_pltx, compare_low_plty[1], "go")
				ax3.plot(compare_low_pltx, compare_low_plty[2], "go")
				ax4.plot(compare_low_pltx, compare_low_rridx, "go")
				bx1.plot(compare_low_pltx, compare_low_plty[3], "go")
				bx2.plot(compare_low_pltx, compare_low_plty[4], "go")
				bx3.plot(compare_low_pltx, compare_low_plty[5], "go")
				bx4.plot(compare_low_pltx, compare_low_plty[6],"go")

				ax1.plot(compare_high_pltx, compare_high_plty[0], "mo")
				ax2.plot(compare_high_pltx, compare_high_plty[1], "mo")
				ax3.plot(compare_high_pltx, compare_high_plty[2], "mo")
				ax4.plot(compare_high_pltx, compare_high_rridx, "mo")
				bx1.plot(compare_high_pltx, compare_high_plty[3], "mo")
				bx2.plot(compare_high_pltx, compare_high_plty[4], "mo")
				bx3.plot(compare_high_pltx, compare_high_plty[5], "mo")
				bx4.plot(compare_high_pltx, compare_high_plty[6], "mo")
				ax5.clear()
				ax5.text(0,0,layer1_name+": \nBlack is low, red is high\n"+plot_string1,fontsize=11)
				ax5.get_yaxis().set_visible(False)
				ax5.get_xaxis().set_visible(False)

				ax6 = fig1.add_subplot(gs[:,3])
				ax6.text(0,0,layer2_name+":\nGreen is low, magenta is high\n"+plot_string2,fontsize=11)
				ax6.get_yaxis().set_visible(False)
				ax6.get_xaxis().set_visible(False)
				
				for child in ax6.get_children():
					if isinstance(child, spn.Spine):
						child.set_color('#ffffff')

				
			#plt.show()
			#sys.exit()
			ready_to_save = True
	
		#If comparing by high and low AI and ecltss
		if conditions[5] and conditions[6]:
			y1, y2, y3, y4, x1, x2, x3, x4, RR1, RR2, RR3, RR4 = get_points(overall, conditions)
			print "Gathered all data"
			all_ys = [y1, y2, y3, y4]
			all_xs = [x1, x2, x3, x4]
			all_RRs = [RR1, RR2, RR3, RR4]
			averages = []
			for index1 in range(len(all_ys)):
				for index2 in range(len(all_ys[index1])):
					all_ys[index1][index2] = sort(all_xs[index1],all_ys[index1][index2])
			temp1, temp3 = average(all_ys[0],all_ys[2])
			temp2, temp4 = average(all_ys[1], all_ys[3])
			averages = [temp1, temp2, temp3, temp4]
			#print averages
			
			
				
			for i in range(len(all_RRs)):
				all_RRs[i] = sort_pop(conditions, all_RRs[i],all_xs[i])
				all_xs[i] = range(0, len(eval(conditions[2][1])))
				averages.append(average_pop(all_RRs[i]))
			print "All data sorted"
		
			xticks = create_ticks(eval(conditions[2][1]), len(all_xs[0]))
			plot_string1 = make_avg_string(list_of_vars, averages[0],averages[3])
			plot_string2 = make_avg_string(list_of_vars,averages[1],averages[2]) 
			print "Ready to plot"
			ready_to_plot = True
			#Plotting begins
			gs = gridspec.GridSpec(2,4)
			fig1 = plt.figure(figsize=(15,8))
			
			ax1 = fig1.add_subplot(gs[0,0])
			ax1.plot(all_xs[0],all_ys[0][0], "k^")
			ax1.plot(all_xs[1],all_ys[1][0], "r^")
			ax1.plot(all_xs[2],all_ys[2][0], "g^")
			ax1.plot(all_xs[3],all_ys[3][0], "m^")
			ax1.set_xticklabels(xticks)
			ax1.set_ylabel("CSWFTOA")
		
			ax2 = fig1.add_subplot(gs[0,1])
			ax2.plot(all_xs[0],all_ys[0][1],"k^")
			ax2.plot(all_xs[1],all_ys[1][1], "r^")
			ax2.plot(all_xs[2],all_ys[2][1], "g^")
			ax2.plot(all_xs[3],all_ys[3][1], "m^")
			
			ax2.set_xticklabels(xticks)
			ax2.set_ylabel("CLWFTOA")
			ax3 = fig1.add_subplot(gs[1,0])
			ax3.plot(all_xs[0],all_ys[0][2],"k^")
			ax3.plot(all_xs[1],all_ys[1][2], "r^")
			ax3.plot(all_xs[2],all_ys[2][2], "g^")
			ax3.plot(all_xs[3],all_ys[3][2], "m^")			
			ax3.set_xticklabels(xticks)
			ax3.set_ylabel("CldTop")
			
			ax4 = fig1.add_subplot(gs[1,1])
			ax4.plot(all_xs[0],all_RRs[0],"k^")
			ax4.plot(all_xs[1],all_RRs[1],"r^")
			ax4.plot(all_xs[2],all_RRs[2],"g^")
			ax4.plot(all_xs[3],all_RRs[3],"m^")
			ax4.set_xticklabels(xticks)
			ax4.set_ylabel("PoP")

			ax5= fig1.add_subplot(gs[:,2])
			for child in ax5.get_children():
				if isinstance(child, spn.Spine):
					child.set_color('#ffffff')
			ax5.get_yaxis().set_visible(False)
			ax5.get_xaxis().set_visible(False)
			
			#ax5.text(0,0,"\nBlack is low AI, low ecltss\n Red is low AI, high ecltss\n Green is high AI, high ecltss\n Magenta is high AI, low ecltss")
			ax5.text(0,0,"Low Ecltss\nBlack is low AI, magenta is high AI\n"+plot_string1,fontsize = 11)
	
			ax6 = fig1.add_subplot(gs[:,3])
			for child in ax6.get_children():
				if isinstance(child, spn.Spine):
					child.set_color('#ffffff')
			ax6.get_yaxis().set_visible(False)
			ax6.get_xaxis().set_visible(False)
			ax6.text(0,0,"High Ecltss\nRed is low AI, green is high AI\n"+plot_string2,fontsize=11)
			
			fig1.subplots_adjust(wspace=.25)
			bs = gridspec.GridSpec(2,2)
			fig2 = plt.figure(figsize=(8,8))
			
			bx1 = fig2.add_subplot(bs[0,0])
			bx1.plot(all_xs[0], all_ys[0][3], "k^")
			bx1.plot(all_xs[1],all_ys[1][3], "r^")
			bx1.plot(all_xs[2],all_ys[2][3], "g^")
			bx1.plot(all_xs[3],all_ys[3][3], "m^")
			bx1.set_xticklabels(xticks)
			bx1.set_ylabel("CF11")
	
			bx2 = fig2.add_subplot(bs[0,1])
			bx2.plot(all_xs[0], all_ys[0][4], "k^")
			bx2.plot(all_xs[1],all_ys[1][4], "r^")
			bx2.plot(all_xs[2],all_ys[2][4], "g^")
			bx2.plot(all_xs[3],all_ys[3][4], "m^")
			bx2.set_xticklabels(xticks)
			bx2.set_ylabel("CF21")
		
			bx3 = fig2.add_subplot(bs[1,0])
			bx3.plot(all_xs[0], all_ys[0][5], "k^")
			bx3.plot(all_xs[1],all_ys[1][5], "r^")
			bx3.plot(all_xs[2],all_ys[2][5], "g^")
			bx3.plot(all_xs[3],all_ys[3][5], "m^")
			bx3.set_xticklabels(xticks)
			bx3.set_ylabel("CF51")
		
			bx4 = fig2.add_subplot(bs[1,1])
			bx4.plot(all_xs[0], all_ys[0][6], "k^")
			bx4.plot(all_xs[1],all_ys[1][6], "r^")
			bx4.plot(all_xs[2],all_ys[2][6], "g^")
			bx4.plot(all_xs[3],all_ys[3][6], "m^")
			bx4.set_xticklabels(xticks)
			bx4.set_ylabel("CF101")
			fig2.subplots_adjust(wspace=.25)
			
			'''fig5 = plt.figure(figsize =(6,8))
			cx1 = fig5.add_subplot(121)
			cx1.set_xticklabels([""],[""])
			cx1.set_yticks([0,1],[])
			cx1.text(0,0,"Low ecltss\n" + plot_string1,fontsize=11)
			cx2 = fig5.add_subplot(122)
			cx2.set_xticklabels([""],[""])
			cx2.set_yticks([0,1],[])
			cx2.text(0,0,"High ecltss \n" + plot_string2,fontsize=11)'''
			

			if ready_to_plot and been_compared:
				fig1.canvas.set_window_title(layer2_name)			
				fig2.canvas.set_window_title(layer2_name)
				#fig5.canvas.set_window_title(layer2_name)
				
				plot_string3 = make_avg_string(list_of_vars,com_averages[0],com_averages[3])
				plot_string4 = make_avg_string(list_of_vars, com_averages[1],com_averages[2])	
					
				
				fig3 = plt.figure(figsize=(15,8))
				fig3.canvas.set_window_title(layer1_name)
				ax11 = fig3.add_subplot(gs[0,0])
				ax11.plot(com_all_xs[0],com_all_ys[0][0], "k^")
				ax11.plot(com_all_xs[1],com_all_ys[1][0], "r^")
				ax11.plot(com_all_xs[2],com_all_ys[2][0], "g^")
				ax11.plot(com_all_xs[3],com_all_ys[3][0], "m^")
				ax11.set_xticklabels(xticks)
				ax11.set_ylabel("CSWFTOA")
				ax21 = fig3.add_subplot(gs[0,1])
				ax21.plot(com_all_xs[0],com_all_ys[0][1],"k^")
				ax21.plot(com_all_xs[1],com_all_ys[1][1], "r^")
				ax21.plot(com_all_xs[2],com_all_ys[2][1], "g^")
				ax21.plot(com_all_xs[3],com_all_ys[3][1], "m^")
				ax21.set_xticklabels(xticks)
				ax21.set_ylabel("CLWFTOA")
				ax31 = fig3.add_subplot(gs[1,0])
				ax31.plot(com_all_xs[0],com_all_ys[0][2],"k^")
				ax31.plot(com_all_xs[1],com_all_ys[1][2], "r^")
				ax31.plot(com_all_xs[2],com_all_ys[2][2], "g^")
				ax31.plot(com_all_xs[3],com_all_ys[3][2], "m^")
				ax31.set_xticklabels(xticks)
				ax31.set_ylabel("CldTop")
				ax41 = fig3.add_subplot(gs[1,1])
				ax41.plot(com_all_xs[0],com_all_RRs[0],"k^")
				ax41.plot(com_all_xs[1],com_all_RRs[1],"r^")
				ax41.plot(com_all_xs[2],com_all_RRs[2],"g^")
				ax41.plot(com_all_xs[3],com_all_RRs[3],"m^")
				ax41.set_xticklabels(xticks)
				ax41.set_ylabel("PoP")

				ax51= fig3.add_subplot(gs[:,2])
				for child in ax51.get_children():
					if isinstance(child, spn.Spine):
						child.set_color('#ffffff')
				ax51.get_yaxis().set_visible(False)
				ax51.get_xaxis().set_visible(False)
				ax51.text(0,0,"Low Ecltss\nBlack is low AI, magenta is high AI\n"+plot_string3,fontsize=11)
			
				ax61= fig3.add_subplot(gs[:,3])
				for child in ax61.get_children():
					if isinstance(child, spn.Spine):
						child.set_color('#ffffff')
				ax61.get_yaxis().set_visible(False)
				ax61.get_xaxis().set_visible(False)
				ax61.text(0,0,"High Ecltss\nRed is low AI, Green is high AI\n"+plot_string4,fontsize=11)
			
				#ax51.text(0,0,"Black is low AI, low ecltss\n Red is low AI, high ecltss\n Green is high AI, high ecltss\n Magenta is high AI, low ecltss" )
				
				fig3.subplots_adjust(wspace=.25)
				fig4 = plt.figure(figsize=(8,8))
				fig4.canvas.set_window_title(layer1_name)
			
				bx11 = fig4.add_subplot(bs[0,0])
				bx11.plot(com_all_xs[0], com_all_ys[0][3], "k^")
				bx11.plot(com_all_xs[1],com_all_ys[1][3], "r^")
				bx11.plot(com_all_xs[2],com_all_ys[2][3], "g^")
				bx11.plot(com_all_xs[3],com_all_ys[3][3], "m^")
				bx11.set_xticklabels(xticks)
				bx1.set_ylabel("CF11")
	
				bx21 = fig4.add_subplot(bs[0,1])
				bx21.plot(com_all_xs[0], com_all_ys[0][4], "k^")
				bx21.plot(com_all_xs[1],com_all_ys[1][4], "r^")
				bx21.plot(com_all_xs[2],com_all_ys[2][4], "g^")
				bx21.plot(com_all_xs[3],com_all_ys[3][4], "m^")
				bx21.set_xticklabels(xticks)
				bx21.set_ylabel("CF21")
		
				bx31 = fig4.add_subplot(bs[1,0])
				bx31.plot(com_all_xs[0], com_all_ys[0][5], "k^")
				bx31.plot(com_all_xs[1],com_all_ys[1][5], "r^")
				bx31.plot(com_all_xs[2],com_all_ys[2][5], "g^")
				bx31.plot(com_all_xs[3],com_all_ys[3][5], "m^")
				bx31.set_xticklabels(xticks)
				bx31.set_ylabel("CF51")
		
				bx41 = fig4.add_subplot(bs[1,1])
				bx41.plot(com_all_xs[0], com_all_ys[0][6], "k^")
				bx41.plot(com_all_xs[1],com_all_ys[1][6], "r^")
				bx41.plot(com_all_xs[2],com_all_ys[2][6], "g^")
				bx41.plot(com_all_xs[3],com_all_ys[3][6], "m^")
				bx41.set_xticklabels(xticks)
				bx41.set_ylabel("CF101")
				fig4.subplots_adjust(wspace=.25)

				'''fig6 = plt.figure(figsize =(6,8))
				dx1 = fig5.add_subplot(121)
				dx1.set_xticklabels([""],[""])
				dx1.set_yticks([0,1],[])
				dx1.text(0,0,"Low ecltss\n" + plot_string3,fontsize=11)
				dx2 = fig5.add_subplot(122)
				dx2.set_xticklabels([""],[""])
				dx2.set_yticks([0,1],[])
				dx2.text(0,0,"High ecltss \n" + plot_string4,fontsize=11)			
				fig6.canvas.set_window_title(layer1_name)'''
			

				
			#plt.show()
			#sys.exit()			
			ready_to_save = True
			

	#If comparing aerosol layer and cloud layer
	elif conditions[8] and not been_compared:
		#If not comparing high and low values
		if not conditions[5]:
			compare_pltx, compare_plty = get_points(overall, conditions)
			print "Gathered all data"
			for (index,y_value) in enumerate(compare_plty):
				compare_plty[index] = sort(compare_pltx, y_value)
			compare_average_values, compare_average_values = average(compare_plty, compare_plty)
			compare_pltx = range(0, len(eval(conditions[2][1])))
			print "Sorted all data"

			print "Been compared"
			been_compared = True
			conditions[9][2] = conditions[9][3]
			print "Switched file"
	
		#If comparing by high and low AI only
		if conditions[5] and not conditions[6]:
			compare_low_pltx, compare_low_plty, compare_high_pltx, compare_high_plty, compare_low_rridx, compare_high_rridx = get_points(overall, conditions)
			print "Gathered all data"
			compare_low_rridx = sort_pop(conditions, compare_low_rridx, compare_low_pltx)
			compare_high_rridx = sort_pop(conditions, compare_high_rridx, compare_high_pltx)
			for (index, low_y) in enumerate(compare_low_plty):
				compare_low_plty[index] = sort(compare_low_pltx, low_y)
			for (index,high_y) in enumerate(compare_high_plty):
				compare_high_plty[index] = sort(compare_high_pltx, high_y)
			compare_low_averages, compare_high_averages = average(compare_low_plty, compare_high_plty)
			#compare_high_averages = average(compare_high_plty)
			compare_average_lowrridx = average_pop(compare_low_rridx)
			compare_average_highrridx = average_pop(compare_high_rridx)
			compare_low_averages.append(compare_average_lowrridx)
			compare_high_averages.append(compare_average_highrridx)
			compare_low_pltx = range(0, len(eval(conditions[2][1])))
			compare_high_pltx = range(0,len(eval(conditions[2][1])))
			print "All data sorted"
		
			print "Been compared"
			been_compared = True
			conditions[9][2] = conditions[9][3]
			print "Switched file"
		
		#If comparing by high and low AI and ecltss
		if conditions[5] and conditions[6]:
			com_y1, com_y2, com_y3, com_y4, com_x1, com_x2, com_x3, com_x4, com_RR1, com_RR2, com_RR3, com_RR4 = get_points(overall, conditions)
			print "Gathered all data"
			com_all_ys = [com_y1, com_y2, com_y3, com_y4]
			com_all_xs = [com_x1, com_x2, com_x3, com_x4]
			com_all_RRs = [com_RR1, com_RR2, com_RR3, com_RR4]
			
			for index1 in range(len(com_all_ys)):
				for index2 in range(len(com_all_ys[index1])):
					com_all_ys[index1][index2] = sort(com_all_xs[index1],com_all_ys[index1][index2])

			temp1, temp3 = average(com_all_ys[0],com_all_ys[2])
			temp2, temp4 = average(com_all_ys[1], com_all_ys[3])
			com_averages = [temp1, temp2, temp3, temp4]
				
			for i in range(len(com_all_RRs)):
				com_all_RRs[i] = sort_pop(conditions, com_all_RRs[i],com_all_xs[i])
				com_all_xs[i] = range(0, len(eval(conditions[2][1])))
			print "All data sorted"

			print "Been compared"
			been_compared = True
			conditions[9][2] = conditions[9][3]
			print "Switched file"
	if ready_to_save:
		conditions[9][0] = False
		#fig_name ="Tristan/" +  aerosol_name + "_" + lat_lon + "_" + _bin + "_" + rain_index + "_" + _ecltss + "_" + touching_index + "_" + _error + "_"
		layer1_fig_name = "Tristan/" +  aerosol_name + "_" + lat_lon + "_" + _bin + "_" + rain_index + "_" + _ecltss + "_" + layer1_name + "_" + _error + "_"
		layer2_fig_name =  "Tristan/" +  aerosol_name + "_" + lat_lon + "_" + _bin + "_" + rain_index + "_" + _ecltss + "_" + layer2_name + "_" + _error + "_"
		fig1_name = layer2_fig_name + "1.jpg"
		fig2_name = layer2_fig_name + "2.jpg"
		fig3_name = layer1_fig_name + "3.jpg"
		fig4_name = layer1_fig_name + "4.jpg"
		'''if conditions[6]:
			fig5_name = fig_name + "5.jpg"
			fig5.savefig(fig5_name)
			if been_compared:
				fig6_name = fig_name + "6.jpg"'''
		print "Saving figures"
		
		fig1.savefig(fig1_name)
		print fig1_name
		fig2.savefig(fig2_name)
		print fig2_name
		if been_compared and conditions[6]:
			fig3.savefig(fig3_name)
			fig4.savefig(fig4_name)
			#fig6.savefig(fig6_name)
	#plt.show()
print "Finished saving"

