#!/usr/bin/env python

from cloud.serialization import cloudpickle
import json
import os
import glob
import sys
import re
from collections import defaultdict
import time, math
import numpy as np
import fakedict
import copy

class NineError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

class NanError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

CLOUDSAT_INDEX = [.5,1.75,3.75,7]
number_none = 0
#GLOBAL VARIABLES
ERROR_DUMP_STRING = ""
ROOTDIR = "/thermal/data/Aerosols/Global/"
DUMP_DIR = "/home/adouglas2/Summer/aerosol/Global/"
#For south pacific files
#ROOTDIR = '/thermal/data/Aerosols/SouthPacific/'
#DUMP_DIR = '/home/adouglas2/Summer/aerosol/SouthPacific/'
#ROOTDIR = '/thermal/data/Aerosols/Global/'
#DUMP_DIR = '/home/adouglas2/Summer/aerosol/Global/'

LIST_OF_VARS = ['species','amsrrridx','rridx','aodidx','geoidx','lwpidx','counter','daycounter','modiscounter','cerescounter','sfc_wind','cwv','amsr_sst','ec_sst','merra_sst','ec_ltss','merra_ltss','merra_omega','pia','fl','rh','eis3','eis4','AI','lwp','merra_evap','cld_top','cld_thick','MODIS_5km_CF','MODIS_COT','MODIS_CTT','MODIS_CTP','MODIS_LWP','MODIS_Re','SFC_emiss','OLR','CLR_OLR','CSWFTOA','CLWFTOA','CSWFSFC','CLWFSFC','CERES_albedo']
REPEATING_VARS = ['counter','daycounter','cerescounter','cloud_fraction','pop','cspop','cld_top','SFC_emiss','OLR','CLR_OLR','CSWFTOA','CLWFTOA','CSWFSFC','CLWFSFC','albedo','clear_sky_albedo','CERES_albedo']
SCOPES = ["12","24","36","48","96"]

START_TIME = time.clock()
#Testing variable to find errors in Tristan's code

error_file = ""

NUMBER_INSTANCES = 0
number_nan = 0
daynight = False

bin_lines = {"AMSR":6,"Geo":4,"Cloud":2}

#Overall dictionary with all variables
globe = defaultdict(lambda:defaultdict(lambda:defaultdict(lambda:defaultdict(dict))))
#Dictionary with only LWP bin limits
lwp_bin_dict = defaultdict(dict)
#RRIDX bin limits
rridx_bin_dict = defaultdict(dict)
#Geo bin limits
geo_bin_dict = defaultdict(dict)
#list of dicts to make it easier to append
order = [rridx_bin_dict, geo_bin_dict, lwp_bin_dict]

def remove_whitespace(line):
	line = re.sub(r'\s','',line)
	return line

def create_aerosol_dict(g, error, lat, lon, touching_status, line_number):
	global number_none
	global number_nan
	global ERROR_DUMP_STRING
	global START_TIME
	global NUMBER_INSTANCES 
	#Read in the line before aerosol name
	g.readline()
	line_number +=1
	#Read in aerosol name
	line = g.readline()
	line_number +=1
	line = remove_whitespace(line)
	aerosol = line
	
	lat_lon = "{}_{}".format(lat, lon)
	
	#Create dictionary for aerosol of all variables
	for var in LIST_OF_VARS:
		globe[lat_lon][error][touching_status][aerosol][var] = []
	for scope in SCOPES:
		for var in REPEATING_VARS:
			temp = scope + "_" + var
			globe[lat_lon][error][touching_status][aerosol][temp] = []
		globe[lat_lon][error][touching_status][aerosol][scope+"_cloud_albedo"] = []
		globe[lat_lon][error][touching_status][aerosol][scope+"_net"] = []
		globe[lat_lon][error][touching_status][aerosol][scope+"_CF_CSWFTOA"] = []
		globe[lat_lon][error][touching_status][aerosol][scope+"_CF_CLWFTOA"] = []
	globe[lat_lon][error][touching_status][aerosol]["CloudSat_pop"] = []
	globe[lat_lon][error][touching_status][aerosol]["amsrr_pop"] = []
	#globe[lat_lon][error][touching_status][aerosol]["48_12_cloud_fraction"] = []
	#globe[lat_lon][error][touching_status][aerosol]["96_48_cloud_fraction"] = []
	#globe[lat_lon][error][touching_status][aerosol]["96_12_cloud_fraction"] = []
	globe[lat_lon][error][touching_status][aerosol]["net"] = []
	#globe[lat_lon][error][touching_status][aerosol]["CF_CLWFTOA"] = []
	#globe[lat_lon][error][touching_status][aerosol]["CF_CSWFTOA"] = []
	#Read in blank line
	g.readline()
	line_number +=1
	#Read in number of cases
	line = g.readline()
	line_number +=1
	line = remove_whitespace(line)
	line = line.split(":")
	number_cases = int(line[0])
	print number_cases
	g.readline()
	line_number +=1
	
	#Begin appending to the dictionary
	for i in range(number_cases):
		offset = 0
		for j in range(5):
			line = g.readline()
			line_number += 1
			line = line.split()
			
			#Begin reading the first line of this case
			for n, item in enumerate(line):
				#If -999 or nan apend none
				if "*" in item:
					globe[lat_lon][error][touching_status][aerosol][LIST_OF_VARS[n+offset]].append(None)
				elif float(item) == -999.0000 or np.isnan(float(item)):
					globe[lat_lon][error][touching_status][aerosol][LIST_OF_VARS[n+offset]].append(None)
				else:
					#print item
					#print LIST_OF_VARS[n+offset]
					globe[lat_lon][error][touching_status][aerosol][LIST_OF_VARS[n+offset]].append(float(item))
					
			offset += len(line)
		
		
		for scope in SCOPES:
			line = g.readline()
			line_number += 1
			line = line.split()
			#Begin reading the first line of this case
			for n, item in enumerate(line):
				variable = "{}_{}".format(scope, REPEATING_VARS[n])
				
			#If -999 or nan apend none
				if "*" in item:
					globe[lat_lon][error][touching_status][aerosol][variable].append(None)
				elif float(item) == -999.0000 or np.isnan(float(item)):
				
					globe[lat_lon][error][touching_status][aerosol][variable].append(None)
				else:
					globe[lat_lon][error][touching_status][aerosol][variable].append(float(item))
				
	#Add in calculated variables
	#Mean value calc variables
		CSWFTOA = globe[lat_lon][error][touching_status][aerosol]['CSWFTOA'][-1]
		CLWFTOA = globe[lat_lon][error][touching_status][aerosol]['CLWFTOA'][-1]
		if CSWFTOA != None and CLWFTOA != None:
			net = CSWFTOA + CLWFTOA
		else:
			net = None
		globe[lat_lon][error][touching_status][aerosol]['net'].append(net)

		for scope in SCOPES:
			CF = globe[lat_lon][error][touching_status][aerosol]["{}_cloud_fraction".format(scope)][-1]
		
			all_sky_albedo = globe[lat_lon][error][touching_status][aerosol]["{}_CERES_albedo".format(scope)][-1]
			clear_sky_albedo = globe[lat_lon][error][touching_status][aerosol]["{}_clear_sky_albedo".format(scope)][-1]
		
			CSWFTOA = globe[lat_lon][error][touching_status][aerosol]["{}_CSWFTOA".format(scope)][-1]
			CLWFTOA = globe[lat_lon][error][touching_status][aerosol]["{}_CLWFTOA".format(scope)][-1]
		
			#Calc cloudy forcings
			if CF != None and CSWFTOA != None:
				CF_CSWFTOA = CF * CSWFTOA
			else:
				CF_CSWFTOA = None
			globe[lat_lon][error][touching_status][aerosol]["{}_CF_CSWFTOA".format(scope)].append(CF_CSWFTOA)
		
			if CF!= None and CLWFTOA != None:
				CF_CLWFTOA = CF * CLWFTOA
			else:
				CF_CLWFTOA = None
			globe[lat_lon][error][touching_status][aerosol]["{}_CF_CLWFTOA".format(scope)].append(CF_CLWFTOA)
		
			#Net
			if CSWFTOA != None and CLWFTOA != None:
				net = CSWFTOA + CLWFTOA
			else:
				net = None
			globe[lat_lon][error][touching_status][aerosol]["{}_net".format(scope)].append(net)
		
			#Cloudy albedo
			if CF != None and CF != 0 and all_sky_albedo != None and clear_sky_albedo != None:
				CF_albedo = (all_sky_albedo - ((1-CF)*clear_sky_albedo))/CF
			else:
				CF_albedo = None
			globe[lat_lon][error][touching_status][aerosol]["{}_cloud_albedo".format(scope)].append(CF_albedo)
	#End of for loop for number of cases
	return line_number

		

list_of_files = glob.glob(ROOTDIR + 'binneddata*.dat')
wet_start_granules = ['05354', '10709', '16017', '18690','21315']
lat_lons = []
for filename in list_of_files:
	filename = filename.split("_")
	longitude = filename[-2].split(".")[-1]
	latitude = filename[-1].split(".")[0]

	if (latitude, longitude) not in lat_lons:
		lat_lons.append((latitude, longitude))

list_of_files = defaultdict(dict)


for lat_lon in lat_lons:
	lat = lat_lon[0]
	lon = lat_lon[1]
	filelist = glob.glob(ROOTDIR + 'binneddata*.' + '{}_{}.dat'.format(lon,lat))
	#for filename in filelist:
		#print filename
	list_of_files["{}_{}".format(lat, lon)] = filelist
#Temporary code

for lat_lon, filelist in list_of_files.iteritems():
	print lat_lon
	for filename in filelist:
		print filename
		f = open(filename, "r")
		line_number = 0
		
		latitude = lat_lon.split("_")[0]
		longitude = lat_lon.split("_")[1]
	
		daynight = False
		if "day" in filename or "night" in filename:
			daynight = True

		
		error_file = filename
		if "touching" in filename and not daynight:
			parts_of_filename = filename.split(".")
			error = parts_of_filename[6]
			parts_of_filename[9] = parts_of_filename[9].split("_")
			touching_status = parts_of_filename[8]
	
		elif "touching" not in filename and not daynight:
			parts_filename = filename.split(".")
			error = parts_filename[6]
			parts_filename[8] = parts_filename[8].split("_")
			touching_status = "all"
		#daynight if day or night and day/night added to touching layer name
		if daynight:
			if "touching" in filename:
				parts_of_filename = filename.split(".")
				error = parts_of_filename[6]
				parts_of_filename[10] = parts_of_filename[10].split("_")
				touching_status = parts_of_filename[8] + "_" + parts_of_filename[9]

			elif "touching" not in filename:
				parts_of_filename = filename.split(".")
				error = parts_of_filename[6]
				parts_of_filename[9] = parts_of_filename[9].split("_")
				touching_status = "all_" + parts_of_filename[8]
		#If season the season is added to the touching layer name
		test_name = filename.split(".")
		start_granule, end_granule = test_name[1].split("_")
		if start_granule != '03607':
			filename_parts = filename.split(".")
			filename_parts = filename_parts[1]
			start_granule = filename_parts.split("_")[0]
			if start_granule in wet_start_granules:
				if start_granule == '05354' and end_granule == '23997':
					season = 'all_year_season'
				else:
					season = 'wet'
			else:
				season = 'dry'
			touching_status = touching_status + "_" + season
			
		##########
		#TEMPORARY

		bins = defaultdict(lambda:defaultdict(dict))
		bin_limit_counts = [4,10,15]
		bin_limits = []
		#Finding bin limits for the specific file
		for i in range(3):
			f.readline()
			line_number += 1
			bin_limit_lines = [[],[]]
			bin_count = bin_limit_counts[i]
			bin_rows = int(math.ceil(bin_count/6.0))
			for j in range(2):
				for l in range(bin_rows):
					splitted = f.readline().split()
					bin_limit_lines[j] += [float(limit) for limit in splitted]
					line_number += 1

			zipped = zip(*bin_limit_lines)
			#bin_limits.append(zipped)
			order[i][touching_status][latitude+"_"+longitude] = zipped

		while line_number < 28:
			f.readline()
			line_number += 1

		print "Making ", latitude, longitude, touching_status
		for i in range(21):
			line_number = create_aerosol_dict(f, error, latitude, longitude, touching_status, line_number)

		f.close()
		#Close the file that contained all the data
		#Write out the file
	print 'current key(s), should be one', globe.keys()
	for latlon, item in globe.iteritems():
		file_dump_name = "{}{}.txt".format(DUMP_DIR, latlon)
		with open(file_dump_name, 'w') as d:
			json.dump(item, d)
	#Remove key since it has been written
	globe.pop(latlon)
	#This should always be 0
	print 'should be empty', globe.keys()
	if len(globe) != 0:
		print 'Error did not dump all keys as planned'
		print globe.keys()
		sys.exit()
	print 'time at', time.clock() - START_TIME
	
	
	
	


for item, filename in zip(order, ["rridx_bin_dict.js","geo_bin_dict.js","lwp_bin_dict.js"]):
	with open(DUMP_DIR + filename, "w") as g:
		json.dump(item, g)
	print "Successfully dumped bin "+ filename[:3]
'''
for latlonkey, latlon in globe.iteritems():
	for errorkey, error in latlon.iteritems():
		for touchingkey, touching in error.iteritems():
			for aerosolkey, aerosol in touching.iteritems():
				all_rrs = globe[latlonkey][errorkey][touchingkey][aerosolkey]["CloudSat_rain"]
				pop = []
				for r in all_rrs:
					if r in [3.0, 4.0, 5.0, 6.0]:
						pop.append(1.0)
					if r == 0:
						pop.append(0)
				
				globe[latlonkey][errorkey][touchingkey][aerosolkey]["CloudSat_pop"] = pop
				all_rrs = globe[latlonkey][errorkey][touchingkey][aerosolkey]["amsrrridx"]
				amsr_pop = []
				for r in all_rrs:
					if r > 0:
						amsr_pop.append(1.0)
					if r == 0:
						amsr_pop.append(0.0)
				
				globe[latlonkey][errorkey][touchingkey][aerosolkey]["amsrr_pop"] = amsr_pop
				
'''
	
#globe[lat_lon][error][touching_status][aerosol][scope+"_CF_CLWFTOA"] = []
#print globe.keys()
'''
error = open("test_NaN_files_numbers","w")
error.write(ERROR_DUMP_STRING)
error.close()'''
#To create file to dump pickle object of all data from that box (touching not touching all error no error)
#Looping through all latitudes and longitudes both + and -
'''
for key, item in globe.iteritems():
	file_dump_name = "{}{}.txt".format(DUMP_DIR, key)
	with open(file_dump_name, "w") as d:
		print file_dump_name
		json.dump(item, d)
	print "Successfully wrote", key
	globe.pop(key)'''
	
	#noerror = cloudpickle.dumps(noerror)
	#error = cloudpickle.dumps(error)
	#with open(file_dump_name,"w") as d:
	#	d.write(noerror)
	#	d.write(error)
		
		#pickle.dump(noerror, d)
		#pickle.dump(error, d)
		
		
