pro gd
  
; Next: Add mixed-phase cloud flag from 2C-CLDCLASS-LIDAR.  Can we use
; the combination of CloudSat/CALIPSO SC liquid flag and AMSR-E to try
; and estimate the amount of SC liquid globally when T < 0C at the
; surface?  I.e. when we're sure the microwave signal is all from SC water.
;
; November 10, 2014: Added radiation budget quantities.
;
; August 12, 2013: Added FLXHR-LIDAR and CLDCLASS to output so that
; AUX files can be used for rainflxhrnew.pro and mean cross-section
; codes as well.  The resulting 2d files are quite large and are now
; output to a directory on boltzmann as opposed to thermal.  However
; the files are comprehensive enough to allow analysis of aerosol and
; cloud heating profiles.
;
; In an effort to streamline the aerosol analysis, especially now that
; multiple runs are being done, this code reads in all input required
; for rainaerosol.pro and generates a single regional subset file that
; contains all data required for the subsequent analysis.
;
; NOTE:  This code may also be a precursor to creating "average"
; CloudSat tracks (e.g. the Sweden cross-sections).

;!PATH = '/solar/home/tristan/PriorWork/IDLref/Routines/Berg_new:' + !PATH

close,17
Rovercp = 287./1004.
pi = 3.141592653
rhol = 1.e6             ; in g/m3

datadir = '/thermal/data/CloudSat/2c-precip-column/P2.R04/'
datadirb = '/thermal/data/CloudSat/2b-geoprof/'
datadirc = '/thermal/data/CloudSat/2c-rain-profile/'
datadird = '/thermal/data/CloudSat/ecmwf-aux/'
datadird2 = '/thermal/data/CloudSat/ecmwf2-aux/'
datadire = '/thermal/data/CloudSat/amsr-aux/'
datadirf = '/thermal/data/CloudSat/merra-aux/2d/'

datadirg = '/thermal/data/CloudSat/2b-flxhr-lidar-aux/'
datadirh = '/thermal/data/CloudSat/2b-cldclass/'
datadiri = '/thermal/data/CloudSat/2b-flxhr-erb/P2_R04/'
datadirj = '/thermal/data/CloudSat/2b-flxhr-lidar/P2_R04/'
datadirk = '/thermal/data/CloudSat/2c-snow-profile/'
datadirl = '/thermal/data/CloudSat/mod06-aux/'
sffxb = '_CS_2B-GEOPROF_GRANULE_P'
sffxc = '_CS_2C-RAIN-PROFILE_GRANULE_P'
sffxd = '_CS_ECMWF-AUX_GRANULE_P'
sffxd2 = '_CS_ECMWF2-AUX_GRANULE_B00'
sffxe = '_CS_AMSR-AUX_GRANULE_P'
sffxf = '_2d_MERRA-AUX.h5'
sffxg = '_CS_FLXHR-LIDAR-AUX_GRANULE_P'
sffxh = '_CS_2B-CLDCLASS_GRANULE_P'
sffxi = '_CS_2B-FLXHR-LIDAR-ERB_GRANULE_P2'
sffxj = '_CS_2B-FLXHR-LIDAR_GRANULE_P2'
sffxk = '_CS_2C-SNOW-PROFILE_GRANULE_P'
sffxl = '_CS_MOD06-1KM-AUX_GRANULE_X'
sffxl2 = '_CS_MOD06-5KM-AUX_GRANULE_X'
gemsdir = '/thermal/data/GEMS/Matchups/'
gemssffx = '_GEMS_AOD.HDF'
maccdir = '/thermal/data/CloudSat/macc-aux/'
maccsffx = '_MACC-AUX_R04_E0*.sav'


;maccsffx = '_MACC-AUX_R04_E02.sav'     ; Pre-2010
;maccsffx = '_MACC-AUX_R04_E03.sav'      ; 2010 (granule 19800) and beyond

outdir1d = '/boltzmann/data2/V2_AuxFiles1d/Temp5/'
outdir2d = '/boltzmann/data2/V2_AuxFiles2d/'

;filespecs = ['*_03*.hdf','*_04*.hdf']
filespecs = ['*_03*.hdf','*_04*.hdf','*_05*.hdf','*_06*.hdf','*_07*.hdf','*_08*.hdf','*_09*.hdf','*_10*.hdf','*_11*.hdf','*_12*.hdf','*_13*.hdf','*_14*.hdf','*_15*.hdf','*_16*.hdf','*_17*.hdf','*_18*.hdf','*_19*.hdf','*_20*.hdf','*_21*.hdf','*_22*.hdf','*_23*.hdf','*_24*.hdf','*_25*.hdf','*_26*.hdf']
colorflag = 1                             ; 0 - B&W, 1 - Color
;Test granules
;startgranule = '03607'     ; January, 2007
;endgranule = '03609'

;endgranule = '04057'
;startgranule = '03156'     ; Dec 2006-Feb 2007
;endgranule = '04465'
;startgranule = '04466'     ; March 1-May 31, 2007
;startgranule = '05413'     ; March 1-May 31, 2007
;endgranule = '05805'
;startgranule = '05806'     ; June 1-Aug. 31, 2007
;endgranule = '07145'
;startgranule = '07146'     ; Sep 1-Nov 30, 2007
;endgranule = '08470'
;startgranule = '03607'     ; 2007
;endgranule = '08921'
startgranule = '08922'     ; Full Year 2008
endgranule = '14251'
;startgranule = '14252'     ; Full Year 2009
;endgranule = '19209'
;startgranule = '19786'     ; Full Year 2010
;endgranule = '24882'

nspecs = n_elements(filespecs)
for i=0,nspecs-1 do begin
 if(i eq 0) then begin
  filelist = findfile(datadir + filespecs(i))
 endif else begin
  filelist = [filelist,findfile(datadir + filespecs(i))]
 endelse
endfor  ;i

filelistb = datadirb + strmid(filelist,strlen(datadir),19) + sffxb + strmid(filelist,strlen(filelist(0))-12,12)
filelistc = datadirc + strmid(filelist,strlen(datadir),19) + sffxc + strmid(filelist,strlen(filelist(0))-12,12)
filelistd = datadird + strmid(filelist,strlen(datadir),19) + sffxd + strmid(filelist,strlen(filelist(0))-12,12)
filelistd2 = datadird2 + strmid(filelist,strlen(datadir),19) + sffxd2 + strmid(filelist,strlen(filelist(0))-12,12)
fileliste = datadire + strmid(filelist,strlen(datadir),19) + sffxe + strmid(filelist,strlen(filelist(0))-12,12)
filelistf = datadirf + strmid(filelist,strlen(datadir),19) + sffxf
filelistg = datadirg + strmid(filelist,strlen(datadir),19) + sffxg + strmid(filelist,strlen(filelist(0))-12,12)
filelisth = datadirh + strmid(filelist,strlen(datadir),19) + sffxh + strmid(filelist,strlen(filelist(0))-12,12)
filelisti = datadiri + strmid(filelist,strlen(datadir),19) + sffxi + strmid(filelist,strlen(filelist(0))-12,12)
filelistj = datadirj + strmid(filelist,strlen(datadir),19) + sffxj + strmid(filelist,strlen(filelist(0))-12,12)
filelistk = datadirk + strmid(filelist,strlen(datadir),19) + sffxk + strmid(filelist,strlen(filelist(0))-12,12)
filelistl = datadirl + strmid(filelist,strlen(datadir),19) + sffxl + strmid(filelist,strlen(filelist(0))-12,12)
filelistl2 = datadirl + strmid(filelist,strlen(datadir),19) + sffxl2 + strmid(filelist,strlen(filelist(0))-12,12)
gemslist = gemsdir + strmid(filelist,strlen(datadir),19) + gemssffx
macclist = maccdir + strmid(filelist,strlen(datadir),19) + maccsffx



nfiles = n_elements(filelist)
granlist = strarr(nfiles)
for i=0,nfiles-1 do begin
  granlist(i) = strmid(filelist(i),strlen(datadir)+14,5)
endfor  ;i
index = where(granlist ge startgranule and granlist le endgranule,ngood)
granlist = granlist(index)
filelist = filelist(index)
filelistb = filelistb(index)
filelistc = filelistc(index)
filelistd = filelistd(index)
filelistd2 = filelistd2(index)
fileliste = fileliste(index)
filelistf = filelistf(index)
filelistg = filelistg(index)
filelisth = filelisth(index)
filelisti = filelisti(index)
filelistj = filelistj(index)
filelistk = filelistk(index)
filelistl = filelistl(index)
filelistl2 = filelistl2(index)
gemslist = gemslist(index)
macclist = macclist(index)



modisstart = strmid(filelist(0),strlen(datadir),7)
modisend = strmid(filelist(ngood-1),strlen(datadir),7)
sprintarsstart = strmid(filelist(0),strlen(datadir),4)
sprintarsend = strmid(filelist(ngood-1),strlen(datadir),4)

yearval = float(strmid(filelist(ngood-1),strlen(datadir),4))
dayval = float(strmid(filelist(ngood-1),strlen(datadir)+4,3))
basejd = julday(1,1,yearval)-1
caldat,basejd+dayval,month1,day1,year1

 if(month1 lt 10) then begin
  if(day1 lt 10) then begin
   cccmend = strcompress(string(year1),/remove_all) + '0' + strcompress(string(month1),/remove_all) + '0' + strcompress(string(day1),/remove_all)
  endif else begin
   cccmend = strcompress(string(year1),/remove_all) + '0' + strcompress(string(month1),/remove_all) + strcompress(string(day1),/remove_all)
  endelse
 endif else begin
  if(day1 lt 10) then begin
   cccmend = strcompress(string(year1),/remove_all) + strcompress(string(month1),/remove_all) + '0' + strcompress(string(day1),/remove_all)
  endif else begin
   cccmend = strcompress(string(year1),/remove_all) + strcompress(string(month1),/remove_all) + strcompress(string(day1),/remove_all)
  endelse
 endelse

yearval = float(strmid(filelist(0),strlen(datadir),4))
basejd = julday(1,1,yearval)-1                             ; Subtract 1 for start of year
dayval = float(strmid(filelist(0),strlen(datadir)+4,3))
caldat,basejd+dayval,month1,day1,year1
refjd = julday(month1,day1,year1)

 if(month1 lt 10) then begin
  if(day1 lt 10) then begin
   cccmstart = strcompress(string(year1),/remove_all) + '0' + strcompress(string(month1),/remove_all) + '0' + strcompress(string(day1),/remove_all)
  endif else begin
   cccmstart = strcompress(string(year1),/remove_all) + '0' + strcompress(string(month1),/remove_all) + strcompress(string(day1),/remove_all)
  endelse
 endif else begin
  if(day1 lt 10) then begin
   cccmstart = strcompress(string(year1),/remove_all) + strcompress(string(month1),/remove_all) + '0' + strcompress(string(day1),/remove_all)
  endif else begin
   cccmstart = strcompress(string(year1),/remove_all) + strcompress(string(month1),/remove_all) + strcompress(string(day1),/remove_all)
  endelse
 endelse

close,24
openw,24,'missingfiles.' + granlist(0) + '_' + granlist(ngood-1) + '.dat'

SPRINTARSdir = '/thermal/tristan/SPRINTARS/'
aerfilessu = SPRINTARSdir + ['tausu_y2006.nc','tausu_y2007.nc','tausu_y2008.nc','tausu_y2009.nc','tausu_y2010.nc']
aerfilesdu = SPRINTARSdir + ['taudu_y2006.nc','taudu_y2007.nc','taudu_y2008.nc','taudu_y2009.nc','taudu_y2010.nc']
aerfilesoc = SPRINTARSdir + ['tauoc_y2006.nc','tauoc_y2007.nc','tauoc_y2008.nc','tauoc_y2009.nc','tauoc_y2010.nc']
aerfilesbc = SPRINTARSdir + ['taubc_y2006.nc','taubc_y2007.nc','taubc_y2008.nc','taubc_y2009.nc','taubc_y2010.nc']
aerfilessa = SPRINTARSdir + ['tausa_y2006.nc','tausa_y2007.nc','tausa_y2008.nc','tausa_y2009.nc','tausa_y2010.nc']
yeartxt = ['2006','2007','2008','2009','2010']
yeardays = [365,365,366,365,365]
nsfiles = n_elements(aerfilessu)
yearlist = strarr(nsfiles)
for i=0,nsfiles-1 do begin
  yearlist(i) = strmid(aerfilessu(i),strlen(SPRINTARSdir)+7,4)
endfor  ;i
index = where(yearlist ge sprintarsstart and yearlist le sprintarsend,nsnew)
aerfilessu = aerfilessu(index)
aerfilessa = aerfilessa(index)
aerfilesdu = aerfilesdu(index)
aerfilesoc = aerfilesoc(index)
aerfilesbc = aerfilesbc(index)
yeartxt = yeartxt(index)
yeardays = yeardays(index)

; C3M and MODIS aerosols are found in separate daily files

cccmdir = '/bohr/data4/CCCM_new/'
cccmhead = cccmdir + 'CER-NEWS_CCCM_Aqua-FM3-MODIS-CAL-CS_RelB1_905906.'
cccmspecs = cccmdir + ['CER-NEWS_CCCM_Aqua-FM3-MODIS-CAL-CS_RelB1_905906.2006*.hdf','CER-NEWS_CCCM_Aqua-FM3-MODIS-CAL-CS_RelB1_905906.2007*.hdf','CER-NEWS_CCCM_Aqua-FM3-MODIS-CAL-CS_RelB1_905906.2008*.hdf','CER-NEWS_CCCM_Aqua-FM3-MODIS-CAL-CS_RelB1_905906.2009*.hdf','CER-NEWS_CCCM_Aqua-FM3-MODIS-CAL-CS_RelB1_905906.2010*.hdf']
ncspecs = n_elements(cccmspecs)
for i=0,ncspecs-1 do begin
 if(i eq 0) then begin
  cccmlist = findfile(cccmspecs(i))
 endif else begin
  cccmlist = [cccmlist,findfile(cccmspecs(i))]
 endelse
endfor  ;i

ncccmfiles = n_elements(cccmlist)
cccmdaylist = strarr(ncccmfiles)
for i=0,ncccmfiles-1 do begin
  cccmdaylist(i) = strmid(cccmlist(i),strlen(cccmdir)+49,8)
endfor  ;i
index = where(cccmdaylist ge cccmstart and cccmdaylist le cccmend,ncccmnew)
cccmlist = cccmlist(index)

modisdir = "/thermal/tristan/MODIS/MYD08/"
modisspecs = modisdir + ["MYD08_D3.A2006*.hdf","MYD08_D3.A2007*.hdf","MYD08_D3.A2008*.hdf","MYD08_D3.A2009*.hdf","MYD08_D3.A2010*.hdf"]
nmspecs = n_elements(modisspecs)
for i=0,nmspecs-1 do begin
 if(i eq 0) then begin
  modislist = findfile(modisspecs(i))
 endif else begin
  modislist = [modislist,findfile(modisspecs(i))]
 endelse
endfor  ;i

nmodisfiles = n_elements(modislist)
modisdaylist = strarr(nmodisfiles)
for i=0,nmodisfiles-1 do begin
  modisdaylist(i) = strmid(modislist(i),strlen(modisdir)+10,7)
endfor  ;i
index = where(modisdaylist ge modisstart and modisdaylist le modisend,nmodisnew)
modislist = modislist(index)

; Establish continent/basin mask from NEWS

; Read mask
;
; 1 - Arctic Ocean
; 2 - North Atlantic Ocean
; 3 - South Atlantic Ocean
; 4 - North Pacific Ocean
; 5 - South Pacific Ocean
; 6 - Gulf of Mexico
; 7 - Mediterranean
; 8 - Black Sea
; 9 - Indian Ocean
; 10 - Australia
; 11 - North America
; 12 - South America
; 13 - Greenland
; 14 - EurAsia
; 15 - Africa
; 16 - Antarctica
; 17 - Uncertain/coast

newsmask = fltarr(360,180)
tempchar = ' '
close,15
openr,15,'Continent_ocean_map.dat'
 for i=0,359 do begin
   readf,15,tempchar
   for j=0,179 do begin
     tmpchar = strmid(tempchar,j,1)
     if(tmpchar eq '1') then begin
       newsmask(i,j) = 1
    endif
    if(tmpchar eq '7') then begin
       newsmask(i,j) = 2
     endif
     if(tmpchar eq 'b') then begin
       newsmask(i,j) = 3
     endif
     if(tmpchar eq '8') then begin
       newsmask(i,j) = 4
     endif
     if(tmpchar eq 'a') then begin
       newsmask(i,j) = 5
     endif
     if(tmpchar eq '2') then begin
       newsmask(i,j) = 6
     endif
     if(tmpchar eq '3') then begin
       newsmask(i,j) = 7
     endif
     if(tmpchar eq '4') then begin
       newsmask(i,j) = 8
     endif
     if(tmpchar eq '9') then begin
       newsmask(i,j) = 9
     endif
     if(tmpchar eq 'R') then begin
       newsmask(i,j) = 10
     endif
     if(tmpchar eq 'N') then begin
       newsmask(i,j) = 11
     endif
     if(tmpchar eq 'S') then begin
       newsmask(i,j) = 12
     endif
     if(tmpchar eq 'G') then begin
       newsmask(i,j) = 13
     endif
     if(tmpchar eq 'U') then begin
       newsmask(i,j) = 14
     endif
     if(tmpchar eq 'A') then begin
       newsmask(i,j) = 15
     endif
     if(tmpchar eq 'H') then begin
       newsmask(i,j) = 16
     endif
     if(tmpchar eq '0') then begin
       newsmask(i,j) = 17
     endif
   endfor  ;i
 endfor  ;j
close,15

; Read SPRINTARS aerosol data.  Append new files onto old ones.

nsfiles = n_elements(aerfilessu)
for fileid=0,nsfiles-1 do begin

 a=ncdf_open(aerfilessu(fileid))

 print, 'Reading from ', aerfilessu(fileid)

 inq=ncdf_inquire(a)

 varnamedata = strarr(inq.nvars)

 for i=0,inq.nvars-1 do begin
  info = ncdf_varinq(a,i)
  varnamedata[i] = info.name
 endfor  ;i

 lonid = 0
 latid = 1
 timeid = 2
 tauid = 3

 ncdf_varget,a,lonid,longrid
 ncdf_varget,a,latid,latgrid
 ncdf_varget,a,timeid,timegridt
 ncdf_varget,a,tauid,sugridt

 ncdf_close,a

 nlon = n_elements(longrid)
 nlat = n_elements(latgrid)

 a=ncdf_open(aerfilesbc(fileid))

 print, 'Reading from ', aerfilesbc(fileid)

 inq=ncdf_inquire(a)

 varnamedata = strarr(inq.nvars)

 for i=0,inq.nvars-1 do begin
  info = ncdf_varinq(a,i)
  varnamedata[i] = info.name
 endfor  ;i

 lonid = 0
 latid = 1
 timeid = 2
 tauid = 3

 ncdf_varget,a,tauid,bcgridt

 ncdf_close,a

 a=ncdf_open(aerfilesoc(fileid))

 print, 'Reading from ', aerfilesoc(fileid)

 inq=ncdf_inquire(a)

 varnamedata = strarr(inq.nvars)

 for i=0,inq.nvars-1 do begin
  info = ncdf_varinq(a,i)
  varnamedata[i] = info.name
 endfor  ;i

 lonid = 0
 latid = 1
 timeid = 2
 tauid = 3

 ncdf_varget,a,tauid,ocgridt

 ncdf_close,a

 a=ncdf_open(aerfilesdu(fileid))

 print, 'Reading from ', aerfilesdu(fileid)

 inq=ncdf_inquire(a)

 varnamedata = strarr(inq.nvars)

 for i=0,inq.nvars-1 do begin
  info = ncdf_varinq(a,i)
  varnamedata[i] = info.name
 endfor  ;i

 lonid = 0
 latid = 1
 timeid = 2
 tauid = 3

 ncdf_varget,a,tauid,dugridt

 ncdf_close,a

 a=ncdf_open(aerfilessa(fileid))

 print, 'Reading from ', aerfilessa(fileid)

 inq=ncdf_inquire(a)

 varnamedata = strarr(inq.nvars)

 for i=0,inq.nvars-1 do begin
  info = ncdf_varinq(a,i)
  varnamedata[i] = info.name
 endfor  ;i

 lonid = 0
 latid = 1
 timeid = 2
 tauid = 3

 ncdf_varget,a,tauid,sagridt

 ncdf_close,a

; For all files other than the first, append data onto the existing grid.

 if(fileid eq 0) then begin
  sugrid = sugridt
  dugrid = dugridt
  ocgrid = ocgridt
  bcgrid = bcgridt
  sagrid = sagridt
  timegrid = timegridt
 endif else begin
  ntimeold = n_elements(sugrid(0,0,*))
  ntimenew = n_elements(timegridt)
  sutmp = sugrid
  dutmp = dugrid
  octmp = ocgrid
  bctmp = bcgrid
  satmp = sagrid
  sugrid = fltarr(nlon,nlat,ntimeold+ntimenew)
  sagrid = fltarr(nlon,nlat,ntimeold+ntimenew)
  ocgrid = fltarr(nlon,nlat,ntimeold+ntimenew)
  bcgrid = fltarr(nlon,nlat,ntimeold+ntimenew)
  dugrid = fltarr(nlon,nlat,ntimeold+ntimenew)
  for i=0,ntimeold-1 do begin
   for k=0,nlon-1 do begin
    for l=0,nlat-1 do begin
     sugrid(k,l,i) = sutmp(k,l,i)
     dugrid(k,l,i) = dutmp(k,l,i)
     sagrid(k,l,i) = satmp(k,l,i)
     ocgrid(k,l,i) = octmp(k,l,i)
     bcgrid(k,l,i) = bctmp(k,l,i)
    endfor  ;l
   endfor  ;k
  endfor  ;i
  for i=0,ntimenew-1 do begin
   for k=0,nlon-1 do begin
    for l=0,nlat-1 do begin
     sugrid(k,l,i+ntimeold) = sugridt(k,l,i)
     sagrid(k,l,i+ntimeold) = sagridt(k,l,i)
     dugrid(k,l,i+ntimeold) = dugridt(k,l,i)
     ocgrid(k,l,i+ntimeold) = ocgridt(k,l,i)
     bcgrid(k,l,i+ntimeold) = bcgridt(k,l,i)
    endfor  ;l
   endfor  ;k
  endfor  ;i
  timegrid = [timegrid,timegridt + max(timegrid) + 12.]
 endelse

endfor  ; fileid

taugrid = sugrid + dugrid + ocgrid + bcgrid + dugrid

sugridt = 0
dugridt = 0
ocgridt = 0
bcgridt = 0
dugridt = 0

; Read and grid MODIS AOD data for easy extraction

; Open HDF file

print,modislist(0)
fileID = HDF_SD_START(modislist(0), /READ)

; Read AOD

DATASETID = HDF_SD_SELECT(fileID, 26)
HDF_SD_GETDATA, DATASETID, TAU
TAU = FLOAT(TAU)
BAD_TAU = WHERE( TAU LT 0, COMPLEMENT = GOOD_TAU )
TAU[GOOD_TAU] = TAU[GOOD_TAU]*0.001
TAU[BAD_TAU] = -999.0
TAU = REVERSE(TAU,2)

; Read Angstrom coefficient

DATASETID = HDF_SD_SELECT(fileID, 202)
HDF_SD_GETDATA, DATASETID, ALPHA
ALPHA = FLOAT(ALPHA)
BAD_ALPHA = WHERE( ALPHA LT 0, COMPLEMENT = GOOD_ALPHA ) 
ALPHA[GOOD_ALPHA] = ALPHA[GOOD_ALPHA]*0.001
ALPHA[BAD_ALPHA] = -999.0
ALPHA = REVERSE(ALPHA,2)

; Read fraction

DATASETID = HDF_SD_SELECT(fileID, 465)
HDF_SD_GETDATA, DATASETID, CF
CF = CF*0.01

; Close HDF file

HDF_SD_END, fileID

; Define annual array and write first days' data

nmodislat = n_elements(tau(0,*))
print,nmodislat
nmodislon = n_elements(tau(*,0))
print,nmodislon
modislatgrid = findgen(nmodislat) - 89.5
modislongrid = findgen(nmodislon) - 179.5
print,size(modislatgrid),size(modislongrid)


modisAOD = fltarr(yeardays(0),nmodislon,nmodislat)
;modisang = modisAOD
modisAI = modisAOD
for i=0,nmodislon-1 do begin
 for j=0,nmodislat-1 do begin
  modisAOD(0,i,j) = TAU(i,j)
;  modisang(0,i,j) = ALPHA(i,j)
  if(TAU(i,j) ne -999. and ALPHA(i,j) ne -999.) then begin
    modisAI(0,i,j) = TAU(i,j)*ALPHA(i,j)
  endif else begin
    modisAI(0,i,j) = -999.
  endelse
 endfor  ;j
endfor  ;i

; Loop through remaining days and add to annual array

nfilesmodis = n_elements(modislist)
for k=1,nfilesmodis-1 do begin
 print,modislist(k)
 fileID = HDF_SD_START(modislist(k), /READ)

 DATASETID = HDF_SD_SELECT(fileID, 26)
 HDF_SD_GETDATA, DATASETID, TAU
 TAU = FLOAT(TAU)
 BAD_TAU = WHERE( TAU LT 0, COMPLEMENT = GOOD_TAU )
 TAU[GOOD_TAU] = TAU[GOOD_TAU]*0.001
 TAU[BAD_TAU] = -999.0
 TAU = REVERSE(TAU,2)

 DATASETID = HDF_SD_SELECT(fileID, 202)
 HDF_SD_GETDATA, DATASETID, ALPHA
 ALPHA = FLOAT(ALPHA)
 BAD_ALPHA = WHERE( ALPHA LT 0, COMPLEMENT = GOOD_ALPHA ) 
 ALPHA[GOOD_ALPHA] = ALPHA[GOOD_ALPHA]*0.001
 ALPHA[BAD_ALPHA] = -999.0
 ALPHA = REVERSE(ALPHA,2)

 DATASETID = HDF_SD_SELECT(fileID, 465)
 HDF_SD_GETDATA, DATASETID, CF
 CF = CF*0.01

 HDF_SD_END, fileID

 for i=0,nmodislon-1 do begin
  for j=0,nmodislat-1 do begin
   modisAOD(k,i,j) = TAU(i,j)
;   modisang(k,i,j) = ALPHA(i,j)
   if(TAU(i,j) ne -999. and ALPHA(i,j) ne -999.) then begin
    modisAI(k,i,j) = TAU(i,j)*ALPHA(i,j)
   endif else begin
    modisAI(k,i,j) = -999.
   endelse
  endfor  ;j
 endfor  ;i
endfor  ;k

; Read in CERES data

  nceresfiles = n_elements(cccmlist)
  finc = cccmlist(0)
  print,finc
  ftest = findfile(finc)
  if(strlen(ftest) lt 10) then begin
   printf,24,finc + ' Granule skipped'
   goto,skiporbit
  endif
  hdf_get, finc, $
    ['Time of observation', $
     'Longitude of CERES FOV at surface', $
     'Colatitude of CERES FOV at surface', $
     'CERES solar zenith at surface', $
     'Surface type index', $
     'Cloud classification', $
     'CERES SW TOA flux - upwards', $
     'CERES SW TOA flux - downwards', $
     'CERES LW TOA flux - upwards', $
     'CERES downward SW surface flux - Model A', $
;     'CERES downward SW surface flux - Model B', $
;     'CERES downward SW surface flux - Model C', $
     'CERES downward LW surface flux - Model A', $
;     'CERES downward LW surface flux - Model B', $
;     'CERES downward LW surface flux - Model C', $
     'CERES net SW surface flux - Model A', $
;     'CERES net SW surface flux - Model B', $
;     'CERES net SW surface flux - Model C', $
     'CERES net LW surface flux - Model A', $
;     'CERES net LW surface flux - Model B', $
;     'CERES net LW surface flux - Model C', $
     'CERES broadband surface albedo', $
     'CERES LW surface emissivity', $
     'Surface pressure', $
     'Precipitable water', $
     'Surface skin temperature'], $
    varname = $
    ['cerestimet', $
     'cereslont', $
     'cerescolatt', $
     'szacerest', $
     'stypecerest', $
     'cldcerest', $
     'fuswtoacerest', $
     'fdswtoacerest', $
     'fulwtoacerest', $
     'fdswsfccerest', $
     'fdlwsfccerest', $
     'fnetswsfccerest', $
     'fnetlwsfccerest', $
     'albedocerest', $
     'semisscerest', $
     'spcerest', $
     'cwvcerest', $
     'stcerest']

    cerestime = cerestimet - refjd + 0.5    ; Correct for the half day in the definition of Julian day
    cereslon = cereslont
    cereslat = 90. - cerescolatt
    szaceres = szacerest
    stypeceres = fltarr(n_elements(cereslat))
    stypeceres(*) = stypecerest(0,0,*)
    cldceres = cldcerest
    fuswtoaceres = fuswtoacerest
    fdswtoaceres = fdswtoacerest
    fulwtoaceres = fulwtoacerest
    fdswsfcceres = fdswsfccerest
    fdlwsfcceres = fdlwsfccerest
    fnetswsfcceres = fnetswsfccerest
    fnetlwsfcceres = fnetlwsfccerest
    albedoceres = albedocerest
    semissceres = semisscerest
    spceres = spcerest
    cwvceres = cwvcerest
    stceres = stcerest

for i=1,nceresfiles-1 do begin

; Append all other CERES data to the first

  finc = cccmlist(i)
  print,finc
  hdf_get, finc, $
    ['Time of observation', $
     'Longitude of CERES FOV at surface', $
     'Colatitude of CERES FOV at surface', $
     'CERES solar zenith at surface', $
     'Surface type index', $
     'Cloud classification', $
     'CERES SW TOA flux - upwards', $
     'CERES SW TOA flux - downwards', $
     'CERES LW TOA flux - upwards', $
     'CERES downward SW surface flux - Model A', $
;     'CERES downward SW surface flux - Model B', $
;     'CERES downward SW surface flux - Model C', $
     'CERES downward LW surface flux - Model A', $
;     'CERES downward LW surface flux - Model B', $
;     'CERES downward LW surface flux - Model C', $
     'CERES net SW surface flux - Model A', $
;     'CERES net SW surface flux - Model B', $
;     'CERES net SW surface flux - Model C', $
     'CERES net LW surface flux - Model A', $
;     'CERES net LW surface flux - Model B', $
;     'CERES net LW surface flux - Model C', $
     'CERES broadband surface albedo', $
     'CERES LW surface emissivity', $
     'Surface pressure', $
     'Precipitable water', $
     'Surface skin temperature'], $
    varname = $
    ['cerestimet', $
     'cereslont', $
     'cerescolatt', $
     'szacerest', $
     'stypecerest', $
     'cldcerest', $
     'fuswtoacerest', $
     'fdswtoacerest', $
     'fulwtoacerest', $
     'fdswsfccerest', $
     'fdlwsfccerest', $
     'fnetswsfccerest', $
     'fnetlwsfccerest', $
     'albedocerest', $
     'semisscerest', $
     'spcerest', $
     'cwvcerest', $
     'stcerest']

    cerestime = [cerestime,cerestimet-refjd+0.5]
    cereslon = [cereslon,cereslont]
    cereslat = [cereslat,90. - cerescolatt]
    szaceres = [szaceres,szacerest]
    stypeceres = [stypeceres,reform(stypecerest(0,0,*))]
    cldceres = [cldceres,cldcerest]
    fuswtoaceres = [fuswtoaceres,fuswtoacerest]
    fdswtoaceres = [fdswtoaceres,fdswtoacerest]
    fulwtoaceres = [fulwtoaceres,fulwtoacerest]
    fdswsfcceres = [fdswsfcceres,fdswsfccerest]
    fdlwsfcceres = [fdlwsfcceres,fdlwsfccerest]
    fnetswsfcceres = [fnetswsfcceres,fnetswsfccerest]
    fnetlwsfcceres = [fnetlwsfcceres,fnetlwsfccerest]
    albedoceres = [albedoceres,albedocerest]
    semissceres = [semissceres,semisscerest]
    spceres = [spceres,spcerest]
    cwvceres = [cwvceres,cwvcerest]
    stceres = [stceres,stcerest]

 endfor  ;i

  cerestime = cerestime*86400.    ; convert to seconds since start of first day (UTC)
  index = where(cereslon ge 180.)
  cereslon(index) = cereslon(index) - 360. ; convert to -180/180 grid

; Now read in CloudSat data and use day ID to match up and extract
; appropriate AODs from the SPRINTARS list.  The SPRINTARS data
; are stored at 12 Z every day and the hour quoted in the timegrid is
; relative to Jaunary 1st in the first file read above (usually 2006).
; Here, we use a simple linear interpolation to determine the sulfate
; AOD at the CloudSat overpass time.

; Set up regional grid.  A separate output file will be generated for each.

fresolution = 15.
restxt = '.' + strmid(strcompress(string(fresolution),/remove_all),0,1) + 'degree.lightrain'
mxlat = 90.
mnlat = -90.
mxlon = 180.
mnlon = -180.
nlats = (mxlat-mnlat)/fresolution
nlons = (mxlon-mnlon)/fresolution
latvec = findgen(nlats)*fresolution + mnlat
lonvec = findgen(nlons)*fresolution + mnlon

; Height array for vertically-resolved fields (reflectivity primarily)

hgtmins = findgen(72)*0.24
hgtmaxs = hgtmins + 0.24
hgtmids = (hgtmaxs + hgtmins)/2.
nhgt = n_elements(hgtmins)

; Loop through files and read data.

nfiles = ngood

for fid=0,nfiles-1 do begin

on_ioerror,skiporbit

; GEOPROF data to supply heights and reflectivity statistics.  If
; GEOPROF doesn't exist then go ahead and skip orbit.

  fin=filelistb(fid)
  print,fin
  ftest = findfile(fin)
  if(strlen(ftest) lt 10) then begin
   printf,24,fin + ' Granule skipped'
   goto,skiporbit
  endif
  hdf_get, fin, $
    ['TAI_start', $
     'UTC_start', $
     'Profile_time', $
     'Height', $
     'SurfaceHeightBin', $
     'CPR_Cloud_mask', $
     'Radar_reflectivity'], $
    varname = $
    ['tai_start', $
     'utc_start', $
     'time', $
     'hgt', $
     'sbin', $
     'cmask', $
     'ref']

  sbin  = sbin - 1    ; Now represents lowest atmospheric layer

  nbad = n_elements(where(hgt(0,*) le 0.))
  npix = n_elements(hgt(0,*))
  nhgts = n_elements(hgt(*,0))
  nflux = 2

; Compute pixel times referenced to 0 UTC on the first day of data
; being processed

  csattime = tai_start + time - float(refjd - julday(1,1,1993))*86400.

; Skip any orbits with substantial missing data

  if(npix lt 10000 or nbad gt 1) then goto,skiporbit

; Next ECMWF data.  Likewise, can skip if ECMWF or MERRA is missing.

  find = filelistd(fid)
  print,find
  ftest = findfile(find)
  if(strlen(ftest) lt 10) then begin
   printf,24,find + ' Granule skipped'
   goto,skiporbit   
  endif
  hdf_get, find, $
    ['Pressure', $
     'Temperature', $
     'Specific_humidity'], $
    varname = $
    ['ecp', $
     'ect', $
     'ecq']

; Also ECMWF2 data (for SST needed to calculate rain water to
; accompany Wentz's LWPs)
  
  find2 = filelistd2(fid)
  print,find2,'supposed to be ecwmf2'
  
  ftest = findfile(find2)
  if(strlen(ftest) lt 10) then begin
   printf,24,find2 + ' Granule skipped'
   goto,skiporbit
  endif
  hdf_get, find2, $
    ['Sea_surface_temperature'], $
    varname = $
    ['ecsst']

; MERRA-AUX provides alternatives to ECMWF and supplies vertical motion.

 find3 = filelistf(fid)
 print,find3,'first'
 

 ftest = findfile(find3)
  if(strlen(ftest) lt 10) then begin
   printf,24,find3 + ' Granule skipped'
   goto,skiporbit
  endif
  
   
; Note, this file is a work in progress so it is advisable to search
; for variable ids based on variable names here.


 id = ncdf_open(find3,/NOWRITE)	; Open the file

 glob = ncdf_inquire(id)		; Find out general info

 varnames = strarr(glob.nvars)

 for yy=0,glob.nvars-1 do begin

; Create array of variable names

	info = ncdf_varinq(id, yy)
        varnames(yy) = info.name
       

; Get attributes if desired
;
;	for j=0,info.natts-1 do begin
;		attname = ncdf_attname(cdfid,i,j)
;		ncdf_attget,cdfid,i,attname,attvalue
;		print,'	Attribute ', attname, '=', string(attvalue)
;	endfor
 endfor
 print,find3
 

 ncdf_close,id
 
 capeid = where(varnames eq 'CAPE')

 liid = where(varnames eq 'LI')
 ltsid = where(varnames eq 'LTS')
 omegaid = where(varnames eq 'OMEGA500')
 psid = where(varnames eq 'PS')
 tsid = where(varnames eq 'TS')

 ;New merra 2d variables, no longer have U10 and V10
 cinid = where(varnames eq 'CIN')
 print,cinid
 cptid = where(varnames eq 'CPT')
 efluxid = where(varnames eq 'EFLUX')
 eis_3id = where(varnames eq 'EIS_3')
 eis_4id = where(varnames eq 'EIS_4')
 evapid = where(varnames eq 'EVAP')
 frcanid = where(varnames eq 'FRCAN')
 frccnid = where(varnames eq 'FRCCN')
 frclsid = where(varnames eq 'FRCLS')
 h500id = where(varnames eq 'H500')
 h850id = where(varnames eq 'H850')
 hfluxid = where(varnames eq 'HFLUX')
 omega700id = where(varnames eq 'OMEGA700')
 pblhid = where(varnames eq 'PBLH')
 precanvid = where(varnames eq 'PRECANV')
 precconid = where(varnames eq 'PRECCON')
 preclscid = where(varnames eq 'PRECLSC')
 prectotid = where(varnames eq 'PRECTOT')
 qlmlid = where(varnames eq 'QLML')
 qv700id = where(varnames eq 'QV700')
 rh700id = where(varnames eq 'RH700')
 print,rh700id
 slpid = where(varnames eq 'SLP')
 t700id = where(varnames eq 'T700')
 thvid = where(varnames eq 'THV')
 tqiid = where(varnames eq 'TQI')
 tqlid = where(varnames eq 'TQL')
 tqvid = where(varnames eq 'TQV')
 u500id = where(varnames eq 'U500')
 u700id = where(varnames eq 'U700')
 u850id = where(varnames eq 'U850')
 v500id = where(varnames eq 'V500')
 v700id = where(varnames eq 'V700')
 v850id = where(varnames eq 'V850')


 id = ncdf_open(find3,/NOWRITE)

  ncdf_varget, id, capeid(0), merracape
  ncdf_varget, id, liid(0), merrali
  ncdf_varget, id, ltsid(0), merralts
  ncdf_varget, id, omegaid(0), merraomega
  ncdf_varget, id, psid(0), merraps
  ncdf_varget, id, tsid(0), merrats
  
  ;add in new merra vars
  ncdf_varget, id, cinid(0), merracin
  ncdf_varget, id, cptid(0), merracpt
  ncdf_varget, id, efluxid(0), merraeflux
  ncdf_varget, id, eis_3id(0), merraeis3
  ncdf_varget, id, eis_4id(0), merraeis4
  ncdf_varget, id, evapid(0), merraevap
  ncdf_varget, id, frcanid(0), merrafrcan
  ncdf_varget, id, frccnid(0), merrafrccn
  ncdf_varget, id, frclsid(0), merrafrcls
  ncdf_varget, id, h500id(0), merrah500
  ncdf_varget, id, h850id(0), merrah850
  ncdf_varget, id, hfluxid(0), merrahflux
  ncdf_varget, id, omega700id(0), merraomega700
  ncdf_varget, id, pblhid(0), merrapblh
  ncdf_varget, id, precanvid(0), merraprecanv
  ncdf_varget, id, precconid(0), merrapreccon
  ncdf_varget, id, preclscid(0), merrapreclscid
  ncdf_varget, id, prectotid(0), merraprectot
  ncdf_varget, id, qlmlid(0), merraqlml
  ncdf_varget, id, qv700id(0), merraqv700
  ncdf_varget, id, rh700id(0), merrarh700

  ncdf_varget, id, slpid(0), merraslp
  ncdf_varget, id, t700id(0), merrat700
  ncdf_varget, id, thvid(0), merrathv
  ncdf_varget, id, tqiid(0), merratqi
  ncdf_varget, id, tqlid(0), merratql
  ncdf_varget, id, u500id(0), merrau500
  ncdf_varget, id, u700id(0), merrau700
  ncdf_varget, id, u850id(0), merrau850
  ncdf_varget, id, v500id(0), merrav500
  ncdf_varget, id, v700id(0), merrav700
  ncdf_varget, id, v850id(0), merrav850

 ncdf_close, id
 print,"here"





; Then amsr-aux.  AMSR-AUX is not required for several applications,
; just set fields to missing values if files are not present.

  finc = fileliste(fid)
  print,finc
  ftest = findfile(finc)
  if(strlen(ftest) lt 10) then begin
   printf,24,finc
   cwvamsr = fltarr(npix)
   cwvamsr(*) = -999.
   sstamsr = cwvamsr
   pamsr = cwvamsr
   lwpamsr = cwvamsr
   goto,skipamsr
  endif
  hdf_get, finc, $
    ['Med_res_vapor', $
     'High_res_cloud', $
     'Low_res_sst', $
     'Rain Rate', $
     'D_rain'], $
    varname = $
    ['cwvamsr', $
     'lwpamsr', $
     'sstamsr', $
     'pamsr', $
     'dist']

lwpamsr = lwpamsr*0.0001
cwvamsr = cwvamsr*0.01
sstamsr = sstamsr*0.01
pamsr = pamsr*0.1

skipamsr:

; Now MOD06-AUX.  Two versions are available: 1km and 5km.

  finc = filelistl(fid)
  print,finc
  ftest = findfile(finc)
  if(strlen(ftest) lt 10) then begin
   printf,24,finc
   remodis = fltarr(npix)
   remodis(*) = -999.
   cotmodis = remodis
   remodis1621 = remodis
   cotmodis1621 = remodis
   rediffmodis = remodis
   lwpmodis = remodis
   lwpmodis1621 = remodis
   remodiserror = remodis
   cotmodiserror = remodis
   lwpmodiserror = remodis
   phasemodis = remodis
   multilayermodis = remodis
   cirrusmodis = remodis
   cirrusflagmodis = remodis
   cmask1modis = remodis
   szamodis = remodis
   stmodis = remodis
   spmodis = remodis
   cttmodis = remodis
   ctpmodis = remodis
   flagmodis = remodis
   cf5modis = remodis
   cemissmodis = remodis
   irphasemodis = remodis
   cmask5modis = remodis
   goto,skipmodis
  endif
  hdf_get, finc, $
    ['Cloud_Effective_Radius', $
     'Cloud_Optical_Thickness', $
     'Cloud_Effective_Radius_1621', $
     'Cloud_Optical_Thickness_1621', $
     'Effective_Radius_Difference', $
     'Cloud_Water_Path', $
     'Cloud_Water_Path_1621', $
     'Cloud_Effective_Radius_Uncertainty', $
     'Cloud_Optical_Thickness_Uncertainty', $
     'Cloud_Water_Path_Uncertainty', $
     'Cloud_Phase_Optical_Properties',$
     'Cloud_Multi_Layer_Flag',$
     'Cirrus_Reflectance',$
     'Cirrus_Reflectance_Flag',$
     'Cloud_Mask_1km',$
     'Quality_Assurance_1km'], $
    varname = $
    ['remodis', $
     'cotmodis', $
     'remodis1621', $
     'cotmodis1621', $
     'rediffmodis', $
     'lwpmodis', $
     'lwpmodis1621', $
     'remodiserror', $
     'cotmodiserror', $
     'lwpmodiserror', $
     'phasemodis',$
     'multilayermodis',$
     'cirrusmodis',$
     'cirrusflagmodis',$
     'cmask1modis',$
     'qa1modis']

;  hdf_get, finc, $
;    ['Cloud_Effective_Radius_scale_factor', $
;     'Cloud_Optical_Thickness_scale_factor', $
;     'Cloud_Effective_Radius_1621_scale_factor', $
;     'Cloud_Optical_Thickness_1621_scale_factor', $
;     'Effective_Radius_Difference_scale_factor', $
;     'Cloud_Water_Path_scale_factor', $
;     'Cloud_Water_Path_1621_scale_factor', $
;     'Cloud_Effective_Radius_Uncertainty_scale_factor', $
;     'Cloud_Optical_Thickness_Uncertainty_scale_factor', $
;     'Cloud_Water_Path_Uncertainty_scale_factor', $
;     'Cloud_Phase_Optical_Properties_scale_factor',$
;     'Cloud_Multi_Layer_Flag_scale_factor',$
;     'Cirrus_Reflectance_scale_factor',$
;     'Cirrus_Reflectance_Flag_scale_factor',$
;     'Cloud_Mask_1km_scale_factor',$
;     'Quality_Assurance_1km_scale_factor'], $
;    varname = $
;    ['remodisscale', $
;     'cotmodisscale', $
;     'remodis1621scale', $
;     'cotmodis1621scale', $
;     'rediffmodisscale', $
;     'lwpmodisscale', $
;     'lwpmodis1621scale', $
;     'remodiserrorscale', $
;     'cotmodiserrorscale', $
;     'lwpmodiserrorscale', $
;     'phasemodisscale',$
;     'multilayermodisscale',$
;     'cirrusmodisscale',$
;     'cirrusflagmodisscale',$
;     'cmask1modisscale',$
;     'qa1kmmodisscale']

  finc = filelistl2(fid)
  print,finc
  ftest = findfile(finc)
  if(strlen(ftest) lt 10) then begin
   printf,24,finc
   szamodis = fltarr(npix)
   szamodis(*) = -999.
   stmodis = szamodis
   spmodis = szamodis
   cttmodis = szamodis
   ctpmodis = szamodis
   flagmodis = szamodis
   cf5modis = szamodis
   cemissmodis = szamodis
   irphasemodis = szamodis
   cmask5modis = szamodis
   goto,skipmodis
  endif
  hdf_get, finc, $
    ['Solar_Zenith', $
     'Surface_Temperature', $
     'Surface_Pressure', $
     'Processing_Flag', $
     'Cloud_Top_Pressure', $
     'Cloud_Top_Temperature', $
     'Cloud_Fraction', $
     'Cloud_Effective_Emissivity', $
     'Cloud_Phase_Infrared', $
     'Cloud_Mask_5km',$
     'Quality_Assurance_5km'], $
    varname = $
    ['szamodis', $
     'stmodis', $
     'spmodis', $
     'flagmodis',$
     'ctpmodis',$
     'cttmodis',$
     'cf5modis', $
     'cemissmodis', $
     'irphasemodis', $
     'cmask5modis',$
     'qa5modis']
;  hdf_get, finc, $
;    ['Solar_Zenith_scale_factor', $
;     'Surface_Temperature_scale_factor', $
;     'Surface_Pressure_scale_factor', $
;     'Processing_Flag_scale_factor', $
;     'Cloud_Top_Pressure_scale_factor', $
;     'Cloud_Top_Temperature_scale_factor', $
;     'Cloud_Fraction_scale_factor', $
;     'Cloud_Effective_Emissivity_scale_factor', $
;     'Cloud_Phase_Infrared_scale_factor', $
;     'Cloud_Mask_5km_scale_factor',$
;     'Quality_Assurance_5km_scale_factor'], $
;    varname = $
;    ['szamodisscale', $
;     'stmodisscale', $
;     'spmodisscale', $
;     'flagmodisscale',$
;     'ctpmodisscale',$
;     'cttmodisscale',$
;     'cf5modisscale', $
;     'cemissmodisscale', $
;     'irphasemodisscale', $
;     'cmask5modisscale',$
;     'qa5kmmodisscale']

remodis = remodis/100.
remodiserror = remodiserror/100.
remodis1621 = remodis1621/100.
cotmodis = cotmodis/100.
cotmodiserror = cotmodiserror/100.
cotmodis1621 = cotmodis1621/100.
lwpmodis = lwpmodis/1.
lwpmodiserror = lwpmodiserror/1.
lwpmodis1621 = lwpmodis1621/1.
cirrusmodis = cirrusmodis/5000.

szamodis = szamodis/100.
stmodis = (stmodis+15000.)/100.
spmodis = spmodis/10.
cttmodis = (cttmodis+15000.)/100.
ctpmodis = ctpmodis/10.
cf5modis = cf5modis/100.
cemissmodis = cemissmodis/100.

skipmodis:

; Then precip column.  Skip granule if missing.

  finc = filelist(fid)
  print,finc
  ftest = findfile(finc)
  if(strlen(ftest) lt 10) then begin
   printf,24,finc + ' Granule skipped'
   goto,skiporbit
  endif
  hdf_get, finc, $
    ['Precip_flag', $
     'Precip_rate', $
     'Status_flag', $
     'Cloud_flag', $
     'PIA_hydrometeor', $
     'Near_surface_reflectivity', $
     'Freezing_level', $
     'Surface_wind', $
     'Rain_top_height', $
     'Conv_strat_flag', $
     'RLWP', $
     'CLWP', $
     'Surface_type', $
     'Latitude', $
     'Longitude'], $
    varname = $
    ['pflag', $
     'rrcpr', $
     'qc', $
     'cflag', $
     'pia', $
     'znearsfc', $
     'fl', $
     'sfcwind', $
     'rth', $
     'csflag', $
     'rlwp', $
     'clwp', $
     'sfctype', $
     'lat', $
     'lon']

; Then rain profile

  finc = filelistc(fid)
  print,finc
  ftest = findfile(finc)
  if(strlen(ftest) lt 10) then begin
   printf,24,finc + ' Granule skipped'
   goto,skiporbit
  endif
  hdf_get, finc, $
    ['rain_rate', $
     'rain_rate_uncertainty', $
     'rain_status_flag', $
     'rain_quality_flag', $
     'precip_liquid_water', $
     'cloud_liquid_water'], $
    varname = $
    ['rrcprprf', $
     'rrerrprf', $
     'prfstatus', $
     'prfqual', $
     'plwc', $
     'clwc']

; Now snow profile.  This is slightly complicated by the clutter flag
; that requires a check of higher bins when set.

  finc = filelistk(fid)
  print,finc
  ftest = findfile(finc)
  if(strlen(ftest) lt 10) then begin
   printf,24,finc + ' Granule skipped'
   goto,skiporbit
  endif
  hdf_get, finc, $
    ['snowfall_rate_sfc', $
     'snowfall_rate_sfc_uncert', $
     'snowfall_rate', $
     'snowfall_rate_uncert', $
     'snow_retrieval_status'], $
    varname = $
    ['srcprprf', $
     'srerrprf', $
     'srprofile', $
     'srprofileerr', $
     'sretstatus']

; Check for clutter and if it exists, take snow rate and error from 2 bins higher up

clutterflag = fltarr(npix)
for kk=0L,npix-1 do begin
   a = reverse(binary(sretstatus(kk)))
   if(a(3) eq 1) then begin
     clutterflag(kk) = 1
     index = where(srprofile(*,kk) gt 0.,nvalid)
     if(srprofile(index(nvalid-1)-2,kk) gt 0.) then begin
      srcprprf(kk) = srprofile(index(nvalid-1)-2,kk)
      srerrprf(kk) = srprofileerr(index(nvalid-1)-2,kk)
     endif else begin
      srcprprf(kk) = 0.
      srerrprf(kk) = 0.
     endelse
   endif
endfor  ;i

srprofile = 0.
srprofileerr = 0.

; Next FLXHR-LIDAR-AUX.  Too many missing files.  Don't skip
; but set all variables to missing values if missing.  If this is
; missing then FLXHR-LIDAR and FLXHR-LIDAR-ERB are also missing.

  missinglidarflag = 0
  finc = filelistg(fid)
  print,finc
  ftest = findfile(finc)
  if(strlen(ftest) lt 10) then begin
   missinglidarflag = 1
   printf,24,finc
   calipsoaod = fltarr(npix)
   calipsoaod(*) = -999.
   calipsotype = calipsoaod
   touching = calipsoaod
   clwtoa = calipsoaod
   clwsfc = calipsoaod
   cswtoa = calipsoaod
   cswsfc = calipsoaod
   cswtoae = calipsoaod
   cswsfce = calipsoaod
   tempcldmask = fltarr(nhgts,npix)
   tempcldmask(*,*) = -999.
   tempaermask = tempcldmask
   lyrtype = fltarr(5,npix)
   lyrtype(*,*) = -999.
   qr = fltarr(nhgts,npix,2)
   qr(*,*,*) = -999.
   fu = qr
   fd = qr
   fuclr = qr
   fdclr = qr
   funa = qr
   fdna = qr
   qre = qr
   fue = qr
   fde = qr
   fuclre = qr
   fdclre = qr
   funae = qr
   fdnae = qr
   goto,skipflxhrlidar
  endif
  hdf_get, finc(0), $
    ['CS_Layer_Top', $
     'CS_Layer_Base', $
     'CS_Layer_Type', $
     'CS_Cloud_Class', $
     'Alay_Tau_532', $
     'Alay_Flag_Byte', $
     'ALay_Top', $
     'ALay_Base', $
     'Clay_Top', $
     'Clay_Base'], $
    varname = $
    ['lyrtop', $
     'lyrbase', $
     'lyrtype', $
     'auxcldclass', $
     'auxaot', $
     'auxtypeflag', $
     'auxaertop', $
     'auxaerbase', $
     'auxcldtop', $
     'auxcldbase']
  
; Finally, FLXHR.  Both standard FLXHR-LIDAR and the ERB version are
; read to simplify future data access.

  fin = findfile(filelisti(fid))
  print,filelisti

  print,fin(0)
  if(strlen(fin(0)) lt 10) then begin
   printf,24,fin
   goto,skiporbit
  endif
  hdf_get, fin(0), $
    ['QR', $
     'FD', $
     'FU', $
     'TOACRE',$
     'BOACRE',$
     'FD_NC',$
     'FU_NC',$
     'FD_NA',$
     'FU_NA',$
     'Status', $
     'TAI_start', $
     'UTC_start', $
     'Profile_time', $
     'Latitude', $
     'Longitude'], $
    varname = $
    ['qre', $
     'fde', $
     'fue', $
     'toacree',$
     'boacree',$
     'fdclre',$
     'fuclre',$
     'fdnae',$
     'funae',$
     'status', $
     'taistart', $
     'utcstart', $
     'time', $
     'lat', $
     'lon']

  qre = float(qre)
  fue = float(fue)
  fde = float(fde)
  fuclre = float(fuclre)
  fdclre = float(fdclre)
  funae = float(funae)
  fdnae = float(fdnae)

  index = where(qre ne -999.,ngood)
  if(ngood gt 0) then begin
   qre(index) = qre(index)/100.     ; Heating rates in K/day
  endif
  index = where(qre eq -999.,nbad)
  if(nbad gt 0) then begin
   qre(index) = -998.
  endif
  index = where(fue eq -999.,nbad)
  if(nbad gt 0) then begin
   fue(index) = -998.
   fuclre(index) = -998.
   funae(index) = -998.
  endif
  index = where(fde eq -999.,nbad)
  if(nbad gt 0) then begin
   fde(index) = -998.
   fdclre(index) = -998.
   fdnae(index) = -998.
  endif

  fin = findfile(filelistj(fid))
  print,fin(0)
  if(strlen(fin(0)) lt 10) then begin
   printf,24,fin
   goto,skiporbit
  endif
  hdf_get, fin(0), $
    ['QR', $
     'FD', $
     'FU', $
     'TOACRE',$
     'BOACRE',$
     'FD_NC',$
     'FU_NC',$
     'FD_NA',$
     'FU_NA'],$
    varname = $
    ['qr', $
     'fd', $
     'fu', $
     'toacre',$
     'boacre',$
     'fdclr',$
     'fuclr',$
     'fdna',$
     'funa']

  qr = float(qr)
  fu = float(fu)
  fd = float(fd)
  fuclr = float(fuclr)
  fdclr = float(fdclr)
  funa = float(funa)
  fdna = float(fdna)

  index = where(qr ne -999.,ngood)
  if(ngood gt 0) then begin
   qr(index) = qr(index)/100.     ; Heating rates in K/day
  endif
  index = where(qr eq -999.,nbad)
  if(nbad gt 0) then begin
   qr(index) = -998.
  endif
  index = where(fu eq -999.,nbad)
  if(nbad gt 0) then begin
   fu(index) = -998.
   fuclr(index) = -998.
   funa(index) = -998.
  endif
  index = where(fd eq -999.,nbad)
  if(nbad gt 0) then begin
   fd(index) = -998.
   fdclr(index) = -998.
   fdna(index) = -998.
  endif

; Extract LW and SW cloud forcing

  clwtoa = fltarr(npix)
  clwsfc = fltarr(npix)
  cswtoa = fltarr(npix)
  cswsfc = fltarr(npix)
  cswtoae = fltarr(npix)
  cswsfce = fltarr(npix)
  for i=0L,npix -1 do begin
   clwtoa(i) = toacre(i,1)
   clwsfc(i) = boacre(i,1)
   cswtoa(i) = toacre(i,0)
   cswsfc(i) = boacre(i,0)
   cswtoae(i) = toacree(i,0)
   cswsfce(i) = boacree(i,0)
  endfor  ;i

skipflxhrlidar:

; CLDCLASS data.  If GEOPROF exists, this is unlikely to be missing so
; skip orbit in those few cases where it is.

  fin = findfile(filelisth(fid))
  print,fin(0)
  if(strlen(fin(0)) lt 10) then begin
   printf,24,fin + ' Granule skipped'
   goto,skiporbit
  endif
  hdf_get, fin(0), $
    ['Cloud_scenario'], $
    varname = $
    ['cld_scenario']

; Determine cloud class:
;    0 = clear
;    1 = cirrus
;    2 = altostratus
;    3 = altocumulus
;    4 = stratus
;    5 = stratocumulus
;    6 = cumulus
;    7 = nimbo stratus
;    8 = deep convection
;    9 = multilayered cloud other than deep conv.

  ncldclasses = 11
  cldclass = fltarr(npix)
  nhgts = n_elements(cld_scenario(*,0))
  for kk=0L,npix-1 do begin
   temparr = fltarr(nhgts)
   if(stddev(cld_scenario(*,kk)) gt 0.01) then begin
    for zz=34,102 do begin
      a = binary(cld_scenario(zz,kk))
      temparr(zz) = 8*a(11) + 4*a(12) + 2*a(13) + a(14)
    endfor ; zz
    idconv = where(temparr eq 8,nconv)
    idnimbo = where(temparr eq 7,nnimbo)
    idcumulus = where(temparr eq 6,ncumulus)
    idstratocu = where(temparr eq 5,nstratocu)
    idstratus = where(temparr eq 4,nstratus)
    idaltocu = where(temparr eq 3,naltocu)
    idaltostrat = where(temparr eq 2,naltostrat)
    idcirrus = where(temparr eq 1,ncirrus)
    if(nconv + nnimbo + ncumulus + nstratocu + nstratus + naltocu + naltostrat + ncirrus eq 0) then begin
     cldclass(kk) = 0
    endif
    if(nconv gt 0) then cldclass(kk) = 8
    if(ncirrus gt 0) then begin
     if(nconv + ncumulus + nstratocu + nstratus + naltocu + naltostrat + nnimbo eq 0) then begin
      cldclass(kk) = 1
     endif else begin
      cldclass(kk) = 9
     endelse
    endif
    if(naltostrat gt 0) then begin
     if(nconv + ncumulus + nstratocu + nstratus + naltocu + nnimbo + ncirrus eq 0) then begin
      cldclass(kk) = 2
     endif else begin
      cldclass(kk) = 9
     endelse
    endif
    if(naltocu gt 0) then begin
     if(nconv + ncumulus + nstratocu + nstratus + nnimbo + naltostrat + ncirrus eq 0) then begin
      cldclass(kk) = 3
     endif else begin
      cldclass(kk) = 9
     endelse
    endif
    if(nstratus gt 0) then begin
     if(nconv + ncumulus + nstratocu + nnimbo + naltocu + naltostrat + ncirrus eq 0) then begin
      cldclass(kk) = 4
     endif else begin
      cldclass(kk) = 9
     endelse
    endif
    if(nstratocu gt 0) then begin
     if(nconv + ncumulus + nnimbo + nstratus + naltocu + naltostrat + ncirrus eq 0) then begin
      cldclass(kk) = 5
     endif else begin
      cldclass(kk) = 9
     endelse
    endif
    if(ncumulus gt 0) then begin
     if(nconv + nnimbo + nstratocu + nstratus + naltocu + naltostrat + ncirrus eq 0) then begin
      cldclass(kk) = 6
     endif else begin
      cldclass(kk) = 9
     endelse
    endif
    if(nnimbo gt 0) then begin
     if(nconv + ncumulus + nstratocu + nstratus + naltocu + naltostrat + ncirrus eq 0) then begin
       cldclass(kk) = 7
     endif else begin
      cldclass(kk) = 9
     endelse
    endif
   endif else begin
     cldclass(kk) = 0
   endelse
  endfor  ;kk

; Add lidar clouds to cloud classification
;    4 = stratus or lidar-only low cloud
;    9 = lidar-only cloud and any other type of cloud
;   10 = lidar-only thin cirrus

if(missinglidarflag eq 0) then begin

; New method

  for kk=0L,npix-1 do begin
    idx = where(lyrtype(*,kk) gt 0,nlidar)
    if(nlidar gt 0) then begin
     topidx = where(hgt(*,kk) gt max(lyrtop(*,kk)),nhigher)
     topt = ect(topidx(nhigher-1),kk)
     baseidx = where(hgt(*,kk) lt min(lyrbase(*,kk)),nlower)
     baset = ect(baseidx(0),kk)
     cmask(topidx(nhigher-1):baseidx(0),kk) = 40
     if(topt gt 273.14 and cldclass(kk) eq 0) then begin
      cldclass(kk) = 4
     endif
     if(topt lt 273.14 and cldclass(kk) eq 0) then begin
      if(nlidar gt 1) then begin
       cldclass(kk) = 9
      endif else begin
       cldclass(kk) = 10
      endelse
     endif
 ;    if(topt lt 273.14 and cldclass(kk) gt 0) then begin
 ;     cldclass(kk) = 9
 ;    endif
    endif
    if(cldclass(kk) gt 0 and nlidar eq 0) then cldclass(kk) = 0
  endfor  ;kk

;  for kk=0L,npix-1 do begin
;    idx = where(lyrtype(*,kk) eq 1,nhigh)
;    idx = where(lyrtype(*,kk) eq 7,nlow)
;    if(nhigh gt 0) then begin
;     if(nlow gt 0 or cldclass(kk) gt 0) then begin
;      cldclass(kk) = 9
;     endif else begin
;      cldclass(kk) = 10
;     endelse
;    endif
;    if(nlow gt 0) then begin
;     if(nhigh gt 0 or cldclass(kk) gt 0) then begin
;      cldclass(kk) = 9
;     endif else begin
;      cldclass(kk) = 4
;     endelse
;    endif
;  endfor  ;kk

endif

; Add, CERES matched dataset (when ready)
;    - this product should consist of matched CERES and FLXHR
;      TOA and SFC fluxes as well as cloud LW and SW forcing
;      and column heating rates from FLXHR
;    - an alternative would be CCCM but it is stored on the CERES
;      footprint so some matching would be involved

    szaceresm = fltarr(npix)
    szaceresm(*) = -999.
    lonceresm = szaceresm
    latceresm = szaceresm
    timeceresm = szaceresm
    stypeceresm = szaceresm
    cldceresm = szaceresm
    fuswtoaceresm = szaceresm
    fdswtoaceresm = szaceresm
    fulwtoaceresm = szaceresm
    fdswsfcceresm = szaceresm
    fdlwsfcceresm = szaceresm
    fuswsfcceresm = szaceresm
    fulwsfcceresm = szaceresm
    albedoceresm = szaceresm
    semissceresm = szaceresm
    spceresm = szaceresm
    cwvceresm = szaceresm
    stceresm = szaceresm

; Find first CloudSat pixel.  Since latitudes and longitudes repeat,
; it is best to first isolate a time-window keeping in mind that Aqua
; flies ~100 seconds ahead of CloudSat.

index = where(abs(cerestime - csattime(0)) lt 200. and abs(cereslat - lat(0)) lt 0.5 and abs(cereslon - lon(0)) lt 0.5,nmatches)
if(nmatches eq 0) then begin

endif else begin

; Refine to closest match

distances = abs(cereslat(index) - lat(0)) + abs(cereslon(index) - lon(0))
temp = where(distances eq min(distances))
closest = index(temp(0))

    lonceresm(0) = cereslon(closest)
    latceresm(0) = cereslat(closest)
    timeceresm(0) = cerestime(closest)
    stypeceresm(0) = stypeceres(closest)
    szaceresm(0) = szaceres(closest)
    cldceresm(0) = cldceres(closest)
    fuswtoaceresm(0) = fuswtoaceres(closest)
    fdswtoaceresm(0) = fdswtoaceres(closest)
    fulwtoaceresm(0) = fulwtoaceres(closest)
    fdswsfcceresm(0) = fdswsfcceres(closest)
    fdlwsfcceresm(0) = fdlwsfcceres(closest)
    if(fnetswsfcceres(closest) lt 2000. and fnetlwsfcceres(closest) lt 2000. and fdswsfcceres(closest) lt 2000. and fdlwsfcceres(closest) lt 2000. ) then begin
     fuswsfcceresm(0) = fdswsfcceres(closest)-fnetswsfcceres(closest)
     fulwsfcceresm(0) = fdlwsfcceres(closest)-fnetlwsfcceres(closest)
    endif else begin
     fdswsfcceresm(0) = -999.
     fdlwsfcceresm(0) = -999.
     fuswsfcceresm(0) = -999.
     fulwsfcceresm(0) = -999.
    endelse
    albedoceresm(0) = albedoceres(closest)
    semissceresm(0) = semissceres(closest)
    spceresm(0) = spceres(closest)
    cwvceresm(0) = cwvceres(closest)
    stceresm(0) = stceres(closest)

; Continue through rest of granule searching the 7 nearest pixels to
; the last match to find the closest.  This allows CERES data to be
; repeated as many times as needed until the next CERES FOV is closer
; to the CloudSat pixel of interest.

  for kk=1L,npix-1 do begin

    index = closest + findgen(7) - 3
    distances = abs(cereslat(index) - lat(kk)) + abs(cereslon(index) - lon(kk))
    temp = where(distances eq min(distances))
    closest = index(temp(0))
;    print,kk,closest,distances(temp(0))
    if(distances(temp(0)) lt 1.5) then begin
     lonceresm(kk) = cereslon(closest)
     latceresm(kk) = cereslat(closest)
     timeceresm(kk) = cerestime(closest)
     stypeceresm(kk) = stypeceres(closest)
     szaceresm(kk) = szaceres(closest)
     cldceresm(kk) = cldceres(closest)
     fuswtoaceresm(kk) = fuswtoaceres(closest)
     fdswtoaceresm(kk) = fdswtoaceres(closest)
     fulwtoaceresm(kk) = fulwtoaceres(closest)
     fdswsfcceresm(kk) = fdswsfcceres(closest)
     fdlwsfcceresm(kk) = fdlwsfcceres(closest)
     if(fnetswsfcceres(closest) lt 2000. and fnetlwsfcceres(closest) lt 2000. and fdswsfcceres(closest) lt 2000. and fdlwsfcceres(closest) lt 2000. ) then begin
      fuswsfcceresm(kk) = fdswsfcceres(closest)-fnetswsfcceres(closest)
      fulwsfcceresm(kk) = fdlwsfcceres(closest)-fnetlwsfcceres(closest)
     endif else begin
      fdswsfcceresm(kk) = -999.
      fdlwsfcceresm(kk) = -999.
      fuswsfcceresm(kk) = -999.
      fulwsfcceresm(kk) = -999.
     endelse
     albedoceresm(kk) = albedoceres(closest)
     semissceresm(kk) = semissceres(closest)
     spceresm(kk) = spceres(closest)
     cwvceresm(kk) = cwvceres(closest)
     stceresm(kk) = stceres(closest)
     endif else begin
       print,kk,lat(kk),lon(kk)
       print,index
       print,cereslat(index)
       print,cereslon(index)
       print,distances
     endelse

  endfor   ;kk

endelse

; Determine cloud top height and thickness

  cldtop = fltarr(npix)
  cldthick = fltarr(npix)
  for i=0L,npix-1 do begin
   if(sbin(i) gt 110 or sbin(i) lt 25) then goto,foundtop
   if(max(cmask(0:sbin(i),i)) ge 20.) then begin
    foundcld = 0
    for j=0,sbin(i) do begin
     if(foundcld eq 0) then begin
      if(cmask(sbin(i)-j,i) ge 20) then begin
       cldtop(i) = hgt(sbin(i)-j,i)/1000.
       cldthick(i) = 0.24
       foundcld = 1
      endif
     endif else begin
      if(cmask(sbin(i)-j,i) ge 20) then begin
       cldtop(i) = hgt(sbin(i)-j,i)/1000.
       cldthick(i) = cldthick(i) + 0.24
      endif else begin
       goto,foundtop
      endelse
     endelse
    endfor  ;j
   endif
foundtop:
  endfor  ;i

  cmask = 0

; Establish continent/basin index for each CloudSat pixel

continentid = intarr(npix)
for i=0L,npix-1 do begin
  latidx = floor((lat(i)+90))
  if(lon(i) gt 0.) then begin
   lonidx = floor(lon(i))
  endif else begin
   lonidx = floor(lon(i)+360.)
  endelse
  if(lonidx lt 0.) then lonidx = 0
  if(lonidx gt 359) then lonidx = 359
  if(latidx lt 0.) then latidx = 0
  if(latidx gt 179) then latidx = 179
  continentid(i) = newsmask(lonidx,latidx)
endfor  ;i

meansw = fltarr(npix)
areas = fltarr(npix)
daytmp = float(strmid(filelist(fid),strlen(datadir)+4,3))
for i=0L,npix-1 do begin  
  dailyinsolation,daytmp,lat(i),swvaltmp
  meansw(i) = swvaltmp
  areas(i) = (sin((lat(i)+0.1)*3.141592653/180.) - sin((lat(i)-0.1)*3.141592653/180.))/2
endfor  ;i

; Use data to generate arrays of column aerosol optical depth, aerosol
; type, and a mask indicating the relative position of aerosol and
; cloud (0 - no contact between layers; 1 - layers touching; 2 - no
; aerosol information available).  When more than one aerosol layer is
; present, the one closest to cloud level is used.

if(missinglidarflag eq 0) then begin

  maxfeatures = n_elements(auxaot(*,0))
  laythick = fltarr(maxfeatures)
  calipsoaod = fltarr(npix)
  calipsotype = intarr(npix)
  tempcldmask = intarr(nhgts,npix)
  tempaermask = intarr(nhgts,npix)
  touching = intarr(npix)
  for i=0L,npix-1 do begin
   if(cldthick(i) gt 0) then begin
    idx = where(hgt(*,i)/1000. le cldtop(i) and hgt(*,i)/1000. ge (cldtop(i)-cldthick(i)))
    tempcldmask(idx,i) = 1
   endif
   index = where(auxaot(*,i)gt 0.,naer)
   if(naer gt 0) then begin
    calipsoaod(i) = total(auxaot(index,i))
    laythick(*) = auxaertop(*,i)-auxaerbase(*,i)
    id = where(laythick eq max(laythick))
    b = reverse(fix(auxtypeflag(id(0),i)))
    a = reverse(binary(b))
    calipsotype(i) = 4*a(11) + 2*a(10) + a(9)
    for j=0,naer-1 do begin
     idx = where(hgt(*,i)/1000. ge auxaerbase(j,i) and hgt(*,i)/1000. le auxaertop(j,i))
     tempaermask(idx,i) = calipsotype(i)
    endfor ;j
   endif
  endfor  ;i

; Go through all pixels a second time and interpolate aerosol
; information through cloudy pixels.  If pixel is clear, do not
; interpolate since CALIPSO should be capable of detecting aerosol in
; such cases.

  for i=0L,npix-1 do begin
   tempaerprof = intarr(nhgts)
   if(calipsoaod(i) eq 0 and cldthick(i) gt 0) then begin
    if(i le 100) then begin
     for j=0,nhgts-1 do begin
      tempaerprof(j) = total(tempaermask(j,0:i+100))
     endfor  ;j
     idx = where(calipsoaod(0:i+100) gt 0.,ngood)
     if(ngood gt 0) then begin
      calipsoaod(i) = mean(calipsoaod(idx))
      nct = fltarr(7)
      for typeid = 1,7 do begin
       nct(typeid-1) = n_elements(where(calipsotype(idx) eq typeid))
      endfor  ; typeid
      maxid = where(nct eq max(nct))
      calipsotype(i) = maxid(0)+1
     endif
    endif
    if(i gt 100 and i lt npix-100) then begin
     for j=0,nhgts-1 do begin
      tempaerprof(j) = total(tempaermask(j,i-100:i+100))
     endfor  ;j
     idx = where(calipsoaod(i-100:i+100) gt 0.,ngood)
     if(ngood gt 0) then begin
      calipsoaod(i) = mean(calipsoaod(idx))
      nct = fltarr(7)
      for typeid = 1,7 do begin
       nct(typeid-1) = n_elements(where(calipsotype(idx) eq typeid))
      endfor  ; typeid
      maxid = where(nct eq max(nct))
      calipsotype(i) = maxid(0)+1
     endif
    endif
    if(i ge npix-100) then begin
     for j=0,nhgts-1 do begin
      tempaerprof(j) = total(tempaermask(j,i-100:npix-1))
     endfor  ;j
     idx = where(calipsoaod(i-100:npix-1) gt 0.,ngood)
     if(ngood gt 0) then begin
      calipsoaod(i) = mean(calipsoaod(idx))
      nct = fltarr(7)
      for typeid = 1,7 do begin
       nct(typeid-1) = n_elements(where(calipsotype(idx) eq typeid))
      endfor  ; typeid
      maxid = where(nct eq max(nct))
      calipsotype(i) = maxid(0)+1
     endif
    endif
    idx = where(tempaerprof gt 0,ngood)
    if(ngood gt 0) then tempaermask(idx,i) = 1
   endif
   touching(i) = total(tempaermask(*,i)*tempcldmask(*,i))
  endfor  ;i

endif

; Read GEMS data.  GEMS has been replaced with MACC (below) as of
; August 2013.  This code should still work, however, if comparisons
; are desired.

;  finc = gemslist(fid)
;  print,finc
;  ftest = findfile(finc)
;  if(strlen(ftest) gt 10) then begin
;   hdf_get, finc, $
;    ['Lat', $
;     'Lon', $
;     'AOD_1240', $
;     'AOD_469', $
;     'AOD_550', $
;     'AOD_670', $
;     'AOD_865', $
;     'AOD_BC', $
;     'AOD_DU', $
;     'AOD_OM', $
;     'AOD_SS', $
;     'AOD_SU'], $
;    varname = $
;    ['gemslat', $
;     'gemslon', $
;     'gems1240', $
;     'gems469', $
;     'gems550', $
;     'gems670', $
;     'gems865', $
;     'gemsbc', $
;     'gemsdu', $
;     'gemsoc', $
;     'gemssa', $
;     'gemssu']
;     gemsaods = gemsoc + gemsbc + gemssa + gemssu + gemsdu
;   endif else begin
;    gemslat = fltarr(npix)
;    gemslat(*) = -999.
;    gemslon = gemslat
;    gems1240 = gemslat
;    gems469 = gemslat
;    gems550 = gemslat
;    gems670 = gemslat
;    gems865 = gemslat
;    gemsbc = gemslat
;    gemsdu = gemslat
;    gemsoc = gemslat
;    gemssa = gemslat
;    gemssu = gemslat
;    gemsaods = gemslat
;  endelse

; August 2013:  Read MACC data.  IDL .sav files contain the array AOD
; which features AODs at 550 and 865 nm followed by black carbon,
; dust, organic carbon, sulphate, and sea salt AODs.  For simplicity,
; the old GEMS arrays are maintained but now contain MACC data.

  gemsbc = fltarr(npix)
  gemsdu = fltarr(npix)
  gemsoc = fltarr(npix)
  gemssu = fltarr(npix)
  gemssa = fltarr(npix)

  finc = macclist(fid)
  print,finc
  ftest = findfile(finc)
  if(strlen(ftest) lt 10) then begin
   printf,24,finc
   gemsbc = fltarr(npix)
   gemsbc(*) = -999.
   gemsdu(*) = -999.
   gemsoc(*) = -999.
   gemssu(*) = -999.
   gemssa(*) = -999.
   goto,skipmacc
  endif
  restore, ftest(0), /verbose
  gemsbc(*) = aod(2,*)
  gemsdu(*) = aod(3,*)
  gemsoc(*) = aod(4,*)
  gemssa(*) = aod(5,*)
  gemssu(*) = aod(6,*)
  gemsaods = gemsoc + gemsbc + gemssa + gemssu + gemsdu

skipmacc:






; Here we do no further stratification of the data.

; Convert maximum rainrates to positive values

  index = where(rrcpr gt -100. and rrcpr lt 0.,nfix)
  if(nfix gt 0) then begin
   rrcpr(index) = abs(rrcpr(index))
   rrcprprf(index) = abs(rrcprprf(index))
  endif

; Calculate rain contribution to total water path and add into AMSR-E
; LWP field

  for i=0L,npix-1 do begin
   if(lwpamsr(i) gt 0.18) then begin
    Htmp = ((ecsst(i)*0.16 + 0.46) > 0.46) < 5.26
    Rtmp = 1.0/Htmp*(lwpamsr(i)/0.18-1)^2

; Compute extra LWP by integrating the M-P LWC through the depth of
; the cloud.  Since Htmp is in km and rhol is in g/m3 there are two
; factors of 1000. that cancel in obtaining a result in kg/m2.

    extralwp = Htmp*8.*pi*rhol*16.e-18/(8.2e-3*Rtmp^(-0.21))^4
    lwpamsr(i) = lwpamsr(i) + extralwp
   endif
  endfor  ;i

; Subset only useable heights

  newsfcbin = fltarr(npix)
  reftmp = ref
  clwctmp = clwc
  plwctmp = plwc
  ecttmp = ect
  ecqtmp = ecq
  ecptmp = ecp
  hgttmp = hgt
  tempaermasktmp = tempaermask
  tempcldmasktmp = tempcldmask
  futmp = fu
  fdtmp = fd
  fuclrtmp = fuclr
  fdclrtmp = fdclr
  funatmp = funa
  fdnatmp = fdna
  qrtmp = qr
  fuetmp = fue
  fdetmp = fde
  fuclretmp = fuclre
  fdclretmp = fdclre
  funaetmp = funae
  fdnaetmp = fdnae
  qretmp = qre
  ref = fltarr(nhgt,npix)
  ect = ref
  ecp = ref
  ecq = ref
  hgt = ref
  plwc = ref
  clwc = ref
  tempaermask = ref
  tempcldmask = ref
  qr = fltarr(nhgt,npix,nflux)
  fu = qr
  fd = qr
  fuclr = qr
  fdclr = qr
  funa = qr
  fdna = qr
  qre = qr
  fue = qr
  fde = qr
  fuclre = qr
  fdclre = qr
  funae = qr
  fdnae = qr
  for i=0L,npix-1 do begin
   topid = where(hgttmp(*,i) gt 0.,ntops)
;   topid = where(hgttmp(*,i) lt 1000.*hgtmaxs(nhgt-1))
   ref(0:nhgt-1,i) = reftmp(topid(ntops-nhgt:ntops-1),i)/100.
   clwc(0:nhgt-1,i) = plwctmp(topid(ntops-nhgt:ntops-1),i)/100.
   plwc(0:nhgt-1,i) = plwctmp(topid(ntops-nhgt:ntops-1),i)/100.
   ect(0:nhgt-1,i) = ecttmp(topid(ntops-nhgt:ntops-1),i)
   ecq(0:nhgt-1,i) = ecqtmp(topid(ntops-nhgt:ntops-1),i)
   ecp(0:nhgt-1,i) = ecptmp(topid(ntops-nhgt:ntops-1),i)
   hgt(0:nhgt-1,i) = hgttmp(topid(ntops-nhgt:ntops-1),i)
   tempaermask(0:nhgt-1,i) = tempaermasktmp(topid(ntops-nhgt:ntops-1),i)
   tempcldmask(0:nhgt-1,i) = tempcldmasktmp(topid(ntops-nhgt:ntops-1),i)
   fu(0:nhgt-1,i,*) = futmp(topid(ntops-nhgt:ntops-1),i,*)
   newsfcbin(i) = nhgt-1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   if(fu(topid(newsfcbin(i)),i,1) lt 0 and fu(0,i,1) gt 0) then newsfcbin(i) = newsfcbin(i) - 1
   fd(0:nhgt-1,i,*) = fdtmp(topid(ntops-nhgt:ntops-1),i,*)
   fuclr(0:nhgt-1,i,*) = fuclrtmp(topid(ntops-nhgt:ntops-1),i,*)
   fdclr(0:nhgt-1,i,*) = fdclrtmp(topid(ntops-nhgt:ntops-1),i,*)
   funa(0:nhgt-1,i,*) = funatmp(topid(ntops-nhgt:ntops-1),i,*)
   fdna(0:nhgt-1,i,*) = fdnatmp(topid(ntops-nhgt:ntops-1),i,*)
   qr(0:nhgt-1,i,*) = qrtmp(topid(ntops-nhgt:ntops-1),i,*)
   fue(0:nhgt-1,i,*) = fuetmp(topid(ntops-nhgt:ntops-1),i,*)
   fde(0:nhgt-1,i,*) = fdetmp(topid(ntops-nhgt:ntops-1),i,*)
   fuclre(0:nhgt-1,i,*) = fuclretmp(topid(ntops-nhgt:ntops-1),i,*)
   fdclre(0:nhgt-1,i,*) = fdclretmp(topid(ntops-nhgt:ntops-1),i,*)
   funae(0:nhgt-1,i,*) = funaetmp(topid(ntops-nhgt:ntops-1),i,*)
   fdnae(0:nhgt-1,i,*) = fdnaetmp(topid(ntops-nhgt:ntops-1),i,*)
   qre(0:nhgt-1,i,*) = qretmp(topid(ntops-nhgt:ntops-1),i,*)
  endfor  ;i

; Calculate lower tropospheric static stability (defined as above)

  ltss = fltarr(npix)
  for i=0L,npix-1 do begin
   sfcid = where(ecp(*,i) gt 0.,necp)
   ulid = where(abs(ecp(*,i)-70000.) eq min(abs(ecp(*,i)-70000.)))
   ltss(i) = ect(ulid,i)*(101300./ecp(ulid,i))^(Rovercp) - ect(sfcid(necp-1),i)*(101300./ecp(sfcid(necp-1),i))^(Rovercp)
  endfor  ;i

; Free memory

;   ect = 0
;   ecq = 0
;   ecp = 0

; Indentify SPRINTARS time ID

 yeartmp = strmid(filelist(fid),strlen(datadir),4)
 daytmp = float(strmid(filelist(fid),strlen(datadir)+4,3))
 index = where(yeartmp eq yeartxt)
 if(index eq 0) then begin
   sprintarstime = (daytmp - 1)*24. + 12.
 endif else begin
   sprintarstime = (total(yeardays(0:index-1)) + daytmp - 1)*24. + 12.
 endelse
 sprintarsid = where(timegrid eq sprintarstime,nfound)
 if(nfound ne 1) then begin
  print,'No unique SPRINTARS time idenentified.'
  print,sprintarsid
 endif

 suaods = fltarr(npix)
 duaods = fltarr(npix)
 ocaods = fltarr(npix)
 bcaods = fltarr(npix)
 saaods = fltarr(npix)
 aods = fltarr(npix)
 for i=0L,npix-1 do begin
  latid = where(abs(lat(i) - latgrid) eq min(abs(lat(i)-latgrid)))
  if(lon(i) ge 0) then begin
   lontmp = lon(i)
  endif else begin
   lontmp = lon(i) + 360.
  endelse
  lonid = where(abs(lontmp - longrid) eq min(abs(lontmp-longrid)))
  bcaods(i) = bcgrid(lonid(0),latid(0),sprintarsid(0))
  ocaods(i) = ocgrid(lonid(0),latid(0),sprintarsid(0))
  duaods(i) = dugrid(lonid(0),latid(0),sprintarsid(0))
  suaods(i) = sugrid(lonid(0),latid(0),sprintarsid(0))
  saaods(i) = sagrid(lonid(0),latid(0),sprintarsid(0))
  aods(i) = bcaods(i)+ocaods(i)+duaods(i)+suaods(i)+saaods(i)
 endfor  ;i

; Identify MODIS time ID

 modisid = round(daytmp-float(strmid(modisstart,4,3)))

 modisaods = fltarr(npix)
 modisais = fltarr(npix)
 for i=0L,npix-1 do begin
  latid = where(abs(lat(i) - modislatgrid) eq min(abs(lat(i)-modislatgrid)))
  lonid = where(abs(lon(i) - modislongrid) eq min(abs(lon(i)-modislongrid)))
  modisaods(i) = modisAOD(modisid,lonid,latid)
  modisais(i) = modisAI(modisid,lonid,latid)
 endfor  ;i

; Extract radiation budget quantities.  How to define surface?

 solar = fltarr(npix)
 solare = fltarr(npix)
 ssr = fltarr(npix)
 ssre = fltarr(npix)
 sfcr = fltarr(npix)
 sfcre = fltarr(npix)
 slr = fltarr(npix)
 sfce = fltarr(npix)
 olr = fltarr(npix)
 osr = fltarr(npix)
 osre = fltarr(npix)
 solar(*) = fd(0,*,0)/10.
 solare(*) = fde(0,*,0)/10.
 osr(*) = fu(0,*,0)/10.
 osre(*) = fue(0,*,0)/10.
 olr(*) = fu(0,*,1)/10.
 for i=0L,npix-1 do begin
  ssr(i) = fd(newsfcbin(i),i,0)/10.
  ssre(i) = fde(newsfcbin(i),i,0)/10.
  sfcr(i) = fu(newsfcbin(i),i,0)/10.
  sfcre(i) = fue(newsfcbin(i),i,0)/10.
  sfce(i) = fu(newsfcbin(i),i,1)/10.
  slr(i) = fd(newsfcbin(i),i,1)/10.
 endfor  ;i

; Write global output file.

 outfile = outdir1d + 'aerosol-aux.1d.' + granlist(fid) + '.nc'
 id = ncdf_create(outfile, /clobber)

; Tag file

            parts = str_sep(outfile, '/')
            szparts = size(parts)
            filenameout_nopath = parts[szparts[1]-1]
            ncdf_attput, id, /global, "thisfile", filenameout_nopath
            timedatestamp = systime()
            ncdf_attput, id, /global, "process_timedate", timedatestamp

; Define dimensions

            nid = ncdf_dimdef(id, 'npix',npix)

; Define variables

            lonid = ncdf_vardef(id, 'longitude', [nid], /float)
            latid = ncdf_vardef(id, 'latitude', [nid], /float)
            sbinid = ncdf_vardef(id, 'sbin', [nid], /float)
            contid = ncdf_vardef(id, 'continentid', [nid], /float)
            areaid = ncdf_vardef(id, 'area', [nid], /float)
            meanswid = ncdf_vardef(id, 'meansw', [nid], /float)
            cswtoaid = ncdf_vardef(id, 'cswftoa', [nid], /float)
            clwtoaid = ncdf_vardef(id, 'clwftoa', [nid], /float)
            cswsfcid = ncdf_vardef(id, 'cswfsfc', [nid], /float)
            clwsfcid = ncdf_vardef(id, 'clwfsfc', [nid], /float)
            cswsfceid = ncdf_vardef(id, 'cswfsfcerb', [nid], /float)
            cswtoaeid = ncdf_vardef(id, 'cswftoaerb', [nid], /float)
            calaertypeid = ncdf_vardef(id, 'calipsotype', [nid], /float)
            calaodid = ncdf_vardef(id, 'calipsoaod', [nid], /float)
            touchid = ncdf_vardef(id, 'touching', [nid], /float)
            pflagid = ncdf_vardef(id, 'pflag', [nid], /float)
            csflagid = ncdf_vardef(id, 'csflag', [nid], /float)
            cldclassid = ncdf_vardef(id, 'cldclass', [nid], /float)
            rthid = ncdf_vardef(id, 'rth', [nid], /float)
            cthid = ncdf_vardef(id, 'cldthick', [nid], /float)
            ctopid = ncdf_vardef(id, 'cldtop', [nid], /float)
            rrcprid = ncdf_vardef(id, 'rrcpr', [nid], /float)
            rrcprprfid = ncdf_vardef(id, 'rrcprprf', [nid], /float)
            rrerrprfid = ncdf_vardef(id, 'rrerrprf', [nid], /float)
            prfstatusid = ncdf_vardef(id, 'prfstatus', [nid], /float)
            prfqualid = ncdf_vardef(id, 'prfqual', [nid], /float)
            srcprprfid = ncdf_vardef(id, 'srcprprf', [nid], /float)
            srerrprfid = ncdf_vardef(id, 'srerrprf', [nid], /float)
            sretstatusid = ncdf_vardef(id, 'sretstatus', [nid], /float)
            clwpid = ncdf_vardef(id, 'clwp', [nid], /float)
            rlwpid = ncdf_vardef(id, 'rlwp', [nid], /float)
            flid = ncdf_vardef(id, 'fl', [nid], /float)
            piaid = ncdf_vardef(id, 'pia', [nid], /float)
            pamsrid = ncdf_vardef(id, 'pamsr', [nid], /float)
            windid = ncdf_vardef(id, 'sfcwind', [nid], /float)
            cwvid = ncdf_vardef(id, 'cwv', [nid], /float)
            lwpid = ncdf_vardef(id, 'lwp', [nid], /float)
            sstid = ncdf_vardef(id, 'sst', [nid], /float)
            ssteid = ncdf_vardef(id, 'ecsst', [nid], /float)
            sstmid = ncdf_vardef(id, 'merrasst', [nid], /float)
            ltssid = ncdf_vardef(id, 'ecltss', [nid], /float)
            ltssmid = ncdf_vardef(id, 'merraltss', [nid], /float)
            omegaid = ncdf_vardef(id, 'merraomega', [nid], /float)
            capeid = ncdf_vardef(id, 'merracape', [nid], /float)
            liid = ncdf_vardef(id, 'merrali', [nid], /float)
            
            cinid = ncdf_vardef(id, 'merracin', [nid], /float)
            cptid = ncdf_vardef(id, 'merracpt', [nid], /float)
            efluxid = ncdf_vardef(id, 'merraeflux', [nid], /float)
            eis_3id = ncdf_vardef(id, 'merraeis3', [nid], /float)
      
            eis_4id = ncdf_vardef(id, 'merraeis4', [nid], /float)
           
           
            evapid = ncdf_vardef(id, 'merraevap', [nid], /float)
            frcanid = ncdf_vardef(id, 'merrafrcan', [nid], /float)
            frccnid = ncdf_vardef(id, 'merrafrccn', [nid], /float)
            frclsid = ncdf_vardef(id, 'merrafrcls', [nid], /float)
            h500id = ncdf_vardef(id, 'merrah500', [nid], /float)
            h850id = ncdf_vardef(id, 'merrah850', [nid], /float)
            hfluxid = ncdf_vardef(id, 'merrahflux', [nid], /float)
            omega700id = ncdf_vardef(id, 'merraomega700', [nid], /float)
            pblhid = ncdf_vardef(id, 'merrapblh', [nid], /float)
            precanvid  = ncdf_vardef(id, 'merraprecanv', [nid], /float)
            precconid = ncdf_vardef(id, 'merrapreccon', [nid], /float)
            preclscid = ncdf_vardef(id, 'merrapreclscid', [nid], /float)
            prectotid = ncdf_vardef(id, 'merraprectot', [nid], /float)
            qlmlid = ncdf_vardef(id, 'merraqlml', [nid], /float)
            qv700id = ncdf_vardef(id, 'merraqv700', [nid], /float)
            rh700id = ncdf_vardef(id, 'merrarh700', [nid], /float)

            slpid = ncdf_vardef(id, 'merraslp', [nid], /float)
            t700id = ncdf_vardef(id, 'merrat700', [nid], /float)
            thvid = ncdf_vardef(id, 'merrathv', [nid], /float)
            tqiid = ncdf_vardef(id, 'merratqi', [nid], /float)
            tqlid = ncdf_vardef(id, 'merratql', [nid], /float)
            u500id = ncdf_vardef(id, 'merrau500', [nid], /float)
            u700id = ncdf_vardef(id, 'merrau700', [nid], /float)
            u850id = ncdf_vardef(id, 'merrau850', [nid], /float)
            v500id = ncdf_vardef(id, 'merrav500', [nid], /float)
            v700id = ncdf_vardef(id, 'merrav700', [nid], /float)
            v850id = ncdf_vardef(id, 'merrav850', [nid], /float)
            
            gemsaodid = ncdf_vardef(id, 'maccaod', [nid], /float)
            gemsbcid = ncdf_vardef(id, 'maccbc', [nid], /float)
            gemsduid = ncdf_vardef(id, 'maccdu', [nid], /float)
            gemsocid = ncdf_vardef(id, 'maccoc', [nid], /float)
            gemssaid = ncdf_vardef(id, 'maccsa', [nid], /float)
            gemssuid = ncdf_vardef(id, 'maccsu', [nid], /float)
            aodid = ncdf_vardef(id, 'saod', [nid], /float)
            bcid = ncdf_vardef(id, 'sbc', [nid], /float)
            duid = ncdf_vardef(id, 'sdu', [nid], /float)
            ocid = ncdf_vardef(id, 'soc', [nid], /float)
            said = ncdf_vardef(id, 'ssa', [nid], /float)
            suid = ncdf_vardef(id, 'ssu', [nid], /float)
            modisaodid = ncdf_vardef(id, 'modisaod', [nid], /float)
            modisaiid = ncdf_vardef(id, 'modisai', [nid], /float)
            solarid = ncdf_vardef(id, 'solar', [nid], /float)
            solareid = ncdf_vardef(id, 'solare', [nid], /float)
            osrid = ncdf_vardef(id, 'osr', [nid], /float)
            osreid = ncdf_vardef(id, 'osre', [nid], /float)
            ssrid = ncdf_vardef(id, 'ssr', [nid], /float)
            ssreid = ncdf_vardef(id, 'ssre', [nid], /float)
            sfcrid = ncdf_vardef(id, 'sfcr', [nid], /float)
            sfcreid = ncdf_vardef(id, 'sfcre', [nid], /float)
            olrid = ncdf_vardef(id, 'olr', [nid], /float)
            slrid = ncdf_vardef(id, 'slr', [nid], /float)
            sfceid = ncdf_vardef(id, 'sfce', [nid], /float)
            szamodisid = ncdf_vardef(id, 'szamodis', [nid], /float)
            stmodisid = ncdf_vardef(id, 'stmodis', [nid], /float)
            spmodisid = ncdf_vardef(id, 'spmodis', [nid], /float)
            flagmodisid = ncdf_vardef(id, 'flagmodis', [nid], /float)
            cmask5modisid = ncdf_vardef(id, 'cmask5modis', [nid], /float)
            cf5modisid = ncdf_vardef(id, 'cf5modis', [nid], /float)
            phasemodisid = ncdf_vardef(id, 'phasemodis', [nid], /float)
            irphasemodisid = ncdf_vardef(id, 'irphasemodis', [nid], /float)
            multilayermodisid = ncdf_vardef(id, 'multilayermodis', [nid], /float)
            cttmodisid = ncdf_vardef(id, 'cttmodis', [nid], /float)
            ctpmodisid = ncdf_vardef(id, 'ctpmodis', [nid], /float)
            remodisid = ncdf_vardef(id, 'remodis', [nid], /float)
            remodiserrorid = ncdf_vardef(id, 'remodiserror', [nid], /float)
            cotmodisid = ncdf_vardef(id, 'cotmodis', [nid], /float)
            cotmodiserrorid = ncdf_vardef(id, 'cotmodiserror', [nid], /float)
            lwpmodisid = ncdf_vardef(id, 'lwpmodis', [nid], /float)
            lwpmodiserrorid = ncdf_vardef(id, 'lwpmodiserror', [nid], /float)
            remodis1621id = ncdf_vardef(id, 'remodis1621', [nid], /float)
            cotmodis1621id = ncdf_vardef(id, 'cotmodis1621', [nid], /float)
            lwpmodis1621id = ncdf_vardef(id, 'lwpmodis1621', [nid], /float)
            latceresmid = ncdf_vardef(id, 'latceresm', [nid], /float)
            lonceresmid = ncdf_vardef(id, 'lonceresm', [nid], /float)
            timeceresmid = ncdf_vardef(id, 'timeceresm', [nid], /float)
            stypeceresmid = ncdf_vardef(id, 'stypeceresm', [nid], /float)
            szaceresmid = ncdf_vardef(id, 'szaceresm', [nid], /float)
            stceresmid = ncdf_vardef(id, 'stceresm', [nid], /float)
            spceresmid = ncdf_vardef(id, 'spceresm', [nid], /float)
            cwvceresmid = ncdf_vardef(id, 'cwvceresm', [nid], /float)
            cldceresmid = ncdf_vardef(id, 'cldceresm', [nid], /float)
            albedoceresmid = ncdf_vardef(id, 'albedoceresm', [nid], /float)
            semissceresmid = ncdf_vardef(id, 'semissceresm', [nid], /float)
            fuswtoaceresmid = ncdf_vardef(id, 'fuswtoaceresm', [nid], /float)
            fdswtoaceresmid = ncdf_vardef(id, 'fdswtoaceresm', [nid], /float)
            fulwtoaceresmid = ncdf_vardef(id, 'fulwtoaceresm', [nid], /float)
            fdswsfcceresmid = ncdf_vardef(id, 'fdswsfcceresm', [nid], /float)
            fdlwsfcceresmid = ncdf_vardef(id, 'fdlwsfcceresm', [nid], /float)
            fuswsfcceresmid = ncdf_vardef(id, 'fuswsfcceresm', [nid], /float)
            fulwsfcceresmid  = ncdf_vardef(id, 'fulwsfcceresm', [nid], /float)
            
           
           
           

; Add attributes (not yet implemented)

;            ncdf_attput, id, npid, "long_name", "Number of pixels in granule segment"
;            ncdf_attput, id, npid, "short_name", "Pixels"
;            ncdf_attput, id, npid, "units", "Number"

            ncdf_control, id, /endef ; switch to data mode
    
            ncdf_varput, id, lonid, lon
            ncdf_varput, id, latid, lat
            ncdf_varput, id, sbinid, newsfcbin
            ncdf_varput, id, contid, continentid
            ncdf_varput, id, areaid, areas
            ncdf_varput, id, meanswid, meansw
            ncdf_varput, id, cswtoaid, cswtoa
            ncdf_varput, id, clwtoaid, clwtoa
            ncdf_varput, id, clwsfcid, clwsfc
            ncdf_varput, id, cswsfcid, cswsfc
            ncdf_varput, id, cswsfceid, cswsfce
            ncdf_varput, id, cswtoaeid, cswtoae
            ncdf_varput, id, calaertypeid, calipsotype
            ncdf_varput, id, calaodid, calipsoaod
            ncdf_varput, id, touchid, touching
            ncdf_varput, id, pflagid, pflag
            ncdf_varput, id, csflagid, csflag
            ncdf_varput, id, cldclassid, cldclass
            ncdf_varput, id, rthid, rth
            ncdf_varput, id, cthid, cldthick
            ncdf_varput, id, ctopid, cldtop
            ncdf_varput, id, rrcprid, rrcpr
            ncdf_varput, id, rrcprprfid, rrcprprf
            ncdf_varput, id, rrerrprfid, rrerrprf
            ncdf_varput, id, prfstatusid, prfstatus
            ncdf_varput, id, prfqualid, prfqual

; Jan. 2015: added snow variables
 
            ncdf_varput, id, srcprprfid, srcprprf
            ncdf_varput, id, srerrprfid, srerrprf
            ncdf_varput, id, sretstatusid, sretstatus
            ncdf_varput, id, clwpid, clwp
            ncdf_varput, id, rlwpid, rlwp
            ncdf_varput, id, flid, fl
            ncdf_varput, id, piaid, pia
            ncdf_varput, id, pamsrid, pamsr
            ncdf_varput, id, windid, sfcwind
            ncdf_varput, id, cwvid, cwvamsr
            ncdf_varput, id, lwpid, lwpamsr
            ncdf_varput, id, sstid, sstamsr
            ncdf_varput, id, ssteid, ecsst
            ncdf_varput, id, sstmid, merrats
            ncdf_varput, id, ltssid, ltss
            ncdf_varput, id, ltssmid, merralts
            ncdf_varput, id, omegaid, merraomega
            ncdf_varput, id, capeid, merracape
            ncdf_varput, id, liid, merrali
            
            ncdf_varput, id, cinid, merracin
            ncdf_varput, id, cptid, merracpt
            ncdf_varput, id, efluxid, merraeflux
            ncdf_varput, id, eis_3id, merraeis3
            ncdf_varput, id, eis_4id, merraeis4
            ncdf_varput, id, evapid, merraevap
            ncdf_varput, id, frcanid, merrafrcan
            ncdf_varput, id, frccnid, merrafrccn
            ncdf_varput, id, frclsid, merrafrcls
            ncdf_varput, id, h500id, merrah500
            ncdf_varput, id, h850id, merrah850
            ncdf_varput, id, hfluxid, merrahflux
            ncdf_varput, id, omega700id, merraomega700
            ncdf_varput, id, pblhid, merrapblh
            ncdf_varput, id, precanvid, merraprecanv
            ncdf_varput, id, precconid, merrapreccon
            ncdf_varput, id, preclscid, merrapreclscid
            ncdf_varput, id, prectotid, merraprectot
            ncdf_varput, id, qlmlid, merraqlml
            ncdf_varput, id, qv700id, merraqv700
            ncdf_varput, id, rh700id, merrarh700

            ncdf_varput, id, slpid, merraslp
            ncdf_varput, id, t700id, merrat700
            ncdf_varput, id, thvid, merrathv
            ncdf_varput, id, tqiid, merratqi
            ncdf_varput, id, tqlid, merratql
            ncdf_varput, id, u500id, merrau500
            ncdf_varput, id, u700id, merrau700
            ncdf_varput, id, u850id, merrau850
            ncdf_varput, id, v500id, merrav500
            ncdf_varput, id, v700id, merrav700
            ncdf_varput, id, v850id, merrav850
            
            ncdf_varput, id, gemsaodid, gemsaods
            ncdf_varput, id, gemsbcid, gemsbc
            ncdf_varput, id, gemsduid, gemsdu
            ncdf_varput, id, gemsocid, gemsoc
            ncdf_varput, id, gemssaid, gemssa
            ncdf_varput, id, gemssuid, gemssu
            ncdf_varput, id, aodid, aods
            ncdf_varput, id, bcid, bcaods
            ncdf_varput, id, duid, duaods
            ncdf_varput, id, ocid, ocaods
            ncdf_varput, id, said, saaods
            ncdf_varput, id, suid, suaods
            ncdf_varput, id, modisaodid, modisaods
            ncdf_varput, id, modisaiid, modisais

; Jan. 2015: added FLXHR fluxes

            ncdf_varput, id, solarid, solar
            ncdf_varput, id, solareid, solare
            ncdf_varput, id, osrid, osr
            ncdf_varput, id, osreid, osre
            ncdf_varput, id, ssrid, ssr
            ncdf_varput, id, ssreid, ssre
            ncdf_varput, id, sfcrid, sfcr
            ncdf_varput, id, sfcreid, sfcre
            ncdf_varput, id, olrid, olr
            ncdf_varput, id, slrid, slr
            ncdf_varput, id, sfceid, sfce

; Jan. 2015: added MOD06 matchups

            ncdf_varput, id, szamodisid, reform(szamodis)
            ncdf_varput, id, stmodisid, reform(stmodis)
            ncdf_varput, id, spmodisid, reform(spmodis)
            ncdf_varput, id, flagmodisid, reform(flagmodis)
            ncdf_varput, id, cmask5modisid, reform(cmask5modis)
            ncdf_varput, id, cf5modisid, reform(cf5modis)
            ncdf_varput, id, phasemodisid, reform(phasemodis)
            ncdf_varput, id, irphasemodisid, reform(irphasemodis)
            ncdf_varput, id, multilayermodisid,reform( multilayermodis)
            ncdf_varput, id, cttmodisid, reform(cttmodis)
            ncdf_varput, id, ctpmodisid, reform(ctpmodis)
            ncdf_varput, id, remodisid, reform(remodis)
            ncdf_varput, id, remodiserrorid, reform(remodiserror)
            ncdf_varput, id, cotmodisid, reform(cotmodis)
            ncdf_varput, id, cotmodiserrorid, reform(cotmodiserror)
            ncdf_varput, id, lwpmodisid, reform(lwpmodis)
            ncdf_varput, id, lwpmodiserrorid, reform(lwpmodiserror)
            ncdf_varput, id, remodis1621id, reform(remodis1621)
            ncdf_varput, id, cotmodis1621id, reform(cotmodis1621)
            ncdf_varput, id, lwpmodis1621id, reform(lwpmodis1621)

; Jan. 2015: added CERES matchups

            ncdf_varput, id, latceresmid, latceresm
            ncdf_varput, id, lonceresmid, lonceresm
            ncdf_varput, id, timeceresmid, timeceresm
            ncdf_varput, id, stypeceresmid, stypeceresm
            ncdf_varput, id, szaceresmid, szaceresm
            ncdf_varput, id, stceresmid, stceresm
            ncdf_varput, id, spceresmid, spceresm
            ncdf_varput, id, cwvceresmid, cwvceresm
            ncdf_varput, id, cldceresmid, cldceresm
            ncdf_varput, id, albedoceresmid, albedoceresm
            ncdf_varput, id, semissceresmid, semissceresm
            ncdf_varput, id, fuswtoaceresmid, fuswtoaceresm
            ncdf_varput, id, fdswtoaceresmid, fdswtoaceresm
            ncdf_varput, id, fulwtoaceresmid, fulwtoaceresm
            ncdf_varput, id, fdswsfcceresmid, fdswsfcceresm
            ncdf_varput, id, fdlwsfcceresmid, fdlwsfcceresm
            ncdf_varput, id, fuswsfcceresmid, fuswsfcceresm
            ncdf_varput, id, fulwsfcceresmid, fulwsfcceresm

 ncdf_close,id

goto,skip2d

; Add a separate file for just a few 2d fields

 outfile = outdir2d + 'aerosol-aux.2d.' + granlist(fid) + '.nc'
 id = ncdf_create(outfile, /clobber)

; Tag file

            parts = str_sep(outfile, '/')
            szparts = size(parts)
            filenameout_nopath = parts[szparts[1]-1]
            ncdf_attput, id, /global, "thisfile", filenameout_nopath
            timedatestamp = systime()
            ncdf_attput, id, /global, "process_timedate", timedatestamp

; Define dimensions

            hid = ncdf_dimdef(id, 'nhgt',nhgt)
            nid = ncdf_dimdef(id, 'npix',npix)
            flid = ncdf_dimdef(id, 'nflux',nflux)

; Define variables

            lonid = ncdf_vardef(id, 'longitude', [nid], /float)
            latid = ncdf_vardef(id, 'latitude', [nid], /float)
            hgtid = ncdf_vardef(id, 'hgt', [hid,nid], /float)
            refid = ncdf_vardef(id, 'ref', [hid,nid], /float)
            clwcid = ncdf_vardef(id, 'clwc', [hid,nid], /float)
            plwcid = ncdf_vardef(id, 'plwc', [hid,nid], /float)
            ectid = ncdf_vardef(id, 'ectemp', [hid,nid], /float)
            ecpid = ncdf_vardef(id, 'ecpres', [hid,nid], /float)
            ecqid = ncdf_vardef(id, 'ecq', [hid,nid], /float)
            aermaskid = ncdf_vardef(id, 'aermask', [hid,nid], /float)
            cldmaskid = ncdf_vardef(id, 'cldmask', [hid,nid], /float)
            qrid = ncdf_vardef(id, 'qr', [hid,nid,flid], /float)
            fuid = ncdf_vardef(id, 'fu', [hid,nid,flid], /float)
            fdid = ncdf_vardef(id, 'fd', [hid,nid,flid], /float)
            fuclrid = ncdf_vardef(id, 'fuclr', [hid,nid,flid], /float)
            fdclrid = ncdf_vardef(id, 'fdclr', [hid,nid,flid], /float)
            funaid = ncdf_vardef(id, 'funa', [hid,nid,flid], /float)
            fdnaid = ncdf_vardef(id, 'fdna', [hid,nid,flid], /float)
            qreid = ncdf_vardef(id, 'qrerb', [hid,nid,flid], /float)
            fueid = ncdf_vardef(id, 'fuerb', [hid,nid,flid], /float)
            fdeid = ncdf_vardef(id, 'fderb', [hid,nid,flid], /float)
            fuclreid = ncdf_vardef(id, 'fuclrerb', [hid,nid,flid], /float)
            fdclreid = ncdf_vardef(id, 'fdclrerb', [hid,nid,flid], /float)
            funaeid = ncdf_vardef(id, 'funaerb', [hid,nid,flid], /float)
            fdnaeid = ncdf_vardef(id, 'fdnaerb', [hid,nid,flid], /float)

; Add attributes (not yet implemented)

;            ncdf_attput, id, npid, "long_name", "Number of pixels in granule segment"
;            ncdf_attput, id, npid, "short_name", "Pixels"
;            ncdf_attput, id, npid, "units", "Number"

            ncdf_control, id, /endef ; switch to data mode
    
            ncdf_varput, id, lonid, lon
            ncdf_varput, id, latid, lat
            ncdf_varput, id, hgtid, hgt
            ncdf_varput, id, refid, ref
            ncdf_varput, id, clwcid, clwc
            ncdf_varput, id, plwcid, plwc
            ncdf_varput, id, ectid, ect
            ncdf_varput, id, ecpid, ecp
            ncdf_varput, id, ecqid, ecq
            ncdf_varput, id, aermaskid, tempaermask
            ncdf_varput, id, cldmaskid, tempcldmask
            ncdf_varput, id, qrid, qr
            ncdf_varput, id, fuid, fu
            ncdf_varput, id, fdid, fd
            ncdf_varput, id, fuclrid, fuclr
            ncdf_varput, id, fdclrid, fdclr
            ncdf_varput, id, funaid, funa
            ncdf_varput, id, fdnaid, fdna
            ncdf_varput, id, qreid, qre
            ncdf_varput, id, fueid, fue
            ncdf_varput, id, fdeid, fde
            ncdf_varput, id, fuclreid, fuclre
            ncdf_varput, id, fdclreid, fdclre
            ncdf_varput, id, funaeid, funae
            ncdf_varput, id, fdnaeid, fdnae
 ncdf_close,id

skip2d:

; Loop over latitude and longitude bins writing output to separate
; regional files.

 for j=0,nlons-1 do begin
  for i=0,nlats-1 do begin
   boxtxt = '.' + strcompress(string(floor(lonvec(j))),/remove_all) + '_' + strcompress(string(floor(latvec(i))),/remove_all)

   index = where(lat ge latvec(i) and lat lt latvec(i) + fresolution and lon ge lonvec(j) and lon lt lonvec(j) + fresolution,ngoodpix)

; Only write out data if swath intersects box

   if(ngoodpix gt 0) then begin

    flt = fl(index)
    latt = lat(index)
    lont = lon(index)
    pflagt = pflag(index)
    areast = areas(index)
    clwtoat = clwtoa(index)
    cswtoat = cswtoa(index)
    clwsfct = clwsfc(index)
    cswsfct = cswsfc(index)
    cswtoaet = cswtoae(index)
    cswsfcet = cswsfce(index)
    cldclasst = cldclass(index)
    meanswt = meansw(index)
    continentidt = continentid(index)
    merracapet = merracape(index)
    merralit = merrali(index)
    csflagt = csflag(index)
    rtht = rth(index)
    clwpt = clwp(index)
    rlwpt = rlwp(index)
    rrcprprft = rrcprprf(index)
    rrerrprft = rrerrprf(index)
    prfstatust = prfstatus(index)
    prfqualt = prfqual(index)
    srcprprft = srcprprf(index)
    srerrprft = srerrprf(index)
    sretstatust = sretstatus(index)
    cldtopt = cldtop(index)
    cldthickt = cldthick(index)
    flt = fl(index)
    rrcprt = rrcpr(index)
    piat = pia(index)
    sfcwindt = sfcwind(index)
    cwvamsrt = cwvamsr(index)
    lwpamsrt = lwpamsr(index)
    sstamsrt = sstamsr(index)
    pamsrt = pamsr(index)
    sbint = sbin(index)
    newsfcbint = newsfcbin(index)
    ecsstt = ecsst(index) - 273.16
    merratst = merrats(index) - 273.16
    merraomegat = merraomega(index)
    
    merrarh700t = merrarh700(index)
    merraeis3t = merraeis3(index)
    print,merraeis3t
    merraeis4t = merraeis4(index)
    merracint = merracin(index)
    merracptt = merracpt(index)
    merraefluxt = merraeflux(index)
    merraevapt = merraevap(index)
    merrafrcant = merrafrcan(index)
    merrafrccnt = merrafrccn(index)
    merrafrclst = merrafrcls(index)
    merrah500t = merrah500(index)
    merrah850t = merrah850(index)
    merrahfluxt = merrahflux(index)
    merraomega700t = merraomega700(index)
    merrapblht = merrapblh(index)
    merraprecanvt = merraprecanv(index)
    merrapreccont = merrapreccon(index)
    merrapreclscidt = merrapreclscid(index)
    merraprectott = merraprectot(index)
    merraqlmlt = merraqlml(index)
    merraqv700t = merraqv700(index)
    
    merraslpt = merraslp(index)
    merrat700t = merrat700(index)
    merrathvt = merrathv(index)
    merratqit = merratqi(index)
    merratqlt = merratql(index)
    merrau500t = merrau500(index)
    merrau700t = merrau700(index)
    merrau850t = merrau850(index)
    merrav500t = merrav500(index)
    merrav700t = merrav700(index)
    merrav850t = merrav850(index)


    ltsst = ltss(index)
    merraltst = merralts(index)
    modisaodst = modisaods(index)
    modisaist = modisais(index)
    calipsoaodt = calipsoaod(index)
    calipsotypet = calipsotype(index)
    touchingt = touching(index)
    gemsbct = gemsbc(index)
    gemsdut = gemsdu(index)
    gemsoct = gemsoc(index)
    gemssat = gemssa(index)
    gemssut = gemssu(index)
    gemsaodst = gemsaods(index)
    bcaodst = bcaods(index)
    duaodst = duaods(index)
    ocaodst = ocaods(index)
    saaodst = saaods(index)
    suaodst = suaods(index)
    aodst = aods(index)
    solart = solar(index)
    solaret = solare(index)
    osrt = osr(index)
    osret = osre(index)
    ssrt = ssr(index)
    ssret = ssre(index)
    sfcrt = sfcr(index)
    sfcret = sfcre(index)
    olrt = olr(index)
    slrt = slr(index)
    sfcet = sfce(index)
    szamodist = reform(szamodis(0,index))
    stmodist = reform(stmodis(0,index))
    spmodist = reform(spmodis(0,index))
    flagmodist = reform(flagmodis(0,index))
    cmask5modist = reform(cmask5modis(0,index))
    cf5modist = reform(cf5modis(0,index))
    phasemodist = reform(phasemodis(0,index))
    irphasemodist = reform(irphasemodis(0,index))
    multilayermodist = reform(multilayermodis(0,index))
    cttmodist = reform(cttmodis(0,index))
    ctpmodist = reform(ctpmodis(0,index))
    remodist = reform(remodis(0,index))
    remodiserrort = reform(remodiserror(0,index))
    cotmodist = reform(cotmodis(0,index))
    cotmodiserrort = reform(cotmodiserror(0,index))
    lwpmodist = reform(lwpmodis(0,index))
    lwpmodiserrort = reform(lwpmodiserror(0,index))
    remodis1621t = reform(remodis1621(0,index))
    cotmodis1621t = reform(cotmodis1621(0,index))
    lwpmodis1621t = reform(lwpmodis1621(0,index))
    latceresmt = latceresm(index)
    lonceresmt = lonceresm(index)
    timeceresmt = timeceresm(index)
    stypeceresmt = stypeceresm(index)
    szaceresmt = szaceresm(index)
    stceresmt = stceresm(index)
    spceresmt = spceresm(index)
    cwvceresmt = cwvceresm(index)
    cldceresmt = cldceresm(index)
    albedoceresmt = albedoceresm(index)
    semissceresmt = semissceresm(index)
    fuswtoaceresmt = fuswtoaceresm(index)
    fdswtoaceresmt = fdswtoaceresm(index)
    fulwtoaceresmt = fulwtoaceresm(index)
    fdswsfcceresmt = fdswsfcceresm(index)
    fdlwsfcceresmt = fdlwsfcceresm(index)
    fuswsfcceresmt = fuswsfcceresm(index)
    fulwsfcceresmt = fulwsfcceresm(index)
    reft = fltarr(nhgt,ngoodpix)
    clwct = reft
    plwct = reft
    hgtt = reft
    qrt = fltarr(nhgt,ngoodpix,nflux)
    fut = qrt
    fdt = qrt
    fuclrt = qrt
    fdclrt = qrt
    funat = qrt
    fdnat = qrt
    qret = qrt
    fuet = qrt
    fdet = qrt
    fuclret = qrt
    fdclret = qrt
    funaet = qrt
    fdnaet = qrt
    ectt = reft
    ecpt = reft
    ecqt = reft
    tempaermaskt = intarr(nhgt,ngoodpix)
    tempcldmaskt = tempaermaskt
    for zz=0L,ngoodpix-1 do begin
     reft(*,zz) = ref(*,index(zz))
     ectt(*,zz) = ect(*,index(zz))
     ecpt(*,zz) = ecp(*,index(zz))
     ecqt(*,zz) = ecq(*,index(zz))
     clwct(*,zz) = plwc(*,index(zz))
     plwct(*,zz) = plwc(*,index(zz))
     hgtt(*,zz) = hgt(*,index(zz))
     fut(*,zz,*) = fu(*,index(zz),*)
     fdt(*,zz,*) = fd(*,index(zz),*)
     fuclrt(*,zz,*) = fuclr(*,index(zz),*)
     fdclrt(*,zz,*) = fdclr(*,index(zz),*)
     funat(*,zz,*) = funa(*,index(zz),*)
     fdnat(*,zz,*) = fdna(*,index(zz),*)
     qrt(*,zz,*) = qr(*,index(zz),*)
     fuet(*,zz,*) = fue(*,index(zz),*)
     fdet(*,zz,*) = fde(*,index(zz),*)
     fuclret(*,zz,*) = fuclre(*,index(zz),*)
     fdclret(*,zz,*) = fdclre(*,index(zz),*)
     funaet(*,zz,*) = funae(*,index(zz),*)
     fdnaet(*,zz,*) = fdnae(*,index(zz),*)
     qret(*,zz,*) = qre(*,index(zz),*)
     tempaermaskt(*,zz) = tempaermask(*,index(zz))
     tempcldmaskt(*,zz) = tempcldmask(*,index(zz))
    endfor  ;zz

    outfile = outdir1d + 'Regional/aerosol-aux.1d.' + granlist(fid) + boxtxt + '.nc'
    print,outfile
    id = ncdf_create(outfile, /clobber)

; Tag file

            parts = str_sep(outfile, '/')
            szparts = size(parts)
            filenameout_nopath = parts[szparts[1]-1]
            ncdf_attput, id, /global, "thisfile", filenameout_nopath
            timedatestamp = systime()
            ncdf_attput, id, /global, "process_timedate", timedatestamp

; Define dimensions

            nid = ncdf_dimdef(id, 'npix',ngoodpix)

; Define variables

            lonid = ncdf_vardef(id, 'longitude', [nid], /float)
            latid = ncdf_vardef(id, 'latitude', [nid], /float)
            sbinid = ncdf_vardef(id, 'sbin', [nid], /float)
            contid = ncdf_vardef(id, 'continentid', [nid], /float)
            areaid = ncdf_vardef(id, 'area', [nid], /float)
            meanswid = ncdf_vardef(id, 'meansw', [nid], /float)
            cswtoaid = ncdf_vardef(id, 'cswftoa', [nid], /float)
            clwtoaid = ncdf_vardef(id, 'clwftoa', [nid], /float)
            cswsfcid = ncdf_vardef(id, 'cswfsfc', [nid], /float)
            clwsfcid = ncdf_vardef(id, 'clwfsfc', [nid], /float)
            cswsfceid = ncdf_vardef(id, 'cswfsfcerb', [nid], /float)
            cswtoaeid = ncdf_vardef(id, 'cswftoaerb', [nid], /float)
            calaertypeid = ncdf_vardef(id, 'calipsotype', [nid], /float)
            calaodid = ncdf_vardef(id, 'calipsoaod', [nid], /float)
            touchid = ncdf_vardef(id, 'touching', [nid], /float)
            pflagid = ncdf_vardef(id, 'pflag', [nid], /float)
            csflagid = ncdf_vardef(id, 'csflag', [nid], /float)
            cldclassid = ncdf_vardef(id, 'cldclass', [nid], /float)
            rthid = ncdf_vardef(id, 'rth', [nid], /float)
            cthid = ncdf_vardef(id, 'cldthick', [nid], /float)
            ctopid = ncdf_vardef(id, 'cldtop', [nid], /float)
            rrcprid = ncdf_vardef(id, 'rrcpr', [nid], /float)
            rrcprprfid = ncdf_vardef(id, 'rrcprprf', [nid], /float)
            rrerrprfid = ncdf_vardef(id, 'rrerrprf', [nid], /float)
            prfstatusid = ncdf_vardef(id, 'prfstatus', [nid], /float)
            prfqualid = ncdf_vardef(id, 'prfqual', [nid], /float)
            srcprprfid = ncdf_vardef(id, 'srcprprf', [nid], /float)
            srerrprfid = ncdf_vardef(id, 'srerrprf', [nid], /float)
            sretstatusid = ncdf_vardef(id, 'sretstatus', [nid], /float)
            clwpid = ncdf_vardef(id, 'clwp', [nid], /float)
            rlwpid = ncdf_vardef(id, 'rlwp', [nid], /float)
            flid = ncdf_vardef(id, 'fl', [nid], /float)
            piaid = ncdf_vardef(id, 'pia', [nid], /float)
            pamsrid = ncdf_vardef(id, 'pamsr', [nid], /float)
            windid = ncdf_vardef(id, 'sfcwind', [nid], /float)
            cwvid = ncdf_vardef(id, 'cwv', [nid], /float)
            lwpid = ncdf_vardef(id, 'lwp', [nid], /float)
            sstid = ncdf_vardef(id, 'sst', [nid], /float)
            ssteid = ncdf_vardef(id, 'ecsst', [nid], /float)
            sstmid = ncdf_vardef(id, 'merrasst', [nid], /float)
            ltssid = ncdf_vardef(id, 'ecltss', [nid], /float)
            ltssmid = ncdf_vardef(id, 'merraltss', [nid], /float)
            omegaid = ncdf_vardef(id, 'merraomega', [nid], /float)
            capeid = ncdf_vardef(id, 'merracape', [nid], /float)
            liid = ncdf_vardef(id, 'merrali', [nid], /float)
            
            cinid = ncdf_vardef(id, 'merracin', [nid], /float)
            cptid = ncdf_vardef(id, 'merracpt', [nid], /float)
            efluxid = ncdf_vardef(id, 'merraeflux', [nid], /float)
            eis_3id = ncdf_vardef(id, 'merraeis3', [nid], /float)
            eis_4id = ncdf_vardef(id, 'merraeis4', [nid], /float)
            evapid = ncdf_vardef(id, 'merraevap', [nid], /float)
            frcanid = ncdf_vardef(id, 'merrafrcan', [nid], /float)
            frccnid = ncdf_vardef(id, 'merrafrccn', [nid], /float)
            frclsid = ncdf_vardef(id, 'merrafrcls', [nid], /float)
            h500id = ncdf_vardef(id, 'merrah500', [nid], /float)
            h850id = ncdf_vardef(id, 'merrah850', [nid], /float)
            hfluxid = ncdf_vardef(id, 'merrahflux', [nid], /float)
            omega700id = ncdf_vardef(id, 'merraomega700', [nid], /float)
            pblhid = ncdf_vardef(id, 'merrapblh', [nid], /float)
            precanvid  = ncdf_vardef(id, 'merraprecanv', [nid], /float)
            precconid = ncdf_vardef(id, 'merrapreccon', [nid], /float)
            preclscid = ncdf_vardef(id, 'merrapreclscid', [nid], /float)
            prectotid = ncdf_vardef(id, 'merraprectot', [nid], /float)
            qlmlid = ncdf_vardef(id, 'merraqlml', [nid], /float)
            qv700id = ncdf_vardef(id, 'merraqv700', [nid], /float)
            rh700id = ncdf_vardef(id, 'merrarh700', [nid], /float)
            
            slpid = ncdf_vardef(id, 'merraslp', [nid], /float)
            t700id = ncdf_vardef(id, 'merrat700', [nid], /float)
            thvid = ncdf_vardef(id, 'merrathv', [nid], /float)
            tqiid = ncdf_vardef(id, 'merratqi', [nid], /float)
            tqlid = ncdf_vardef(id, 'merratql', [nid], /float)
            u500id = ncdf_vardef(id, 'merrau500', [nid], /float)
            u700id = ncdf_vardef(id, 'merrau700', [nid], /float)
            u850id = ncdf_vardef(id, 'merrau850', [nid], /float)
            v500id = ncdf_vardef(id, 'merrav500', [nid], /float)
            v700id = ncdf_vardef(id, 'merrav700', [nid], /float)
            v850id = ncdf_vardef(id, 'merrav850', [nid], /float)
            
            
            gemsaodid = ncdf_vardef(id, 'maccaod', [nid], /float)
            gemsbcid = ncdf_vardef(id, 'maccbc', [nid], /float)
            gemsduid = ncdf_vardef(id, 'maccdu', [nid], /float)
            gemsocid = ncdf_vardef(id, 'maccoc', [nid], /float)
            gemssaid = ncdf_vardef(id, 'maccsa', [nid], /float)
            gemssuid = ncdf_vardef(id, 'maccsu', [nid], /float)
            aodid = ncdf_vardef(id, 'saod', [nid], /float)
            bcid = ncdf_vardef(id, 'sbc', [nid], /float)
            duid = ncdf_vardef(id, 'sdu', [nid], /float)
            ocid = ncdf_vardef(id, 'soc', [nid], /float)
            said = ncdf_vardef(id, 'ssa', [nid], /float)
            suid = ncdf_vardef(id, 'ssu', [nid], /float)
            modisaodid = ncdf_vardef(id, 'modisaod', [nid], /float)
            modisaiid = ncdf_vardef(id, 'modisai', [nid], /float)
            solarid = ncdf_vardef(id, 'solar', [nid], /float)
            solareid = ncdf_vardef(id, 'solare', [nid], /float)
            osrid = ncdf_vardef(id, 'osr', [nid], /float)
            osreid = ncdf_vardef(id, 'osre', [nid], /float)
            ssrid = ncdf_vardef(id, 'ssr', [nid], /float)
            ssreid = ncdf_vardef(id, 'ssre', [nid], /float)
            sfcrid = ncdf_vardef(id, 'sfcr', [nid], /float)
            sfcreid = ncdf_vardef(id, 'sfcre', [nid], /float)
            olrid = ncdf_vardef(id, 'olr', [nid], /float)
            slrid = ncdf_vardef(id, 'slr', [nid], /float)
            sfceid = ncdf_vardef(id, 'sfce', [nid], /float)
            szamodisid = ncdf_vardef(id, 'szamodis', [nid], /float)
            stmodisid = ncdf_vardef(id, 'stmodis', [nid], /float)
            spmodisid = ncdf_vardef(id, 'spmodis', [nid], /float)
            flagmodisid = ncdf_vardef(id, 'flagmodis', [nid], /float)
            cmask5modisid = ncdf_vardef(id, 'cmask5modis', [nid], /float)
            cf5modisid = ncdf_vardef(id, 'cf5modis', [nid], /float)
            phasemodisid = ncdf_vardef(id, 'phasemodis', [nid], /float)
            irphasemodisid = ncdf_vardef(id, 'irphasemodis', [nid], /float)
            multilayermodisid = ncdf_vardef(id, 'multilayermodis', [nid], /float)
            cttmodisid = ncdf_vardef(id, 'cttmodis', [nid], /float)
            ctpmodisid = ncdf_vardef(id, 'ctpmodis', [nid], /float)
            remodisid = ncdf_vardef(id, 'remodis', [nid], /float)
            remodiserrorid = ncdf_vardef(id, 'remodiserror', [nid], /float)
            cotmodisid = ncdf_vardef(id, 'cotmodis', [nid], /float)
            cotmodiserrorid = ncdf_vardef(id, 'cotmodiserror', [nid], /float)
            lwpmodisid = ncdf_vardef(id, 'lwpmodis', [nid], /float)
            lwpmodiserrorid = ncdf_vardef(id, 'lwpmodiserror', [nid], /float)
            remodis1621id = ncdf_vardef(id, 'remodis1621', [nid], /float)
            cotmodis1621id = ncdf_vardef(id, 'cotmodis1621', [nid], /float)
            lwpmodis1621id = ncdf_vardef(id, 'lwpmodis1621', [nid], /float)
            latceresmid = ncdf_vardef(id, 'latceresm', [nid], /float)
            lonceresmid = ncdf_vardef(id, 'lonceresm', [nid], /float)
            timeceresmid = ncdf_vardef(id, 'timeceresm', [nid], /float)
            stypeceresmid = ncdf_vardef(id, 'stypeceresm', [nid], /float)
            szaceresmid = ncdf_vardef(id, 'szaceresm', [nid], /float)
            stceresmid = ncdf_vardef(id, 'stceresm', [nid], /float)
            spceresmid = ncdf_vardef(id, 'spceresm', [nid], /float)
            cwvceresmid = ncdf_vardef(id, 'cwvceresm', [nid], /float)
            cldceresmid = ncdf_vardef(id, 'cldceresm', [nid], /float)
            albedoceresmid = ncdf_vardef(id, 'albedoceresm', [nid], /float)
            semissceresmid = ncdf_vardef(id, 'semissceresm', [nid], /float)
            fuswtoaceresmid = ncdf_vardef(id, 'fuswtoaceresm', [nid], /float)
            fdswtoaceresmid = ncdf_vardef(id, 'fdswtoaceresm', [nid], /float)
            fulwtoaceresmid = ncdf_vardef(id, 'fulwtoaceresm', [nid], /float)
            fdswsfcceresmid = ncdf_vardef(id, 'fdswsfcceresm', [nid], /float)
            fdlwsfcceresmid = ncdf_vardef(id, 'fdlwsfcceresm', [nid], /float)
            fuswsfcceresmid = ncdf_vardef(id, 'fuswsfcceresm', [nid], /float)
            fulwsfcceresmid  = ncdf_vardef(id, 'fulwsfcceresm', [nid], /float)

; Add attributes (not yet implemented)

;            ncdf_attput, id, npid, "long_name", "Number of pixels in granule segment"
;            ncdf_attput, id, npid, "short_name", "Pixels"
;            ncdf_attput, id, npid, "units", "Number"

            ncdf_control, id, /endef ; switch to data mode
    
            ncdf_varput, id, lonid, lont
            ncdf_varput, id, latid, latt
            ncdf_varput, id, sbinid, newsfcbint
            ncdf_varput, id, contid, continentidt
            ncdf_varput, id, areaid, areast
            ncdf_varput, id, meanswid, meanswt
            ncdf_varput, id, cswtoaid, cswtoat
            ncdf_varput, id, clwtoaid, clwtoat
            ncdf_varput, id, clwsfcid, clwsfct
            ncdf_varput, id, cswsfcid, cswsfct
            ncdf_varput, id, cswsfceid, cswsfcet
            ncdf_varput, id, cswtoaeid, cswtoaet
            ncdf_varput, id, calaertypeid, calipsotypet
            ncdf_varput, id, calaodid, calipsoaodt
            ncdf_varput, id, touchid, touchingt
            ncdf_varput, id, pflagid, pflagt
            ncdf_varput, id, csflagid, csflagt
            ncdf_varput, id, cldclassid, cldclasst
            ncdf_varput, id, rthid, rtht
            ncdf_varput, id, cthid, cldthickt
            ncdf_varput, id, ctopid, cldtopt
            ncdf_varput, id, rrcprid, rrcprt
            ncdf_varput, id, rrcprprfid, rrcprprft
            ncdf_varput, id, rrerrprfid, rrerrprft
            ncdf_varput, id, prfstatusid, prfstatust
            ncdf_varput, id, prfqualid, prfqualt

; Jan. 2015: added snow variables
 
            ncdf_varput, id, srcprprfid, srcprprft
            ncdf_varput, id, srerrprfid, srerrprft
            ncdf_varput, id, sretstatusid, sretstatust
            ncdf_varput, id, clwpid, clwpt
            ncdf_varput, id, rlwpid, rlwpt
            ncdf_varput, id, flid, flt
            ncdf_varput, id, piaid, piat
            ncdf_varput, id, pamsrid, pamsrt
            ncdf_varput, id, windid, sfcwindt
            ncdf_varput, id, cwvid, cwvamsrt
            ncdf_varput, id, lwpid, lwpamsrt
            ncdf_varput, id, sstid, sstamsrt
            ncdf_varput, id, ssteid, ecsstt
            ncdf_varput, id, sstmid, merratst
            ncdf_varput, id, ltssid, ltsst
            ncdf_varput, id, ltssmid, merraltst
            ncdf_varput, id, omegaid, merraomegat
            ncdf_varput, id, capeid, merracapet
            ncdf_varput, id, liid, merralit
            
            ncdf_varput, id, cinid, merracint
            ncdf_varput, id, cptid, merracptt
            ncdf_varput, id, efluxid, merraefluxt
            ncdf_varput, id, eis_3id, merraeis3t
            ncdf_varput, id, eis_4id, merraeis4t
            ncdf_varput, id, evapid, merraevapt
            ncdf_varput, id, frcanid, merrafrcant
            ncdf_varput, id, frccnid, merrafrccnt
            ncdf_varput, id, frclsid, merrafrclst
            ncdf_varput, id, h500id, merrah500t
            ncdf_varput, id, h850id, merrah850t
            ncdf_varput, id, hfluxid, merrahfluxt
            ncdf_varput, id, omega700id, merraomega700t
            ncdf_varput, id, pblhid, merrapblht
            ncdf_varput, id, precanvid, merraprecanvt
            ncdf_varput, id, precconid, merrapreccont
            ncdf_varput, id, preclscid, merrapreclscidt
            ncdf_varput, id, prectotid, merraprectott
            ncdf_varput, id, qlmlid, merraqlmlt
            ncdf_varput, id, qv700id, merraqv700t
            ncdf_varput, id, rh700id, merrarh700t
            
            ncdf_varput, id, slpid, merraslpt
            ncdf_varput, id, t700id, merrat700t
            ncdf_varput, id, thvid, merrathvt
            ncdf_varput, id, tqiid, merratqit
            ncdf_varput, id, tqlid, merratqlt
            ncdf_varput, id, u500id, merrau500t
            ncdf_varput, id, u700id, merrau700t
            ncdf_varput, id, u850id, merrau850t
            ncdf_varput, id,v500id, merrav500t
            ncdf_varput, id, v700id, merrav700t
            ncdf_varput, id, v850id, merrav850t
            
            ncdf_varput, id, gemsaodid, gemsaodst
            ncdf_varput, id, gemsbcid, gemsbct
            ncdf_varput, id, gemsduid, gemsdut
            ncdf_varput, id, gemsocid, gemsoct
            ncdf_varput, id, gemssaid, gemssat
            ncdf_varput, id, gemssuid, gemssut
            ncdf_varput, id, aodid, aodst
            ncdf_varput, id, bcid, bcaodst
            ncdf_varput, id, duid, duaodst
            ncdf_varput, id, ocid, ocaodst
            ncdf_varput, id, said, saaodst
            ncdf_varput, id, suid, suaodst
            ncdf_varput, id, modisaodid, modisaodst
            ncdf_varput, id, modisaiid, modisaist

; Jan. 2015: added FLXHR fluxes

            ncdf_varput, id, solarid, solart
            ncdf_varput, id, solareid, solaret
            ncdf_varput, id, osrid, osrt
            ncdf_varput, id, osreid, osret
            ncdf_varput, id, ssrid, ssrt
            ncdf_varput, id, ssreid, ssret
            ncdf_varput, id, sfcrid, sfcrt
            ncdf_varput, id, sfcreid, sfcret
            ncdf_varput, id, olrid, olrt
            ncdf_varput, id, slrid, slrt
            ncdf_varput, id, sfceid, sfcet

; Jan. 2015: added MOD06 matchups

            ncdf_varput, id, szamodisid, szamodist
            ncdf_varput, id, stmodisid, stmodist
            ncdf_varput, id, spmodisid, spmodist
            ncdf_varput, id, flagmodisid, flagmodist
            ncdf_varput, id, cmask5modisid, cmask5modist
            ncdf_varput, id, cf5modisid, cf5modist
            ncdf_varput, id, phasemodisid, phasemodist
            ncdf_varput, id, irphasemodisid, irphasemodist
            ncdf_varput, id, multilayermodisid,multilayermodist
            ncdf_varput, id, cttmodisid, cttmodist
            ncdf_varput, id, ctpmodisid, ctpmodist
            ncdf_varput, id, remodisid, remodist
            ncdf_varput, id, remodiserrorid, remodiserrort
            ncdf_varput, id, cotmodisid, cotmodist
            ncdf_varput, id, cotmodiserrorid, cotmodiserrort
            ncdf_varput, id, lwpmodisid, lwpmodist
            ncdf_varput, id, lwpmodiserrorid, lwpmodiserrort
            ncdf_varput, id, remodis1621id, remodis1621t
            ncdf_varput, id, cotmodis1621id, cotmodis1621t
            ncdf_varput, id, lwpmodis1621id, lwpmodis1621t

; Jan. 2015: added CERES matchups

            ncdf_varput, id, latceresmid, latceresmt
            ncdf_varput, id, lonceresmid, lonceresmt
            ncdf_varput, id, timeceresmid, timeceresmt
            ncdf_varput, id, stypeceresmid, stypeceresmt
            ncdf_varput, id, szaceresmid, szaceresmt
            ncdf_varput, id, stceresmid, stceresmt
            ncdf_varput, id, spceresmid, spceresmt
            ncdf_varput, id, cwvceresmid, cwvceresmt
            ncdf_varput, id, cldceresmid, cldceresmt
            ncdf_varput, id, albedoceresmid, albedoceresmt
            ncdf_varput, id, semissceresmid, semissceresmt
            ncdf_varput, id, fuswtoaceresmid, fuswtoaceresmt
            ncdf_varput, id, fdswtoaceresmid, fdswtoaceresmt
            ncdf_varput, id, fulwtoaceresmid, fulwtoaceresmt
            ncdf_varput, id, fdswsfcceresmid, fdswsfcceresmt
            ncdf_varput, id, fdlwsfcceresmid, fdlwsfcceresmt
            ncdf_varput, id, fuswsfcceresmid, fuswsfcceresmt
            ncdf_varput, id, fulwsfcceresmid, fulwsfcceresmt
            

            

     ncdf_close,id

goto,skip2d2

; Add a separate file for just a few 2d fields

     outfile = outdir2d + 'Regional/aerosol-aux.2d.' + granlist(fid) + boxtxt + '.nc'
     id = ncdf_create(outfile, /clobber)

; Tag file

            parts = str_sep(outfile, '/')
            szparts = size(parts)
            filenameout_nopath = parts[szparts[1]-1]
            ncdf_attput, id, /global, "thisfile", filenameout_nopath
            timedatestamp = systime()
            ncdf_attput, id, /global, "process_timedate", timedatestamp

; Define dimensions

            hid = ncdf_dimdef(id, 'nhgt',nhgt)
            nid = ncdf_dimdef(id, 'npix',ngoodpix)
            flid = ncdf_dimdef(id, 'nflux',nflux)

; Define variables

            lonid = ncdf_vardef(id, 'longitude', [nid], /float)
            latid = ncdf_vardef(id, 'latitude', [nid], /float)
            hgtid = ncdf_vardef(id, 'hgt', [hid,nid], /float)
            refid = ncdf_vardef(id, 'ref', [hid,nid], /float)
            clwcid = ncdf_vardef(id, 'clwc', [hid,nid], /float)
            plwcid = ncdf_vardef(id, 'plwc', [hid,nid], /float)
            ectid = ncdf_vardef(id, 'ectemp', [hid,nid], /float)
            ecpid = ncdf_vardef(id, 'ecpres', [hid,nid], /float)
            ecqid = ncdf_vardef(id, 'ecq', [hid,nid], /float)
            aermaskid = ncdf_vardef(id, 'aermask', [hid,nid], /float)
            cldmaskid = ncdf_vardef(id, 'cldmask', [hid,nid], /float)
            qrid = ncdf_vardef(id, 'qr', [hid,nid,flid], /float)
            fuid = ncdf_vardef(id, 'fu', [hid,nid,flid], /float)
            fdid = ncdf_vardef(id, 'fd', [hid,nid,flid], /float)
            fuclrid = ncdf_vardef(id, 'fuclr', [hid,nid,flid], /float)
            fdclrid = ncdf_vardef(id, 'fdclr', [hid,nid,flid], /float)
            funaid = ncdf_vardef(id, 'funa', [hid,nid,flid], /float)
            fdnaid = ncdf_vardef(id, 'fdna', [hid,nid,flid], /float)
            qreid = ncdf_vardef(id, 'qrerb', [hid,nid,flid], /float)
            fueid = ncdf_vardef(id, 'fuerb', [hid,nid,flid], /float)
            fdeid = ncdf_vardef(id, 'fderb', [hid,nid,flid], /float)
            fuclreid = ncdf_vardef(id, 'fuclrerb', [hid,nid,flid], /float)
            fdclreid = ncdf_vardef(id, 'fdclrerb', [hid,nid,flid], /float)
            funaeid = ncdf_vardef(id, 'funaerb', [hid,nid,flid], /float)
            fdnaeid = ncdf_vardef(id, 'fdnaerb', [hid,nid,flid], /float)

; Add attributes (not yet implemented)

;            ncdf_attput, id, npid, "long_name", "Number of pixels in granule segment"
;            ncdf_attput, id, npid, "short_name", "Pixels"
;            ncdf_attput, id, npid, "units", "Number"

            ncdf_control, id, /endef ; switch to data mode
    
            ncdf_varput, id, lonid, lont
            ncdf_varput, id, latid, latt
            ncdf_varput, id, hgtid, hgtt
            ncdf_varput, id, refid, reft
            ncdf_varput, id, clwcid, clwct
            ncdf_varput, id, plwcid, plwct
            ncdf_varput, id, ectid, ectt
            ncdf_varput, id, ecpid, ecpt
            ncdf_varput, id, ecqid, ecqt
            ncdf_varput, id, aermaskid, tempaermaskt
            ncdf_varput, id, cldmaskid, tempcldmaskt
            ncdf_varput, id, qrid, qrt
            ncdf_varput, id, fuid, fut
            ncdf_varput, id, fdid, fdt
            ncdf_varput, id, fuclrid, fuclrt
            ncdf_varput, id, fdclrid, fdclrt
            ncdf_varput, id, funaid, funat
            ncdf_varput, id, fdnaid, fdnat
            ncdf_varput, id, qreid, qret
            ncdf_varput, id, fueid, fuet
            ncdf_varput, id, fdeid, fdet
            ncdf_varput, id, fuclreid, fuclret
            ncdf_varput, id, fdclreid, fdclret
            ncdf_varput, id, funaeid, funaet
            ncdf_varput, id, fdnaeid, fdnaet
	
	
   ncdf_close,id

skip2d2:

    endif

  endfor  ;i
 endfor  ;j

skiporbit:

endfor  ;fid

close,24



END
