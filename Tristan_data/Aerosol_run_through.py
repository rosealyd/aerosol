#!/usr/bin/env python

from tabulate import tabulate
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.patheffects
from collections import defaultdict
import colorsys as cls
import random
from pylab import *
import matplotlib.cm as cm
	
#Find delta
def delta(overall, opt, touching_status, rain_status, variable):
	
	delta_x = []
	for lwp_status in ['low','medium','high']:
		x1s = overall[opt][touching_status][rain_status][lwp_status][variable]["low"]
		x2s = overall[opt][touching_status][rain_status][lwp_status][variable]["high"]
		deltas = [x2-x1 for x1,x2 in zip(x1s, x2s)]
		delta_x.append(deltas)
	return delta_x


#Find the average when adding the low and high
def average(overall, opt, touching_status, rain_status, variable):
	delta_x = []
	for lwp_status in ['low','medium','high']:
		x1s = overall[opt][touching_status][rain_status][lwp_status][variable]["low"]
		x2s = overall[opt][touching_status][rain_status][lwp_status][variable]["high"]
		deltas = [(x2+x1)/2 for x1,x2 in zip(x1s, x2s)]
		delta_x.append(deltas)
	return delta_x
	
#To return the high and low value of a list and return a graph array for color mapping
def find_h_l(overall,variable, find_diff, find_average):
	graph_array = []
	max_diff = 0.0
	min_diff = 100000.0
	net_max = 0
	net_min = 10000.0

	for aerosol_key, aerosol in overall.iteritems():
		for touch_key, touch in aerosol.iteritems():
			for rain_key, rain_state in touch.iteritems():
				for lwp_key, lwp_stat in rain_state.iteritems():
					for var_key,var in lwp_stat.iteritems():
						for ai_key, ai in var.iteritems():
							temp = sum(ai)
							if temp > net_max:
								net_max = temp
							elif temp < net_min:
								net_min = temp
				if find_average == True:
					diff = average(overall, aerosol_key, touch_key, rain_key, variable)
				else:
					diff = delta(overall, aerosol_key, touch_key, rain_key, variable)
				for lwp_stat in diff:
					for difference in lwp_stat:
						if difference > max_diff:
							max_diff = difference
						
						if difference < min_diff and difference != 0:
							min_diff = difference
							
						graph_array.append(difference)
					
	if find_diff == True:
		return min_diff, max_diff, graph_array
	else:
		return min_diff, max_diff, net_min, net_max
	


#To check user input
def check(options,user_input):
	letters = list(map(chr,range(97, 97+len(options))))
	if user_input in letters:
		return
	else:
		print "That was bad input"
		sys.exit()

#Creates a table using a nested loop of high variables and low variables that can be outputted tabulate
def create_table(high_variables, low_variables, variance):
	table = [[],[],[]]
	x = 0
	for i in range(3):
		for j in range(3):
			temp = str(low_variables[i+j+x]) + "  " + str(high_variables[i+j+x])
			try:
				table[i].append(temp)
			except:
				print type(table)
				print i,j
				sys.exit()
		x+=2
	return table

#Create variable dictionary with high/low
def sort_into_dicts(low_variables, high_variables, list_of_vars):
	return_dictionary = defaultdict(dict)
	
	for i,(l,h) in enumerate(zip(low_variables, high_variables)):
		return_dictionary[list_of_vars[i]] = {"low": l, "high": h}
		
		
	return return_dictionary
		
#Error processing
error = raw_input("Do you wish to process an error file?: a) yes b) no\n")
check(["y","n"],error)
plotting = raw_input("Do you wish to plot or make tables? a) plot b) tables\n")
check(["p","t"],plotting)
deriv = raw_input("Would you like to look at deriv files? a) yes b) no\n")
check(["y","n"], deriv)

if deriv == "a":
	deriv = True
	if error == "a":
		error_file = "error"
		#Where the data files are kept
		rootdir = "./deriv_error_data_files/" 
		if plotting == "b":
			#Where the files are being saved
			save_path =  "./deriv_error_AISU_dotdat_files/"
		else:
			#Where the files are being saved
			save_path =  "./deriv_error_AISU_plot_files/"
	
	else:
		error_file = "noerror"
		#Where the data files are kept
		rootdir = "./deriv_data_files/" 
		if plotting == "b":
			#Where the files are being saved
			save_path =  "./deriv_AISU_dotdat_files/"
		else:
			#Where the files are being saved
			save_path =  "./deriv_AISU_plot_files/"
elif deriv == "b":
	deriv = False
	if error == "a":
		error_file = "error"
		#Where the data files are kept
		rootdir = "./error_data_files/" 
		if plotting == "b":
			#Where the files are being saved
			save_path =  "./error_AISU_dotdat_files/"
		else:
			#Where the files are being saved
			save_path =  "./error_AISU_plot_files/"
	
	else:
		error_file = "noerror"
		#Where the data files are kept
		rootdir = "./data_files/" 
		if plotting == "b":
			#Where the files are being saved
			save_path =  "./AISU_dotdat_files/"
		else:
			#Where the files are being saved
			save_path =  "./AISU_plot_files/"

#rootdir = './deriv_data_files/'	
print save_path
#Variables
list_of_vars = ["cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss","lwpidx","ai","dCTdAI","all_dCTdAI","dCF51dAI","all_dCF51dAI","dSWdAI","all_dSWdAI","dCLWdAI","all_dCLWdAI","dCLWdCT", "all_dCLWdCT"]
#Letters
letters = list(map(chr,range(97, 97+27)))
#Rain statuses
rain_status = ["all","nonraining","raining"]
#Boolean telling whether or not on first item in directory
repeat = False
#Boolean telling whether appending or writing
appending = False
#Dictionary that holds all aerosols
overall = defaultdict(lambda:defaultdict(dict))
#Different lwp possibilities
lwps = ["low","medium","high"]
#Touching or not tocching aerosol layer?
touching_status = ""
#What the coordinates could be
coord_opts = [(0,0),(0,-15),(0,-30),(-15,0),(-15,-15),(-15,-30),(-30,0),(-30,-15),(-30,-30)]

lats = [-30,-15,0,-30,-15,0,-30,-15,0]


#f = open("test.dat", 'r')



	

#Running through and gathering the data from the files outputted in Aerosol.py
for root, dirs, files in os.walk(rootdir):
	for directories in dirs:
		touching_status = directories
		for second_root, second_dirs, filenames in os.walk(rootdir + "/" + touching_status):
			for filename in filenames:
				
				f = open(rootdir + "/" + touching_status + "/" + filename, 'r')
				lwp = {}
				for box in range(9):
					for count in range(3):
						low_variables = [[] for _ in xrange(len(list_of_vars))]
						high_variables = [[] for _ in xrange(len(list_of_vars))]
						for i in range(len(list_of_vars)):
							
							line = f.readline()
							
							line = line.replace(")","")
							line = line.replace("(","")
							
							temp = line.split(",")
							low = temp[0].strip()
							high = temp[1].strip()
							low_variables[i].append(float(low))
							high_variables[i].append(float(high))
	
						if box ==0:
							variables = sort_into_dicts(low_variables, high_variables, list_of_vars)
							
							lwp[lwps[count]] = variables
							
			
						else:
							variables = sort_into_dicts(low_variables, high_variables, list_of_vars)
							for var_key, var in lwp[lwps[count]].iteritems():
					
				
								low_ammending = variables[var_key]['low'][0]
								
								high_ammending = variables[var_key]['high'][0]
								
				
								lwp[lwps[count]][var_key]['low'].append(low_ammending)
								lwp[lwps[count]][var_key]['high'].append(high_ammending)

					
					
				f.close()
				parts_of_filename = filename.split("_")

				aerosol_name = parts_of_filename[0]
				#print aerosol_name

				rain_index = parts_of_filename[2]
				

				if not aerosol_name in overall:
					overall[aerosol_name][touching_status][rain_index] = {}
				overall[aerosol_name][touching_status][rain_index] = lwp
				
				
				
#print overall["MODISAI"]["touching"]["all"]["low"].keys()
#print overall["GEMSSU"]["touching"]["all"]["low"]["cswftoa"]["low"]

	

if plotting == "b":
	
	#Prints the data to files each named after the variable
	filename_format = "_LPWbin_AI_ECLTSS_{}.dat".format(error_file)
	
	for aerosol_key, aerosol in overall.iteritems():
		for touching_stat_key, touching_stat in aerosol.iteritems():
			for rain_state_key, rain_state in touching_stat.iteritems():
				for variable_key, variable in rain_state.iteritems():
					low_temp = np.std(variable["low"])
					high_temp = np.std(variable["high"])
					standard_deviation = (low_temp + high_temp)/2
					temp_table = create_table(variable["low"], variable["high"], standard_deviation)
					filename = os.path.join(save_path, touching_stat_key + "_" + variable_key + filename_format)
					writing_string = aerosol_key + "\n"+ rain_state_key+ "\n"+ variable_key +"\n"+ tabulate(temp_table)+ "\n\n"
			
					if appending:
						try:
							w = open(filename, 'a')
							w.write( writing_string)
						except:
							print appending
							sys.exit()
					else:
						try:
							w = open(filename, 'w')
							w.write( writing_string)
							appending = True
						except:
							print appending
							sys.exit()

	sys.exit()
			


else:
	yvariable_not = False
	
	min_lwp, max_lwp, temp, temmp1 = find_h_l(overall, "lwpidx", False, False)
	
	markers = ["v", "o", "s", "8","_","+"]
	aerosol_markers = {"SPRINTARSSU":["1","_","2"],"GEMSSU":["<","+",">"],"SPRINTARSSA":["d", "|","D"], "GEMSSA":["x","p",","],"MODISAI":["v","o","*"],"CALIPSOPD":["H","8","h"]}

	#Plotting
	lat= [0, -15, -30]
	#Finding the x-axis
	for i in range(len(list_of_vars)):
		print letters[i], list_of_vars[i]
	xvariable = raw_input ("Which variable do you wish to use as your x axis?\n")
	check(list_of_vars, xvariable)
	if xvariable < "l" or xvariable in ["m","o","q","s","u"]:
		xvariable = list_of_vars[letters.index(xvariable)]
		x_min, x_max, x_net_min, x_net_max = find_h_l(overall, xvariable, False, False)
	
		#print x_min, x_max
		x_ticks = np.linspace(x_min, x_max, 11)

		#Finding the y-axis
		for i in range(len(list_of_vars)):
			print letters[i], list_of_vars[i]
		yvariable = raw_input ("Which variable do you wish to use as your y axis?\n")
		check(list_of_vars, yvariable)
		yvariable = list_of_vars[letters.index(yvariable)]
		y_min, y_max, y_net_min, y_net_max= find_h_l(overall, yvariable, False, False)
		#print y_min, y_max
		y_ticks = np.linspace(y_min, y_max, 11)
		yvariable_not = True
	
	
	else:
		xvariable = list_of_vars[letters.index(xvariable)]
		yvariable = xvariable
	
	
	
	#Finding which aerosols to plot
	print "\n"
	for key in overall:
		print letters[overall.keys().index(key)], key
	user_input= raw_input("Which aerosol(s) would you like to plot?(Enter comma separated letters if more than one)\n")
	user_input = user_input.split(",")
	for opt in user_input:
		check(overall.keys(),opt)
		user_input[user_input.index(opt)] = overall.keys()[letters.index(opt)]
	
	#Finding which rain status to plot
	print "\n"
	for i in range(len(overall.itervalues().next().itervalues().next().keys())):
		print letters[i], overall.itervalues().next().itervalues().next().keys()[i]
	rain_status = raw_input ("Which rain status do you wish to use?\n")
	check(["a","b","c"], rain_status)
	rain_status = overall.itervalues().next().itervalues().next().keys()[letters.index(rain_status)]

	#Find which touching status to plot
	print "\n"
	for i in range(len(overall.itervalues().next().keys())):
		print letters[i],overall.itervalues().next().keys()[i]
	touching_status = raw_input("Which touching status do you wish to use?\n")
	check(["t","nt"],touching_status)
	touching_status = overall.itervalues().next().keys()[letters.index(touching_status)]
	
	figname = rain_status + "_" + xvariable + "_vs_" + yvariable + "_"
	
	#Averages?
	averages = raw_input("\nWould you like to check the averages for each aerosol?\na) yes\nb) no\n")
	check(["y","n"], averages)
	if averages is "a":
		averages = True
		figname = "averages_" + figname
	else:
		averages = False
	
	#Choosing the color variable
	print "\n"
	for i in range(len(list_of_vars)):
		print letters[i],")", list_of_vars[i]
	color_var = raw_input("\nWhat would you like the color bar variable to be?\n")
	check(list_of_vars, color_var)
	color_var = list_of_vars[letters.index(color_var)]
	min_diff, max_diff, graph_array = find_h_l(overall, color_var, True, True)
	#print min_diff ,max_diff
	
	#What LWPs to plot
	print "\n"
	for i,l in enumerate(lwps):
		print letters[i],")", l
	plot_lwps = raw_input("Which lwp(s) would you like to plot? (Enter comma separated letters if more than one)\n")
	plot_lwps = plot_lwps.split(",")
	
	for i,plot_lwp in enumerate(plot_lwps):
		check(lwps, plot_lwp)
		plot_lwps[i] = lwps[letters.index(plot_lwp)]
		figname = plot_lwps[i][:2] + "_" + figname
	print plot_lwps
		
	figname = "lwps_" + figname
	
	figname = "cb_" + color_var + "_" + figname
	
	figname = touching_status + "_" + figname
	
	

	rcParams['figure.figsize'] = 13, 8 #width, height
	
	
	
	for i in range(len(user_input)):
		opt = user_input[i]
		figname = figname + opt + "_"
		delta_x = []
		delta_y = []
		delta_ecltss = []
		color = []
		
		delta_ecltss = delta(overall, opt, touching_status, rain_status, 'ecltss')
		xvariable_label = xvariable
		yvariable_label = yvariable
		#If high and low deriv, then x and y are same variable
		if letters[list_of_vars.index(xvariable)] in ["l","n","p","r","t"]:
			delta_x = []
			delta_y = []
			for poss in lwps:
				delta_x.append(overall[opt][touching_status][rain_status][poss][xvariable]["high"])
				delta_y.append(overall[opt][touching_status][rain_status][poss][xvariable]["low"])
			xvariable_label = xvariable + "_high"
			yvariable_label = yvariable + "_low"
		#If x variable a total deriv
		if letters[list_of_vars.index(xvariable)] in ["m","o","q","s","u"]:
		
			delta_x = []
			for poss in lwps:
				delta_x.append(overall[opt][touching_status][rain_status][poss][xvariable]["high"])
		#If y variable a total deriv
		if letters[list_of_vars.index(yvariable)] in ["m","o","q","s","u"]:
			delta_y = []
			for poss in lwps:
				delta_y.append(overall[opt][touching_status][rain_status][poss][yvariable]["high"])
		#If x variable not a deriv
		if letters[list_of_vars.index(xvariable)] in letters[:11]:
			delta_x = delta(overall, opt, touching_status, rain_status, xvariable)
		#If y variable not a deriv
		if letters[list_of_vars.index(yvariable)] in letters[:11]:
			delta_y =  delta(overall, opt, touching_status, rain_status, yvariable)
		
		
		
		color = average(overall, opt, touching_status, rain_status, color_var)
		
		markers_list = [None] * len(delta_x)
		
		ax = plt.gca()
		if not deriv:
			plt.xlim(x_min, x_max)
			plt.ylim(y_min, y_max)
		
		ax.spines['left'].set_position('zero')
		ax.spines['right'].set_color('none')
		ax.spines['bottom'].set_position('zero')
		ax.spines['top'].set_color('none')
		ax.xaxis.set_ticks_position('bottom')
		ax.yaxis.set_ticks_position('left')
		
		
		ax.set_xlabel(xvariable_label.upper())
		ax.xaxis.set_label_position('top')
		ax.set_ylabel(yvariable_label.upper())
		ax.yaxis.set_label_position('right')
		
		
		if xvariable == "cswftoa" or xvariable == "clwftoa":
			if yvariable == "cswftoa" or yvariable == "clwftoa":
				for tmpx, tmpy in zip(delta_x, delta_y):
					net = [x2 + x1 for x1, x2 in zip(tmpx, tmpy)]
					net = ["Net: " + str(round(x,2)) for x in net]
				plt.xlim(-60,20)
				x_min = -60
				x_max = 20
				plt.plot([x_min, x_max], [x_min, x_max], 'r-' , linewidth = 2.0)
				#plt.annotate("Net = 0",xytext = (x_max -4.5, x_max - 4.5))
				plt.annotate("Cooling",  xy=(10,20), xytext=(14, 19.5), arrowprops=dict(facecolor='black', shrink=0.05),)
				plt.annotate("Heating", xy=(3,-7),xytext=(-6,-7.5), arrowprops=dict(facecolor='black', shrink=0.05),)
		elif xvariable == "cldtop":
			net = ["Net: " + str(sum(l)) for l in delta_y]
				
		else:
			net = ["" for l in delta_y]
		
		#For each bin in the differenced x, y
		for i,(x_list, y_list) in enumerate(zip(delta_x, delta_y)):
			if averages == False and lwps[i] in plot_lwps:
				for j,(x,y) in enumerate(zip(x_list, y_list)):
				#Plots the high-low for x_variable, y_variable, color is average of two determined by color_var, label is the number cooresponding to location, the ecltss, and the lwp bin it is (low, med, high)
					plt.scatter(x,y, s=200, marker=markers[i], c=color[i][j], cmap=cm.jet, vmin=min_diff, vmax=max_diff, label="{}: {}  {}".format(j, round(delta_ecltss[i][j],2), lwps[i]))
					#Annotation tells the bin it is
					plt.annotate("{}".format(j), xy = (x, y), xytext = (10,10), textcoords = 'offset points', ha = 'right', va = 'bottom', fontsize = 9)
				
			if averages == True:
			#Plotting the average per low, med, high lwp bin (meaning all 9 regions for each bin averaged)
				x = np.mean(x_list)
				y = np.mean(y_list)
				plt.scatter(x,y, marker = aerosol_markers[opt][i], s = 350, c= np.mean(color[i]), cmap=cm.jet, vmin=min_diff, vmax=max_diff, label="{} {}".format(opt,net[i]))
				plt.annotate("{}".format(opt[-2:]), xy = (x,y), xytext = (10,10), textcoords = 'offset points', ha = 'right', va = 'bottom', fontsize = 9)
		
				
		
	
	ts = np.linspace(min_diff, max_diff, 8, True)
	print ts
	for i in range(len(ts)):
		ts[i] = round(ts[i],2)

	clbr = plt.colorbar()
	clbr.set_ticks(ts)

	clbr.set_label("Average {}".format(color_var))

	plt.legend(bbox_to_anchor=(.44,.15), prop={'size':7}, scatterpoints=1, title="Point: diff in ecltss", ncol=3, borderpad = .09,  labelspacing= .2, columnspacing = .15)
	
	figname = figname + ".jpg"
	print figname
	plt.savefig(save_path + figname, dpi=plt.gcf().dpi)
	plt.show()
	





