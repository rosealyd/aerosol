#!/usr/bin/env python

from tabulate import tabulate
import sys
import os

list_of_vars = ["cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss","PoP"]
rain_status = ["all","nonraining","raining"]

end_path = ""

repeat = False

filename = sys.argv[1]


f = open(filename, 'r')


if len(sys.argv)>2:
	var =int(sys.argv[2])
else:
	var = 0

if len(sys.argv) >3:
	repeat=True
else:
	repeat = False


low_variables = [[],[],[],[],[],[],[],[],[],[]]
high_variables = [[],[],[],[],[],[],[],[],[],[]]

i = 0
for line in f.readlines():
	line = line.replace(")","")
	line = line.replace("(","")
	temp = line.split(",")
	low = temp[0].strip()
	high = temp[1].strip()
	low_variables[i].append(low)
	high_variables[i].append(high)
	i = (i + 1) % 10



f.close()
x=0
table = [[],[],[]]
for i in range(3):
	for j in range(3):
		if (float(high_variables[var][i+j+x]) - float(low_variables[var][i+j+x])) > 5:
			temp = low_variables[var][i+j+x] + "  " + high_variables[var][i+j+x]
			try:
				table[i].append(temp)
			except:
				print type(table)
				print i,j
				sys.exit()
		else:
			table[i].append("X  X")
	x+=2


end_path = "data_files" + "/" + list_of_vars[var]
if not os.path.isdir(end_path):
	os.makedirs(end_path)

print "Filename:",filename
filename = filename.replace(".dat","")
filename = filename[filename.rindex("/")+1:]
print "Post",filename
filename = filename + str(var) + ".dat"

file_header = "all" 
parts = filename.split("_")
file_header =parts[0].strip("/") + "\n"+ file_header 


if parts[0].strip("/") != "MODISAI":
	repeat = True
	

if repeat == True:
	filename = filename.replace(parts[0], "MODISAI")
	for rain_type in rain_status:
		if rain_type in filename:
			filename = filename.replace(rain_type, rain_status[0])
			file_header = parts[0].strip("/") + "\n" + rain_type

	f = open(end_path + "/" + filename, 'a')
else:
	f = open(end_path + "/" + filename, 'w')

f.write(file_header + "\n")
f.write(tabulate(table))
f.write("\n")
f.close()

