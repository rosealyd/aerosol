#!/usr/bin/env python

from tabulate import tabulate
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
#To check user input
def check(options,user_input):
	letters = list(map(chr,range(97, 97+len(options))))
	if user_input in letters:
		return
	else:
		print "That was bad input"
		sys.exit()

#Creates a table using a nested loop of high variables and low variables that can be outputted tabulate
def create_table(high_variables, low_variables, variance):
	table = [[],[],[]]
	x = 0
	for i in range(3):
		for j in range(3):
			temp = str(low_variables[i+j+x]) + "  " + str(high_variables[i+j+x])
			try:
				table[i].append(temp)
			except:
				print type(table)
				print i,j
				sys.exit()
		x+=2
	return table

def sort_into_dicts(low_variables, high_variables, list_of_vars):
	return_dictionary = {}
	for (l,h) in zip(low_variables, high_variables):
		for (low_item, high_item) in zip(l,h):
			
			
			return_dictionary[list_of_vars[low_variables.index(l)]] = {"low": l, "high": h}
			
	return return_dictionary

#Variables
list_of_vars = ["cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss","PoP"]
#Letters
letters = list(map(chr,range(97, 97+27)))
#Rain statuses
rain_status = ["all","nonraining","raining"]
#Boolean telling whether or not on first item in directory
repeat = False
#Boolean telling whether appending or writing
appending = False
#Touching status
touching_status = ""
#Dictionary that holds all aerosols
overall = defaultdict(dict)
rootdir = "./data_files/" 
save_path =  "./AISU_plot_files/"

print os.walk(rootdir)

for root, dirs, files in os.walk(rootdir):
	for directories in dirs:
		touching_status = directories
		for second_root, second_dirs, filenames in os.walk(rootdir + "/" + directories):
			for filename in filenames:
				print filename
		




sys.exit()
	
