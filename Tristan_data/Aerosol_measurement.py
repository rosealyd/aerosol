#!/usr/bin/env python

from tabulate import tabulate
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict




#Variables
list_of_vars = ["cswftoa","clwftoa","cldtop","cf11","cf21","cf51","cf101","merraomega","ecltss","lwpidx","PoP"]
#Options for coordinates
coord_opts = [(0,0),(0,-15),(0,-30),(-15,0),(-15,-15),(-15,-30),(-30,0),(-30,-15),(-30,-30)]
#Rain statuses
rain_status = ["all","nonraining","raining"]
#Dictionary that holds all aerosols
overall = defaultdict(lambda:defaultdict(dict))
#Touching or not tocching aerosol layer?
touching_status = ""
#Root directory
rootdir = "./data_files/"

#Running through and gathering the data from the files outputted in Aerosol.py
for root, dirs, files in os.walk(rootdir):
	for directories in dirs:
		touching_status = directories
		for second_root, second_dirs, filenames in os.walk(rootdir + "/" + touching_status):
			for filename in filenames:
				
				parts_of_filename = filename.split("_")	
				aerosol_name = parts_of_filename[0]

				rain_index = parts_of_filename[2]

				if not aerosol_name in overall:
					overall[aerosol_name][touching_status] = {}
				
				
				f = open(rootdir + "/" + touching_status + "/" + filename, 'r')

				region = 0
				index = 0
				for line in f.readlines():
					
					
					region = index//11
					
				
					if index%11==0:
						measurement = [[],[]]
						count = 0
						
					line = line.replace(")","")
					line = line.replace("(","")
					temp = line.split(",")
					low = temp[0].strip()
					high = temp[1].strip()
					#temp = {}
					#temp[list_of_vars[count]] = {"low":float(low),"high":float(high)}
					#measurement.append(temp)
					measurement[0].append(float(low))
					measurement[1].append(float(high))
					
					count += 1
					if index != 0 and index%11==0:
						coords = {"coordinates":coord_opts[region]}
						overall[aerosol_name][touching_status][rain_index][coords] = measurement
					index += 1
				

				f.close()
				print overall
				sys.exit()
				
				
print overall
					

				
				
				
				
				
				
				

