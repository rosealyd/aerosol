#!/usr/bin/env python

import pickle
import sys
from collections import defaultdict
import os

bin_file = "/thermal/data/Aerosols/newregionalaodbins.shallow.ocean.03607_24860.0.1res.dat"
dump_file = "ai_bin_dict.p"

fixing_dict ={
"SPRINTARS AOD" :"SPRINTARSAOD",
"SPRINTARS Sulfate":"SPRINTARSSU",
"SPRINTARS Sea Salt":"SPRINTARSSA",
"SPRINTARS Dust":"SPRINTARSDU",
"SPRINTARS Organics":"SPRINTARSOC",
"SPRINTARS Black Carbon":"SPRINTARSBC",
"MACC AOD": "GEMSAOD",
"MACC Sea Salt":"GEMSSA",
"MACC Dust":"GEMSDU",
"MACC Organics":"GEMSOC",
"MACC Black Carbon":"GEMSBC",
"MACC Sulfate":"GEMSSU",
"CALIPSO AOD" :"CALIPSOAOD",
"CALIPSO Clean Marine": "CALIPSOCM",
"CALIPSO Dust": "CALIPSODU",
"CALIPSO Polluted Continental": "CALIPSOPC",
"CALIPSO Clean Continental":"CALIPSOCC",
"CALIPSO Polluted Dust":"CALIPSOPD",
"CALIPSO Smoke": "CALIPSOSM"}



num_aerosols = 36
num_lat_lons = 192
global_bins = defaultdict(dict)

with open(bin_file, "r") as f:
	for i in range(38):
		f.readline()
	for j in range(num_lat_lons):
		line = f.readline()
		line = line.split()
		longitude = line[0].split(".")[0]
		latitude = line[1].split(".")[0]
		
		for i in range(num_aerosols):
			line = f.readline()
			line = line.split(":")
			line[0]= line[0].split()
			for i, item in enumerate(line[0]):
				line[0][i] = float(item)
			
			line[1] = line[1].strip()
			if fixing_dict.has_key(line[1]):
				line[1] = fixing_dict[line[1]]
				
			else:
				line[1] = line[1].replace(" ", "")
			global_bins["{}-{}".format(latitude, longitude)][line[1]] = line[0]
	f.close()

with open(dump_file, "w+") as d:
	pickle.dump(global_bins, d)		
		

