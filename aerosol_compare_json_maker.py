#!/usr/bin/env python

import json
import sys
import glob
from collections import defaultdict

#Root directory
ROOTDIR = "/thermal/data/Aerosols/Test/"
#List of aerosols as reference
AEROSOLS = ["MODISAOD","MODISAI","CALIPSOAOD","CALIPSOCM","CALIPSODU","CALIPSOPC","CALIPSOCC","CALIPSOPD","CALIPSOSM","SPRINTARSAOD","SPRINTARSSU",
"SPRINTARSSA","SPRINTARSDU","SPRINTARSOC","SPRINTARSBC","GEMSAOD","GEMSSU","GEMSSA","GEMSDU","GEMSOC", "GEMSBC"]
#Dumping name
DUMP_NAME = "AI_comparison.js"


global_aods = globe = defaultdict(lambda:defaultdict(lambda:defaultdict))

for filename in glob.glob(ROOTDIR+"aerosoldata*24860*touching.*.dat"):
	
	if "day" not in filename and "night" not in filename:
		
	#Do not want day and night specfic files
		parts_of_filename = filename.split(".")
		#getting latitude and longitude
		lat = parts_of_filename[9].split("_")[0]
		
		lon = parts_of_filename[9].split("_")[1]
		
		global_aods[lat+"-"+lon]["touching"] = defaultdict(lambda:defaultdict(lambda:defaultdict))
		with open(filename,"r") as f:
			#Skip first line because it is text
			f.readline()
			for aerosol in range(len(AEROSOLS)):
				#Creating dictionary with values
				line = f.readline()
				line = line.split()

				first_aerosol = AEROSOLS[int(line[0])]
				f_a_int = int(line[0])
				second_aerosol = AEROSOLS[int(line[1])]
				s_a_int = int(line[1])
				print first_aerosol, second_aerosol
				
				if first_aerosol == second_aerosol:
					first_aerosol = first_aerosol + "_prime"
					
				one_temp = float(line[4])
				two_temp = float(line[5])
				global_aods[lat+"-"+lon]["touching"][first_aerosol+"vs"+second_aerosol] = {first_aerosol:[one_temp], second_aerosol:[two_temp]}
				
			
				for i in range(9):
				
					line = f.readline()
					
					line = line.split()
					if int(line[0]) == s_a_int and int(line[1]) == f_a_int:
					
						one_temp = float(line[5])
						two_temp = float(line[4])
						
					else:
						one_temp = float(line[4])
						two_temp = float(line[5])
					global_aods[lat+"-"+lon]["touching"][first_aerosol+"vs"+second_aerosol][first_aerosol].append(one_temp)
					global_aods[lat+"-"+lon]["touching"][first_aerosol+"vs"+second_aerosol][second_aerosol].append(two_temp)
print filename, line
#print global_aods["0--15"]["touching"]["CALIPSOCCvsMODISAOD"]
with open(DUMP_NAME, "w") as d:
	json.dump(global_aods, d)

