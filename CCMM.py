#!/usr/bin/env python
import h5py as hp
from netCDF4 import Dataset
import numpy as np
import json
from collections import defaultdict
import sys
from pyhdf.SD import SDS 
import time
import glob
from pyhdf.SD import SD, SDC 
import copy
from pyhdf.HDF import *
from pyhdf.VS import *
from pyhdf.SD import *
import calendar 
DUMP_DIR = '/home/adouglas2/Summer/aerosol/CCMM_test/'
lats = ['-60.0000'  ,   '-45.0000'  ,   '-30.0000'   ,  '-15.0000'    ,  '0.00000'  ,    '15.0000',
      '30.0000'    ,  '45.0000']
      
lons = [  '-180.000'   ,  '-165.000'  ,  '-150.000'    , '-135.000'   ,  '-120.000'   ,  '-105.000',
     '-90.0000'  ,   '-75.0000'   ,  '-60.0000'   ,  '-45.0000' ,    '-30.0000' ,    '-15.0000',
     '0.00000'   ,  '15.0000'     , '30.0000' ,     '45.0000'   ,  '60.0000' ,     '75.0000',
      '90.0000'  ,    '105.000'  ,    '120.000' ,     '135.000' ,    '150.000'   ,   '165.000']

clear_sky_dicts = defaultdict(dict)

for lat in lats:
	for lon in lons:
		clear_sky_dicts['{}_{}'.format(lat, lon)] = []
			
ceres_dir = '/thermal/data/CERES_CLR/'
#clear sky 2 starts at jan of 2004, each increment of time is one month until december 2010
ceres_filename = 'CERES_SYN1deg-Month_Terra-Aqua-MODIS_Ed3A_Subset_200101-201412.nc'

CLR_fluxes = Dataset(ceres_dir + ceres_filename, mode = 'r')
for var in CLR_fluxes.variables:
	print var



time = list(CLR_fluxes['time'][:])
latitudes = list(CLR_fluxes['lat'][:])
longitudes = list(CLR_fluxes['lon'][:])
clear_toa = list(CLR_fluxes['toa_alb_clr_mon'][:])

year = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
years_dict = defaultdict(dict)
years = []
for month in year:
	years_dict[month] = copy.deepcopy(clear_sky_dicts)
print len(year)

for y in range(7):
	print y
	for i, month in enumerate(year):
		years.append(month)
		
print len(years), len(time)

for i, (month, clear_sky_flux) in enumerate(zip(years, clear_toa)):
	print month
	for lat, lat_fluxes in zip(latitudes, clear_sky_flux):
		for lon, lon_flux in zip(longitudes, lat_fluxes):
			for i, ll_lat in enumerate(lats):
				for j, ll_lon in enumerate(lons):
					if lat >= float(ll_lat) and lat <= (float(ll_lat) + 15):
						if lon >= float(ll_lon) and lon <= (float(ll_lon) + 15):
							years_dict[month][ll_lat + '_' + ll_lon].append(lon_flux)
	
averaged_years = copy.deepcopy(years_dict)			
for month_key, month_dict in years_dict.iteritems():
	for lat_lon_key, lat_lon_flux in month_dict.iteritems():
		if len(lat_lon_flux) > 0:
			averaged_years[month_key][lat_lon_key] = np.mean(lat_lon_flux)
		else:
			averaged_years[month_key][lat_lon_key] = 0.0
for month_key, month_dict in averaged_years.iteritems():
	print 'writing', month_key
	sd = SD(DUMP_DIR + month_key + '.hdf', SDC.WRITE | SDC.CREATE)
	for lat_lon, flux in clear_sky_dicts.iteritems():
		sds = sd.create(lat_lon, SDC.FLOAT32, (1))
		sds.setfillvalue(0)
		dim0 = sds.dim(0)
		dim0.setname('Flux')
		sds[:] = flux
		sds.endaccess()
	sd.end()
	
'''

ceres_dir = '/home/adouglas2/ceres_files/'



	month = test_file.replace(ceres_dir,'').split('.')[0]
	month = calendar.month_name[int(month)]
	print month
	fluxes = Dataset(test_file, mode = 'r')

	latitudes = fluxes['latitude'][:]
	longitudes = fluxes['longitude'][:]
	clear_sky_fluxes = fluxes['rsutcs'][:]
	for i, longitude in enumerate(longitudes):
		longitudes[i] = longitude - 180.

	for lat, lat_fluxes in zip(latitudes, clear_sky_fluxes):
		for lon, flux in zip(longitudes, lat_fluxes):
			for i, ll_lat in enumerate(lats):
				for j, ll_lon in enumerate(lons):
					if lat >= float(ll_lat) and lat <= (float(ll_lat) + 15):
						if lon >= float(ll_lon) and lon <= (float(ll_lon) + 15):
							clear_sky_dicts[ll_lat + '_' + ll_lon].append(flux)
	for key, item in clear_sky_dicts.iteritems():
		clear_sky_dicts[key] = np.mean(item)
		
	sd = SD(DUMP_DIR + month + '.hdf', SDC.WRITE | SDC.CREATE)
	for lat_lon, flux in clear_sky_dicts.iteritems():
		sds = sd.create(lat_lon, SDC.FLOAT32, (1))
		sds.setfillvalue(0)
		dim0 = sds.dim(0)
		dim0.setname('Flux')
		sds[:] = flux
		sds.endaccess()
	sd.end()


count_good = 0
count_bad = 0

DUMP_DIR = '/home/adouglas2/Summer/aerosol/CCMM/'

lats = [ -60.0000  ,   -45.0000  ,   -30.0000   ,  -15.0000    ,  0.00000  ,    15.0000,
      30.0000    ,  45.0000]
      
lons = [  -180.000   ,  -165.000   ,  -150.000    , -135.000   ,  -120.000   ,  -105.000,
     -90.0000  ,   -75.0000   ,  -60.0000   ,  -45.0000 ,    -30.0000 ,    -15.0000,
      0.00000    ,  15.0000     , 30.0000 ,     45.0000   ,  60.0000 ,     75.0000,
      90.0000  ,    105.000  ,    120.000 ,     135.000  ,    150.000   ,   165.000]
      

clear_sky_dicts = defaultdict(dict)


for lat in lats:
	for lon in lons:
		clear_sky_dicts['{}_{}'.format(lat, lon)] = []
year_clr = defaultdict(dict)

mean_ceres_clear_sky_filename = 'nasa_ceres_rsutcs_200301_201012_Annual.nc'
clear = Dataset(mean_ceres_clear_sky_filename, mode = 'r')
lats_clear = clear['latitude'][:]
lons_clear = clear['longitude'][:]
clear_fluxes = clear['rsutcs'][:]
print lats_clear
for lat, lat_flux in zip(lats_clear, clear_fluxes):
	for lon, clr_flux in zip(lons_clear, lat_flux):
		for ll_lat in lats:
			for ll_lon in lons:
				if lat > ll_lat and lat <= (ll_lat + 15) and lon > ll_lon and lon <= (ll_lon + 15):
					
					
sys.exit()
'''
'''
for i in range(12):
	year_clr[str(i)] = copy.deepcopy(clear_sky_dicts)
for year in [2006, 2007, 2008, 2009, 2010]:

	CERES_DIR = '/bohr/data4/CCCM_new/'

	for filename in glob.glob(CERES_DIR + '*.' + str(year) + '*.hdf'):
	
		print filename
		hdf_object = SD(filename, SDC.READ)
	
		longitudes = hdf_object.select('Longitude of CERES FOV at surface')[:]
		latitudes = hdf_object.select('Colatitude of CERES FOV at surface')[:]
		cloud_class = list(hdf_object.select('Cloud area untuned')[:])
		

		SW_TOA_up = hdf_object.select('CERES SW TOA flux - upwards')[:]
		SW_TOA_down = hdf_object.select('CERES SW TOA flux - downwards')[:]
		month = int(filename.split('.')[-2][4:6])
		for lat, lon, cld, up, down in zip(latitudes, longitudes, cloud_class, SW_TOA_up, SW_TOA_down):
			for ll_lat in lats:
				for ll_lon in lons:
					if lat >= ll_lat and lat <= (ll_lat + 15) and lon >= ll_lon and lon <= (ll_lon + 15) and cld < 15:
						if abs(up) < 1400 and abs(down) < 1400:
							count_good +=1
							year_clr[str(month-1)]['{}_{}'.format(ll_lat, ll_lon)].append(abs(down) - abs(up))
						else:
							
							count_bad +=1
						
		
'''
'''
print '###############################'
for month, month_dict in year_clr.iteritems():
	print month
	for lat_lon, fluxes in month_dict.iteritems():
		year_clr[month][lat_lon] = np.mean(fluxes)
		
print count_good, count_bad
for month, month_dict in year_clr.iteritems():
	sd = SD(DUMP_DIR + month + '.hdf', SDC.WRITE | SDC.CREATE)
	for lat_lon, flux in month_dict.iteritems():
		sds = sd.create(lat_lon, SDC.FLOAT32, (1))
		sds.setfillvalue(0)
		dim0 = sds.dim(0)
		dim0.setname('Flux')
		sds[:] = flux
		sds.endaccess()
	sd.end()
		
	'''
					

